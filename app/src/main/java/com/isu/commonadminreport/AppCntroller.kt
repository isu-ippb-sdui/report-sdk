package com.isu.commonadminreport

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class AppCntroller:Application()