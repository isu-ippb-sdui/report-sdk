package com.isu.commonadminreport

import android.app.ProgressDialog
import android.content.ContentValues.TAG
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import ca.mimic.oauth2library.OAuth2Client
import ca.mimic.oauth2library.OAuthResponse
import com.commonadmin.report.ReportSDKActivity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.json.JSONObject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private lateinit var login: Button
    private lateinit var dialog: ProgressDialog

    @OptIn(DelicateCoroutinesApi::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        login = findViewById(R.id.btnLogin)
        dialog = ProgressDialog(this)


        login.setOnClickListener {
            dialog.setMessage("Please wait..!!!")
            dialog.setCancelable(false)
            dialog.show()
            GlobalScope.launch(Dispatchers.IO) {
                oAuthApiCall()
            }


        }
    }

    private fun oAuthApiCall() {
//        val userName = "priyanka2"
//        val password = "Test@125"

        val userName = "Ankeeta01"
        val password = "Test@125"
//  val userName = "technewadmin"
//        val password = "TesTspa@190"

//        val userName = "jashjen"
//        val password = "Test@125"


//        val userName = "Androidtest01"
//        val password = "Test@809"


        try {
            val client = OAuth2Client.Builder(
                userName,
                password,
                "common-admins-oauth2-client",
                "common-admins-oauth-password",
                "https://oauth2-auth-server-common-admin-prod.iserveu.tech/oauth/token"
            ).grantType("password").build()

            val response: OAuthResponse? = client.requestAccessToken()
            if (response != null) {
                Log.d("LoginTag", "loginUsingOAuth: response is not null")
                dialog.dismiss()
                if (response.isSuccessful) {
                    Log.d("LoginTag", "loginUsingOAuth: response is successful")

                    val token = response.accessToken
                    val refreshToken = response.refreshToken
                    val expiresIn = response.expiresIn
                    val body = response.body
                    val jsonBody: JSONObject?
                    jsonBody = JSONObject(body)
                    val bankCode = jsonBody.getString("bankCode")
                    val jti = jsonBody.getString("jti")
                    val adminName = jsonBody.getString("adminName")

                    val bundle = Bundle()
                    bundle.putString("ActivityName", "AadhaarPayReport")
                    bundle.putString("ADMINNAME", adminName)
                    bundle.putString("TOKEN", token)
                    bundle.putString("USERNAME", userName)
                    bundle.putString("bankCode", "common")
                    val intent = Intent(this@MainActivity, ReportSDKActivity::class.java)
                    intent.putExtra("sendReportData", bundle)
                    startActivity(intent)
                    finish()
                }
            }
        } catch (e: Exception) {
            dialog.dismiss()
            e.printStackTrace()
            Log.e(TAG, "oAuthApiCallException: " + e.localizedMessage)
            // Toast.makeText(this,"Exception"+e.localizedMessage,Toast.LENGTH_SHORT).show()
        }
    }

}

