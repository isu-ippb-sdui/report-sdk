package com.commonadmin.report.domain.reposiroty

import com.commonadmin.report.data.remote.models.Wallet2ReqModel
import com.commonadmin.report.data.remote.models.Wallet2RespModel
import com.commonadmin.report.utils.ApiResult
import kotlinx.coroutines.flow.Flow

interface Wallet2ReportRepo {
    suspend fun wallet2ReportApiCall(url:String,wallet2ReqModel: Wallet2ReqModel,token:String) : Flow<ApiResult<Wallet2RespModel>>

}