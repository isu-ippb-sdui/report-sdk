package com.commonadmin.report.domain.reposiroty

import com.commonadmin.report.data.remote.models.LivLongReportReqModel
import com.commonadmin.report.data.remote.models.LivLongRespModel
import com.commonadmin.report.utils.ApiResult
import kotlinx.coroutines.flow.Flow

interface LivLongReportRepo {
    suspend fun livLongReportApiCall(url:String, livLongReportReqModel: LivLongReportReqModel, token:String) : Flow<ApiResult<LivLongRespModel>>

}