package com.commonadmin.report.domain.usercases



import com.commonadmin.report.data.remote.models.RechargeReqModel
import com.commonadmin.report.data.remote.models.RechargeResponseModel
import com.commonadmin.report.domain.reposiroty.RechargeReportRepo
import com.commonadmin.report.utils.ApiResult
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class RechargeReportUseCase @Inject constructor(private val rechargeReportRepo: RechargeReportRepo) {
    suspend operator fun invoke(url:String,rechargeReqModel: RechargeReqModel,token:String): Flow<ApiResult<RechargeResponseModel>> {
       return rechargeReportRepo.rechargeReportApiCall(url,rechargeReqModel,token)
    }
}