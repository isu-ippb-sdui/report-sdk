package com.commonadmin.report.domain.reposiroty

import com.commonadmin.report.data.remote.models.WalletTopUpRequestReqModel
import com.commonadmin.report.data.remote.models.WalletTopUpRequestRespModel
import com.commonadmin.report.utils.ApiResult
import kotlinx.coroutines.flow.Flow

interface WalletTopUpRequestReportRepo {
    suspend fun walletTopUpRequestApiCall(url:String,walletTopUpRequestReqModel: WalletTopUpRequestReqModel) : Flow<ApiResult<WalletTopUpRequestRespModel>>

}