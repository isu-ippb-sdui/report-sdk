package com.commonadmin.report.domain.usercases




import com.commonadmin.report.data.remote.models.DmtGatewayReqModels
import com.commonadmin.report.data.remote.models.DmtGatewayRespModel
import com.commonadmin.report.domain.reposiroty.DmtGatewayReportRepo
import com.commonadmin.report.utils.ApiResult
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class DmtGateWayReportUseCase @Inject constructor(private val dmtGatewayReportRepo: DmtGatewayReportRepo) {
    suspend operator fun invoke(url:String,dmtGatewayReqModels: DmtGatewayReqModels,token:String): Flow<ApiResult<DmtGatewayRespModel>> {
       return dmtGatewayReportRepo.dmtGateWayReportApiCall(url,dmtGatewayReqModels,token)
    }
}