package com.commonadmin.report.domain.usercases




import com.commonadmin.report.data.remote.models.LivLongReportReqModel
import com.commonadmin.report.data.remote.models.LivLongRespModel
import com.commonadmin.report.domain.reposiroty.LivLongReportRepo
import com.commonadmin.report.utils.ApiResult
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class LivLongReportUseCase @Inject constructor(private val livLongReportRepo: LivLongReportRepo) {
    suspend operator fun invoke(url:String, livLongReportReqModel: LivLongReportReqModel, token:String): Flow<ApiResult<LivLongRespModel>> {
       return livLongReportRepo.livLongReportApiCall(url,livLongReportReqModel,token)
    }
}