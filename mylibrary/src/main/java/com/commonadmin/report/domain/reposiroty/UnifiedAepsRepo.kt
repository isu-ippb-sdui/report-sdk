package com.commonadmin.report.domain.reposiroty

import com.commonadmin.report.data.remote.models.UnifiedAepsReqModel
import com.commonadmin.report.data.remote.models.UnifiedAepsRespModel
import com.commonadmin.report.utils.ApiResult
import kotlinx.coroutines.flow.Flow

interface UnifiedAepsRepo {
    suspend fun reportApiCall(uri:String, unifiedAepsReqModel: UnifiedAepsReqModel, token:String) : Flow<ApiResult<UnifiedAepsRespModel>>

}