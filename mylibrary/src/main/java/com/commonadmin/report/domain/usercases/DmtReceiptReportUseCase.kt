package com.commonadmin.report.domain.usercases

import com.commonadmin.report.data.remote.models.DmtReceiptReportReqModel
import com.commonadmin.report.data.remote.models.DmtReceiptReportRespModel
import com.commonadmin.report.domain.reposiroty.DmtReceiptReportRepo
import com.commonadmin.report.utils.ApiResult
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class DmtReceiptReportUseCase @Inject constructor(private val dmtReceiptReportRepo: DmtReceiptReportRepo) {
    suspend operator fun invoke(url:String,dmtReceiptReportReqModel: DmtReceiptReportReqModel): Flow<ApiResult<DmtReceiptReportRespModel>> {
       return dmtReceiptReportRepo.dmtReceiptReportApiCall(url,dmtReceiptReportReqModel)
    }
}