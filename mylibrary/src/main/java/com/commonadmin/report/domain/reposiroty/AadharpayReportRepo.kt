package com.commonadmin.report.domain.reposiroty

import com.commonadmin.report.data.remote.models.AadharpayReqModel
import com.commonadmin.report.data.remote.models.AadharpayRespModel
import com.commonadmin.report.utils.ApiResult
import kotlinx.coroutines.flow.Flow

interface AadharpayReportRepo {
    suspend fun aadharPayReportApiCall(url:String, aadharpayReqModel: AadharpayReqModel, token:String) : Flow<ApiResult<AadharpayRespModel>>

}