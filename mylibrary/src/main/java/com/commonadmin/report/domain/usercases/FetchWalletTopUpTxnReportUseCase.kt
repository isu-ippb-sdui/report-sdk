package com.commonadmin.report.domain.usercases

import com.commonadmin.report.data.remote.models.FetchWalletTopUpTxnReqModel
import com.commonadmin.report.data.remote.models.FetchWalletTopUpTxnRespModel
import com.commonadmin.report.domain.reposiroty.FetchWalletTopUpTxnReportRepo
import com.commonadmin.report.utils.ApiResult
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class FetchWalletTopUpTxnReportUseCase @Inject constructor(private val fetchWalletTopUpTxnReportRepo: FetchWalletTopUpTxnReportRepo) {
    suspend operator fun invoke(url:String,fetchWalletTopUpTxnReqModel: FetchWalletTopUpTxnReqModel): Flow<ApiResult<FetchWalletTopUpTxnRespModel>> {
       return fetchWalletTopUpTxnReportRepo.fetchWalletTopUpTxnReportApiCall(url,fetchWalletTopUpTxnReqModel)
    }
}