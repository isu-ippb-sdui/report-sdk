package com.commonadmin.report.domain.reposiroty


import com.commonadmin.report.data.remote.models.DmtReceiptReportReqModel
import com.commonadmin.report.data.remote.models.DmtReceiptReportRespModel
import com.commonadmin.report.utils.ApiResult
import kotlinx.coroutines.flow.Flow

interface DmtReceiptReportRepo {
    suspend fun dmtReceiptReportApiCall(url:String,dmtReceiptReportReqModel: DmtReceiptReportReqModel) : Flow<ApiResult<DmtReceiptReportRespModel>>

}