package com.commonadmin.report.domain.reposiroty

import com.commonadmin.report.data.remote.models.Wallet1ReqModel
import com.commonadmin.report.data.remote.models.Wallet1RespModel
import com.commonadmin.report.utils.ApiResult
import kotlinx.coroutines.flow.Flow

interface Wallet1ReportRepo {
    suspend fun wallet1ReportApiCall(url:String,wallet1ReqModel: Wallet1ReqModel,token:String) : Flow<ApiResult<Wallet1RespModel>>

}