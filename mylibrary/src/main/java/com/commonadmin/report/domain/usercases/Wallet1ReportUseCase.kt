package com.commonadmin.report.domain.usercases


import com.commonadmin.report.data.remote.models.Wallet1ReqModel
import com.commonadmin.report.data.remote.models.Wallet1RespModel
import com.commonadmin.report.domain.reposiroty.Wallet1ReportRepo
import com.commonadmin.report.utils.ApiResult
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class Wallet1ReportUseCase @Inject constructor(private val wallet1ReportRepo: Wallet1ReportRepo) {
    suspend operator fun invoke(url:String,wallet1ReqModel: Wallet1ReqModel,token:String): Flow<ApiResult<Wallet1RespModel>> {
       return wallet1ReportRepo.wallet1ReportApiCall(url,wallet1ReqModel,token)
    }
}