package com.commonadmin.report.domain.usercases




import com.commonadmin.report.data.remote.models.UpiReqModel
import com.commonadmin.report.data.remote.models.UpiRespModel
import com.commonadmin.report.domain.reposiroty.UpiReportRepo
import com.commonadmin.report.utils.ApiResult
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class UpiReportUseCase @Inject constructor(private val upiReportRepo: UpiReportRepo) {
    suspend operator fun invoke(url:String, upiReqModel: UpiReqModel, token:String): Flow<ApiResult<UpiRespModel>> {
       return upiReportRepo.upiReportApiCall(url,upiReqModel,token)
    }
}