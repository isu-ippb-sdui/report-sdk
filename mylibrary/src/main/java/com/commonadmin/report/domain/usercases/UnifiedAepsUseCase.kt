package com.commonadmin.report.domain.usercases

import com.commonadmin.report.data.remote.models.UnifiedAepsReqModel
import com.commonadmin.report.data.remote.models.UnifiedAepsRespModel
import com.commonadmin.report.domain.reposiroty.UnifiedAepsRepo
import com.commonadmin.report.utils.ApiResult
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class UnifiedAepsUseCase @Inject constructor(private val unifiedAepsRepo: UnifiedAepsRepo) {
    suspend operator fun invoke(url:String, unifiedAepsReqModel: UnifiedAepsReqModel, token:String): Flow<ApiResult<UnifiedAepsRespModel>> {
       return unifiedAepsRepo.reportApiCall(url,unifiedAepsReqModel,token)
    }
}