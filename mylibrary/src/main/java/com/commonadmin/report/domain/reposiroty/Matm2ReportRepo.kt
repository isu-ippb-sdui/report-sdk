package com.commonadmin.report.domain.reposiroty

import com.commonadmin.report.data.remote.models.Matm2ReqModel
import com.commonadmin.report.data.remote.models.Matm2RespModel
import com.commonadmin.report.utils.ApiResult
import kotlinx.coroutines.flow.Flow

interface Matm2ReportRepo {
    suspend fun mAtm2ReportApiCall(url:String,matm2ReqModel: Matm2ReqModel,token:String) : Flow<ApiResult<Matm2RespModel>>

}