package com.commonadmin.report.domain.usercases



import com.commonadmin.report.data.remote.models.BBPSReportReqModel
import com.commonadmin.report.data.remote.models.BBPSReportRespModel
import com.commonadmin.report.domain.reposiroty.BBPSReportRepo
import com.commonadmin.report.utils.ApiResult
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class BBPSReportUseCase @Inject constructor(private val bbpsReportRepo: BBPSReportRepo) {
    suspend operator fun invoke(url:String,bbpsReportReqModel: BBPSReportReqModel,token:String): Flow<ApiResult<BBPSReportRespModel>> {
       return bbpsReportRepo.bbpsReportApiCall(url,bbpsReportReqModel,token)
    }
}