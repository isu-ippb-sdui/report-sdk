package com.commonadmin.report.domain.reposiroty

import com.commonadmin.report.data.remote.models.WalletTopUpFetchUserReqModel
import com.commonadmin.report.data.remote.models.WalletTopUpFetchUserRespModel
import com.commonadmin.report.utils.ApiResult
import kotlinx.coroutines.flow.Flow

interface WalletTopUpFetchReportRepo {
    suspend fun walletTopUpFetchApiCall(url:String,walletTopUpFetchUserReqModel: WalletTopUpFetchUserReqModel) : Flow<ApiResult<WalletTopUpFetchUserRespModel>>

}