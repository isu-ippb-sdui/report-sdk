package com.commonadmin.report.domain.reposiroty

import com.commonadmin.report.data.remote.models.BBPSReportReqModel
import com.commonadmin.report.data.remote.models.BBPSReportRespModel
import com.commonadmin.report.utils.ApiResult
import kotlinx.coroutines.flow.Flow

interface BBPSReportRepo {
    suspend fun bbpsReportApiCall(url:String,bbpsReportReqModel: BBPSReportReqModel,token:String) : Flow<ApiResult<BBPSReportRespModel>>

}