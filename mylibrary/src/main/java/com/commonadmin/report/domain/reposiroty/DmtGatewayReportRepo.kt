package com.commonadmin.report.domain.reposiroty

import com.commonadmin.report.data.remote.models.DmtGatewayReqModels
import com.commonadmin.report.data.remote.models.DmtGatewayRespModel
import com.commonadmin.report.utils.ApiResult
import kotlinx.coroutines.flow.Flow

interface DmtGatewayReportRepo {
    suspend fun dmtGateWayReportApiCall(url:String,dmtGatewayReqModels: DmtGatewayReqModels,token:String) : Flow<ApiResult<DmtGatewayRespModel>>

}