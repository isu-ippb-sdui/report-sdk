package com.commonadmin.report.domain.usercases




import com.commonadmin.report.data.remote.models.WalletTopUpFetchUserReqModel
import com.commonadmin.report.data.remote.models.WalletTopUpFetchUserRespModel
import com.commonadmin.report.domain.reposiroty.WalletTopUpFetchReportRepo
import com.commonadmin.report.utils.ApiResult
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class WalletTopUpFetchUserUseCase @Inject constructor(private val walletTopUpFetchReportRepo: WalletTopUpFetchReportRepo) {
    suspend operator fun invoke(url:String,walletTopUpFetchUserReqModel: WalletTopUpFetchUserReqModel): Flow<ApiResult<WalletTopUpFetchUserRespModel>> {
       return walletTopUpFetchReportRepo.walletTopUpFetchApiCall(url,walletTopUpFetchUserReqModel)
    }
}