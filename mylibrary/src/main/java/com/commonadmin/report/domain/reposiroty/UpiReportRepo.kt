package com.commonadmin.report.domain.reposiroty


import com.commonadmin.report.data.remote.models.UpiReqModel
import com.commonadmin.report.data.remote.models.UpiRespModel
import com.commonadmin.report.utils.ApiResult
import kotlinx.coroutines.flow.Flow

interface UpiReportRepo {
    suspend fun upiReportApiCall(url:String, upiReqModel: UpiReqModel, token:String) : Flow<ApiResult<UpiRespModel>>

}