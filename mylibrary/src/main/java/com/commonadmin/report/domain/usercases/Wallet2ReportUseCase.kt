package com.commonadmin.report.domain.usercases
import com.commonadmin.report.data.remote.models.Wallet2ReqModel
import com.commonadmin.report.data.remote.models.Wallet2RespModel
import com.commonadmin.report.domain.reposiroty.Wallet2ReportRepo
import com.commonadmin.report.utils.ApiResult
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class Wallet2ReportUseCase @Inject constructor(private val wallet2ReportRepo: Wallet2ReportRepo) {
    suspend operator fun invoke(url:String,wallet2ReqModel: Wallet2ReqModel,token:String): Flow<ApiResult<Wallet2RespModel>> {
       return wallet2ReportRepo.wallet2ReportApiCall(url,wallet2ReqModel,token)
    }
}