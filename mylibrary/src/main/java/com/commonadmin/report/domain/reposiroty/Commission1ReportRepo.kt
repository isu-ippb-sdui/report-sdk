package com.commonadmin.report.domain.reposiroty

import com.commonadmin.report.data.remote.models.Commission1ReportReqModel
import com.commonadmin.report.data.remote.models.Commission1ReportRespModel
import com.commonadmin.report.utils.ApiResult
import kotlinx.coroutines.flow.Flow

interface Commission1ReportRepo {
    suspend fun commission1ReportApiCall(url:String, commission1ReportReqModel: Commission1ReportReqModel, token:String) : Flow<ApiResult<Commission1ReportRespModel>>

}