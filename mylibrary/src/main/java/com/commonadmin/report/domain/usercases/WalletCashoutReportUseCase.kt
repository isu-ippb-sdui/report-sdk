package com.commonadmin.report.domain.usercases

import com.commonadmin.report.data.remote.models.WalletCashoutReqModel
import com.commonadmin.report.data.remote.models.WalletCashoutRespModel
import com.commonadmin.report.domain.reposiroty.WalletCashoutReportRepo
import com.commonadmin.report.utils.ApiResult
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class WalletCashoutReportUseCase @Inject constructor(private val walletCashoutReportRepo: WalletCashoutReportRepo) {
    suspend operator fun invoke(url:String,walletCashoutReqModel: WalletCashoutReqModel,token:String): Flow<ApiResult<WalletCashoutRespModel>> {
       return walletCashoutReportRepo.walletCashoutReportApiCall(url,walletCashoutReqModel,token)
    }
}