package com.commonadmin.report.domain.reposiroty

import com.commonadmin.report.data.remote.models.WalletTopUpReqModel
import com.commonadmin.report.data.remote.models.WalletTopUpRespModel
import com.commonadmin.report.utils.ApiResult
import kotlinx.coroutines.flow.Flow

interface WalletTopUpReportRepo {
    suspend fun walletTopUpReportApiCall(url:String,walletTopUpReqModel: WalletTopUpReqModel,token:String) : Flow<ApiResult<WalletTopUpRespModel>>

}