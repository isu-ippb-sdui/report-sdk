package com.commonadmin.report.domain.reposiroty


import com.commonadmin.report.data.remote.models.WalletInterchangeReqModel
import com.commonadmin.report.data.remote.models.WalletInterchangeRespModel
import com.commonadmin.report.utils.ApiResult
import kotlinx.coroutines.flow.Flow

interface WalletInterChangeReportRepo {
    suspend fun walletInterChangeReportApiCall(url:String,walletInterchangeReqModel: WalletInterchangeReqModel,token:String) : Flow<ApiResult<WalletInterchangeRespModel>>

}