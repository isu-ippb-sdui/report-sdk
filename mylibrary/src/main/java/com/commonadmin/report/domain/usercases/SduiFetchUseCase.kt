package com.commonadmin.report.domain.usercases

import com.commonadmin.report.data.remote.models.SduiFetchReqModels
import com.commonadmin.report.data.remote.models.SduiFetchRespModels
import com.commonadmin.report.domain.reposiroty.SduiFetchRepo
import com.commonadmin.report.utils.ApiResult
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class SduiFetchUseCase @Inject constructor(private val sduiFetchRepo: SduiFetchRepo) {
    suspend operator fun invoke(sduiFetchReqModels: SduiFetchReqModels): Flow<ApiResult<SduiFetchRespModels>> {
       return sduiFetchRepo.sduiFetchApi(sduiFetchReqModels)
    }
}