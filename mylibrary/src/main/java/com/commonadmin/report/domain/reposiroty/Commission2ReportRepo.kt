package com.commonadmin.report.domain.reposiroty

import com.commonadmin.report.data.remote.models.Commission2ReportReqModel
import com.commonadmin.report.data.remote.models.Commission2ReportRespModel
import com.commonadmin.report.utils.ApiResult
import kotlinx.coroutines.flow.Flow

interface Commission2ReportRepo {
    suspend fun commission2ReportApiCall(url:String, commission2ReportReqModel: Commission2ReportReqModel, token:String) : Flow<ApiResult<Commission2ReportRespModel>>

}