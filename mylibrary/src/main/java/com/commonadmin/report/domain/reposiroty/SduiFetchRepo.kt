package com.commonadmin.report.domain.reposiroty

import com.commonadmin.report.data.remote.models.SduiFetchReqModels
import com.commonadmin.report.data.remote.models.SduiFetchRespModels
import com.commonadmin.report.utils.ApiResult
import kotlinx.coroutines.flow.Flow

interface SduiFetchRepo {
    suspend fun sduiFetchApi(sduiFetchReqModels: SduiFetchReqModels) : Flow<ApiResult<SduiFetchRespModels>>

}