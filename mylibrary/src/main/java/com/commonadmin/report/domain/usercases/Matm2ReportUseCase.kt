package com.commonadmin.report.domain.usercases
import com.commonadmin.report.data.remote.models.Matm2ReqModel
import com.commonadmin.report.data.remote.models.Matm2RespModel
import com.commonadmin.report.domain.reposiroty.Matm2ReportRepo
import com.commonadmin.report.utils.ApiResult
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class Matm2ReportUseCase @Inject constructor(private val matm2ReportRepo: Matm2ReportRepo) {
    suspend operator fun invoke(url:String,matm2ReqModel: Matm2ReqModel,token:String): Flow<ApiResult<Matm2RespModel>> {
       return matm2ReportRepo.mAtm2ReportApiCall(url,matm2ReqModel,token)
    }
}