package com.commonadmin.report.domain.usercases



import com.commonadmin.report.data.remote.models.Commission1ReportReqModel
import com.commonadmin.report.data.remote.models.Commission1ReportRespModel
import com.commonadmin.report.domain.reposiroty.Commission1ReportRepo
import com.commonadmin.report.utils.ApiResult
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class Commission1ReportUseCase @Inject constructor(private val commission1ReportRepo: Commission1ReportRepo) {
    suspend operator fun invoke(url:String, commission1ReportReqModel: Commission1ReportReqModel, token:String): Flow<ApiResult<Commission1ReportRespModel>> {
       return commission1ReportRepo.commission1ReportApiCall(url,commission1ReportReqModel,token)
    }
}