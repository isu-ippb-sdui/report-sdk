package com.commonadmin.report.domain.usercases
import com.commonadmin.report.data.remote.models.WalletInterchangeReqModel
import com.commonadmin.report.data.remote.models.WalletInterchangeRespModel
import com.commonadmin.report.domain.reposiroty.WalletInterChangeReportRepo
import com.commonadmin.report.utils.ApiResult
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class WalletInterchangeReportUseCase @Inject constructor(private val walletInterChangeReportRepo: WalletInterChangeReportRepo) {
    suspend operator fun invoke(url:String,walletInterchangeReqModel: WalletInterchangeReqModel,token:String): Flow<ApiResult<WalletInterchangeRespModel>> {
       return walletInterChangeReportRepo.walletInterChangeReportApiCall(url,walletInterchangeReqModel,token)
    }
}