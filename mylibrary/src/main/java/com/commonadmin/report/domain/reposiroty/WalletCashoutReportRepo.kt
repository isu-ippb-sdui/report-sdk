package com.commonadmin.report.domain.reposiroty

import com.commonadmin.report.data.remote.models.WalletCashoutReqModel
import com.commonadmin.report.data.remote.models.WalletCashoutRespModel
import com.commonadmin.report.utils.ApiResult
import kotlinx.coroutines.flow.Flow

interface WalletCashoutReportRepo {
    suspend fun walletCashoutReportApiCall(url:String,walletCashoutReqModel: WalletCashoutReqModel,token:String) : Flow<ApiResult<WalletCashoutRespModel>>

}