package com.commonadmin.report.domain.usercases




import com.commonadmin.report.data.remote.models.AadharpayReqModel
import com.commonadmin.report.data.remote.models.AadharpayRespModel
import com.commonadmin.report.domain.reposiroty.AadharpayReportRepo
import com.commonadmin.report.utils.ApiResult
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class AadharPayReportUseCase @Inject constructor(private val aadharpayReportReportRepo: AadharpayReportRepo) {
    suspend operator fun invoke(url:String, aadharpayReqModel: AadharpayReqModel, token:String): Flow<ApiResult<AadharpayRespModel>> {
       return aadharpayReportReportRepo.aadharPayReportApiCall(url,aadharpayReqModel,token)
    }
}