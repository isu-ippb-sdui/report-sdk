package com.commonadmin.report.domain.usercases




import com.commonadmin.report.data.remote.models.WalletTopUpRequestReqModel
import com.commonadmin.report.data.remote.models.WalletTopUpRequestRespModel
import com.commonadmin.report.domain.reposiroty.WalletTopUpRequestReportRepo
import com.commonadmin.report.utils.ApiResult
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class WalletTopUpReqestReportUseCase @Inject constructor(private val walletTopUpRequestReportRepo: WalletTopUpRequestReportRepo) {
    suspend operator fun invoke(url:String,walletTopUpRequestReqModel: WalletTopUpRequestReqModel): Flow<ApiResult<WalletTopUpRequestRespModel>> {
       return walletTopUpRequestReportRepo.walletTopUpRequestApiCall(url,walletTopUpRequestReqModel)
    }
}