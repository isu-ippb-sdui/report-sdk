package com.commonadmin.report.domain.usercases



import com.commonadmin.report.data.remote.models.Commission2ReportReqModel
import com.commonadmin.report.data.remote.models.Commission2ReportRespModel
import com.commonadmin.report.domain.reposiroty.Commission2ReportRepo
import com.commonadmin.report.utils.ApiResult
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class Commission2ReportUseCase @Inject constructor(private val commission2ReportRepo: Commission2ReportRepo) {
    suspend operator fun invoke(url:String, commission2ReportReqModel: Commission2ReportReqModel, token:String): Flow<ApiResult<Commission2ReportRespModel>> {
       return commission2ReportRepo.commission2ReportApiCall(url,commission2ReportReqModel,token)
    }
}