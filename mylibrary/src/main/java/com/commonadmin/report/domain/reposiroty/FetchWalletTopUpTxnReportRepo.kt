package com.commonadmin.report.domain.reposiroty


import com.commonadmin.report.data.remote.models.FetchWalletTopUpTxnReqModel
import com.commonadmin.report.data.remote.models.FetchWalletTopUpTxnRespModel
import com.commonadmin.report.utils.ApiResult
import kotlinx.coroutines.flow.Flow

interface FetchWalletTopUpTxnReportRepo {
    suspend fun fetchWalletTopUpTxnReportApiCall(url:String,fetchWalletTopUpTxnReqModel: FetchWalletTopUpTxnReqModel) : Flow<ApiResult<FetchWalletTopUpTxnRespModel>>

}