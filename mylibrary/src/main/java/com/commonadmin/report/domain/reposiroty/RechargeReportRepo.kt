package com.commonadmin.report.domain.reposiroty


import com.commonadmin.report.data.remote.models.RechargeReqModel
import com.commonadmin.report.data.remote.models.RechargeResponseModel
import com.commonadmin.report.utils.ApiResult
import kotlinx.coroutines.flow.Flow

interface RechargeReportRepo {
    suspend fun rechargeReportApiCall(url:String,rechargeReqModel: RechargeReqModel,token:String) : Flow<ApiResult<RechargeResponseModel>>

}