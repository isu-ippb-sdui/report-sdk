package com.commonadmin.report.domain.usercases
import com.commonadmin.report.data.remote.models.WalletTopUpReqModel
import com.commonadmin.report.data.remote.models.WalletTopUpRespModel
import com.commonadmin.report.domain.reposiroty.WalletTopUpReportRepo
import com.commonadmin.report.utils.ApiResult
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class WalletTopUpReportUseCase @Inject constructor(private val walletTopUpReportRepo: WalletTopUpReportRepo) {
    suspend operator fun invoke(url:String,walletTopUpReqModel: WalletTopUpReqModel,token:String): Flow<ApiResult<WalletTopUpRespModel>> {
       return walletTopUpReportRepo.walletTopUpReportApiCall(url,walletTopUpReqModel,token)
    }
}