package com.commonadmin.report.ui.theme

import androidx.compose.ui.graphics.Color

val Purple80 = Color(0xFFD0BCFF)
val PurpleGrey80 = Color(0xFFCCC2DC)
val Pink80 = Color(0xFFEFB8C8)

val Purple40 = Color(0xFF6650a4)
val PurpleGrey40 = Color(0xFF625b71)
val Pink40 = Color(0xFF7D5260)


val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)
var primaryColor = Color(0xFF00c3d7)
var Red = Color(0xFFff000d)
var Grey=Color(0xFFE6DFDB)
var GreyPrimary=Color(0xFFE9E4E7)
var Yellow=Color(0xFFC0AD06)
var txnBkgtextColor = Color(0xFF373636)
var txnBkglightblue = Color(0xFF00c3d7)
var txnBkgbkgblue = Color(0xFF84E5EF)
var msTextColor = Color(0xFF454545)
var txnTextColor = Color(0xFF373636)
var fingerGrey = Color(0xFFEDF0F1)
val very_light_grey = Color(0xFFC7C6C6)
val ProgressBarGreen = Color(0xFF07b553)
val redColor = Color(0xFFed1b24)
val greenColor = Color(0xFF2bc48a)
val lightGrey = Color(0xFFDEE0E2)
val whiteColor = Color(0xFFFFFFFF)
val orange = Color(0xFFFFA500)
val Green = Color(0xFF0CAF13)