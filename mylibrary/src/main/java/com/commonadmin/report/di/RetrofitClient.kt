package com.commonadmin.report.di

import com.commonadmin.report.data.remote.BaseApis.Companion.AadharpayReport
import com.commonadmin.report.data.remote.BaseApis.Companion.AepsReportApi
import com.commonadmin.report.data.remote.BaseApis.Companion.BBPSReportApi
import com.commonadmin.report.data.remote.BaseApis.Companion.Commission1ReportApi
import com.commonadmin.report.data.remote.BaseApis.Companion.DmtGateWayApi
import com.commonadmin.report.data.remote.BaseApis.Companion.DmtReceiptApi
import com.commonadmin.report.data.remote.BaseApis.Companion.FetchWalletTopUpTxn
import com.commonadmin.report.data.remote.BaseApis.Companion.LivLongReportApi
import com.commonadmin.report.data.remote.BaseApis.Companion.RechargeReportApi
import com.commonadmin.report.data.remote.BaseApis.Companion.ReportApi
import com.commonadmin.report.data.remote.BaseApis.Companion.SDUIfetchApi
import com.commonadmin.report.data.remote.BaseApis.Companion.UpiReportApi
import com.commonadmin.report.data.remote.BaseApis.Companion.Wallet1ReportApi
import com.commonadmin.report.data.remote.BaseApis.Companion.WalletInterchangeApi
import com.commonadmin.report.data.remote.BaseApis.Companion.WalletTopUpFetchUser
import com.commonadmin.report.data.remote.BaseApis.Companion.WalletTopupApi
import com.commonadmin.report.data.remote.apiservice.AadharPayApiService
import com.commonadmin.report.data.remote.apiservice.BBPSApiService
import com.commonadmin.report.data.remote.apiservice.Commission1ApiService
import com.commonadmin.report.data.remote.apiservice.Commission2ApiService
import com.commonadmin.report.data.remote.apiservice.DmtGateWayApiService
import com.commonadmin.report.data.remote.apiservice.DmtReceiptReportApiService
import com.commonadmin.report.data.remote.apiservice.FetchSduiApiService
import com.commonadmin.report.data.remote.apiservice.FetchWalletTopupTxnApiService
import com.commonadmin.report.data.remote.apiservice.LivLongApiService
import com.commonadmin.report.data.remote.apiservice.Matm2ApiService
import com.commonadmin.report.data.remote.apiservice.RechargeApiService
import com.commonadmin.report.data.remote.apiservice.UnifiedApiService
import com.commonadmin.report.data.remote.apiservice.UpiApiService
import com.commonadmin.report.data.remote.apiservice.Wallet1ApiService
import com.commonadmin.report.data.remote.apiservice.Wallet2ApiService
import com.commonadmin.report.data.remote.apiservice.WalletCashOutApiService
import com.commonadmin.report.data.remote.apiservice.WalletInterchangeApiService
import com.commonadmin.report.data.remote.apiservice.WalletTopUpApiService
import com.commonadmin.report.data.remote.apiservice.WalletTopUpFetchUserApiService
import com.commonadmin.report.data.remote.apiservice.WalletTopUpRequestApiService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class RetrofitClient {
    @Singleton
    @Provides
    fun provideApiService(): UnifiedApiService {
        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(Interceptor { chain ->
            val original = chain.request()
            val request = original.newBuilder()
                .method(original.method, original.body)
                .build()
            chain.proceed(request)
        })
        val logging = HttpLoggingInterceptor()
        // set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY)
        //  logging.setLevel(HttpLoggingInterceptor.Level.HEADERS);
        // add logging as last interceptor
        httpClient.addInterceptor(logging) // <-- this is the important line!
        httpClient.readTimeout(60, TimeUnit.SECONDS)
        httpClient.writeTimeout(60, TimeUnit.SECONDS)
        httpClient.connectTimeout(60, TimeUnit.SECONDS)
        httpClient.callTimeout(60, TimeUnit.SECONDS)
        httpClient.retryOnConnectionFailure(true)

        return Retrofit.Builder()
            .baseUrl(AepsReportApi)
            .addConverterFactory(GsonConverterFactory.create())
            .client(httpClient.build())
            .build().create(UnifiedApiService::class.java)
    }

    @Singleton
    @Provides
    fun provideSduiFetchApiService(): FetchSduiApiService {

        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(Interceptor { chain ->
            val original = chain.request()
            val request = original.newBuilder()
                .method(original.method, original.body)
                .build()
            chain.proceed(request)
        })
        val logging = HttpLoggingInterceptor()
        logging.setLevel(HttpLoggingInterceptor.Level.BODY)
        httpClient.addInterceptor(logging) // <-- this is the important line!
        httpClient.readTimeout(60, TimeUnit.SECONDS)
        httpClient.writeTimeout(60, TimeUnit.SECONDS)
        httpClient.connectTimeout(60, TimeUnit.SECONDS)
        httpClient.callTimeout(60, TimeUnit.SECONDS)
        httpClient.retryOnConnectionFailure(true)

        return Retrofit.Builder()
            .baseUrl(SDUIfetchApi)
            .addConverterFactory(GsonConverterFactory.create())
            .client(httpClient.build())
            .build()
            .create(FetchSduiApiService::class.java)
    }

    @Singleton
    @Provides
    fun provideWallet1ApiService(): Wallet1ApiService {

        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(Interceptor { chain ->
            val original = chain.request()
            val request = original.newBuilder()
                .method(original.method, original.body)
                .build()
            chain.proceed(request)
        })
        val logging = HttpLoggingInterceptor()
        logging.setLevel(HttpLoggingInterceptor.Level.BODY)
        httpClient.addInterceptor(logging) // <-- this is the important line!
        httpClient.readTimeout(60, TimeUnit.SECONDS)
        httpClient.writeTimeout(60, TimeUnit.SECONDS)
        httpClient.connectTimeout(60, TimeUnit.SECONDS)
        httpClient.callTimeout(60, TimeUnit.SECONDS)
        httpClient.retryOnConnectionFailure(true)

        return Retrofit.Builder()
            .baseUrl(Wallet1ReportApi)
            .addConverterFactory(GsonConverterFactory.create())
            .client(httpClient.build())
            .build()
            .create(Wallet1ApiService::class.java)
    }

    @Singleton
    @Provides
    fun provideWallet2ApiService(): Wallet2ApiService {
        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(Interceptor { chain ->
            val original = chain.request()
            val request = original.newBuilder()
                .method(original.method, original.body)
                .build()
            chain.proceed(request)
        })
        val logging = HttpLoggingInterceptor()
        logging.setLevel(HttpLoggingInterceptor.Level.BODY)
        httpClient.addInterceptor(logging) // <-- this is the important line!
        httpClient.readTimeout(60, TimeUnit.SECONDS)
        httpClient.writeTimeout(60, TimeUnit.SECONDS)
        httpClient.connectTimeout(60, TimeUnit.SECONDS)
        httpClient.callTimeout(60, TimeUnit.SECONDS)
        httpClient.retryOnConnectionFailure(true)

        return Retrofit.Builder()
            .baseUrl(ReportApi)
            .addConverterFactory(GsonConverterFactory.create())
            .client(httpClient.build())
            .build()
            .create(Wallet2ApiService::class.java)
    }

    @Singleton
    @Provides
    fun provideMatm2ReportApiService(): Matm2ApiService {

        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(Interceptor { chain ->
            val original = chain.request()
            val request = original.newBuilder()
                .method(original.method, original.body)
                .build()
            chain.proceed(request)
        })
        val logging = HttpLoggingInterceptor()
        logging.setLevel(HttpLoggingInterceptor.Level.BODY)
        httpClient.addInterceptor(logging) // <-- this is the important line!
        httpClient.readTimeout(60, TimeUnit.SECONDS)
        httpClient.writeTimeout(60, TimeUnit.SECONDS)
        httpClient.connectTimeout(60, TimeUnit.SECONDS)
        httpClient.callTimeout(60, TimeUnit.SECONDS)
        httpClient.retryOnConnectionFailure(true)

        return Retrofit.Builder()
            .baseUrl(ReportApi)
            .addConverterFactory(GsonConverterFactory.create())
            .client(httpClient.build())
            .build()
            .create(Matm2ApiService::class.java)
    }

    @Singleton
    @Provides
    fun provideCommission1ReportApiService(): Commission1ApiService {
        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(Interceptor { chain ->
            val original = chain.request()
            val request = original.newBuilder()
                .method(original.method, original.body)
                .build()
            chain.proceed(request)
        })
        val logging = HttpLoggingInterceptor()
        logging.setLevel(HttpLoggingInterceptor.Level.BODY)
        httpClient.addInterceptor(logging) // <-- this is the important line!
        httpClient.readTimeout(60, TimeUnit.SECONDS)
        httpClient.writeTimeout(60, TimeUnit.SECONDS)
        httpClient.connectTimeout(60, TimeUnit.SECONDS)
        httpClient.callTimeout(60, TimeUnit.SECONDS)
        httpClient.retryOnConnectionFailure(true)

        return Retrofit.Builder()
            .baseUrl(Commission1ReportApi)
            .addConverterFactory(GsonConverterFactory.create())
            .client(httpClient.build())
            .build()
            .create(Commission1ApiService::class.java)
    }


    @Singleton
    @Provides
    fun provideCommission2ReportApiService(): Commission2ApiService {
        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(Interceptor { chain ->
            val original = chain.request()
            val request = original.newBuilder()
                .method(original.method, original.body)
                .build()
            chain.proceed(request)
        })
        val logging = HttpLoggingInterceptor()
        logging.setLevel(HttpLoggingInterceptor.Level.BODY)
        httpClient.addInterceptor(logging) // <-- this is the important line!
        httpClient.readTimeout(60, TimeUnit.SECONDS)
        httpClient.writeTimeout(60, TimeUnit.SECONDS)
        httpClient.connectTimeout(60, TimeUnit.SECONDS)
        httpClient.callTimeout(60, TimeUnit.SECONDS)
        httpClient.retryOnConnectionFailure(true)

        return Retrofit.Builder()
            .baseUrl(ReportApi)
            .addConverterFactory(GsonConverterFactory.create())
            .client(httpClient.build())
            .build()
            .create(Commission2ApiService::class.java)
    }

    @Singleton
    @Provides
    fun provideWalletTopUpReportApiService(): WalletTopUpApiService {
        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(Interceptor { chain ->
            val original = chain.request()
            val request = original.newBuilder()
                .method(original.method, original.body)
                .build()
            chain.proceed(request)
        })
        val logging = HttpLoggingInterceptor()
        logging.setLevel(HttpLoggingInterceptor.Level.BODY)
        httpClient.addInterceptor(logging) // <-- this is the important line!
        httpClient.readTimeout(60, TimeUnit.SECONDS)
        httpClient.writeTimeout(60, TimeUnit.SECONDS)
        httpClient.connectTimeout(60, TimeUnit.SECONDS)
        httpClient.callTimeout(60, TimeUnit.SECONDS)
        httpClient.retryOnConnectionFailure(true)

        return Retrofit.Builder()
            .baseUrl(WalletTopupApi)
            .addConverterFactory(GsonConverterFactory.create())
            .client(httpClient.build())
            .build()
            .create(WalletTopUpApiService::class.java)
    }

    @Singleton
    @Provides
    fun provideWalletCashOutReportApiService(): WalletCashOutApiService {

        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(Interceptor { chain ->
            val original = chain.request()
            val request = original.newBuilder()
                .method(original.method, original.body)
                .build()
            chain.proceed(request)
        })
        val logging = HttpLoggingInterceptor()
        logging.setLevel(HttpLoggingInterceptor.Level.BODY)
        httpClient.addInterceptor(logging) // <-- this is the important line!
        httpClient.readTimeout(60, TimeUnit.SECONDS)
        httpClient.writeTimeout(60, TimeUnit.SECONDS)
        httpClient.connectTimeout(60, TimeUnit.SECONDS)
        httpClient.callTimeout(60, TimeUnit.SECONDS)
        httpClient.retryOnConnectionFailure(true)
        return Retrofit.Builder()
            .baseUrl(ReportApi)
            .addConverterFactory(GsonConverterFactory.create())
            .client(httpClient.build())
            .build()
            .create(WalletCashOutApiService::class.java)
    }

    @Singleton
    @Provides
    fun provideWalletInterchangeReportApiService(): WalletInterchangeApiService {

        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(Interceptor { chain ->
            val original = chain.request()
            val request = original.newBuilder()
                .method(original.method, original.body)
                .build()
            chain.proceed(request)
        })
        val logging = HttpLoggingInterceptor()
        logging.setLevel(HttpLoggingInterceptor.Level.BODY)
        httpClient.addInterceptor(logging) // <-- this is the important line!
        httpClient.readTimeout(60, TimeUnit.SECONDS)
        httpClient.writeTimeout(60, TimeUnit.SECONDS)
        httpClient.connectTimeout(60, TimeUnit.SECONDS)
        httpClient.callTimeout(60, TimeUnit.SECONDS)
        httpClient.retryOnConnectionFailure(true)
        return Retrofit.Builder()
            .baseUrl(WalletInterchangeApi)
            .addConverterFactory(GsonConverterFactory.create())
            .client(httpClient.build())
            .build()
            .create(WalletInterchangeApiService::class.java)
    }
    @Singleton
    @Provides
    fun provideDmtGatewayReportApiService(): DmtGateWayApiService {

        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(Interceptor { chain ->
            val original = chain.request()
            val request = original.newBuilder()
                .method(original.method, original.body)
                .build()
            chain.proceed(request)
        })
        val logging = HttpLoggingInterceptor()
        logging.setLevel(HttpLoggingInterceptor.Level.BODY)
        httpClient.addInterceptor(logging) // <-- this is the important line!
        httpClient.readTimeout(60, TimeUnit.SECONDS)
        httpClient.writeTimeout(60, TimeUnit.SECONDS)
        httpClient.connectTimeout(60, TimeUnit.SECONDS)
        httpClient.callTimeout(60, TimeUnit.SECONDS)
        httpClient.retryOnConnectionFailure(true)
        return Retrofit.Builder()
            .baseUrl(DmtGateWayApi)
            .addConverterFactory(GsonConverterFactory.create())
            .client(httpClient.build())
            .build()
            .create(DmtGateWayApiService::class.java)
    }
 @Singleton
    @Provides
    fun provideDmtReceiptReportApiService(): DmtReceiptReportApiService {
     val httpClient = OkHttpClient.Builder()
     httpClient.addInterceptor(Interceptor { chain ->
         val original = chain.request()
         val request = original.newBuilder()
             .method(original.method, original.body)
             .build()
         chain.proceed(request)
     })
     val logging = HttpLoggingInterceptor()
     logging.setLevel(HttpLoggingInterceptor.Level.BODY)
     httpClient.addInterceptor(logging) // <-- this is the important line!
     httpClient.readTimeout(60, TimeUnit.SECONDS)
     httpClient.writeTimeout(60, TimeUnit.SECONDS)
     httpClient.connectTimeout(60, TimeUnit.SECONDS)
     httpClient.callTimeout(60, TimeUnit.SECONDS)
     httpClient.retryOnConnectionFailure(true)
        return Retrofit.Builder()
            .baseUrl(DmtReceiptApi)
            .addConverterFactory(GsonConverterFactory.create())
            .client(httpClient.build())
            .build()
            .create(DmtReceiptReportApiService::class.java)
    }

    @Singleton
    @Provides
    fun provideBBPSReportApiService(): BBPSApiService {
        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(Interceptor { chain ->
            val original = chain.request()
            val request = original.newBuilder()
                .method(original.method, original.body)
                .build()
            chain.proceed(request)
        })
        val logging = HttpLoggingInterceptor()
        logging.setLevel(HttpLoggingInterceptor.Level.BODY)
        httpClient.addInterceptor(logging) // <-- this is the important line!
        httpClient.readTimeout(60, TimeUnit.SECONDS)
        httpClient.writeTimeout(60, TimeUnit.SECONDS)
        httpClient.connectTimeout(60, TimeUnit.SECONDS)
        httpClient.callTimeout(60, TimeUnit.SECONDS)
        httpClient.retryOnConnectionFailure(true)
        return Retrofit.Builder()
            .baseUrl(BBPSReportApi)
            .addConverterFactory(GsonConverterFactory.create())
            .client(httpClient.build())
            .build()
            .create(BBPSApiService::class.java)
    }
    @Singleton
    @Provides
    fun provideRechargeReportApiService(): RechargeApiService {
        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(Interceptor { chain ->
            val original = chain.request()
            val request = original.newBuilder()
                .method(original.method, original.body)
                .build()
            chain.proceed(request)
        })
        val logging = HttpLoggingInterceptor()
        logging.setLevel(HttpLoggingInterceptor.Level.BODY)
        httpClient.addInterceptor(logging) // <-- this is the important line!
        httpClient.readTimeout(60, TimeUnit.SECONDS)
        httpClient.writeTimeout(60, TimeUnit.SECONDS)
        httpClient.connectTimeout(60, TimeUnit.SECONDS)
        httpClient.callTimeout(60, TimeUnit.SECONDS)
        httpClient.retryOnConnectionFailure(true)
        return Retrofit.Builder()
            .baseUrl(RechargeReportApi)
            .addConverterFactory(GsonConverterFactory.create())
            .client(httpClient.build())
            .build()
            .create(RechargeApiService::class.java)
    }
    @Singleton
    @Provides
    fun provideUpiReportApiService(): UpiApiService {
        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(Interceptor { chain ->
            val original = chain.request()
            val request = original.newBuilder()
                .method(original.method, original.body)
                .build()
            chain.proceed(request)
        })
        val logging = HttpLoggingInterceptor()
        logging.setLevel(HttpLoggingInterceptor.Level.BODY)
        httpClient.addInterceptor(logging) // <-- this is the important line!
        httpClient.readTimeout(60, TimeUnit.SECONDS)
        httpClient.writeTimeout(60, TimeUnit.SECONDS)
        httpClient.connectTimeout(60, TimeUnit.SECONDS)
        httpClient.callTimeout(60, TimeUnit.SECONDS)
        httpClient.retryOnConnectionFailure(true)
        return Retrofit.Builder()
            .baseUrl(UpiReportApi)
            .addConverterFactory(GsonConverterFactory.create())
            .client(httpClient.build())
            .build()
            .create(UpiApiService::class.java)
    }
    @Singleton
    @Provides
    fun provideFetchWalletTopUpTxnApiService(): FetchWalletTopupTxnApiService {
        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(Interceptor { chain ->
            val original = chain.request()
            val request = original.newBuilder()
                .method(original.method, original.body)
                .build()
            chain.proceed(request)
        })
        val logging = HttpLoggingInterceptor()
        logging.setLevel(HttpLoggingInterceptor.Level.BODY)
        httpClient.addInterceptor(logging) // <-- this is the important line!
        httpClient.readTimeout(60, TimeUnit.SECONDS)
        httpClient.writeTimeout(60, TimeUnit.SECONDS)
        httpClient.connectTimeout(60, TimeUnit.SECONDS)
        httpClient.callTimeout(60, TimeUnit.SECONDS)
        httpClient.retryOnConnectionFailure(true)
        return Retrofit.Builder()
            .baseUrl(FetchWalletTopUpTxn)
            .addConverterFactory(GsonConverterFactory.create())
            .client(httpClient.build())
            .build()
            .create(FetchWalletTopupTxnApiService::class.java)
    }
    @Singleton
    @Provides
    fun provideFetchWalletTopUpRequestApiService(): WalletTopUpRequestApiService {
        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(Interceptor { chain ->
            val original = chain.request()
            val request = original.newBuilder()
                .method(original.method, original.body)
                .build()
            chain.proceed(request)
        })
        val logging = HttpLoggingInterceptor()
        logging.setLevel(HttpLoggingInterceptor.Level.BODY)
        httpClient.addInterceptor(logging) // <-- this is the important line!
        httpClient.readTimeout(60, TimeUnit.SECONDS)
        httpClient.writeTimeout(60, TimeUnit.SECONDS)
        httpClient.connectTimeout(60, TimeUnit.SECONDS)
        httpClient.callTimeout(60, TimeUnit.SECONDS)
        httpClient.retryOnConnectionFailure(true)
        return Retrofit.Builder()
            .baseUrl(FetchWalletTopUpTxn)
            .addConverterFactory(GsonConverterFactory.create())
            .client(httpClient.build())
            .build()
            .create(WalletTopUpRequestApiService::class.java)
    }
    @Singleton
    @Provides
    fun provideWalletTopUpFetchApiService(): WalletTopUpFetchUserApiService {
        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(Interceptor { chain ->
            val original = chain.request()
            val request = original.newBuilder()
                .method(original.method, original.body)
                .build()
            chain.proceed(request)
        })
        val logging = HttpLoggingInterceptor()
        logging.setLevel(HttpLoggingInterceptor.Level.BODY)
        httpClient.addInterceptor(logging) // <-- this is the important line!
        httpClient.readTimeout(60, TimeUnit.SECONDS)
        httpClient.writeTimeout(60, TimeUnit.SECONDS)
        httpClient.connectTimeout(60, TimeUnit.SECONDS)
        httpClient.callTimeout(60, TimeUnit.SECONDS)
        httpClient.retryOnConnectionFailure(true)
        return Retrofit.Builder()
            .baseUrl(WalletTopUpFetchUser)
            .addConverterFactory(GsonConverterFactory.create())
            .client(httpClient.build())
            .build()
            .create(WalletTopUpFetchUserApiService::class.java)
    }
    @Singleton
    @Provides
    fun provideLivLongApiService(): LivLongApiService {
        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(Interceptor { chain ->
            val original = chain.request()
            val request = original.newBuilder()
                .method(original.method, original.body)
                .build()
            chain.proceed(request)
        })
        val logging = HttpLoggingInterceptor()
        logging.setLevel(HttpLoggingInterceptor.Level.BODY)
        httpClient.addInterceptor(logging) // <-- this is the important line!
        httpClient.readTimeout(60, TimeUnit.SECONDS)
        httpClient.writeTimeout(60, TimeUnit.SECONDS)
        httpClient.connectTimeout(60, TimeUnit.SECONDS)
        httpClient.callTimeout(60, TimeUnit.SECONDS)
        httpClient.retryOnConnectionFailure(true)
        return Retrofit.Builder()
            .baseUrl(LivLongReportApi)
            .addConverterFactory(GsonConverterFactory.create())
            .client(httpClient.build())
            .build()
            .create(LivLongApiService::class.java)
    }

    @Singleton
    @Provides
    fun provideAdharpayApiService(): AadharPayApiService {
        val httpClient = OkHttpClient.Builder()
        httpClient.addInterceptor(Interceptor { chain ->
            val original = chain.request()
            val request = original.newBuilder()
                .method(original.method, original.body)
                .build()
            chain.proceed(request)
        })
        val logging = HttpLoggingInterceptor()
        logging.setLevel(HttpLoggingInterceptor.Level.BODY)
        httpClient.addInterceptor(logging) // <-- this is the important line!
        httpClient.readTimeout(60, TimeUnit.SECONDS)
        httpClient.writeTimeout(60, TimeUnit.SECONDS)
        httpClient.connectTimeout(60, TimeUnit.SECONDS)
        httpClient.callTimeout(60, TimeUnit.SECONDS)
        httpClient.retryOnConnectionFailure(true)
        return Retrofit.Builder()
            .baseUrl(AadharpayReport)
            .addConverterFactory(GsonConverterFactory.create())
            .client(httpClient.build())
            .build()
            .create(AadharPayApiService::class.java)
    }
}