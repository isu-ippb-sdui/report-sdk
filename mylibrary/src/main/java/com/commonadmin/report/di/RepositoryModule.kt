package com.commonadmin.report.di

import com.commonadmin.report.data.remote.apiservice.AadharPayApiService
import com.commonadmin.report.data.remote.apiservice.BBPSApiService
import com.commonadmin.report.data.remote.apiservice.Commission1ApiService
import com.commonadmin.report.data.remote.apiservice.Commission2ApiService
import com.commonadmin.report.data.remote.apiservice.DmtGateWayApiService
import com.commonadmin.report.data.remote.apiservice.DmtReceiptReportApiService
import com.commonadmin.report.data.remote.apiservice.FetchSduiApiService
import com.commonadmin.report.data.remote.apiservice.FetchWalletTopupTxnApiService
import com.commonadmin.report.data.remote.apiservice.LivLongApiService
import com.commonadmin.report.data.remote.apiservice.Matm2ApiService
import com.commonadmin.report.data.remote.apiservice.RechargeApiService
import com.commonadmin.report.data.remote.apiservice.UnifiedApiService
import com.commonadmin.report.data.remote.apiservice.UpiApiService
import com.commonadmin.report.data.remote.apiservice.Wallet1ApiService
import com.commonadmin.report.data.remote.apiservice.Wallet2ApiService
import com.commonadmin.report.data.remote.apiservice.WalletCashOutApiService
import com.commonadmin.report.data.remote.apiservice.WalletInterchangeApiService
import com.commonadmin.report.data.remote.apiservice.WalletTopUpApiService
import com.commonadmin.report.data.remote.apiservice.WalletTopUpFetchUserApiService
import com.commonadmin.report.data.remote.apiservice.WalletTopUpRequestApiService
import com.commonadmin.report.data.repositoryimpl.AadharPayRepoImpl
import com.commonadmin.report.data.repositoryimpl.BBPSRepoImpl
import com.commonadmin.report.data.repositoryimpl.Commission1RepoImpl
import com.commonadmin.report.data.repositoryimpl.Commission2RepoImpl
import com.commonadmin.report.data.repositoryimpl.DmtGateWayRepoImpl
import com.commonadmin.report.data.repositoryimpl.DmtReceiptRepoImpl
import com.commonadmin.report.data.repositoryimpl.FetchWalletTopUpTxnRepoImpl
import com.commonadmin.report.data.repositoryimpl.LivLongRepoImpl
import com.commonadmin.report.data.repositoryimpl.Matm2RepoImpl
import com.commonadmin.report.data.repositoryimpl.RechargeRepoImpl
import com.commonadmin.report.data.repositoryimpl.SduiFetchRepoImpl
import com.commonadmin.report.data.repositoryimpl.UnifiedAepsRepoImpl
import com.commonadmin.report.data.repositoryimpl.UpiRepoImpl
import com.commonadmin.report.data.repositoryimpl.Wallet1RepoImpl
import com.commonadmin.report.data.repositoryimpl.Wallet2RepoImpl
import com.commonadmin.report.data.repositoryimpl.WalletCashoutRepoImpl
import com.commonadmin.report.data.repositoryimpl.WalletInterchangeRepoImpl
import com.commonadmin.report.data.repositoryimpl.WalletTopUpFetchRepoImpl
import com.commonadmin.report.data.repositoryimpl.WalletTopUpRepoImpl
import com.commonadmin.report.data.repositoryimpl.WalletTopUpRequestRepoImpl
import com.commonadmin.report.domain.reposiroty.AadharpayReportRepo
import com.commonadmin.report.domain.reposiroty.BBPSReportRepo
import com.commonadmin.report.domain.reposiroty.Commission1ReportRepo
import com.commonadmin.report.domain.reposiroty.Commission2ReportRepo
import com.commonadmin.report.domain.reposiroty.DmtGatewayReportRepo
import com.commonadmin.report.domain.reposiroty.DmtReceiptReportRepo
import com.commonadmin.report.domain.reposiroty.FetchWalletTopUpTxnReportRepo
import com.commonadmin.report.domain.reposiroty.LivLongReportRepo
import com.commonadmin.report.domain.reposiroty.Matm2ReportRepo
import com.commonadmin.report.domain.reposiroty.RechargeReportRepo
import com.commonadmin.report.domain.reposiroty.SduiFetchRepo
import com.commonadmin.report.domain.reposiroty.UnifiedAepsRepo
import com.commonadmin.report.domain.reposiroty.UpiReportRepo
import com.commonadmin.report.domain.reposiroty.Wallet1ReportRepo
import com.commonadmin.report.domain.reposiroty.Wallet2ReportRepo
import com.commonadmin.report.domain.reposiroty.WalletCashoutReportRepo
import com.commonadmin.report.domain.reposiroty.WalletInterChangeReportRepo
import com.commonadmin.report.domain.reposiroty.WalletTopUpFetchReportRepo
import com.commonadmin.report.domain.reposiroty.WalletTopUpReportRepo
import com.commonadmin.report.domain.reposiroty.WalletTopUpRequestReportRepo
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {
    /**
     * Sdui fetch api
     */
    @Singleton
    @Provides
    fun provideSduiFetchRepository(sduiApiService: FetchSduiApiService): SduiFetchRepo {
        return SduiFetchRepoImpl(
            sduiApiService = sduiApiService
        )
    }

    /**
     * Aeps Report
     */
    @Singleton
    @Provides
    fun provideAepsRepository(apiServices: UnifiedApiService): UnifiedAepsRepo {
        return UnifiedAepsRepoImpl(
            unifiedAepsService = apiServices
        )
    }

    /**
     * Wallet1 Report
     */
    @Singleton
    @Provides
    fun provideWallet1Repository(apiServices: Wallet1ApiService): Wallet1ReportRepo {
        return Wallet1RepoImpl(
            wallet1ApiService = apiServices
        )
    }

    /**
     * Wallet2 Report
     */
    @Singleton
    @Provides
    fun provideWallet2Repository(apiServices: Wallet2ApiService): Wallet2ReportRepo {
        return Wallet2RepoImpl(
            wallet2ApiService = apiServices
        )
    }

    /**
     * matm2 Report
     */
    @Singleton
    @Provides
    fun provideMatm2Repository(apiServices: Matm2ApiService): Matm2ReportRepo {
        return Matm2RepoImpl(
            matm2ApiService = apiServices
        )
    }
    /**
     * Commission1 Report
     */
    @Singleton
    @Provides
    fun provideCommission1Repository(apiServices: Commission1ApiService): Commission1ReportRepo {
        return Commission1RepoImpl(
            commission1ApiService = apiServices
        )
    }
    /**
     * Commission2 Report
     */
    @Singleton
    @Provides
    fun provideCommission2Repository(apiServices: Commission2ApiService): Commission2ReportRepo {
        return Commission2RepoImpl(
            commission2ApiService = apiServices
        )
    }
  /**
     * WalletTopUp Report
     */
    @Singleton
    @Provides
    fun provideWalletTopUpRepository(apiServices: WalletTopUpApiService): WalletTopUpReportRepo {
        return WalletTopUpRepoImpl(
            walletTopUpApiService = apiServices
        )
    }
    /**
     * WalletTopUp Report
     */
    @Singleton
    @Provides
    fun provideWalletCashoutRepository(apiServices: WalletCashOutApiService): WalletCashoutReportRepo {
        return WalletCashoutRepoImpl(
            walletCashOutApiService = apiServices
        )
    }
   /**
     * WalletInterchange Report
     */
    @Singleton
    @Provides
    fun provideWalletInterchangeRepository(apiServices: WalletInterchangeApiService): WalletInterChangeReportRepo {
        return WalletInterchangeRepoImpl(
            walletInterchangeApiService = apiServices
        )
    }
 /**
     * DmtGateWay Report
     */
    @Singleton
    @Provides
    fun provideDmtGateWayRepository(apiServices: DmtGateWayApiService): DmtGatewayReportRepo {
        return DmtGateWayRepoImpl(
            dmtGateWayApiService = apiServices
        )
    }
    /**
     * DmtGateWay Report
     */
    @Singleton
    @Provides
    fun provideDmtReceiptRepository(apiServices: DmtReceiptReportApiService): DmtReceiptReportRepo {
        return DmtReceiptRepoImpl(
            dmtReceiptReportApiService = apiServices
        )
    }
    /**
     * BBPS Report
     */
    @Singleton
    @Provides
    fun provideBBPSRepository(apiServices: BBPSApiService): BBPSReportRepo {
        return BBPSRepoImpl(
            bbpsApiService = apiServices
        )
    }
    /**
     * Recharge Report
     */
    @Singleton
    @Provides
    fun provideRechargeRepository(apiServices: RechargeApiService): RechargeReportRepo {
        return RechargeRepoImpl(
            rechargeApiService = apiServices
        )
    }

    /**
     * Upi Report
     */
    @Singleton
    @Provides
    fun provideUpiRepository(apiServices: UpiApiService): UpiReportRepo {
        return UpiRepoImpl(
            upiApiService = apiServices
        )
    }
    /**
     * Fetch WalletTopUpTxn Report
     */
    @Singleton
    @Provides
    fun provideFetchWalletTopUpTxnRepository(apiServices: FetchWalletTopupTxnApiService): FetchWalletTopUpTxnReportRepo {
        return FetchWalletTopUpTxnRepoImpl(
            fetchWalletTopupTxnApiService = apiServices
        )
    }
    /**
     * WalletTopRequest Fetch
     */
    @Singleton
    @Provides
    fun provideWalletTopUpRequestRepository(apiServices: WalletTopUpRequestApiService): WalletTopUpRequestReportRepo {
        return WalletTopUpRequestRepoImpl(
            walletTopUpRequestApiService  = apiServices
        )
    }
    /**
     * WalletTop Fetch User
     */
    @Singleton
    @Provides
    fun provideWalletTopUpFetchUserRepository(apiServices: WalletTopUpFetchUserApiService):WalletTopUpFetchReportRepo {
        return WalletTopUpFetchRepoImpl(
            walletTopUpFetchApiService  = apiServices
        )
    }
    /**
     * LivLong Report
     */
    @Singleton
    @Provides
    fun provideLivLongRepository(apiServices:LivLongApiService): LivLongReportRepo {
        return LivLongRepoImpl(
            livLongApiService  = apiServices
        )
    }

    /**
     * AadharPay Report
     */
    @Singleton
    @Provides
    fun provideAadharpayRepository(apiServices: AadharPayApiService): AadharpayReportRepo {
        return AadharPayRepoImpl(
            aadharPayApiService  = apiServices
        )
    }
}
