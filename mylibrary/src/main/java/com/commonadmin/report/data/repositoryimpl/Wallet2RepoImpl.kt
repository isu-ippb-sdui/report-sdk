package com.commonadmin.report.data.repositoryimpl

import com.commonadmin.report.data.remote.apiservice.Wallet2ApiService
import com.commonadmin.report.data.remote.models.Wallet2ReqModel
import com.commonadmin.report.data.remote.models.Wallet2RespModel
import com.commonadmin.report.utils.ApiResult
import com.commonadmin.report.utils.handleResponse
import com.commonadmin.report.domain.reposiroty.Wallet2ReportRepo
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class Wallet2RepoImpl @Inject constructor(private val wallet2ApiService: Wallet2ApiService) :
    Wallet2ReportRepo {
    override suspend fun wallet2ReportApiCall(url:String,wallet2ReqModel: Wallet2ReqModel,token:String): Flow<ApiResult<Wallet2RespModel>> {
        return handleResponse {
            wallet2ApiService.requestForWallet2Report(url,wallet2ReqModel,token)
        }
    }
}