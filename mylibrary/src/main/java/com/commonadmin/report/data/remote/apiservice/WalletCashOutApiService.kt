package com.commonadmin.report.data.remote.apiservice

import com.commonadmin.report.data.remote.models.WalletCashoutReqModel
import com.commonadmin.report.data.remote.models.WalletCashoutRespModel
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.Url

interface WalletCashOutApiService {
    @Headers("Accept:application/json", "Content-Type:application/json")
    @POST
    suspend fun reqForWalletCashoutReport(@Url url:String,@Body param: WalletCashoutReqModel,@Header("Authorization")token:String): Response<WalletCashoutRespModel>

}