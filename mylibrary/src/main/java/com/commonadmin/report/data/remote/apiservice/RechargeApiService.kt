package com.commonadmin.report.data.remote.apiservice


import com.commonadmin.report.data.remote.models.RechargeReqModel
import com.commonadmin.report.data.remote.models.RechargeResponseModel
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.Url

interface RechargeApiService {
    @Headers("Accept:application/json", "Content-Type:application/json")
    @POST
    suspend fun requestForRechargeReport(@Url url:String,@Body param: RechargeReqModel,@Header("Authorization")token:String): Response<RechargeResponseModel>

}