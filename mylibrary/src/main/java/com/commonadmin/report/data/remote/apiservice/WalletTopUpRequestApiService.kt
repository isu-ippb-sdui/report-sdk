package com.commonadmin.report.data.remote.apiservice


import com.commonadmin.report.data.remote.models.WalletTopUpRequestReqModel
import com.commonadmin.report.data.remote.models.WalletTopUpRequestRespModel
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.Url

interface WalletTopUpRequestApiService {
    @Headers("Accept:application/json", "Content-Type:application/json")
    @POST
    suspend fun requestForWalletTopUpRequest(@Url url:String,@Body param: WalletTopUpRequestReqModel): Response<WalletTopUpRequestRespModel>

}