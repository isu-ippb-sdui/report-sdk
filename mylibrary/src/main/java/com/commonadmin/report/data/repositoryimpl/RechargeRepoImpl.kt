package com.commonadmin.report.data.repositoryimpl

import com.commonadmin.report.data.remote.apiservice.RechargeApiService
import com.commonadmin.report.data.remote.models.RechargeReqModel
import com.commonadmin.report.data.remote.models.RechargeResponseModel
import com.commonadmin.report.utils.ApiResult
import com.commonadmin.report.utils.handleResponse
import com.commonadmin.report.domain.reposiroty.RechargeReportRepo
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class RechargeRepoImpl @Inject constructor(private val rechargeApiService: RechargeApiService) :
    RechargeReportRepo {
    override suspend fun rechargeReportApiCall(
        url: String,
        rechargeReqModel: RechargeReqModel,token:String
    ): Flow<ApiResult<RechargeResponseModel>> {
        return handleResponse {
            rechargeApiService.requestForRechargeReport(url, rechargeReqModel,token)
        }
    }
}