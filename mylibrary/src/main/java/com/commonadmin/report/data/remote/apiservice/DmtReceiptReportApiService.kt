package com.commonadmin.report.data.remote.apiservice

import com.commonadmin.report.data.remote.models.DmtReceiptReportReqModel
import com.commonadmin.report.data.remote.models.DmtReceiptReportRespModel
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.Url

interface DmtReceiptReportApiService {
    @Headers("Accept:application/json", "Content-Type:application/json")
    @POST
    suspend fun requestForDmtReceiptReport(@Url url:String,@Body param: DmtReceiptReportReqModel): Response<DmtReceiptReportRespModel>

}