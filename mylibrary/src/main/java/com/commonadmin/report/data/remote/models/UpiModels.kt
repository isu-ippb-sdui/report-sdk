package com.commonadmin.report.data.remote.models

import com.google.gson.annotations.SerializedName

data class UpiReqModel(

    @field:SerializedName("start_date")
    val startDate: String? = null,

    @field:SerializedName("end_date")
    val endDate: String? = null,
    @field:SerializedName("userName")
    val userName: String? = null,

    @field:SerializedName("transaction_type")
    val transactionType: List<String>? = null,
    @field:SerializedName("operationPerformed")
    val operationPerformed: List<String>? = null,

    @field:SerializedName("status")
    val status: String? = null
)

data class UpiRespModel(

    @field:SerializedName("length")
    val length: Int? = null,

    @field:SerializedName("message")
    val message: String? = null,

    @field:SerializedName("results")
    val results: UPIResults? = null,

    @field:SerializedName("status")
    val status: Int? = null
)

data class UPIResults(

    @field:SerializedName("BQReport")
    val bQReport: List<UpiReportItem>? = null
)

data class UpiReportItem(
    @SerializedName("Id") val id: String,
    @SerializedName("previousAmount") val previousAmount: String,
    @SerializedName("amountTransacted") val amountTransacted: String,
    @SerializedName("balanceAmount") val balanceAmount: String,
    @SerializedName("apiTid") val apiTid: String,
    @SerializedName("apiComment") val apiComment: String,
    @SerializedName("operationPerformed") val operationPerformed: String,
    @SerializedName("status") val status: String,
    @SerializedName("transactionMode") val transactionMode: String,
    @SerializedName("userName") val userName: String,
    @SerializedName("masterName") val masterName: String,
    @SerializedName("createdDate") val createdDate: Long,
    @SerializedName("updatedDate") val updatedDate: Long,
    @SerializedName("referenceNo") val referenceNo: String,
    @SerializedName("param_a") val paramA: String,
    @SerializedName("param_b") val paramB: String,
    @SerializedName("param_c") val paramC: String
)
