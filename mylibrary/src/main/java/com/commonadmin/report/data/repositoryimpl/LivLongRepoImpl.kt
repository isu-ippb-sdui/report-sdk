package com.commonadmin.report.data.repositoryimpl

import com.commonadmin.report.data.remote.apiservice.LivLongApiService
import com.commonadmin.report.data.remote.models.LivLongReportReqModel
import com.commonadmin.report.data.remote.models.LivLongRespModel
import com.commonadmin.report.utils.ApiResult
import com.commonadmin.report.utils.handleResponse
import com.commonadmin.report.domain.reposiroty.LivLongReportRepo
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class LivLongRepoImpl @Inject constructor(private val livLongApiService: LivLongApiService) :
    LivLongReportRepo {
    override suspend fun livLongReportApiCall(
        url: String,
        livLongReportReqModel: LivLongReportReqModel, token:String
    ): Flow<ApiResult<LivLongRespModel>> {
        return handleResponse {
            livLongApiService.requestForLivLongReport(url, livLongReportReqModel,token)
        }
    }
}