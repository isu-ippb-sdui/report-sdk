package com.commonadmin.report.data.repositoryimpl

import com.commonadmin.report.data.remote.apiservice.WalletTopUpRequestApiService
import com.commonadmin.report.data.remote.models.WalletTopUpRequestReqModel
import com.commonadmin.report.data.remote.models.WalletTopUpRequestRespModel
import com.commonadmin.report.utils.ApiResult
import com.commonadmin.report.utils.handleResponse
import com.commonadmin.report.domain.reposiroty.WalletTopUpRequestReportRepo
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class WalletTopUpRequestRepoImpl @Inject constructor(private val walletTopUpRequestApiService: WalletTopUpRequestApiService) :
    WalletTopUpRequestReportRepo {
    override suspend fun walletTopUpRequestApiCall(
        url: String,
        walletTopUpRequestReqModel: WalletTopUpRequestReqModel
    ): Flow<ApiResult<WalletTopUpRequestRespModel>> {
        return handleResponse {
            walletTopUpRequestApiService.requestForWalletTopUpRequest(url,walletTopUpRequestReqModel )
        }
    }
}