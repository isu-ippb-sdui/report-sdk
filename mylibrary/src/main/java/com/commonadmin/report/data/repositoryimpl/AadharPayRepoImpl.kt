package com.commonadmin.report.data.repositoryimpl


import com.commonadmin.report.data.remote.apiservice.AadharPayApiService
import com.commonadmin.report.data.remote.models.AadharpayReqModel
import com.commonadmin.report.data.remote.models.AadharpayRespModel
import com.commonadmin.report.domain.reposiroty.AadharpayReportRepo
import com.commonadmin.report.utils.ApiResult
import com.commonadmin.report.utils.handleResponse
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class AadharPayRepoImpl @Inject constructor(private val aadharPayApiService: AadharPayApiService) :
    AadharpayReportRepo {
    override suspend fun aadharPayReportApiCall(
        url: String,
        aadharpayReqModel: AadharpayReqModel, token:String
    ): Flow<ApiResult<AadharpayRespModel>> {
        return handleResponse {
            aadharPayApiService.requestForAadharpayReport(url, aadharpayReqModel,token)
        }
    }
}