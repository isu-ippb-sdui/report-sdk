package com.commonadmin.report.data.remote.apiservice

import com.commonadmin.report.data.remote.models.SduiFetchReqModels
import com.commonadmin.report.data.remote.models.SduiFetchRespModels
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST


interface FetchSduiApiService {
    @Headers("Accept:application/json", "Content-Type:application/json")
    @POST("fetchData")
    suspend fun requestForFetchSDUI(@Body param: SduiFetchReqModels): Response<SduiFetchRespModels>

}