package com.commonadmin.report.data.repositoryimpl

import com.commonadmin.report.data.remote.apiservice.WalletInterchangeApiService
import com.commonadmin.report.data.remote.models.WalletInterchangeReqModel
import com.commonadmin.report.data.remote.models.WalletInterchangeRespModel
import com.commonadmin.report.utils.ApiResult
import com.commonadmin.report.utils.handleResponse
import com.commonadmin.report.domain.reposiroty.WalletInterChangeReportRepo
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class WalletInterchangeRepoImpl @Inject constructor(private val walletInterchangeApiService: WalletInterchangeApiService) :
    WalletInterChangeReportRepo {
    override suspend fun walletInterChangeReportApiCall(
        url: String,
        walletInterchangeReqModel: WalletInterchangeReqModel,token:String
    ): Flow<ApiResult<WalletInterchangeRespModel>> {
        return handleResponse {
            walletInterchangeApiService.requestForWalletInterchangeReport(url, walletInterchangeReqModel,token)
        }
    }
}