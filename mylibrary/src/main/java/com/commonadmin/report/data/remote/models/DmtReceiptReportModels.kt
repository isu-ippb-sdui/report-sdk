package com.commonadmin.report.data.remote.models

import com.google.gson.annotations.SerializedName

data class DmtReceiptReportRespModel(

    @field:SerializedName("length")
    val length: Int? = null,

    @field:SerializedName("message")
    val message: String? = null,

    @field:SerializedName("results")
    val results: ArrayList<DmtReceiptItem>? = null,

    @field:SerializedName("status")
    val status: Int? = null
)

data class DmtReceiptItem(
    @field:SerializedName("transactionMode")
    val transactionMode: String? = null,

    @field:SerializedName("operationPerformed")
    val operationPerformed: String? = null,

    @field:SerializedName("amountTransacted")
    val amountTransacted: String? = null,

    @field:SerializedName("beneAccountNumber")
    val beneAccountNumber: String? = null,

    @field:SerializedName("updatedDate")
    val updatedDate: Long? = null,

    @field:SerializedName("balanceAmount")
    val balanceAmount: String? = null,

    @field:SerializedName("userName")
    val userName: String? = null,

    @field:SerializedName("previousAmount")
    val previousAmount: String? = null,

    @field:SerializedName("gateWayData")
    val gateWayData: ArrayList<DmtGateWayDataItem>? = null,

    @field:SerializedName("transactionType")
    val transactionType: String? = null,

    @field:SerializedName("createdDate")
    val createdDate: Long? = null,

    @field:SerializedName("beneBankName")
    val beneBankName: String? = null,

    @field:SerializedName("beneName")
    val beneName: String? = null,

    @field:SerializedName("customerMobileNumber")
    val customerMobileNumber: String? = null,

    @field:SerializedName("Id")
    val id: String? = null,

    @field:SerializedName("apiComment")
    val apiComment: String? = null,

    @field:SerializedName("status")
    val status: String? = null,

    @field:SerializedName("masterName")
    val masterName: String? = null
)

data class DmtGateWayDataItem(

    @field:SerializedName("amount")
    val amount: Int? = null,

    @field:SerializedName("transaction_status_code")
    val transactionStatusCode: String? = null,

    @field:SerializedName("transacted_bank_name")
    val transactedBankName: String? = null,

    @field:SerializedName("status_desc")
    val statusDesc: String? = null,

    @field:SerializedName("id")
    val id: String? = null,

    @field:SerializedName("status")
    val status: String? = null,

    @field:SerializedName("rrn")
    val rrn: String? = null
)

data class DmtReceiptReportReqModel(
    @field:SerializedName("$1")
    val jsonMember1: String? = null,

    @field:SerializedName("$2")
    val jsonMember2: String? = null,

    @field:SerializedName("$3")
    val jsonMember3: ArrayList<String>? = null,
    @field:SerializedName("$4")
    val jsonMember4: String? = null,

    @field:SerializedName("$5")
    val jsonMember5: String? = null,

    @field:SerializedName("$6")
    val jsonMember6: ArrayList<String>? = null,

    @field:SerializedName("$7")
    val jsonMember7: ArrayList<String>? = null,

    @field:SerializedName("$10")
    val jsonMember10: ArrayList<Long>? = null,
)
