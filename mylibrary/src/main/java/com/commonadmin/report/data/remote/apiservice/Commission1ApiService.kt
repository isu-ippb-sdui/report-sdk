package com.commonadmin.report.data.remote.apiservice

import com.commonadmin.report.data.remote.models.Commission1ReportReqModel
import com.commonadmin.report.data.remote.models.Commission1ReportRespModel
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.Url

interface Commission1ApiService {
    @Headers("Accept:application/json", "Content-Type:application/json")
    @POST
    suspend fun requestForCommission1Report(@Url url:String, @Body param: Commission1ReportReqModel, @Header("Authorization")token:String): Response<Commission1ReportRespModel>

}