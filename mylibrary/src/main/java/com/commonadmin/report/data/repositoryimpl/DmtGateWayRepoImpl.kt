package com.commonadmin.report.data.repositoryimpl

import com.commonadmin.report.data.remote.apiservice.DmtGateWayApiService
import com.commonadmin.report.data.remote.models.DmtGatewayReqModels
import com.commonadmin.report.data.remote.models.DmtGatewayRespModel
import com.commonadmin.report.utils.ApiResult
import com.commonadmin.report.utils.handleResponse
import com.commonadmin.report.domain.reposiroty.DmtGatewayReportRepo
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class DmtGateWayRepoImpl @Inject constructor(private val dmtGateWayApiService: DmtGateWayApiService) :
    DmtGatewayReportRepo {
    override suspend fun dmtGateWayReportApiCall(
        url: String,
       dmtGatewayReqModels: DmtGatewayReqModels,
        token:String
    ): Flow<ApiResult<DmtGatewayRespModel>> {
        return handleResponse {
            dmtGateWayApiService.requestForDmtGateWayReport(url, dmtGatewayReqModels,token)
        }
    }
}