package com.commonadmin.report.data.repositoryimpl


import com.commonadmin.report.data.remote.apiservice.UpiApiService
import com.commonadmin.report.data.remote.models.UpiReqModel
import com.commonadmin.report.data.remote.models.UpiRespModel
import com.commonadmin.report.utils.ApiResult
import com.commonadmin.report.utils.handleResponse
import com.commonadmin.report.domain.reposiroty.UpiReportRepo
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class UpiRepoImpl @Inject constructor(private val upiApiService: UpiApiService) :
    UpiReportRepo {
    override suspend fun upiReportApiCall(
        url: String,
       upiReqModel: UpiReqModel,
        token:String
    ): Flow<ApiResult<UpiRespModel>> {
        return handleResponse {
            upiApiService.requestForUpiReport(url, upiReqModel,token)
        }
    }
}