package com.commonadmin.report.data.repositoryimpl

import com.commonadmin.report.data.remote.apiservice.WalletTopUpApiService
import com.commonadmin.report.data.remote.models.WalletTopUpReqModel
import com.commonadmin.report.data.remote.models.WalletTopUpRespModel
import com.commonadmin.report.utils.ApiResult
import com.commonadmin.report.utils.handleResponse
import com.commonadmin.report.domain.reposiroty.WalletTopUpReportRepo
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class WalletTopUpRepoImpl @Inject constructor(private val walletTopUpApiService: WalletTopUpApiService) :
    WalletTopUpReportRepo {
    override suspend fun walletTopUpReportApiCall(
        url: String,
        walletTopUpReqModel: WalletTopUpReqModel,token:String
    ): Flow<ApiResult<WalletTopUpRespModel>> {
        return handleResponse {
            walletTopUpApiService.requestForWalletTopUpReport(url, walletTopUpReqModel,token)
        }
    }
}