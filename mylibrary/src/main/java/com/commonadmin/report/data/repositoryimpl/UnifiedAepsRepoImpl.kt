package com.commonadmin.report.data.repositoryimpl

import com.commonadmin.report.data.remote.apiservice.UnifiedApiService
import com.commonadmin.report.data.remote.models.UnifiedAepsReqModel
import com.commonadmin.report.data.remote.models.UnifiedAepsRespModel
import com.commonadmin.report.utils.ApiResult
import com.commonadmin.report.utils.handleResponse
import com.commonadmin.report.domain.reposiroty.UnifiedAepsRepo
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class UnifiedAepsRepoImpl @Inject constructor(private val unifiedAepsService: UnifiedApiService) :
    UnifiedAepsRepo {
    override suspend fun reportApiCall(url:String, unifiedAepsReqModel: UnifiedAepsReqModel, token:String): Flow<ApiResult<UnifiedAepsRespModel>> {
        return handleResponse {
            unifiedAepsService.requestForAepsReport(url,unifiedAepsReqModel,token)
        }
    }


}