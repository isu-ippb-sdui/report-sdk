package com.commonadmin.report.data.remote


class BaseApis {
    companion object {
        const val AepsReportApi = "https://ind-aeps-report.iserveu.tech/"
        const val Wallet1ReportApi = "https://inddmtreportprod.iserveu.tech/"
        const val SDUIfetchApi = "https://sdui-module-prod.iserveu.tech/report/"
        const val ReportApi = "https://ind-matm-txn-report.iserveu.tech/"
        const val WalletTopupApi = "https://indwallettopupprod.iserveu.tech/"
        const val WalletInterchangeApi = "https://indwalletinterchange.iserveu.tech/"
        const val DmtGateWayApi = "https://inddmtreportprod.iserveu.tech/"
        const val DmtReceiptApi = "https://ippbdmtemailreportprod.iserveu.tech/"
        const val BBPSReportApi = "https://ind-bbps-prod.iserveu.tech/"
        const val RechargeReportApi = "https://unified-all-txn-report-prod.iserveu.tech/"
        const val Commission1ReportApi = "https://newdmtreport.iserveu.tech/"
        const val UpiReportApi = "https://us-central1-creditapp-29bf2.cloudfunctions.net/new_node_bigquery_report/"
        const val FetchWalletTopUpTxn = "https://wallet-topup-new-prod-vn3k2k7q7q-uc.a.run.app/users/"
        const val WalletTopUpFetchUser = "https://wallet-topup-new-prod.iserveu.tech/retailerBank/"
        const val LivLongReportApi = "https://unified-all-txn-report-prod.iserveu.tech/"
        const val AadharpayReport = "https://comm-ret-unifiedaeps-txn-report.iserveu.tech/aeps_report/"

    }
}
