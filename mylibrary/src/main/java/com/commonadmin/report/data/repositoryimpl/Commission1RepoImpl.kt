package com.commonadmin.report.data.repositoryimpl

import com.commonadmin.report.data.remote.apiservice.Commission1ApiService
import com.commonadmin.report.data.remote.apiservice.Commission2ApiService
import com.commonadmin.report.data.remote.models.Commission1ReportReqModel
import com.commonadmin.report.data.remote.models.Commission1ReportRespModel
import com.commonadmin.report.data.remote.models.Commission2ReportReqModel
import com.commonadmin.report.data.remote.models.Commission2ReportRespModel
import com.commonadmin.report.domain.reposiroty.Commission1ReportRepo
import com.commonadmin.report.utils.ApiResult
import com.commonadmin.report.utils.handleResponse
import com.commonadmin.report.domain.reposiroty.Commission2ReportRepo
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class Commission1RepoImpl @Inject constructor(private val commission1ApiService: Commission1ApiService) :
    Commission1ReportRepo {
    override suspend fun commission1ReportApiCall(
        url: String,
        commission1ReportReqModel: Commission1ReportReqModel,
        token:String
    ): Flow<ApiResult<Commission1ReportRespModel>> {
        return handleResponse {
            commission1ApiService.requestForCommission1Report(url, commission1ReportReqModel,token)
        }
    }
}