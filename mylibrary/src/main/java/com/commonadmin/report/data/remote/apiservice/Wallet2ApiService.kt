package com.commonadmin.report.data.remote.apiservice

import com.commonadmin.report.data.remote.models.Wallet2ReqModel
import com.commonadmin.report.data.remote.models.Wallet2RespModel
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.Url

interface Wallet2ApiService {
    @Headers("Accept:application/json", "Content-Type:application/json")
    @POST
    suspend fun requestForWallet2Report(@Url url:String,@Body param: Wallet2ReqModel,@Header("Authorization")token:String): Response<Wallet2RespModel>

}