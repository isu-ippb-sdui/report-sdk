package com.commonadmin.report.data.remote.models

import com.google.gson.annotations.SerializedName

data class Commission1ReportReqModel(
	@field:SerializedName("$1")
	val jsonMember1: String? = null,

	@field:SerializedName("$2")
	val jsonMember2: String? = null,

	@field:SerializedName("$4")
	val jsonMember4: String? = null,

	@field:SerializedName("$5")
	val jsonMember5: String? = null,

	@field:SerializedName("$7")
	val jsonMember7: List<String>? = null,


)

data class Commission1ReportRespModel(

	@field:SerializedName("Report")
	val report: ArrayList<Commission1ReportItem>? = null,

	@field:SerializedName("length")
	val length: Int? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class Commission1ReportItem(

	@field:SerializedName("Tds")
	val tds: Any? = null,

	@field:SerializedName("planType")
	val planType: String? = null,

	@field:SerializedName("relationalAmount")
	val relationalAmount: Int? = null,

	@field:SerializedName("walletStatusDescription")
	val walletStatusDescription: String? = null,

	@field:SerializedName("amountTransacted")
	val amountTransacted: Double? = null,

	@field:SerializedName("planName")
	val planName: String? = null,

	@field:SerializedName("updatedDate")
	val updatedDate: Long? = null,

	@field:SerializedName("balanceAmount")
	val balanceAmount: Any? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("previousAmount")
	val previousAmount: Any? = null,

	@field:SerializedName("distributerName")
	val distributerName: Any? = null,

	@field:SerializedName("transactionType")
	val transactionType: String? = null,

	@field:SerializedName("statusDescription")
	val statusDescription: String? = null,

	@field:SerializedName("creditedUser")
	val creditedUser: String? = null,

	@field:SerializedName("createdDate")
	val createdDate: Long? = null,

	@field:SerializedName("relationalOperation")
	val relationalOperation: String? = null,

	@field:SerializedName("walletStatus")
	val walletStatus: String? = null,

	@field:SerializedName("Id")
	val id: String? = null,

	@field:SerializedName("relationalId")
	val relationalId: String? = null,

	@field:SerializedName("status")
	val status: String? = null,

	@field:SerializedName("masterName")
	val masterName: Any? = null
)
