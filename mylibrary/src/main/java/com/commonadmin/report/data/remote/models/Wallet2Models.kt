package com.commonadmin.report.data.remote.models

import com.google.gson.annotations.SerializedName

data class Wallet2ReqModel(
    @field:SerializedName("$1")
    val jsonMember1: String? = null,

    @field:SerializedName("$2")
    val jsonMember2: String? = null,

    @field:SerializedName("$4")
    val jsonMember4: String? = null,

    @field:SerializedName("$5")
    val jsonMember5: String? = null,
 @field:SerializedName("$7")
    val jsonMember7: ArrayList<String>? = null,


    )

data class Wallet2RespModel(

    @field:SerializedName("results")
    val report: List<Wallet2ReportItem>? = null,

    @field:SerializedName("length")
    val length: Int? = null,

    @field:SerializedName("message")
    val message: String? = null,

    @field:SerializedName("status")
    val status: Int? = null
)

data class Wallet2ReportItem(
    @field:SerializedName("relationalId")
    val relatioanlId: String? = null,
    @field:SerializedName("createdDate")
    val createdDate: Long? = null,
    @field:SerializedName("transactionType")
    val transactionType: String? = null,
    @field:SerializedName("relationalOperation")
    val operationPerformed: String? = null,
    @field:SerializedName("currentBalance")
    val currentAmount: Double? = null,
    @field:SerializedName("CrAmount")
    val creditAmount: Double? = null,
    @field:SerializedName("previousBalance")
    val previousAmount: Double? = null,
    @field:SerializedName("DrAmount")
    val debitAmount: Double? = null,
    @field:SerializedName("Id")
    val id: String? = null,
    @field:SerializedName("updatedDate")
    val updatedDate: Long? = null,
    @field:SerializedName("userName")
    val userName: String? = null,

    @field:SerializedName("status")
    val status: String? = null,
    )