package com.commonadmin.report.data.repositoryimpl

import com.commonadmin.report.data.remote.apiservice.WalletTopUpFetchUserApiService
import com.commonadmin.report.data.remote.models.WalletTopUpFetchUserReqModel
import com.commonadmin.report.data.remote.models.WalletTopUpFetchUserRespModel
import com.commonadmin.report.utils.ApiResult
import com.commonadmin.report.utils.handleResponse
import com.commonadmin.report.domain.reposiroty.WalletTopUpFetchReportRepo
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class WalletTopUpFetchRepoImpl @Inject constructor(private val walletTopUpFetchApiService:WalletTopUpFetchUserApiService) :
    WalletTopUpFetchReportRepo {
    override suspend fun walletTopUpFetchApiCall(
        url: String,
        walletTopUpFetchUserReqModel: WalletTopUpFetchUserReqModel
    ): Flow<ApiResult<WalletTopUpFetchUserRespModel>> {
        return handleResponse {
            walletTopUpFetchApiService.requestForWalletTopUpFetch(url, walletTopUpFetchUserReqModel)
        }
    }
}