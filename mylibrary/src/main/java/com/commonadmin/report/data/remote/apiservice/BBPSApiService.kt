package com.commonadmin.report.data.remote.apiservice

import com.commonadmin.report.data.remote.models.BBPSReportReqModel
import com.commonadmin.report.data.remote.models.BBPSReportRespModel
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.Url

interface BBPSApiService {
    @Headers("Accept:application/json", "Content-Type:application/json")
    @POST
    suspend fun requestForBBPSReport(@Url url:String,@Body param: BBPSReportReqModel,@Header("Authorization")token:String): Response<BBPSReportRespModel>

}