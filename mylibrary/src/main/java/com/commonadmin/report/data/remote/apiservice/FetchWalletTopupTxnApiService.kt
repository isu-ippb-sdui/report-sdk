package com.commonadmin.report.data.remote.apiservice


import com.commonadmin.report.data.remote.models.FetchWalletTopUpTxnReqModel
import com.commonadmin.report.data.remote.models.FetchWalletTopUpTxnRespModel
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.Url

interface FetchWalletTopupTxnApiService {
    @Headers("Accept:application/json", "Content-Type:application/json")
    @POST
    suspend fun requestForFetchWalletTopUPTxnReport(@Url url:String,@Body param: FetchWalletTopUpTxnReqModel): Response<FetchWalletTopUpTxnRespModel>

}