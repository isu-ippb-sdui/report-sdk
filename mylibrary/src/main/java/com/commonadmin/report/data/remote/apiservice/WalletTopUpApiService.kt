package com.commonadmin.report.data.remote.apiservice


import com.commonadmin.report.data.remote.models.WalletTopUpReqModel
import com.commonadmin.report.data.remote.models.WalletTopUpRespModel
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.Url

interface WalletTopUpApiService {
    @Headers("Accept:application/json", "Content-Type:application/json")
    @POST
    suspend fun requestForWalletTopUpReport(@Url url:String,@Body param: WalletTopUpReqModel,@Header("Authorization")token:String): Response<WalletTopUpRespModel>

}