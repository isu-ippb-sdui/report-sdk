package com.commonadmin.report.data.repositoryimpl

import com.commonadmin.report.data.remote.apiservice.Commission2ApiService
import com.commonadmin.report.data.remote.models.Commission2ReportReqModel
import com.commonadmin.report.data.remote.models.Commission2ReportRespModel
import com.commonadmin.report.utils.ApiResult
import com.commonadmin.report.utils.handleResponse
import com.commonadmin.report.domain.reposiroty.Commission2ReportRepo
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class Commission2RepoImpl @Inject constructor(private val commission2ApiService: Commission2ApiService) :
    Commission2ReportRepo {
    override suspend fun commission2ReportApiCall(
        url: String,
        commission2ReportReqModel: Commission2ReportReqModel,
        token:String
    ): Flow<ApiResult<Commission2ReportRespModel>> {
        return handleResponse {
            commission2ApiService.requestForCommission2Report(url, commission2ReportReqModel,token)
        }
    }
}