package com.commonadmin.report.data.repositoryimpl

import com.commonadmin.report.data.remote.apiservice.DmtReceiptReportApiService
import com.commonadmin.report.data.remote.models.DmtReceiptReportReqModel
import com.commonadmin.report.data.remote.models.DmtReceiptReportRespModel
import com.commonadmin.report.utils.ApiResult
import com.commonadmin.report.utils.handleResponse
import com.commonadmin.report.domain.reposiroty.DmtReceiptReportRepo
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class DmtReceiptRepoImpl @Inject constructor(private val dmtReceiptReportApiService: DmtReceiptReportApiService) :
    DmtReceiptReportRepo {
    override suspend fun dmtReceiptReportApiCall(
        url: String,
      dmtReceiptReportReqModel: DmtReceiptReportReqModel
    ): Flow<ApiResult<DmtReceiptReportRespModel>> {
        return handleResponse {
            dmtReceiptReportApiService.requestForDmtReceiptReport(url, dmtReceiptReportReqModel)
        }
    }
}