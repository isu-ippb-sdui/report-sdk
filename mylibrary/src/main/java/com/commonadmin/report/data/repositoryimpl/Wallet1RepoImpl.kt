package com.commonadmin.report.data.repositoryimpl

import com.commonadmin.report.data.remote.apiservice.Wallet1ApiService
import com.commonadmin.report.data.remote.models.Wallet1ReqModel
import com.commonadmin.report.data.remote.models.Wallet1RespModel
import com.commonadmin.report.utils.ApiResult
import com.commonadmin.report.utils.handleResponse
import com.commonadmin.report.domain.reposiroty.Wallet1ReportRepo
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class Wallet1RepoImpl @Inject constructor(private val wallet1ApiService: Wallet1ApiService) :
    Wallet1ReportRepo {
    override suspend fun wallet1ReportApiCall(url:String,wallet1ReqModel: Wallet1ReqModel,token:String): Flow<ApiResult<Wallet1RespModel>> {
        return handleResponse {
            wallet1ApiService.requestForWallet1Report(url,wallet1ReqModel,token)
        }
    }
}