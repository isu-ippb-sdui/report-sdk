package com.commonadmin.report.data.remote.apiservice

import com.commonadmin.report.data.remote.models.Wallet1ReqModel
import com.commonadmin.report.data.remote.models.Wallet1RespModel
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.Url

interface Wallet1ApiService {
    @Headers("Accept:application/json", "Content-Type:application/json")
    @POST
    suspend fun requestForWallet1Report(@Url url:String,@Body param: Wallet1ReqModel,@Header("Authorization")token:String): Response<Wallet1RespModel>

}