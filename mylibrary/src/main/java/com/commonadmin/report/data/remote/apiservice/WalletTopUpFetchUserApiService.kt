package com.commonadmin.report.data.remote.apiservice

import com.commonadmin.report.data.remote.models.WalletTopUpFetchUserReqModel
import com.commonadmin.report.data.remote.models.WalletTopUpFetchUserRespModel
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.Url

interface WalletTopUpFetchUserApiService {
    @Headers("Accept:application/json", "Content-Type:application/json")
    @POST
    suspend fun requestForWalletTopUpFetch(@Url url:String,@Body param: WalletTopUpFetchUserReqModel): Response<WalletTopUpFetchUserRespModel>

}