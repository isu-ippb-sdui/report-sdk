package com.commonadmin.report.data.remote.models

import com.google.gson.annotations.SerializedName

data class SduiFetchRespModels(

    @field:SerializedName("data")
    val data: SDUIData? = null,

    @field:SerializedName("status")
    val status: Int? = null
)

data class SDUIAPIs(

    @field:SerializedName("Commission2")
    val commission2: String? = null,

    @field:SerializedName("Commission1")
    val commission1: String? = null,

    @field:SerializedName("UnifiedAeps")
    val unifiedAeps: String? = null,

    @field:SerializedName("matm2")
    val matm2: String? = null,

    @field:SerializedName("wallet1")
    val wallet1: String? = null,

    @field:SerializedName("recharge")
    val recharge: String? = null,

    @field:SerializedName("wallettopup")
    val wallettopup: String? = null,

    @field:SerializedName("wallet2")
    val wallet2: String? = null,

    @field:SerializedName("AadhaarPay")
    val aadhaarPay: String? = null,

    @field:SerializedName("walletcashout")
    val walletcashout: String? = null,

    @field:SerializedName("dmt")
    val dmt: String? = null,
    @field:SerializedName("emailReport")
    val emailReport: String? = null,

    @field:SerializedName("walletInterchange")
    val walletInterchange: String? = null,

    @field:SerializedName("upi")
    val upi: String? = null,
    @field:SerializedName("bbpsreport")
    val bbpsreport: String? = null ,
    @field:SerializedName("updateWalletTopupTxn")
    val updateWalletTopupTxn: String? = null ,
    @field:SerializedName("update_wallettopup_request")
    val update_wallettopup_request: String? = null ,
    @field:SerializedName("retailerBankFetch")
    val retailerBankFetch: String? = null,
    @field:SerializedName("liveLong")
    val liveLong: String? = null
)

data class SDUIData(

    @field:SerializedName("ui_json")
    val uiJson: SDUiJson? = null,

    @field:SerializedName("version")
    val version: String? = null
)

data class SDUiJson(

    @field:SerializedName("APIs")
    val aPIs: SDUIAPIs? = null,

    @field:SerializedName("primaryColor")
    val primaryColor: String? = null
)

data class SduiFetchReqModels(
    @field:SerializedName("user_name")
    val userName: String? = null,
    @field:SerializedName("bank_code")
    val bankCode: String? = null
)
