package com.commonadmin.report.data.remote.apiservice

import com.commonadmin.report.data.remote.models.UnifiedAepsReqModel
import com.commonadmin.report.data.remote.models.UnifiedAepsRespModel
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.Url

interface UnifiedApiService {
    @Headers("Accept:application/json", "Content-Type:application/json")
    @POST
    suspend fun requestForAepsReport(@Url url: String, @Body param: UnifiedAepsReqModel, @Header("Authorization")token:String): Response<UnifiedAepsRespModel>

}