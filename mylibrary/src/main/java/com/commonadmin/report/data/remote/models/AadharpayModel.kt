package com.commonadmin.report.data.remote.models

import com.google.gson.annotations.SerializedName

data class AadharpayReqModel(
	@field:SerializedName("$1")
	val jsonMember1: String? = null,

	@field:SerializedName("$2")
	val jsonMember2: String? = null,

	@field:SerializedName("$3")
	val jsonMember3: ArrayList<String>? = null,
	@field:SerializedName("$4")
	val jsonMember4: String? = null,

	@field:SerializedName("$5")
	val jsonMember5: String? = null,

	@field:SerializedName("$6")
	val jsonMember6: List<String?>? = null,

	@field:SerializedName("$7")
	val jsonMember7: List<String?>? = null
)
data class AadharpayRespModel(

	@field:SerializedName("length")
	val length: Int? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("results")
	val results: AadharPayResults? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class AadharPayResults(

	@field:SerializedName("BQReport")
	val bQReport: List<AadharpayReportItem>? = null
)

data class AadharpayReportItem(

	@field:SerializedName("referenceNo")
	val referenceNo: String? = null,

	@field:SerializedName("param_b")
	val paramB: Any? = null,

	@field:SerializedName("transactionMode")
	val transactionMode: String? = null,

	@field:SerializedName("param_c")
	val paramC: Any? = null,

	@field:SerializedName("operationPerformed")
	val operationPerformed: String? = null,

	@field:SerializedName("amountTransacted")
	val amountTransacted: Int? = null,

	@field:SerializedName("bankName")
	val bankName: String? = null,

	@field:SerializedName("updatedDate")
	val updatedDate: Long? = null,

	@field:SerializedName("balanceAmount")
	val balanceAmount: Int? = null,

	@field:SerializedName("userName")
	val userName: String? = null,

	@field:SerializedName("previousAmount")
	val previousAmount: Int? = null,

	@field:SerializedName("createdDate")
	val createdDate: Long? = null,

	@field:SerializedName("apiTid")
	val apiTid: String? = null,

	@field:SerializedName("Id")
	val id: String? = null,

	@field:SerializedName("apiComment")
	val apiComment: String? = null,

	@field:SerializedName("param_a")
	val paramA: Any? = null,

	@field:SerializedName("gateway")
	val gateway: String? = null,

	@field:SerializedName("status")
	val status: String? = null,

	@field:SerializedName("masterName")
	val masterName: Any? = null
)