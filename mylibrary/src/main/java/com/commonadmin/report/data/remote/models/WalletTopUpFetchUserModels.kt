package com.commonadmin.report.data.remote.models

import com.google.gson.annotations.SerializedName

data class WalletTopUpFetchUserReqModel(

	@field:SerializedName("userName")
	val userName: String? = null
)
data class WalletTopUpFetchUserRespModel(

	@field:SerializedName("errorMessage")
	val errorMessage: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)
