package com.commonadmin.report.data.remote.models

import com.google.gson.annotations.SerializedName

data class WalletInterchangeReqModel(
    @field:SerializedName("$1")
    val jsonMember1: String? = null,

    @field:SerializedName("$2")
    val jsonMember2: String? = null,
    @field:SerializedName("$4")
    val jsonMember4: String? = null,

    @field:SerializedName("$5")
    val jsonMember5: String? = null,

    @field:SerializedName("$7")
    val jsonMember7: ArrayList<String>? = null,

    )

data class WalletInterchangeRespModel(

    @field:SerializedName("Report")
    val report: ArrayList<WalletInterchangeReportItem>? = null,

    @field:SerializedName("length")
    val length: Int? = null,

    @field:SerializedName("message")
    val message: String? = null,

    @field:SerializedName("status")
    val status: Int? = null
)

data class WalletInterchangeReportItem(

    @field:SerializedName("w2Id")
    val w2Id: String? = null,

    @field:SerializedName("w1TransactionType")
    val w1TransactionType: String? = null,

    @field:SerializedName("w1Id")
    val w1Id: String? = null,

    @field:SerializedName("w1Status")
    val w1Status: String? = null,

    @field:SerializedName("adminname")
    val adminname: String? = null,

    @field:SerializedName("w2UpdatedDate")
    val w2UpdatedDate: Long? = null,

    @field:SerializedName("w1CreatedDate")
    val w1CreatedDate: Long? = null,

    @field:SerializedName("distributerName")
    val distributerName: String? = null,

    @field:SerializedName("w2CreatedDate")
    val w2CreatedDate: Long? = null,

    @field:SerializedName("w1BalanceAmount")
    val w1BalanceAmount: Any? = null,

    @field:SerializedName("w2TransactionType")
    val w2TransactionType: String? = null,

    @field:SerializedName("w2AmountTransacted")
    val w2AmountTransacted: Int? = null,

    @field:SerializedName("retaileruserName")
    val retaileruserName: String? = null,

    @field:SerializedName("w2PreviousAmount")
    val w2PreviousAmount: Any? = null,

    @field:SerializedName("superAdminName")
    val superAdminName: String? = null,

    @field:SerializedName("w1AmountTransacted")
    val w1AmountTransacted: Int? = null,

    @field:SerializedName("w1UpdatedDate")
    val w1UpdatedDate: Long? = null,

    @field:SerializedName("w2Status")
    val w2Status: String? = null,

    @field:SerializedName("w2BalanceAmount")
    val w2BalanceAmount: Any? = null,

    @field:SerializedName("w1PreviousAmount")
    val w1PreviousAmount: Any? = null,

    @field:SerializedName("masterName")
    val masterName: String? = null
)
