package com.commonadmin.report.data.remote.models

import com.google.gson.annotations.SerializedName

data class Matm2ReqModel(
    @field:SerializedName("$1")
    val jsonMember1: String? = null,

    @field:SerializedName("$2")
    val jsonMember2: String? = null,

    @field:SerializedName("$3")
    val jsonMember3: List<String?>? = null,

    @field:SerializedName("$4")
    val jsonMember4: String? = null,

    @field:SerializedName("$5")
    val jsonMember5: String? = null,

    @field:SerializedName("$6")
    val jsonMember6: ArrayList<String>? = null,
    @field:SerializedName("$7")
    val jsonMember7: ArrayList<String>? = null,
)

data class Matm2RespModel(

    @field:SerializedName("results")
    val results: Matm2Results? = null,

    @field:SerializedName("length")
    val length: Int? = null,

    @field:SerializedName("message")
    val message: String? = null,

    @field:SerializedName("status")
    val status: Int? = null
)

data class Matm2Results(
    @field:SerializedName("BQReport")
    val report: List<Matm2ReportItem>? = null,

    )

data class Matm2ReportItem(

    @field:SerializedName("transactionMode")
    val transactionMode: String? = null,

    @field:SerializedName("operationPerformed")
    val operationPerformed: String? = null,

    @field:SerializedName("card_type")
    val cardType: String? = null,

 @field:SerializedName("deviceVersion")
    val deviceVersion: String? = null,
 @field:SerializedName("deviceSerialNo")
    val deviceSerialNo: String? = null,
 @field:SerializedName("deviceType")
    val deviceType: String? = null,

    @field:SerializedName("amountTransacted")
    val amountTransacted: Int? = null,

    @field:SerializedName("updatedDate")
    val updatedDate: Long? = null,

    @field:SerializedName("mobileNo")
    val mobileNo: String? = null,

    @field:SerializedName("balanceAmount")
    val balanceAmount: Double? = null,

    @field:SerializedName("previousAmount")
    val previousAmount: Double? = null,

    @field:SerializedName("createdDate")
    val createdDate: Long? = null,

    @field:SerializedName("apiTid")
    val apiTid: String? = null,

    @field:SerializedName("card_holdername")
    val cardHoldername: String? = null,

    @field:SerializedName("cardDetails")
    val cardDetails: String? = null,

    @field:SerializedName("Id")
    val id: String? = null,

    @field:SerializedName("apiComment")
    val apiComment: String? = null,

    @field:SerializedName("status")
    val status: String? = null
)