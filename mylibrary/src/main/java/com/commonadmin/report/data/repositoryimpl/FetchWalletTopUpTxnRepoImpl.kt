package com.commonadmin.report.data.repositoryimpl

import com.commonadmin.report.data.remote.apiservice.FetchWalletTopupTxnApiService
import com.commonadmin.report.data.remote.models.FetchWalletTopUpTxnReqModel
import com.commonadmin.report.data.remote.models.FetchWalletTopUpTxnRespModel
import com.commonadmin.report.utils.ApiResult
import com.commonadmin.report.utils.handleResponse
import com.commonadmin.report.domain.reposiroty.FetchWalletTopUpTxnReportRepo
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class FetchWalletTopUpTxnRepoImpl @Inject constructor(private val fetchWalletTopupTxnApiService: FetchWalletTopupTxnApiService) :
    FetchWalletTopUpTxnReportRepo {
    override suspend fun fetchWalletTopUpTxnReportApiCall(
        url: String,
        fetchWalletTopUpTxnReqModel: FetchWalletTopUpTxnReqModel
    ): Flow<ApiResult<FetchWalletTopUpTxnRespModel>> {
        return handleResponse {
            fetchWalletTopupTxnApiService.requestForFetchWalletTopUPTxnReport(url, fetchWalletTopUpTxnReqModel)
        }
    }
}