package com.commonadmin.report.data.remote.apiservice

import com.commonadmin.report.data.remote.models.UpiReqModel
import com.commonadmin.report.data.remote.models.UpiRespModel
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.Url

interface UpiApiService {
    @Headers("Accept:application/json", "Content-Type:application/json")
    @POST
    suspend fun requestForUpiReport(@Url url:String, @Body param: UpiReqModel, @Header("Authorization")token:String): Response<UpiRespModel>

}