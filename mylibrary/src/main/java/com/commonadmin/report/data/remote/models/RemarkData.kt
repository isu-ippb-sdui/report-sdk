package com.commonadmin.report.data.remote.models

data class RemarkData(
    val remarks: String,
    val created_time: String,
    val author: String,
    val type: String
)