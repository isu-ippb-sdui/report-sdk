package com.commonadmin.report.data.repositoryimpl

import com.commonadmin.report.data.remote.apiservice.BBPSApiService
import com.commonadmin.report.data.remote.models.BBPSReportReqModel
import com.commonadmin.report.data.remote.models.BBPSReportRespModel
import com.commonadmin.report.utils.ApiResult
import com.commonadmin.report.utils.handleResponse
import com.commonadmin.report.domain.reposiroty.BBPSReportRepo
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class BBPSRepoImpl @Inject constructor(private val bbpsApiService: BBPSApiService) :
    BBPSReportRepo {
    override suspend fun bbpsReportApiCall(
        url: String,
        bbpsReportReqModel: BBPSReportReqModel,token:String
    ): Flow<ApiResult<BBPSReportRespModel>> {
        return handleResponse {
            bbpsApiService.requestForBBPSReport(url, bbpsReportReqModel,token)
        }
    }
}