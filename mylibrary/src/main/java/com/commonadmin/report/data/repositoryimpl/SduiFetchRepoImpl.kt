package com.commonadmin.report.data.repositoryimpl

import com.commonadmin.report.data.remote.apiservice.FetchSduiApiService
import com.commonadmin.report.data.remote.models.SduiFetchReqModels
import com.commonadmin.report.data.remote.models.SduiFetchRespModels
import com.commonadmin.report.utils.ApiResult
import com.commonadmin.report.utils.handleResponse
import com.commonadmin.report.domain.reposiroty.SduiFetchRepo
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class SduiFetchRepoImpl @Inject constructor(private val sduiApiService: FetchSduiApiService) :
    SduiFetchRepo {
    override suspend fun sduiFetchApi(sduiFetchReqModels: SduiFetchReqModels): Flow<ApiResult<SduiFetchRespModels>> {
        return handleResponse {
            sduiApiService.requestForFetchSDUI(sduiFetchReqModels)
        }
    }


}