package com.commonadmin.report.data.remote.apiservice

import com.commonadmin.report.data.remote.models.WalletInterchangeReqModel
import com.commonadmin.report.data.remote.models.WalletInterchangeRespModel
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.Url

interface WalletInterchangeApiService {
    @Headers("Accept:application/json", "Content-Type:application/json")
    @POST
    suspend fun requestForWalletInterchangeReport(@Url url:String,@Body param: WalletInterchangeReqModel,@Header("Authorization")token:String): Response<WalletInterchangeRespModel>

}