package com.commonadmin.report.data.remote.models

import com.google.gson.annotations.SerializedName


data class WalletCashoutReqModel(

    @field:SerializedName("$1")
    val jsonMember1: String? = null,
  @field:SerializedName("$2")
    val jsonMember2: String? = null,

    @field:SerializedName("$4")
    val jsonMember4: String? = null,

    @field:SerializedName("$5")
    val jsonMember5: String? = null,

    @field:SerializedName("$7")
    val jsonMember7: ArrayList<String>? = null,
    @field:SerializedName("subcatVal")
    val subcatVal: String? = null



)

data class WalletCashoutRespModel(

    @field:SerializedName("results")
    val report: List<WalletCashOutReportItem>? = null,

    @field:SerializedName("length")
    val length: Int? = null,

    @field:SerializedName("message")
    val message: String? = null,

    @field:SerializedName("status")
    val status: Int? = null
)

data class WalletCashOutReportItem(

    @field:SerializedName("toAccount")
    val toAccount: String? = null,

    @field:SerializedName("transactionMode")
    val transactionMode: String? = null,

    @field:SerializedName("operationPerformed")
    val operationPerformed: String? = null,

    @field:SerializedName("amountTransacted")
    val amountTransacted: Int? = null,

    @field:SerializedName("updatedDate")
    val updatedDate: Long? = null,

    @field:SerializedName("mobileNo")
    val mobileNo: Any? = null,

    @field:SerializedName("currentBalance")
    val balanceAmount: String? = null,

    @field:SerializedName("previousBalance")
    val previousAmount: String? = null,

    @field:SerializedName("createdDate")
    val createdDate: Long? = null,

    @field:SerializedName("apiTid")
    val apiTid: Any? = null,

    @field:SerializedName("Id")
    val id: String? = null,

    @field:SerializedName("apiComment")
    val apiComment: String? = null,

    @field:SerializedName("status")
    val status: String? = null
)
