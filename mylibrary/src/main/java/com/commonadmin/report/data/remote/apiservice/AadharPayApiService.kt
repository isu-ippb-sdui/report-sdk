package com.commonadmin.report.data.remote.apiservice


import com.commonadmin.report.data.remote.models.AadharpayReqModel
import com.commonadmin.report.data.remote.models.AadharpayRespModel
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.Url

interface AadharPayApiService {
    @Headers("Accept:application/json", "Content-Type:application/json")
    @POST
    suspend fun requestForAadharpayReport(@Url url:String, @Body param: AadharpayReqModel, @Header("Authorization")token:String): Response<AadharpayRespModel>

}