package com.commonadmin.report.data.remote.apiservice

import com.commonadmin.report.data.remote.models.LivLongReportReqModel
import com.commonadmin.report.data.remote.models.LivLongRespModel
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.Url

interface LivLongApiService {
    @Headers("Accept:application/json", "Content-Type:application/json")
    @POST
    suspend fun requestForLivLongReport(@Url url:String, @Body param: LivLongReportReqModel, @Header("Authorization")token:String): Response<LivLongRespModel>

}