package com.commonadmin.report.data.remote.models

import com.google.gson.annotations.SerializedName

data class RetailerBankReqModel(

	@field:SerializedName("userName")
	val userName: String? = null
)

data class RetailerBankRespModel(

	@field:SerializedName("errorMessage")
	val errorMessage: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)
