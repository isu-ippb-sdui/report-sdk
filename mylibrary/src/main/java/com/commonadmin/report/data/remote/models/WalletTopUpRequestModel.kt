package com.commonadmin.report.data.remote.models

import com.google.gson.annotations.SerializedName

data class WalletTopUpRequestReqModel(

	@field:SerializedName("transaction_id")
	val transactionId: String? = null,

	@field:SerializedName("amount")
	val amount: String? = null,

	@field:SerializedName("senderBankName")
	val senderBankName: String? = null,

	@field:SerializedName("senderAccountNo")
	val senderAccountNo: String? = null,

	@field:SerializedName("receiverBankName")
	val receiverBankName: String? = null,

	@field:SerializedName("bankRefId")
	val bankRefId: String? = null,

	@field:SerializedName("userName")
	val userName: String? = null,

	@field:SerializedName("transactionType")
	val transactionType: String? = null,

	@field:SerializedName("senderName")
	val senderName: String? = null,

	@field:SerializedName("depositDate")
	val depositDate: String? = null,

	@field:SerializedName("receiptDownloadUrl")
	val receiptDownloadUrl: String? = null,

	@field:SerializedName("transferType")
	val transferType: String? = null,

	@field:SerializedName("api_user")
	val apiUser: String? = null,

	@field:SerializedName("remarks")
	val remarks: String? = null
)
data class WalletTopUpRequestRespModel(

	@field:SerializedName("status")
	val status: Int? = null,
	@field:SerializedName("message")
	val message: String? = null
)
