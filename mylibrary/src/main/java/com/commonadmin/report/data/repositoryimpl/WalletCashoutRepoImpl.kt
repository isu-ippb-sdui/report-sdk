package com.commonadmin.report.data.repositoryimpl

import com.commonadmin.report.data.remote.apiservice.WalletCashOutApiService
import com.commonadmin.report.data.remote.models.WalletCashoutReqModel
import com.commonadmin.report.data.remote.models.WalletCashoutRespModel
import com.commonadmin.report.utils.ApiResult
import com.commonadmin.report.utils.handleResponse
import com.commonadmin.report.domain.reposiroty.WalletCashoutReportRepo
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class WalletCashoutRepoImpl @Inject constructor(private val walletCashOutApiService: WalletCashOutApiService) :
    WalletCashoutReportRepo {
    override suspend fun walletCashoutReportApiCall(
        url: String,
        walletCashoutReqModel: WalletCashoutReqModel,token:String
    ): Flow<ApiResult<WalletCashoutRespModel>> {
        return handleResponse {
            walletCashOutApiService.reqForWalletCashoutReport(url, walletCashoutReqModel,token)
        }
    }
}