package com.commonadmin.report.data.remote.apiservice

import com.commonadmin.report.data.remote.models.DmtGatewayReqModels
import com.commonadmin.report.data.remote.models.DmtGatewayRespModel
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.Url

interface DmtGateWayApiService {
    @Headers("Accept:application/json", "Content-Type:application/json")
    @POST
    suspend fun requestForDmtGateWayReport(@Url url:String,@Body param: DmtGatewayReqModels,@Header("Authorization")token:String): Response<DmtGatewayRespModel>

}