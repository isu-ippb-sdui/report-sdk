package com.commonadmin.report.data.remote.models

import com.google.gson.annotations.SerializedName

data class FetchWalletTopUpTxnReqModel(

	@field:SerializedName("transaction_id")
	val transactionId: String? = null
)


data class FetchWalletTopUpTxnRespModel(

	@field:SerializedName("data")
	val data: FetchWalletTopUpTxnData? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class FetchWalletTopUpTxnData(

	@field:SerializedName("transaction_id")
	val transactionId: String? = null,

	@field:SerializedName("transaction_detils")
	val transactionDetils: TransactionDetils? = null
)

data class TransactionDetils(

	@field:SerializedName("amount")
	val amount: Int? = null,

	@field:SerializedName("transaction_status_code")
	val transactionStatusCode: Int? = null,

	@field:SerializedName("user_name")
	val userName: String? = null,

	@field:SerializedName("receiver_bank_name")
	val receiverBankName: String? = null,

	@field:SerializedName("sender_name")
	val senderName: String? = null,

	@field:SerializedName("transaction_type")
	val transactionType: String? = null,

	@field:SerializedName("receipt_link")
	val receiptLink: String? = null,

	@field:SerializedName("txn_type")
	val txnType: String? = null,

	@field:SerializedName("sender_account")
	val senderAccount: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("sender_bank_name")
	val senderBankName: String? = null,

	@field:SerializedName("bnk_ref_id")
	val bnkRefId: String? = null,

	@field:SerializedName("remarks")
	val remarks: List<RemarksItem>? = null,

	@field:SerializedName("deposite_date_time")
	val depositeDateTime: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class RemarksItem(

	@field:SerializedName("created_time")
	val createdTime: String? = null,

	@field:SerializedName("author")
	val author: String? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("remarks")
	val remarks: String? = null
)

