package com.commonadmin.report.data.remote.models

import com.google.gson.annotations.SerializedName

data class WalletTopUpReqModel(
    @field:SerializedName("$1")
    val jsonMember1: String? = null,

    @field:SerializedName("$2")
    val jsonMember2: String? = null,

    @field:SerializedName("$4")
    val jsonMember4: String? = null,

    @field:SerializedName("$5")
    val jsonMember5: String? = null,

    @field:SerializedName("$6")
    val jsonMember6: ArrayList<String>? = null,
   @field:SerializedName("$7")
    val jsonMember7: ArrayList<String>? = null,
   @field:SerializedName("$13")
    val jsonMember13: ArrayList<String>? = null,
   @field:SerializedName("$14")
    val jsonMember14: ArrayList<String>? = null,
   @field:SerializedName("$15")
    val jsonMember15: ArrayList<String>? = null,


)

data class WalletTopUpRespModel(

    @field:SerializedName("Report")
    val report: ArrayList<WalletTopUpReportItem>? = null,

    @field:SerializedName("length")
    val length: Int? = null,

    @field:SerializedName("message")
    val message: String? = null,

    @field:SerializedName("status")
    val status: Int? = null
)

data class WalletTopUpReportItem(

    @field:SerializedName("amount")
    val amount: Int? = null,

    @field:SerializedName("depositedBankName")
    val depositedBankName: String? = null,

    @field:SerializedName("statusDesc")
    val statusDesc: String? = null,

    @field:SerializedName("requestDateTime")
    val requestDateTime: Long? = null,

    @field:SerializedName("operationPerformed")
    val operationPerformed: String? = null,

    @field:SerializedName("bankRefId")
    val bankRefId: String? = null,

    @field:SerializedName("approvalTime")
    val approvalTime: Any? = null,

    @field:SerializedName("balanceAmount")
    val balanceAmount: String? = null,

    @field:SerializedName("previousAmount")
    val previousAmount: String? = null,

    @field:SerializedName("transactionId")
    val transactionId: String? = null,

    @field:SerializedName("originIdentifier")
    val originIdentifier: String? = null,

    @field:SerializedName("transactionType")
    val transactionType: String? = null,

    @field:SerializedName("paymentRequestType")
    val paymentRequestType: String? = null,

    @field:SerializedName("requestTo")
    val requestTo: String? = null,

    @field:SerializedName("remarks1")
    val remarks1: String? = null,

    @field:SerializedName("transferType")
    val transferType: String? = null,

    @field:SerializedName("depositDateTime")
    val depositDateTime: Long? = null,

    @field:SerializedName("Id")
    val id: String? = null,

    @field:SerializedName("creditDebit")
    val creditDebit: String? = null,

    @field:SerializedName("remarks")
    val remarks: String? = null,

    @field:SerializedName("requestFrom")
    val requestFrom: String? = null,

    @field:SerializedName("status")
    val status: String? = null
)