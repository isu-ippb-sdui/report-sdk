package com.commonadmin.report.data.repositoryimpl

import com.commonadmin.report.data.remote.apiservice.Matm2ApiService
import com.commonadmin.report.data.remote.models.Matm2ReqModel
import com.commonadmin.report.data.remote.models.Matm2RespModel
import com.commonadmin.report.utils.ApiResult
import com.commonadmin.report.utils.handleResponse
import com.commonadmin.report.domain.reposiroty.Matm2ReportRepo
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class Matm2RepoImpl @Inject constructor(private val matm2ApiService: Matm2ApiService) :
    Matm2ReportRepo {
    override suspend fun mAtm2ReportApiCall(url:String,matm2ReqModel: Matm2ReqModel,token:String): Flow<ApiResult<Matm2RespModel>> {
        return handleResponse {
            matm2ApiService.requestForMatm2Report(url,matm2ReqModel,token)
        }
    }
}