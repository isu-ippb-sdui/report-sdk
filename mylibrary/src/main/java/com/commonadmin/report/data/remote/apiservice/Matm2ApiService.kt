package com.commonadmin.report.data.remote.apiservice

import com.commonadmin.report.data.remote.models.Matm2ReqModel
import com.commonadmin.report.data.remote.models.Matm2RespModel
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.Headers
import retrofit2.http.POST
import retrofit2.http.Url

interface Matm2ApiService {
    @Headers("Accept:application/json", "Content-Type:application/json")
    @POST
    suspend fun requestForMatm2Report(@Url url: String,@Body param: Matm2ReqModel,@Header("Authorization")token:String): Response<Matm2RespModel>

}