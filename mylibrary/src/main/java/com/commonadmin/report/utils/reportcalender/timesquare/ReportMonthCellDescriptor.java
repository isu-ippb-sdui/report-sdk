// Copyright 2012 Square, Inc.

package com.commonadmin.report.utils.reportcalender.timesquare;


import java.util.Date;

class ReportMonthCellDescriptor {

  private final Date date;
  private final int value;
  private final boolean isCurrentMonth;
  private boolean isSelected;
  private final boolean isToday;
  private final boolean isSelectable;
  private boolean isHighlighted;
  private ReportRangeState reportRangeState;

  ReportMonthCellDescriptor(Date date, boolean currentMonth, boolean selectable, boolean selected,
                            boolean today, boolean highlighted, int value, ReportRangeState reportRangeState) {
    this.date = date;
    isCurrentMonth = currentMonth;
    isSelectable = selectable;
    isHighlighted = highlighted;
    isSelected = selected;
    isToday = today;
    this.value = value;
    this.reportRangeState = reportRangeState;
  }

  public Date getDate() {
    return date;
  }

  public boolean isCurrentMonth() {
    return isCurrentMonth;
  }

  public boolean isSelectable() {
    return isSelectable;
  }

  public boolean isSelected() {
    return isSelected;
  }

  public void setSelected(boolean selected) {
    isSelected = selected;
  }

  boolean isHighlighted() {
    return isHighlighted;
  }

  void setHighlighted(boolean highlighted) {
    isHighlighted = highlighted;
  }

  public boolean isToday() {
    return isToday;
  }

  public ReportRangeState getRangeState() {
    return reportRangeState;
  }

  public void setRangeState(ReportRangeState reportRangeState) {
    this.reportRangeState = reportRangeState;
  }

  public int getValue() {
    return value;
  }

  @Override
  public String toString() {
    return "ReportMonthCellDescriptor{"
        + "date="
        + date
        + ", value="
        + value
        + ", isCurrentMonth="
        + isCurrentMonth
        + ", isSelected="
        + isSelected
        + ", isToday="
        + isToday
        + ", isSelectable="
        + isSelectable
        + ", isHighlighted="
        + isHighlighted
        + ", reportRangeState="
        + reportRangeState
        + '}';
  }
}
