package com.commonadmin.report.utils.reportcalender;

import static android.widget.Toast.LENGTH_SHORT;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.commonadmin.report.R;
import com.commonadmin.report.utils.DataConstants;
import com.commonadmin.report.utils.reportcalender.timesquare.ReportCalendarPickerView;
import com.commonadmin.report.utils.reportcalender.timesquare.ReportDefaultReportDayViewAdapter;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class ReportCalenderActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    ReportCalendarPickerView datePicker;
    ArrayList<Date> checkDateList = new ArrayList();
    Date dateThreeMonthBack, dateActive, dateActiveForSelect;
//    DrawerLayout drawer;

    TextView tvFromDate, tvToDate, tvSave;
    ImageView ivClose, ivReset;


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_calender);
//        drawer = findViewById(R.id.drawer_layout);



        init();

        initCalender();

        datePicker.setOnDateSelectedListener(new ReportCalendarPickerView.OnDateSelectedListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onDateSelected(Date date) {
                //String selectedDate = DateFormat.getDateInstance(DateFormat.FULL).format(date);
                Calendar calSelected = Calendar.getInstance();
                calSelected.setTime(date);

                Calendar calMaxSelectable = Calendar.getInstance();
                calMaxSelectable.setTime(date);
                calMaxSelectable.add(Calendar.DAY_OF_MONTH, 10);
                Date maxDateSelectable = calMaxSelectable.getTime();

                if (datePicker.getSelectedDates().size() == 1) {
                    checkDateList.clear();
                    checkDateList.add(date);


                    try {
                        String firstDate = "" + calSelected.get(Calendar.DAY_OF_MONTH)
                                + "-" + (calSelected.get(Calendar.MONTH) + 1)
                                + "-" + calSelected.get(Calendar.YEAR);

                        Date date2 = new SimpleDateFormat("dd-MM-yyyy").parse(firstDate);
                        ReportCalenderConstant.selectFromDate = new SimpleDateFormat("yyyy-MM-dd").format(date2);
                        ReportCalenderConstant.selectToDate = new SimpleDateFormat("yyyy-MM-dd").format(date2);

                        ReportCalenderConstant.selectToolbarFromDate = new SimpleDateFormat("dd-MM-yyyy").format(date2);
                        ReportCalenderConstant.selectToolbarToDate = new SimpleDateFormat("dd-MM-yyyy").format(date2);

                        tvFromDate.setText(new SimpleDateFormat("MMM dd").format(date2));
                        tvToDate.setText(new SimpleDateFormat("MMM dd").format(date2));

                        //Toast.makeText(ReportCalenderActivity.this, "Select to date", LENGTH_SHORT).show();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    //Check if maxSelectableDate is after dateActive
                    if (maxDateSelectable.after(dateActive)) {
                        //if yes: set maxdate as today
                        datePicker.init(date, dateActive) //
                                .inMode(ReportCalendarPickerView.SelectionMode.RANGE) //
                                .withSelectedDates(checkDateList);
                    } else {
                        //if no: set maxdate as 10days from selected from date
                        datePicker.init(date, maxDateSelectable) //
                                .inMode(ReportCalendarPickerView.SelectionMode.RANGE) //
                                .withSelectedDates(checkDateList);
                    }


                } else {
                    if (datePicker.getSelectedDates().size() > 10) {
                        checkDateList.add(datePicker.getSelectedDates().get(9));

                        datePicker.setDecorators(Collections.emptyList());
                        datePicker.init(dateThreeMonthBack, dateActive) //
                                .inMode(ReportCalendarPickerView.SelectionMode.RANGE) //
                                .withSelectedDates(checkDateList);

                        Toast.makeText(ReportCalenderActivity.this, "More than 10 days can't be selected.", Toast.LENGTH_LONG).show();

                    } else {
                        //ReportMonthView.setDividerColor(R.color.colorPrimary);
                        try {
                            String selectedDate = "" + calSelected.get(Calendar.DAY_OF_MONTH)
                                    + "-" + (calSelected.get(Calendar.MONTH) + 1)
                                    + "-" + calSelected.get(Calendar.YEAR);

                            Date date2 = new SimpleDateFormat("dd-MM-yyyy").parse(selectedDate);

                            tvToDate.setText(new SimpleDateFormat("MMM dd").format(date2));
                            ReportCalenderConstant.selectToDate = new SimpleDateFormat("yyyy-MM-dd").format(date2);
                            ReportCalenderConstant.selectToolbarToDate = new SimpleDateFormat("dd-MM-yyyy").format(date2);

                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onDateUnselected(Date date) {

            }
        });

        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* Intent returnIntent = new Intent(ReportCalenderActivity.this, MainActivity.class);
                setResult(Activity.RESULT_CANCELED, returnIntent);
                startActivity(returnIntent);*/
               finish();
            }
        });

        ivReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvFromDate.setText("Start date");
                tvToDate.setText("End date");

                ReportCalenderConstant.selectFromDate = "";
                ReportCalenderConstant.selectToDate = "";
                ReportCalenderConstant.selectToolbarFromDate = "";
                ReportCalenderConstant.selectToolbarToDate = "";

                initCalender();
            }
        });

        tvSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ReportCalenderConstant.selectFromDate.equals(""))
                    Toast.makeText(ReportCalenderActivity.this, "Please select Start date ", LENGTH_SHORT).show();
                else {
                    if (ReportCalenderConstant.selectToDate.equals(""))
                        ReportCalenderConstant.selectToDate = ReportCalenderConstant.selectFromDate;

                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("from_date", ReportCalenderConstant.selectFromDate);
                    returnIntent.putExtra("to_date", ReportCalenderConstant.selectToDate);
                    setResult(Activity.RESULT_OK, returnIntent);
                    DataConstants.calenderGo = true;
                    finish();
                }

            }
        });

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void initCalender() {
        //Select From dates
        Calendar calenderThreeMonthBack = Calendar.getInstance();
        calenderThreeMonthBack.add(Calendar.DAY_OF_MONTH, -90);
        dateThreeMonthBack = calenderThreeMonthBack.getTime();

        //Select To dates
        Calendar calenderActive = Calendar.getInstance();
        Calendar calenderActiveForSelect = Calendar.getInstance();

        Boolean isLastDateOfMonth = checkLastDateOfMonth(calenderActive);

        calenderActive.add(Calendar.DAY_OF_MONTH, 1);
        calenderActiveForSelect.add(Calendar.DAY_OF_MONTH, 0);
        dateActive = calenderActive.getTime();
        dateActiveForSelect = calenderActiveForSelect.getTime();
        initButtonListeners(dateThreeMonthBack, dateActive, dateActiveForSelect, isLastDateOfMonth);

    }

    private Boolean checkLastDateOfMonth(Calendar calenderActive) {
        Boolean isLastDate = false;

        int year = calenderActive.get(Calendar.YEAR);
        int month = calenderActive.get(Calendar.MONTH);
        int dateOfMonth = calenderActive.get(Calendar.DAY_OF_MONTH);

        if (month == 0 || month == 2 || month == 4 || month == 6 || month == 7 || month == 9 || month == 11) {
            if (dateOfMonth == 31) {
                isLastDate = true;
            }
        } else if (month == 1) {
            if (year % 4 == 0) {
                if (dateOfMonth == 29) {
                    isLastDate = true;
                }
            } else {
                if (dateOfMonth == 28) {
                    isLastDate = true;
                }
            }
        } else if (month == 3 || month == 5 || month == 8 || month == 10) {
            if (dateOfMonth == 30) {
                isLastDate = true;
            }
        }

        return isLastDate;
    }


    private void init() {
        ReportCalenderConstant.selectFromDate = "";
        ReportCalenderConstant.selectToDate = "";
        ReportCalenderConstant.selectToolbarFromDate = "";
        ReportCalenderConstant.selectToolbarToDate = "";

        datePicker = findViewById(R.id.calendar);
        tvFromDate = findViewById(R.id.tv_from_date);
        tvToDate = findViewById(R.id.tv_to_date);
        tvSave = findViewById(R.id.tv_save);

        ivClose = findViewById(R.id.iv_calender_close);
        ivReset = findViewById(R.id.iv_calender_reset);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void initButtonListeners(Date dateThreeMonthBack, Date dateActive, Date dateActiveForSelect, Boolean isLastDateOfMonth) {
        datePicker.setCustomDayView(new ReportDefaultReportDayViewAdapter());

        datePicker.setDecorators(Collections.emptyList());

        datePicker.init(dateThreeMonthBack, dateActive)
                .inMode(ReportCalendarPickerView.SelectionMode.RANGE);

        if (isLastDateOfMonth)
            datePicker.scrollToDate(dateActiveForSelect);
        else
            datePicker.scrollToDate(dateActive);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
       /* Intent intent = new Intent(ReportCalenderActivity.this,MainActivity.class);
        startActivity(intent);*/
        finish();
    }
}