package com.commonadmin.report.utils

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import org.json.JSONObject
import retrofit2.HttpException
import retrofit2.Response
import java.io.IOException

sealed class ApiResult<T>(val data: T? = null, val message: String? = null) {

    class Success<T>(data: T?) : ApiResult<T>(data)

    class Loading<T>(val isLoading: Boolean) : ApiResult<T>(null)
    class Error<T>(message: String, data: T? = null) : ApiResult<T>(data, message)

}
fun <T> handleResponse(call: suspend () -> Response<T>): Flow<ApiResult<T>> {
    return flow {
        emit(ApiResult.Loading(false))
        try {
            val apiResponse = call.invoke()
            if (apiResponse.isSuccessful && apiResponse.body() != null) {
                emit(ApiResult.Success(apiResponse.body()))
            } else {
                val error = JSONObject(apiResponse.errorBody()!!.string())
                if (apiResponse.code().toString().startsWith("4") ||
                    apiResponse.code().toString().startsWith("5")
                ) {
                    if (error.has("apiComment")) {
                        emit(ApiResult.Error(error.getString("apiComment")))
                    } else if (error.has("message")) {
                        emit(ApiResult.Error(error.getString("message")))
                    }  else if (error.has("data")) {
                        val dataObject = error.getJSONObject("data")
                        if (dataObject.has("statusDesc")) {
                            emit(ApiResult.Error(dataObject.getString("statusDesc")))
                        }
                    }else if (error.has("fault")) {
                        val faultObject = error.getJSONObject("fault")
                        if (faultObject.has("faultstring")) {
                            emit(ApiResult.Error(faultObject.getString("faultstring")))
                        }
                    }  else if (error.has("transactionStatus")) {
                        emit(ApiResult.Error(error.getString("transactionStatus")))
                    }else if (error.has("statusDesc")) {
                        emit(ApiResult.Error(error.getString("statusDesc")))
                    } else if (error.has("error_description")) {
                        emit(ApiResult.Error(error.getString("error_description")))
                    } else {
                        emit(ApiResult.Error("Unknown Error"))
                    }
                }
            }
        } catch (e: IOException) {
            e.message?.let { emit(ApiResult.Error(it)) }
        } catch (e: HttpException) {
            e.message?.let { emit(ApiResult.Error(it)) }
        } catch (e: IllegalStateException) {
            e.message?.let { emit(ApiResult.Error(it)) }
        } catch (e: Exception) {
            emit(ApiResult.Error(e.message ?: "Unknown Error"))
        }
    }
}