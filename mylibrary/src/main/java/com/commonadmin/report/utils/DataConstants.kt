package com.commonadmin.report.utils

import android.Manifest
import android.annotation.SuppressLint
import com.commonadmin.report.presentation.viewmodels.SduiFetchState
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Date

class DataConstants {
    companion object {


        var ActivityName = ""

        /**
         * SharedPref Constants
         */
        const val SHOW_LOG = true
        const val KEY_STORE = "Keystore"
        const val IPPB_PREFERENCE = "IPPBREPORT_Preference"
        const val USERNAME = "UserName"
        const val ADMINNAME = "AdminName"
        const val TOKEN = "Token"
        const val BANKCODE = "BankCode"
        const val DmtGatewayStatus = ""

        var ShopName = "ShopName"
        var BrandName = "BrandName"
        var MobileNumber = "MobileNumber"
        var UserEmail = "UserEmail"

        /**
         * Constants
         */
        var sduiFetchSaveData: androidx.compose.runtime.State<SduiFetchState>? = null
        var ReceiptID = 0L
        @JvmField
        var  calenderGo= false


        val permissionsList = arrayOf(
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA,
            Manifest.permission.ACCESS_FINE_LOCATION,
            "android.permission.BLUETOOTH_SCAN",
            "android.permission.BLUETOOTH_CONNECT"
        )
        val permissionsList1 = arrayOf(
            Manifest.permission.READ_MEDIA_AUDIO,
            Manifest.permission.READ_MEDIA_IMAGES,
            Manifest.permission.READ_MEDIA_VIDEO,
            Manifest.permission.CAMERA,
            Manifest.permission.ACCESS_FINE_LOCATION,
            "android.permission.BLUETOOTH_SCAN",
            "android.permission.BLUETOOTH_CONNECT"
        )

        @SuppressLint("SimpleDateFormat")
        fun getDateTime(getDateTime: Long): String? {
            val timeT: Long = getDateTime
            val formatter = SimpleDateFormat("dd-MM-yyyy  hh:mm a")
            return formatter.format(timeT)
        }

        fun getDateFromGmt(time: String): String {
            @SuppressLint("NewApi", "LocalSuppress")
            val sdf: DateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX")
            var outPutDate = ""
            var d: Date? = null
            try {
                d = sdf.parse(time)
                outPutDate = SimpleDateFormat("dd-MM-yyyy  hh:mm a").format(d)
            } catch (e: ParseException) {
                return ""
            }
            return outPutDate
            // return time
        }
    }
}