package com.commonadmin.report.utils.reportcalender.timesquare;

public interface ReportDayViewAdapter {
  void makeCellView(ReportCalendarCellView parent);
}
