package com.commonadmin.report.utils.reportcalender.timesquare;

import android.util.Log;

import com.intuit.sdp.BuildConfig;


/** Log utility class to handle the log tag and DEBUG-only logging. */
final class ReportLogr {
  public static void d(String message) {
    if (BuildConfig.DEBUG) {
      Log.d("TimesSquare", message);
    }
  }

  public static void d(String message, Object... args) {
    if (BuildConfig.DEBUG) {
      d(String.format(message, args));
    }
  }
}
