package com.commonadmin.report.utils.reportcalender.timesquare;

import static android.view.Gravity.CENTER_VERTICAL;
import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

import android.view.ContextThemeWrapper;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.commonadmin.report.R;


public class ReportDefaultReportDayViewAdapter implements ReportDayViewAdapter {
  @Override
  public void makeCellView(ReportCalendarCellView parent) {
    TextView textView = new TextView(
        new ContextThemeWrapper(parent.getContext(), R.style.CalendarCell_CalendarDate));
    textView.setDuplicateParentStateEnabled(true);

    parent.addView(textView,
        new FrameLayout.LayoutParams(MATCH_PARENT, WRAP_CONTENT, CENTER_VERTICAL));
    parent.setDayOfMonthTextView(textView);
  }
}
