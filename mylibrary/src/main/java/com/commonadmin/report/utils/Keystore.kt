package com.commonadmin.report.utils

import android.content.Context
import android.content.SharedPreferences
import com.commonadmin.report.utils.DataConstants.Companion.IPPB_PREFERENCE
import com.commonadmin.report.utils.DataConstants.Companion.KEY_STORE
import com.commonadmin.report.utils.DataConstants.Companion.SHOW_LOG
import timber.log.Timber

class Keystore private constructor(context: Context) {
    private val sharedPreferences: SharedPreferences

    init {
        sharedPreferences =
            context.applicationContext.getSharedPreferences(IPPB_PREFERENCE, 0)
    }

    fun put(key: String, value: String) {
        if (SHOW_LOG) {
            Timber.tag(KEY_STORE).v("PUT $key $value")
        }
        val editor = sharedPreferences.edit()
        editor.putString(key, value)
        editor.apply()
    }

    fun putBoolean(key: String, value: Boolean) {
        if (SHOW_LOG) {
            Timber.tag(KEY_STORE).v("PUT $key  ,value$value")
        }
        val editor = sharedPreferences.edit()
        editor.putBoolean(key, value)
        editor.apply()
    }

    operator fun get(key: String): String? {
        if (SHOW_LOG) {
            Timber.tag(KEY_STORE).v("get from $key")
        }
        return sharedPreferences.getString(key, "")
    }

    fun getBoolean(key: String): Boolean {
        if (SHOW_LOG) {
            Timber.tag(KEY_STORE).v("get from $key")
        }
        return sharedPreferences.getBoolean(key, false)
    }

    fun getInt(key: String): Int {
        if (SHOW_LOG) {
            Timber.tag(KEY_STORE).v("get int from $key")
        }
        return sharedPreferences.getInt(key, 0)
    }

    fun putInt(key: String?, num: Int) {
        val editor = sharedPreferences.edit()
        editor.putInt(key, num)
        editor.apply()
    }

    fun clear() {
        if (SHOW_LOG) {
            Timber.tag(KEY_STORE).v("delete all shared preferences")
        }
        val editor = sharedPreferences.edit()
        editor.clear()
        editor.apply()
    }

    fun remove() {
        val editor = sharedPreferences.edit()
        editor.remove(IPPB_PREFERENCE)
        editor.apply()
    }

    companion object {
        private var store: Keystore? = null
        fun getInstance(context: Context): Keystore? {
            if (store == null) {
                if (SHOW_LOG) {
                    Timber.tag(KEY_STORE).v("new key store created")
                }
                store = Keystore(context)
            }
            return store
        }
    }
}

