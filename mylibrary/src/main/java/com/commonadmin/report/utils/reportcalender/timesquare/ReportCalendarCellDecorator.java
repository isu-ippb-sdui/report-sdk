package com.commonadmin.report.utils.reportcalender.timesquare;

import java.util.Date;

public interface ReportCalendarCellDecorator {
  void decorate(ReportCalendarCellView cellView, Date date);
}
