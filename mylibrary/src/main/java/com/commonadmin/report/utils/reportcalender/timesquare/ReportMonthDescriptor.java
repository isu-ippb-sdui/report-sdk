package com.commonadmin.report.utils.reportcalender.timesquare;

import java.util.Date;

class ReportMonthDescriptor {
  private final int month;
  private final int year;
  private final Date date;
  private String label;

  ReportMonthDescriptor(int month, int year, Date date, String label) {
    this.month = month;
    this.year = year;
    this.date = date;
    this.label = label;
  }

  public int getMonth() {
    return month;
  }

  public int getYear() {
    return year;
  }

  public Date getDate() {
    return date;
  }

  public String getLabel() {
    return label;
  }

  void setLabel(String label) {
    this.label = label;
  }

  @Override
  public String toString() {
    return "ReportMonthDescriptor{"
        + "label='"
        + label
        + '\''
        + ", month="
        + month
        + ", year="
        + year
        + '}';
  }
}
