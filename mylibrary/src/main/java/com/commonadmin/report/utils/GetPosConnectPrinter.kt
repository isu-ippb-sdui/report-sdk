package com.commonadmin.report.utils

import com.isu.printer_library.vriddhi.AEMPrinter


class GetPosConnectPrinter(aemPrinter: AEMPrinter) {
    private var aemPrinter: AEMPrinter
        get() = Companion.aemPrinter!!

    companion object {
         var aemPrinter: AEMPrinter? = null
    }

    init {
        this.aemPrinter = aemPrinter
    }
}