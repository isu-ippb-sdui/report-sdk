package com.commonadmin.report.utils

import android.content.Context
import com.commonadmin.report.utils.Keystore

object SharedPrefClass {

    /**
     * Save data in sharedPref
     */
    fun saveDmtReportApi(ctx: Context?, key: String?, value: String?) {
        Keystore.getInstance(ctx!!)!!.put(key!!, value!!)
    }
  fun saveUPIReportApi(ctx: Context?, key: String?, value: String?) {
        Keystore.getInstance(ctx!!)!!.put(key!!, value!!)
    }
fun saveRechargeReportApi(ctx: Context?, key: String?, value: String?) {
        Keystore.getInstance(ctx!!)!!.put(key!!, value!!)
    }

    fun saveMatm2ReportApi(ctx: Context?, key: String?, value: String?) {
        Keystore.getInstance(ctx!!)!!.put(key!!, value!!)
    }

    fun saveWallet1ReportApi(ctx: Context?, key: String?, value: String?) {
        Keystore.getInstance(ctx!!)!!.put(key!!, value!!)
    }

    fun saveWallet2ReportApi(ctx: Context?, key: String?, value: String?) {
        Keystore.getInstance(ctx!!)!!.put(key!!, value!!)
    }


    fun saveCommission1ReportApi(ctx: Context?, key: String?, value: String?) {
        Keystore.getInstance(ctx!!)!!.put(key!!, value!!)
    }

    fun saveWallettopupReportApi(ctx: Context?, key: String?, value: String?) {
        Keystore.getInstance(ctx!!)!!.put(key!!, value!!)
    }

    fun saveUnifiedAepsReportApi(ctx: Context?, key: String?, value: String?) {
        Keystore.getInstance(ctx!!)!!.put(key!!, value!!)
    }

    fun saveWalletInterchangeReportApi(ctx: Context?, key: String?, value: String?) {
        Keystore.getInstance(ctx!!)!!.put(key!!, value!!)
    }

    fun saveWalletcashoutReportApi(ctx: Context?, key: String?, value: String?) {
        Keystore.getInstance(ctx!!)!!.put(key!!, value!!)
    }

    fun saveDmtReceiptReportApi(ctx: Context?, key: String?, value: String?) {
        Keystore.getInstance(ctx!!)!!.put(key!!, value!!)
    }

  fun saveBBPSReportApi(ctx: Context?, key: String?, value: String?) {
        Keystore.getInstance(ctx!!)!!.put(key!!, value!!)
    }
 fun saveFetchWalletTopUpTxn(ctx: Context?, key: String?, value: String?) {
        Keystore.getInstance(ctx!!)!!.put(key!!, value!!)
    }
    fun saveWalleUpRequest(ctx: Context?, key: String?, value: String?) {
        Keystore.getInstance(ctx!!)!!.put(key!!, value!!)
    }
 fun saveBankFetchUser(ctx: Context?, key: String?, value: String?) {
        Keystore.getInstance(ctx!!)!!.put(key!!, value!!)
    }
    fun saveLivLongReport(ctx: Context?, key: String?, value: String?) {
        Keystore.getInstance(ctx!!)!!.put(key!!, value!!)
    }

    fun saveAadhaarPayReportApi(ctx: Context?, key: String?, value: String?) {
        Keystore.getInstance(ctx!!)!!.put(key!!, value!!)
    }

    fun saveCommission2ReportApi(ctx: Context?, key: String?, value: String?) {
        Keystore.getInstance(ctx!!)!!.put(key!!, value!!)
    }

    fun savePrimaryColorReportApi(ctx: Context?, key: String?, value: String?) {
        Keystore.getInstance(ctx!!)!!.put(key!!, value!!)
    }

    fun saveUserName(ctx: Context?, key: String?, value: String?) {
        Keystore.getInstance(ctx!!)!!.put(key!!, value!!)
    }

    fun saveAdminName(ctx: Context?, key: String?, value: String?) {
        Keystore.getInstance(ctx!!)!!.put(key!!, value!!)
    }

    fun saveToken(ctx: Context?, key: String?, value: String?) {
        Keystore.getInstance(ctx!!)!!.put(key!!, value!!)
    }

    fun saveShopName(ctx: Context?, key: String?, value: String?) {
        Keystore.getInstance(ctx!!)!!.put(key!!, value!!)
    }

    fun saveBrandName(ctx: Context?, key: String?, value: String?) {
        Keystore.getInstance(ctx!!)!!.put(key!!, value!!)
    }
    fun saveBankCode(ctx: Context?, key: String?, value: String?) {
        Keystore.getInstance(ctx!!)!!.put(key!!, value!!)
    }

    fun saveMobile(ctx: Context?, key: String?, value: String?) {
        Keystore.getInstance(ctx!!)!!.put(key!!, value!!)
    }

    fun saveEmail(ctx: Context?, key: String?, value: String?) {
        Keystore.getInstance(ctx!!)!!.put(key!!, value!!)
    }
    fun saveAadharPayReport(ctx: Context?, key: String?, value: String?) {
        Keystore.getInstance(ctx!!)!!.put(key!!, value!!)
    }


    /**
     * Get Data in sharedPref
     */

    fun getStringValue(ctx: Context?, key: String?): String? {
        return Keystore.getInstance(ctx!!)!![key!!]
    }

    fun getBooleanValue(ctx: Context?, key: String?): Boolean {
        return Keystore.getInstance(ctx!!)!!.getBoolean(key!!)
    }

    fun getIntValue(ctx: Context?, key: String?): Int {
        return Keystore.getInstance(ctx!!)!!.getInt(key!!)
    }

    fun clearData(ctx: Context?) {
        Keystore.getInstance(ctx!!)!!.clear()
        return
    }
}
