package com.commonadmin.report.utils.reportcalender.timesquare;

public enum ReportRangeState {
    NONE, FIRST, MIDDLE, LAST
}
