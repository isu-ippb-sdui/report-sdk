package com.commonadmin.report.presentation.reportscreens.walletTopup

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.OutlinedButton
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.commonadmin.report.data.remote.models.WalletTopUpReportItem
import com.commonadmin.report.ui.theme.Green
import com.commonadmin.report.ui.theme.GreyPrimary
import com.commonadmin.report.ui.theme.Yellow
import com.commonadmin.report.ui.theme.lightGrey
import com.commonadmin.report.ui.theme.primaryColor
import com.commonadmin.report.ui.theme.whiteColor
import java.text.SimpleDateFormat
import java.util.Locale

@Composable
fun WalletTopUpReportRecycleScreen(
    walletTopUpReportItem: WalletTopUpReportItem,
    remarks:(walletTopUpReportItem: WalletTopUpReportItem)->Unit ,
    edTrns:(walletTopUpReportItem: WalletTopUpReportItem)->Unit) {
    var createdDate = ""
    if (walletTopUpReportItem.depositDateTime != null) {
        val date: Long = walletTopUpReportItem.depositDateTime.toLong()
        val format = SimpleDateFormat("dd-MM-yyyy  hh:mm a")
        createdDate = format.format(date)
    } else {
        createdDate = "N/A"
    }
    var updatedDate = ""
    if (walletTopUpReportItem.requestDateTime != null) {
        val date2: Long = walletTopUpReportItem.requestDateTime.toLong()
        val format2 = SimpleDateFormat("dd-MM-yyyy  hh:mm a")
        updatedDate = format2.format(date2)
    } else {
        updatedDate = "N/A"
    }
    val isExpand = remember {
        mutableStateOf(false)
    }

    Card(
        colors = CardDefaults.cardColors(
            containerColor = GreyPrimary,

            ),
        modifier = Modifier
            .wrapContentHeight()
            .clickable {
                isExpand.value = !isExpand.value
            }.background(color = if (isExpand.value) lightGrey else Color.White),
        elevation = CardDefaults.cardElevation(defaultElevation = 10.dp)
    ) {

        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(20.dp)
        ) {
            Text(
                text = "Transfer Type: ${walletTopUpReportItem.transactionType} (${walletTopUpReportItem.operationPerformed})",
                color = Color.Black,
                style = MaterialTheme.typography.headlineLarge,
                fontSize = 15.sp,
                modifier = Modifier.padding(start = 2.dp)
            )
            Spacer(modifier = Modifier.height(5.dp))
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Text(
                    text = "Txn Id: ${walletTopUpReportItem.id}",
                    color = Color.Black,
                    style = MaterialTheme.typography.headlineLarge,
                    fontSize = 15.sp,
                    textAlign = TextAlign.Center,
                    modifier = Modifier.padding(start = 2.dp)
                )
                Text(
                    text = walletTopUpReportItem.status.toString(),
                    color = when (walletTopUpReportItem.status?.uppercase(Locale.ROOT)) {
                        "REFUNDED" -> {
                            Yellow
                        }

                        "SUCCESS", "APPROVED" -> {
                            Green
                        }

                        "FAILED", "REJECTED", "ONHOLD" -> {
                            Color.Red
                        }
                        "INITIATED" ->{
                            Yellow
                        }

                        else -> {
                            Color.Black
                        }
                    },
                    modifier = Modifier.padding(end = 2.dp, top = 2.dp),
                    style = MaterialTheme.typography.headlineLarge,
                    fontSize = 15.sp,

                    )
            }
            Spacer(modifier = Modifier.height(5.dp))
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Text(
                    text = "Txn Type: ${walletTopUpReportItem.transactionType} (${walletTopUpReportItem.creditDebit})",
                    color = Color.Black,
                    style = MaterialTheme.typography.headlineLarge,
                    fontSize = 15.sp,
                    modifier = Modifier.padding(start = 2.dp)
                )
                Text(
                    text = "₹" + walletTopUpReportItem.amount.toString(),
                    color = Color.Black,
                    style = MaterialTheme.typography.headlineLarge,
                    fontSize = 15.sp,
                    modifier = Modifier.padding(end = 2.dp)
                )
            }
            Spacer(modifier = Modifier.height(3.dp))
            AnimatedVisibility(visible = isExpand.value) {
                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .wrapContentHeight()
                        .padding(2.dp)
                ) {

                    Text(
                        text = "Bank Name: ${
                            walletTopUpReportItem.depositedBankName.toString().ifEmpty { "N/A" }
                        }",
                        color = Color.Black,
                        style = MaterialTheme.typography.headlineLarge,
                        fontSize = 15.sp
                    )
                    Spacer(modifier = Modifier.height(3.dp))
                    Text(
                        text = "Bank Ref: ${
                            walletTopUpReportItem.bankRefId.toString().ifEmpty { "N/A" }
                        }",
                        color = Color.Black,
                        style = MaterialTheme.typography.headlineLarge,
                        fontSize = 15.sp
                    )
                    Spacer(modifier = Modifier.height(3.dp))
                    Text(
                        text = "Origin Identifier: ${
                            walletTopUpReportItem.originIdentifier.toString().ifEmpty { "N/A" }
                        }",
                        color = Color.Black,
                        style = MaterialTheme.typography.headlineLarge,
                        fontSize = 15.sp
                    )
                    Spacer(modifier = Modifier.height(3.dp))
                    Text(
                        text = "Deposited Date: $createdDate",
                        color = Color.Black,
                        style = MaterialTheme.typography.headlineLarge,
                        fontSize = 15.sp
                    )
                    Spacer(modifier = Modifier.height(3.dp))
                    Text(
                        text = "Requested Date: $updatedDate",
                        color = Color.Black,
                        style = MaterialTheme.typography.headlineLarge,
                        fontSize = 15.sp
                    )
                    Spacer(modifier = Modifier.height(3.dp))
                    Text(
                        text = "Approval Time: ${walletTopUpReportItem.approvalTime?.let { "$it" } ?: "N/A"}",
                        color = Color.Black,
                        style = MaterialTheme.typography.headlineLarge,
                        fontSize = 15.sp
                    )
                    Spacer(modifier = Modifier.height(3.dp))
                    Text(
                        text = "Status Desc: ${
                            walletTopUpReportItem.statusDesc.toString().ifEmpty { "N/A" }
                        }",
                        color = Color.Black,
                        style = MaterialTheme.typography.headlineLarge,
                        fontSize = 14.sp
                    )

                    Spacer(modifier = Modifier.height(3.dp))
                    Row(modifier = Modifier.align(alignment = Alignment.End)) {
                        if (walletTopUpReportItem.status.toString()
                                .equals("ONHOLD", ignoreCase = true)
                        ) {
                            OutlinedButton(modifier = Modifier
                                .padding(0.dp),
                                border = BorderStroke(2.dp, lightGrey),
                                colors = ButtonDefaults.outlinedButtonColors(
                                    backgroundColor = whiteColor,
                                    contentColor = lightGrey
                                ),
                                onClick = {
                                    edTrns.invoke(walletTopUpReportItem)


                                }) {
                                Text(
                                    text = "Edit",
                                    color = Color.Black,
                                    modifier = Modifier.padding(0.dp)
                                )
                            }
                            Spacer(modifier = Modifier.width(5.dp))
                        }
                        OutlinedButton(modifier = Modifier
                            .padding(0.dp),
                            border = BorderStroke(2.dp, primaryColor),
                            colors = ButtonDefaults.outlinedButtonColors(
                                backgroundColor = whiteColor,
                                contentColor = primaryColor
                            ),
                            onClick = { remarks.invoke(walletTopUpReportItem) }) {
                            Text(
                                text = "Remark",
                                color = primaryColor,
                                modifier = Modifier.padding(0.dp)
                            )
                        }
                    }


                }
            }

        }
    }
}