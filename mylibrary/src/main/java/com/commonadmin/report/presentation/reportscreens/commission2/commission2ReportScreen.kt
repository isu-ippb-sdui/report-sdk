package com.commonadmin.report.presentation.reportscreens.commission2

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Text
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import com.commonadmin.report.data.remote.models.Commission2ReportItem
import com.commonadmin.report.data.remote.models.Commission2ReportReqModel
import com.commonadmin.report.presentation.viewmodels.Commission2ReportViewModel
import com.commonadmin.report.ui.theme.primaryColor
import com.commonadmin.report.utils.DataConstants
import com.commonadmin.report.utils.SharedPrefClass
import com.commonadmin.report.utils.reportcalender.ReportCalenderConstant
import java.util.Locale

@Composable
fun Commission2ReportScreen(commission2ReportViewModel: Commission2ReportViewModel) {
    val commissionReportState = commission2ReportViewModel.commission2ReportState.collectAsState()
    val context = LocalContext.current
    LaunchedEffect(key1 = true) {

        val json7ArrayList: ArrayList<String> = ArrayList()
        json7ArrayList.add("commission")




        val commission2ReportReqModel = Commission2ReportReqModel(
            "new_commission_common",
            SharedPrefClass.getStringValue(context, DataConstants.USERNAME),
            ReportCalenderConstant.selectFromDate,
            ReportCalenderConstant.selectToDate,
            json7ArrayList
        )

        if (commissionReportState.value.commission2ReportRespModel == null) {
            commission2ReportViewModel.getCommission2ReportData(
                SharedPrefClass.getStringValue(context, "Commission2Report").toString(),
                commission2ReportReqModel,SharedPrefClass.getStringValue(context,DataConstants.TOKEN).toString()
            )
        }
    }

    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.TopCenter
    ) {
        if (commissionReportState.value.loading == true && commissionReportState.value.commission2ReportRespModel == null) {
            Box(modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ){
                CircularProgressIndicator(color = primaryColor)
            }
        } else if (commissionReportState.value.loading == false && commissionReportState.value.commission2ReportRespModel != null) {
            commissionReportState.value.commission2ReportRespModel?.let { walletReport ->

                val filteredList: ArrayList<Commission2ReportItem?> = arrayListOf()
                val reportList: List<Commission2ReportItem?>? = walletReport.results?.report

                var trnsAmount = 0.0
                for (report in reportList?.indices!!) {
                    if(reportList[report]?.status?.equals("SUCCESS",true) == true) {
                        trnsAmount += reportList[report]?.amountTransacted!!
                    }
                }
                val formatted = String.format("%.2f", trnsAmount)
                commission2ReportViewModel.amountTransacted.value = formatted


                Column {
                    LazyColumn(
                        contentPadding = PaddingValues(0.dp),
                        verticalArrangement = Arrangement.spacedBy(10.dp),
                        content = {
                            if (commission2ReportViewModel.searchText.value.isEmpty()) {
                                commission2ReportViewModel.totalLength.value =
                                    reportList.size.toString()
                                items(reportList.size) { item ->
                                    Commission2ReportRecycleScreen(walletReport.results?.report!![item])
                                }
                            } else {
                                for (report in walletReport.results?.report!!) {
                                    if (report.relationalOperation?.lowercase(Locale.ROOT)?.contains(
                                                commission2ReportViewModel.searchText.value.lowercase(
                                                    Locale.ROOT
                                                )
                                            )==true ||
                                        report.status?.lowercase(Locale.ROOT)?.contains(
                                            commission2ReportViewModel.searchText.value.lowercase(
                                                Locale.ROOT
                                            )
                                        )==true ||
                                        report.id?.lowercase(Locale.ROOT)?.contains(
                                            commission2ReportViewModel.searchText.value.lowercase(
                                                Locale.ROOT
                                            )
                                        )==true
                                        ||
                                        report.relationalId?.lowercase(Locale.ROOT)?.contains(
                                            commission2ReportViewModel.searchText.value.lowercase(
                                                Locale.ROOT
                                            )
                                        )==true ||
                                        report.type?.lowercase(Locale.ROOT)?.contains(
                                            commission2ReportViewModel.searchText.value.lowercase(
                                                Locale.ROOT
                                            )
                                        )==true
                                    // report.apiTid?.lowercase(Locale.ROOT)!!.contains(reportViewModel.searchText.value.lowercase(Locale.ROOT))*/

                                    ) {
                                        filteredList.add(report)
                                    }
                                }
                                filteredList.let {
                                    commission2ReportViewModel.totalLength.value = it.size.toString()
                                    var ammount = 0.0
                                    for (report in it.indices) {
                                        if(filteredList[report]?.status?.equals("SUCCESS",true) == true) {
                                            ammount += filteredList[report]?.amountTransacted!!
                                        }
                                    }
                                    val filFormatted = String.format("%.2f", ammount)
                                    commission2ReportViewModel.amountTransacted.value = filFormatted
                                    items(it.size) { item ->
                                        Commission2ReportRecycleScreen(it[item]!!)
                                    }
                                }
                            }
                        }
                    )
                    if (commission2ReportViewModel.searchText.value != "" && filteredList.isEmpty() || reportList.isEmpty()) {
                        Box(modifier = Modifier.fillMaxSize(),
                            contentAlignment = Alignment.Center
                        ){
                            Text(text = "No Data Available")
                        }
                    }
                }
            }
        } else {
            Box(modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ){
                Text(text = "Something went to wrong")
            }
        }
    }

}