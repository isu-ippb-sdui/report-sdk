package com.commonadmin.report.presentation.viewmodels

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.commonadmin.report.data.remote.models.DmtGateWayDataItem
import com.commonadmin.report.data.remote.models.DmtReceiptReportReqModel
import com.commonadmin.report.data.remote.models.DmtReceiptReportRespModel
import com.commonadmin.report.data.remote.models.Matm2ReportItem
import com.commonadmin.report.domain.usercases.DmtReceiptReportUseCase
import com.commonadmin.report.utils.ApiResult
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DmtReceiptReportViewModel @Inject constructor(private val userCases: DmtReceiptReportUseCase) :
    ViewModel() {
    var dmtReceiptReportState: MutableStateFlow<DmtReceiptReportState> =
        MutableStateFlow(DmtReceiptReportState())
        private set

    var isVisible: MutableState<Boolean> = mutableStateOf(false)
        private set
    var searchText: MutableState<String> = mutableStateOf("")
        private set
    var totalLength: MutableState<String> = mutableStateOf("")
        private set
    var amountTransacted: MutableLiveData<String> = MutableLiveData("")
        private set
    var isStartPrinting: MutableState<Boolean> = mutableStateOf(false)
        private set
    var showPrinterDialogue: MutableState<Boolean> = mutableStateOf(value = false)
        private set




    fun getReceiptReportData(url: String, dmtReceiptReportReqModel: DmtReceiptReportReqModel) {
        viewModelScope.launch {
            userCases.invoke(url = url, dmtReceiptReportReqModel = dmtReceiptReportReqModel)
                .collect { networkResult ->
                    when (networkResult) {
                        is ApiResult.Success -> {
                            dmtReceiptReportState.emit(
                                dmtReceiptReportState.value.copy(
                                    dmtReceiptResponse = networkResult.data,
                                    loading = false,
                                    error = null
                                )
                            )
                        }

                        is ApiResult.Loading -> {
                            dmtReceiptReportState.emit(
                                dmtReceiptReportState.value.copy(
                                    dmtReceiptResponse = null,
                                    loading = true,
                                    error = null
                                )
                            )
                        }

                        is ApiResult.Error -> {
                            dmtReceiptReportState.emit(
                                dmtReceiptReportState.value.copy(
                                    dmtReceiptResponse = null,
                                    loading = false,
                                    error = networkResult.message
                                )
                            )
                        }
                    }

                }

        }
    }


}

data class DmtReceiptReportState(
    var dmtReceiptResponse: DmtReceiptReportRespModel? = null,
    val loading: Boolean? = null,
    val error: String? = null
)
