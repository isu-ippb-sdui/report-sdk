package com.commonadmin.report.presentation.viewmodels

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.commonadmin.report.data.remote.models.Wallet2ReqModel
import com.commonadmin.report.data.remote.models.Wallet2RespModel
import com.commonadmin.report.domain.usercases.Wallet2ReportUseCase
import com.commonadmin.report.utils.ApiResult
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class Wallet2ReportViewModel @Inject constructor(private val userCases: Wallet2ReportUseCase) :
    ViewModel() {
    var wallet2ReportState: MutableStateFlow<Wallet2ReportState> =
        MutableStateFlow(Wallet2ReportState())
        private set

    var isVisible: MutableState<Boolean> = mutableStateOf(false)
        private set
    var searchText: MutableState<String> = mutableStateOf("")
        private set
    var totalLength: MutableState<String> = mutableStateOf("")
        private set
    var crAmount: MutableLiveData<String> = MutableLiveData("")
        private set
    var drAmount: MutableLiveData<String> = MutableLiveData("")
        private set

    fun getWallet2ReportData(url: String, wallet2ReqModel: Wallet2ReqModel, token: String) {
        viewModelScope.launch {
            userCases.invoke(url=url,wallet2ReqModel = wallet2ReqModel,token = token)
                .collect { networkResult ->
                    when (networkResult) {
                        is ApiResult.Success -> {
                            wallet2ReportState.emit(
                                wallet2ReportState.value.copy(
                                    wallet2RespModel = networkResult.data,
                                    loading = false,
                                    error = null
                                )
                            )
                        }

                        is ApiResult.Loading -> {
                            wallet2ReportState.emit(
                                wallet2ReportState.value.copy(
                                    wallet2RespModel = null,
                                    loading = true,
                                    error = null
                                )
                            )
                        }

                        is ApiResult.Error -> {
                            wallet2ReportState.emit(
                                wallet2ReportState.value.copy(
                                    wallet2RespModel = null,
                                    loading = false,
                                    error = networkResult.message
                                )
                            )
                        }
                    }

                }

        }
    }


}

data class Wallet2ReportState(
    var wallet2RespModel: Wallet2RespModel? = null,
    val loading: Boolean? = null,
    val error: String? = null
)
