package com.commonadmin.report.presentation.reportscreens.walletinterchange

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.material3.Divider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.commonadmin.report.data.remote.models.WalletInterchangeReportItem
import com.commonadmin.report.ui.theme.Green
import com.commonadmin.report.ui.theme.Yellow
import com.commonadmin.report.ui.theme.primaryColor
import java.text.SimpleDateFormat
import java.util.Locale

@Composable
fun WalletInterchangeRecycleScreen(walletInterchangeReportItem: WalletInterchangeReportItem) {

    val w1timeT: Long = walletInterchangeReportItem.w1CreatedDate!!.toLong()
    val w1formatter = SimpleDateFormat("dd-MM-yyyy  hh:mm a")
    val w1TimeDate = w1formatter.format(w1timeT)

    val w2timeT: Long = walletInterchangeReportItem.w2CreatedDate!!.toLong()
    val w2Format = SimpleDateFormat("dd-MM-yyyy  hh:mm a")
    val w2TimeDate = w2Format.format(w2timeT)


    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(10.dp)

    ) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Text(
                text = "Date/Time: $w1TimeDate",
                color = Color.Black,
                style = MaterialTheme.typography.headlineLarge,
                fontSize = 15.sp,
                textAlign = TextAlign.Center
            )
            Text(
                text = walletInterchangeReportItem.w1Status.toString(),
                color = when (walletInterchangeReportItem.w1Status?.uppercase(Locale.ROOT)) {
                    "REFUNDED" -> {
                        Yellow
                    }

                    "SUCCESS" -> {
                        Green
                    }

                    "FAILED" -> {
                        Color.Red
                    }

                    else -> {
                        Color.Black
                    }
                },
                style = MaterialTheme.typography.headlineLarge,
                fontSize = 15.sp
            )
        }
        Spacer(modifier = Modifier.height(3.dp))
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Text(
                text = "Wallet ID: ${walletInterchangeReportItem.w1Id}",
                color = Color.Black,
                style = MaterialTheme.typography.headlineLarge,
                fontSize = 15.sp
            )
            Text(
                text = "₹"+walletInterchangeReportItem.w1AmountTransacted.toString(),
                color = Color.Black,
                style = MaterialTheme.typography.headlineLarge,
                fontSize = 15.sp
            )
        }
        Spacer(modifier = Modifier.height(3.dp))
        Text(
            text = "Wallet Previous Amount: ${"₹"+walletInterchangeReportItem.w1PreviousAmount}",
            color = Color.Black,
            style = MaterialTheme.typography.headlineLarge,
            fontSize = 15.sp
        )
        Spacer(modifier = Modifier.height(3.dp))
        Text(
            text = "Wallet Balance Amount: ${"₹"+walletInterchangeReportItem.w1BalanceAmount}",
            color = Color.Black,
            style = MaterialTheme.typography.headlineLarge,
            fontSize = 15.sp
        )
        Spacer(modifier = Modifier.height(3.dp))
        Text(
            text = "Wallet Transaction Type: ${walletInterchangeReportItem.w1TransactionType}",
            color = Color.Black,
            style = MaterialTheme.typography.headlineLarge,
            fontSize = 15.sp
        )
        Spacer(modifier = Modifier.height(3.dp))
        Divider(
            thickness = 1.dp,
            color = Color.Gray,
            modifier = Modifier.wrapContentWidth().padding(100.dp,10.dp)
        )

        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Text(
                text = "Date/Time: $w2TimeDate",
                color = Color.Black,
                style = MaterialTheme.typography.headlineLarge,
                fontSize = 15.sp,
                textAlign = TextAlign.Center
            )
            Text(
                text = walletInterchangeReportItem.w2Status.toString(),
                color = when (walletInterchangeReportItem.w2Status?.uppercase(Locale.ROOT)) {
                    "REFUNDED" -> {
                        Yellow
                    }

                    "SUCCESS" -> {
                        Green
                    }

                    "FAILED" -> {
                        Color.Red
                    }

                    else -> {
                        Color.Black
                    }
                },
                style = MaterialTheme.typography.headlineLarge,
                fontSize = 15.sp
            )
        }
        Spacer(modifier = Modifier.height(3.dp))
        Text(
            text = "Wallet2 ID: ${walletInterchangeReportItem.w2Id}",
            color = Color.Black,
            style = MaterialTheme.typography.headlineLarge,
            fontSize = 15.sp
        )
        Spacer(modifier = Modifier.height(3.dp))
        Text(
            text = "Wallet2 Previous Amount: ${"₹"+walletInterchangeReportItem.w2PreviousAmount}",
            color = Color.Black,
            style = MaterialTheme.typography.headlineLarge,
            fontSize = 15.sp
        )
        Spacer(modifier = Modifier.height(3.dp))
        Text(
            text = "Wallet2 Balance Amount: ${"₹"+walletInterchangeReportItem.w2BalanceAmount}",
            color = Color.Black,
            style = MaterialTheme.typography.headlineLarge,
            fontSize = 15.sp
        )
        Spacer(modifier = Modifier.height(3.dp))
        Text(
            text = "Wallet2 Transaction Type: ${walletInterchangeReportItem.w2TransactionType}",
            color = Color.Black,
            style = MaterialTheme.typography.headlineLarge,
            fontSize = 15.sp
        )

        Spacer(modifier = Modifier.height(3.dp))
        Divider(
            thickness = 2.dp,
            color = primaryColor
        )
    }
}