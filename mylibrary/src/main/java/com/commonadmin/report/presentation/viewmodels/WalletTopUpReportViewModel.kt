package com.commonadmin.report.presentation.viewmodels

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.commonadmin.report.data.remote.models.FetchWalletTopUpTxnReqModel
import com.commonadmin.report.data.remote.models.FetchWalletTopUpTxnRespModel
import com.commonadmin.report.data.remote.models.WalletTopUpFetchUserReqModel
import com.commonadmin.report.data.remote.models.WalletTopUpFetchUserRespModel
import com.commonadmin.report.data.remote.models.WalletTopUpReqModel
import com.commonadmin.report.data.remote.models.WalletTopUpRequestReqModel
import com.commonadmin.report.data.remote.models.WalletTopUpRequestRespModel
import com.commonadmin.report.data.remote.models.WalletTopUpRespModel
import com.commonadmin.report.domain.usercases.FetchWalletTopUpTxnReportUseCase
import com.commonadmin.report.domain.usercases.WalletTopUpFetchUserUseCase
import com.commonadmin.report.domain.usercases.WalletTopUpReportUseCase
import com.commonadmin.report.domain.usercases.WalletTopUpReqestReportUseCase
import com.commonadmin.report.utils.ApiResult
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class WalletTopUPReportViewModel @Inject constructor(private val userCases: WalletTopUpReportUseCase,
                                                     private val fetchWalletTopupTxn: FetchWalletTopUpTxnReportUseCase,
                                                     private val walletTopUpReq:WalletTopUpReqestReportUseCase,
                                                     private val walletTopUpFetch:WalletTopUpFetchUserUseCase

                                                     ) :
    ViewModel() {
    var walletTopUpReportState: MutableStateFlow<WalletTopUPReportState> =
        MutableStateFlow(WalletTopUPReportState())
        private set
    var fetchWalletTopUPTxnReportState: MutableStateFlow<FetchWalletTopUPTxnReportState> =
        MutableStateFlow(FetchWalletTopUPTxnReportState())
        private set
    var walletTopUPRequestState: MutableStateFlow<WalletTopUPRequestReportState> =
        MutableStateFlow(WalletTopUPRequestReportState())
        private set

    var walletTopUpFetchUserState: MutableStateFlow<WalletTopUpFetchUserState> =
        MutableStateFlow(WalletTopUpFetchUserState())


    var isVisible: MutableState<Boolean> = mutableStateOf(false)
        private set
    var searchText: MutableState<String> = mutableStateOf("")
        private set
    var totalLength: MutableState<String> = mutableStateOf("")
        private set
    var amountTransacted: MutableLiveData<String> = MutableLiveData("")
        private set
    var getRemark: MutableLiveData<String> = MutableLiveData("")
        private set
    var getEditTrnsId: MutableLiveData<String> = MutableLiveData("")
        private set
    var isRemarkDialogVisible: MutableState<Boolean> = mutableStateOf(false)
        private set
    var isEditDialogVisible: MutableState<Boolean> = mutableStateOf(false)
        private set

    /**
     * EditDialog Declaration
     */
    var receiverBank: MutableState<String> = mutableStateOf("")
        private set
    var transferType: MutableState<String> = mutableStateOf("")
        private set
    var transactionType: MutableState<String> = mutableStateOf("")
        private set
    var amount: MutableState<String> = mutableStateOf("")
        private set
    var accountNumber: MutableState<String> = mutableStateOf("")
        private set
    var senderBankName: MutableState<String> = mutableStateOf("")
        private set
    var senderName: MutableState<String> = mutableStateOf("")
        private set
    var bankReference: MutableState<String> = mutableStateOf("")
        private set
    var selectedDate: MutableState<String> = mutableStateOf("")
        private set
    var remark: MutableState<String> = mutableStateOf("")
        private set
  var getFileName: MutableState<String> = mutableStateOf("")
        private set
    var receiptLink: MutableState<String> = mutableStateOf("")
        private set
    var hints: MutableState<String> = mutableStateOf("")
        private set


    fun getWalletTopUpReportData(
        url: String,
        walletTopUpReqModel: WalletTopUpReqModel,
        token: String
    ) {
        viewModelScope.launch {
            userCases.invoke(url = url, walletTopUpReqModel = walletTopUpReqModel, token = token)
                .collect { networkResult ->
                    when (networkResult) {
                        is ApiResult.Success -> {
                            walletTopUpReportState.emit(
                                walletTopUpReportState.value.copy(
                                    walletTopUpRespModel = networkResult.data,
                                    loading = false,
                                    error = null
                                )
                            )
                        }

                        is ApiResult.Loading -> {
                            walletTopUpReportState.emit(
                                walletTopUpReportState.value.copy(
                                    walletTopUpRespModel = null,
                                    loading = true,
                                    error = null
                                )
                            )
                        }

                        is ApiResult.Error -> {
                            walletTopUpReportState.emit(
                                walletTopUpReportState.value.copy(
                                    walletTopUpRespModel = null,
                                    loading = false,
                                    error = networkResult.message
                                )
                            )
                        }
                    }

                }

        }
    }

    fun getFetchWalletTopUpTxn(
        url: String,
        fetchWalletTopUpTxnReqModel: FetchWalletTopUpTxnReqModel
    ) {
        viewModelScope.launch {
            fetchWalletTopupTxn.invoke(
                url = url,
                fetchWalletTopUpTxnReqModel = fetchWalletTopUpTxnReqModel
            )
                .collect { networkResult ->
                    when (networkResult) {
                        is ApiResult.Success -> {
                            fetchWalletTopUPTxnReportState.emit(
                                fetchWalletTopUPTxnReportState.value.copy(
                                    fetchWalletTopUpTxnRespModel = networkResult.data,
                                    loading = false,
                                    error = null
                                )
                            )
                        }

                        is ApiResult.Loading -> {
                            fetchWalletTopUPTxnReportState.emit(
                                fetchWalletTopUPTxnReportState.value.copy(
                                    fetchWalletTopUpTxnRespModel = null,
                                    loading = true,
                                    error = null
                                )
                            )
                        }

                        is ApiResult.Error -> {
                            fetchWalletTopUPTxnReportState.emit(
                                fetchWalletTopUPTxnReportState.value.copy(
                                    fetchWalletTopUpTxnRespModel = null,
                                    loading = false,
                                    error = networkResult.message
                                )
                            )
                        }
                    }

                }

        }
    }

    fun getwalletTopUpRequest(url: String, walletTopUpRequestReqModel: WalletTopUpRequestReqModel) {
        viewModelScope.launch {
            walletTopUpReq.invoke(
                url = url,
                walletTopUpRequestReqModel = walletTopUpRequestReqModel
            )
                .collect { networkResult ->
                    when (networkResult) {
                        is ApiResult.Success -> {
                            walletTopUPRequestState.emit(
                                walletTopUPRequestState.value.copy(
                                    walletTopUpRequestRespModel = networkResult.data,
                                    loading = false,
                                    error = null
                                )
                            )
                        }

                        is ApiResult.Loading -> {
                            walletTopUPRequestState.emit(
                                walletTopUPRequestState.value.copy(
                                    walletTopUpRequestRespModel = null,
                                    loading = true,
                                    error = null
                                )
                            )
                        }

                        is ApiResult.Error -> {
                            walletTopUPRequestState.emit(
                                walletTopUPRequestState.value.copy(
                                    walletTopUpRequestRespModel = null,
                                    loading = false,
                                    error = networkResult.message
                                )
                            )
                        }
                    }

                }

        }
    }

    fun getwalletTopUpFetchUser(
        url: String,
        walletTopUpFetchUserReqModel: WalletTopUpFetchUserReqModel
    ) {
        viewModelScope.launch {
            walletTopUpFetch.invoke(
                url = url,
                walletTopUpFetchUserReqModel = walletTopUpFetchUserReqModel
            )
                .collectLatest { networkResult ->
                    when (networkResult) {
                        is ApiResult.Success -> {
                            walletTopUpFetchUserState.emit(
                                walletTopUpFetchUserState.value.copy(
                                    walletTopUpFetchUserRespModel = networkResult.data,
                                    loading = false,
                                    error = null
                                )
                            )
                        }

                        is ApiResult.Loading -> {
                            walletTopUpFetchUserState.emit(
                                walletTopUpFetchUserState.value.copy(
                                    walletTopUpFetchUserRespModel = null,
                                    loading = true,
                                    error = null
                                )
                            )
                        }

                        is ApiResult.Error -> {
                            walletTopUpFetchUserState.emit(
                                walletTopUpFetchUserState.value.copy(
                                    walletTopUpFetchUserRespModel = null,
                                    loading = false,
                                    error = networkResult.message
                                )
                            )
                        }
                    }

                }

        }
    }
}

data class WalletTopUPReportState(
    var walletTopUpRespModel: WalletTopUpRespModel? =null,
    val loading: Boolean? = null,
    val error: String? = null
)

data class FetchWalletTopUPTxnReportState(
    var fetchWalletTopUpTxnRespModel: FetchWalletTopUpTxnRespModel?=null,
    val loading: Boolean? = null,
    val error: String? = null
)
data class WalletTopUPRequestReportState(
    var walletTopUpRequestRespModel: WalletTopUpRequestRespModel?=null,
    val loading: Boolean? = null,
    val error: String? = null
)

data class WalletTopUpFetchUserState(
    var walletTopUpFetchUserRespModel: WalletTopUpFetchUserRespModel?=null,
    var loading:Boolean?=null,
    var error:String?=null
)
