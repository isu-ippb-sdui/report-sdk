    package com.commonadmin.report.presentation.reportscreens.walletcashout

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Text
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import com.commonadmin.report.data.remote.models.WalletCashOutReportItem
import com.commonadmin.report.data.remote.models.WalletCashoutReqModel
import com.commonadmin.report.presentation.viewmodels.WalletCashoutReportViewModel
import com.commonadmin.report.ui.theme.primaryColor
import com.commonadmin.report.utils.DataConstants
import com.commonadmin.report.utils.SharedPrefClass
import com.commonadmin.report.utils.reportcalender.ReportCalenderConstant
import java.util.Locale

@Composable
fun WalletCashoutReportScreen(walletCashoutReportViewModel: WalletCashoutReportViewModel) {
    val walletCashOutReportState =
        walletCashoutReportViewModel.walletCashoutReportState.collectAsState()
    val context = LocalContext.current

    LaunchedEffect(key1 = true) {


        val json7ArrayList: ArrayList<String> = ArrayList()
        json7ArrayList.add("WALLET2CASHOUT_IMPS")
        json7ArrayList.add("WALLET2CASHOUT_NEFT")
        json7ArrayList.add("WALLET2CASHOUT")


        val walletCashoutReqModel = WalletCashoutReqModel(
            "wallet2cashout_common",
            SharedPrefClass.getStringValue(context, DataConstants.USERNAME),
            ReportCalenderConstant.selectFromDate,
            ReportCalenderConstant.selectToDate,
            json7ArrayList,"wallet2"
        )

        if (walletCashOutReportState.value.walletCashoutRespModel == null) {
            walletCashoutReportViewModel.getWalletCashOutReportData(
                SharedPrefClass.getStringValue(context, "WalletCashoutReport").toString(),
                walletCashoutReqModel,SharedPrefClass.getStringValue(context,DataConstants.TOKEN).toString()
            )
        }
    }

    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.TopCenter
    ) {
        if (walletCashOutReportState.value.loading == true && walletCashOutReportState.value.walletCashoutRespModel == null) {
            Box(modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ){
                CircularProgressIndicator(color = primaryColor)
            }
        } else if (walletCashOutReportState.value.loading == false && walletCashOutReportState.value.walletCashoutRespModel != null) {
            walletCashOutReportState.value.walletCashoutRespModel?.let { walletReport ->

                val filteredList: ArrayList<WalletCashOutReportItem?> = arrayListOf()
                val reportList: List<WalletCashOutReportItem?>? = walletReport.report
                var trnsAmount = 0.0
                for (report in reportList?.indices!!) {
                    if (reportList[report]?.status?.equals("SUCCESS", true) == true) {
                        trnsAmount += reportList[report]?.amountTransacted!!
                    }
                }
                 val formatted = String.format("%.2f", trnsAmount)
                walletCashoutReportViewModel.amountTransacted.value = formatted


                Column {
                    LazyColumn(
                        contentPadding = PaddingValues(0.dp),
                        verticalArrangement = Arrangement.spacedBy(10.dp),
                        content = {
                            if (walletCashoutReportViewModel.searchText.value.isEmpty()) {
                                walletCashoutReportViewModel.totalLength.value =
                                    reportList.size.toString()
                                items(reportList.size) { item ->
                                    WalletCashoutReportRecycleScreen(walletReport.report[item])
                                }
                            } else {
                                for (report in walletReport.report) {
                                    if (report.operationPerformed?.lowercase(Locale.ROOT)!!
                                            .contains(
                                                walletCashoutReportViewModel.searchText.value.lowercase(
                                                    Locale.ROOT
                                                )
                                            ) ||
                                        report.status?.lowercase(Locale.ROOT)!!.contains(
                                            walletCashoutReportViewModel.searchText.value.lowercase(
                                                Locale.ROOT
                                            )
                                        ) ||
                                        report.id?.lowercase(Locale.ROOT)!!.contains(
                                            walletCashoutReportViewModel.searchText.value.lowercase(
                                                Locale.ROOT
                                            )
                                        )
                                    ) {
                                        filteredList.add(report)
                                    }
                                }
                                filteredList.let {
                                    walletCashoutReportViewModel.totalLength.value =
                                        it.size.toString()
                                    var amount = 0.0
                                    for (report in filteredList.indices) {
                                        if (filteredList[report]?.status?.equals(
                                                "SUCCESS",
                                                true
                                            ) == true
                                        ) {
                                            amount += filteredList[report]?.amountTransacted!!
                                        }
                                    }
                                    val fformatted = String.format("%.4f", amount)
                                    walletCashoutReportViewModel.amountTransacted.value =
                                        fformatted

                                    items(it.size) { item ->
                                        WalletCashoutReportRecycleScreen(it[item]!!)
                                    }
                                }
                            }
                        }
                    )
                    if (walletCashoutReportViewModel.searchText.value != "" && filteredList.isEmpty() || reportList.isEmpty()) {
                        Box(modifier = Modifier.fillMaxSize(),
                            contentAlignment = Alignment.Center
                        ){
                            Text(text = "No Data Available")
                        }
                    }
                }
            }
        } else {
            Box(modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ){
                Text(text = "Something went to wrong")
            }
        }
    }

}