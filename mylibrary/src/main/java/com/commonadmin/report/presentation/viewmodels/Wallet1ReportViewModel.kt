package com.commonadmin.report.presentation.viewmodels

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.commonadmin.report.data.remote.models.Wallet1ReqModel
import com.commonadmin.report.data.remote.models.Wallet1RespModel
import com.commonadmin.report.domain.usercases.Wallet1ReportUseCase
import com.commonadmin.report.utils.ApiResult
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class Wallet1ReportViewModel @Inject constructor(private val userCases: Wallet1ReportUseCase) :
    ViewModel() {
    var wallet1ReportState: MutableStateFlow<Wallet1ReportState> =
        MutableStateFlow(Wallet1ReportState())
        private set
    var isVisible: MutableState<Boolean> = mutableStateOf(false)
        private set
    var searchText: MutableState<String> = mutableStateOf("")
        private set
    var totalLength: MutableState<String> = mutableStateOf("")
        private set
    var crAmount: MutableLiveData<String> = MutableLiveData("")
        private set
    var drAmount: MutableLiveData<String> = MutableLiveData("")
        private set

    fun getWallet1ReportData(url: String, wallet1ReqModel: Wallet1ReqModel, token: String) {
        viewModelScope.launch {
            userCases.invoke(url = url, wallet1ReqModel = wallet1ReqModel,token=token)
                .collect { networkResult ->
                    when (networkResult) {
                        is ApiResult.Success -> {
                            wallet1ReportState.emit(
                                wallet1ReportState.value.copy(
                                    wallet1RespModel = networkResult.data,
                                    loading = false,
                                    error = null
                                )
                            )
                        }

                        is ApiResult.Loading -> {
                            wallet1ReportState.emit(
                                wallet1ReportState.value.copy(
                                    wallet1RespModel = null,
                                    loading = true,
                                    error = null
                                )
                            )
                        }

                        is ApiResult.Error -> {
                            wallet1ReportState.emit(
                                wallet1ReportState.value.copy(
                                    wallet1RespModel = null,
                                    loading = false,
                                    error = networkResult.message
                                )
                            )
                        }
                    }
                }
        }
    }
}

data class Wallet1ReportState(
    var wallet1RespModel: Wallet1RespModel? = null,
    val loading: Boolean? = null,
    val error: String? = null
)
