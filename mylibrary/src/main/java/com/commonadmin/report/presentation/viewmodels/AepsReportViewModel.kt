package com.commonadmin.report.presentation.viewmodels

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.commonadmin.report.data.remote.models.AepsReportItem
import com.commonadmin.report.data.remote.models.UnifiedAepsReqModel
import com.commonadmin.report.data.remote.models.UnifiedAepsRespModel
import com.commonadmin.report.domain.usercases.UnifiedAepsUseCase
import com.commonadmin.report.utils.ApiResult
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AepsReportViewModel @Inject constructor(private val userCases: UnifiedAepsUseCase) :
    ViewModel() {
    var aepsReportState: MutableStateFlow<UnifiedAepsReportState> =
        MutableStateFlow(UnifiedAepsReportState())
        private set

    var isVisible: MutableState<Boolean> = mutableStateOf(false)
        private set
    var searchText: MutableState<String> = mutableStateOf("")
        private set

    var setTrnsLength: MutableState<String> = mutableStateOf("")
        private set
    var setcashTrnsLength: MutableState<String> = mutableStateOf("")
        private set
    var setBalanceTrnsLength: MutableState<String> = mutableStateOf("")
        private set
    var setCashDepositTrnsLength: MutableState<String> = mutableStateOf("")
        private set
 var setMinistatementTrnsLength: MutableState<String> = mutableStateOf("")
        private set

    var setAllTrnsAmount: MutableState<String> = mutableStateOf("")
        private set

 var setBalanceEnqTrnsAmount: MutableState<String> = mutableStateOf("")
        private set

 var setCashWidTrnsAmount: MutableState<String> = mutableStateOf("")
        private set
var setCashDepositTrnsAmount: MutableState<String> = mutableStateOf("")
        private set
    var setMiniStatemntTrnsAmount: MutableState<String> = mutableStateOf("")
        private set


    var balanceEnqList: MutableState<List<AepsReportItem?>?> =
        mutableStateOf(emptyList())

    var cashWidrawList: MutableState<List<AepsReportItem?>?> =
        mutableStateOf(emptyList())
    var reportList: MutableState<List<AepsReportItem?>?> =
        mutableStateOf(emptyList())

 var miniStementList: MutableState<List<AepsReportItem?>?> =
        mutableStateOf(emptyList())

 var depositCashList: MutableState<List<AepsReportItem?>?> =
        mutableStateOf(emptyList())





    fun getUnifiedAepsReportData(url: String, aepsReqModel: UnifiedAepsReqModel, token: String) {
        viewModelScope.launch {
            userCases.invoke(url = url, unifiedAepsReqModel = aepsReqModel, token = token)
                .collect { networkResult ->
                    when (networkResult) {
                        is ApiResult.Success -> {
                            aepsReportState.emit(
                                aepsReportState.value.copy(
                                    unifiedAepsRespModel = networkResult.data,
                                    loading = false,
                                    error = null
                                )
                            )
                        }

                        is ApiResult.Loading -> {
                            aepsReportState.emit(
                                aepsReportState.value.copy(
                                    unifiedAepsRespModel = null,
                                    loading = true,
                                    error = null
                                )
                            )
                        }

                        is ApiResult.Error -> {
                            aepsReportState.emit(
                                aepsReportState.value.copy(
                                    unifiedAepsRespModel = null,
                                    loading = false,
                                    error = networkResult.message
                                )
                            )
                        }
                    }

                }

        }
    }


}

data class UnifiedAepsReportState(
    var unifiedAepsRespModel: UnifiedAepsRespModel? = null,
    val loading: Boolean? = null,
    val error: String? = null
)
