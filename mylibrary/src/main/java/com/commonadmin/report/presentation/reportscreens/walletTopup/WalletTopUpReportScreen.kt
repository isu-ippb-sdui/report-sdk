package com.commonadmin.report.presentation.reportscreens.walletTopup

import android.os.Build
import androidx.annotation.RequiresApi
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Text
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import com.commonadmin.report.data.remote.models.WalletTopUpReportItem
import com.commonadmin.report.data.remote.models.WalletTopUpReqModel
import com.commonadmin.report.presentation.viewmodels.WalletTopUPReportViewModel
import com.commonadmin.report.ui.theme.primaryColor
import com.commonadmin.report.utils.DataConstants
import com.commonadmin.report.utils.SharedPrefClass
import com.commonadmin.report.utils.reportcalender.ReportCalenderConstant
import java.util.Locale

@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun WalletTopUpReportScreen(walletTopUPReportViewModel: WalletTopUPReportViewModel) {
    val walletTopUpReportState = walletTopUPReportViewModel.walletTopUpReportState.collectAsState()
    val context = LocalContext.current


    LaunchedEffect(key1 = true) {

        val json6ArrayList :ArrayList<String> = ArrayList()
        json6ArrayList.add("interwallet")
        json6ArrayList.add("NETBANKING")
        json6ArrayList.add("wallet2 topup")
        json6ArrayList.add("PG")
        json6ArrayList.add("wallet_topup")
        json6ArrayList.add("Wallet Topup")
        val json7ArrayList: ArrayList<String> = ArrayList()

        json7ArrayList.add("Inter_Wallet_Transfer")
        json7ArrayList.add("CDM Card")
        json7ArrayList.add("topup")
        json7ArrayList.add("VA")
        json7ArrayList.add("PG_Internet Banking")
        json7ArrayList.add("Virtual_Balance_Transfer")
        json7ArrayList.add("Inter_Wallet")
        json7ArrayList.add("virtual_balance")
        json7ArrayList.add("wallet_topup")
        json7ArrayList.add("UPI_COLLECT")
        json7ArrayList.add("QR_COLLECT")
        json7ArrayList.add("cash_deposite")
        json7ArrayList.add("CDM Card")
        json7ArrayList.add("Inter_Wallet_Transfer")


        val json13ArrayList: ArrayList<String> = ArrayList()
        json13ArrayList.add("ALL")

          val json14ArrayList: ArrayList<String> = ArrayList()
        json14ArrayList.add("ALL")

          val json15ArrayList: ArrayList<String> = ArrayList()
        json15ArrayList.add("ALL")



        val walletTopReqModel = WalletTopUpReqModel(
            "wallet_report_web",
            SharedPrefClass.getStringValue(context, DataConstants.USERNAME),
            ReportCalenderConstant.selectFromDate,
            ReportCalenderConstant.selectToDate,
            json6ArrayList,
            json7ArrayList,
            json13ArrayList,
            json14ArrayList,
            json15ArrayList
        )

        if (walletTopUpReportState.value.walletTopUpRespModel == null) {
            walletTopUPReportViewModel.getWalletTopUpReportData(
                SharedPrefClass.getStringValue(context, "WalletTopupReport").toString(),
                walletTopReqModel,SharedPrefClass.getStringValue(context,DataConstants.TOKEN).toString()
            )
        }
    }

    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.TopCenter
    ) {
        if (walletTopUpReportState.value.loading == true && walletTopUpReportState.value.walletTopUpRespModel == null) {
            Box(modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ){
                CircularProgressIndicator(color = primaryColor)
            }
        } else if (walletTopUpReportState.value.loading == false && walletTopUpReportState.value.walletTopUpRespModel != null) {
            walletTopUpReportState.value.walletTopUpRespModel?.let { walletReport ->

                val filteredList: ArrayList<WalletTopUpReportItem?> = arrayListOf()
                val reportList: List<WalletTopUpReportItem?>? = walletReport.report
                var trnsAmount = 0.0
                for (report in reportList?.indices!!) {
                    if (reportList[report]?.status?.equals("SUCCESS", true) == true) {
                        trnsAmount += reportList[report]?.amount!!
                    }
                }
                // val formatted = String.format("%.4f", trnsAmount)
                walletTopUPReportViewModel.amountTransacted.value = trnsAmount.toString()


                Column {
                    LazyColumn(
                        contentPadding = PaddingValues(5.dp),
                        verticalArrangement = Arrangement.spacedBy(10.dp)
                    ) {
                        if (walletTopUPReportViewModel.searchText.value.isEmpty()) {
                            walletTopUPReportViewModel.totalLength.value = reportList.size.toString()
                            items(reportList.size) { item ->
                                WalletTopUpReportRecycleScreen(walletReport.report[item], remarks = { getRemarks->
                                    walletTopUPReportViewModel.getRemark.value= getRemarks.remarks
                                    walletTopUPReportViewModel.isRemarkDialogVisible.value = true
                                }, edTrns = { getEditTrns->
                                    var trnsIDStr = getEditTrns.id
                                    trnsIDStr = trnsIDStr?.replace("#","")
                                    walletTopUPReportViewModel.getEditTrnsId.value= trnsIDStr
                                    walletTopUPReportViewModel.isEditDialogVisible.value = true
                                }

                                )
                            }
                        } else {
                            for (report in walletReport.report) {
                                if (report.operationPerformed?.lowercase(Locale.ROOT)!!
                                        .contains(
                                            walletTopUPReportViewModel.searchText.value.lowercase(
                                                Locale.ROOT
                                            )
                                        ) ||
                                    report.status?.lowercase(Locale.ROOT)!!.contains(
                                        walletTopUPReportViewModel.searchText.value.lowercase(
                                            Locale.ROOT
                                        )
                                    ) ||
                                    report.id?.lowercase(Locale.ROOT)!!.contains(
                                        walletTopUPReportViewModel.searchText.value.lowercase(
                                            Locale.ROOT
                                        )
                                    )
                                // report.apiTid?.lowercase(Locale.ROOT)!!.contains(reportViewModel.searchText.value.lowercase(Locale.ROOT))*/

                                ) {
                                    filteredList.add(report)
                                }
                            }
                            filteredList.let {
                                walletTopUPReportViewModel.totalLength.value =
                                    it.size.toString()
                                var ammount = 0.0
                                for (report in it.indices) {
                                    if (filteredList[report]?.status?.equals(
                                            "SUCCESS",
                                            true
                                        ) == true
                                    ) {
                                        ammount += filteredList[report]?.amount!!
                                    }
                                }
                                walletTopUPReportViewModel.amountTransacted.value =
                                    ammount.toString()
                                items(it.size) { item ->
                                    WalletTopUpReportRecycleScreen(it[item]!!, remarks = { getRemarks->
                                        walletTopUPReportViewModel.getRemark.value= getRemarks.remarks
                                        walletTopUPReportViewModel.isRemarkDialogVisible.value = true
                                    }, edTrns = { getEditTrns->
                                        var trnsIDStr = getEditTrns.id
                                        trnsIDStr = trnsIDStr?.replace("#","")
                                        walletTopUPReportViewModel.getEditTrnsId.value= trnsIDStr
                                        walletTopUPReportViewModel.isEditDialogVisible.value = true
                                    }
                                    )
                                }
                            }
                        }
                    }
                    if (walletTopUPReportViewModel.searchText.value != "" && filteredList.isEmpty() || reportList.isEmpty()) {
                        Box(modifier = Modifier.fillMaxSize(),
                            contentAlignment = Alignment.Center
                        ){
                            Text(text = "No Data Available")
                        }
                    }
                }
            }
        } else {
            Box(modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ){
                Text(text = "Something went to wrong")
            }
        }
    }
    if (walletTopUPReportViewModel.isRemarkDialogVisible.value){
        WalletTopUpCustomDialog(walletTopUPReportViewModel = walletTopUPReportViewModel, showDialog = walletTopUPReportViewModel.isRemarkDialogVisible, onDoneClick = {
            walletTopUPReportViewModel.isRemarkDialogVisible.value = false
        })
    }

  if (walletTopUPReportViewModel.isEditDialogVisible.value){
      EditRequestScreen(walletTopUPReportViewModel = walletTopUPReportViewModel, showDialog = walletTopUPReportViewModel.isEditDialogVisible, onDoneClick = {
            walletTopUPReportViewModel.isEditDialogVisible.value = false
        })
    }


}