package com.commonadmin.report.presentation.reportscreens.dmtgateway

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Divider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment.Companion.End
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.commonadmin.report.data.remote.models.DmtGateWayReportItem
import com.commonadmin.report.ui.theme.Green
import com.commonadmin.report.ui.theme.Yellow
import com.commonadmin.report.ui.theme.primaryColor
import java.text.SimpleDateFormat
import java.util.Locale

@Composable
fun DmtGateWayRecycleScreen(dmtGateWayReportItem: DmtGateWayReportItem, onClick:(dmtGateWayReportItem: DmtGateWayReportItem)->Unit) {

    val timeT: Long = dmtGateWayReportItem.createdDate!!.toLong()
    val formatter = SimpleDateFormat("dd-MM-yyyy  hh:mm a")
    val timeDate = formatter.format(timeT)

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 10.dp)

    ) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Text(
                text = "Date/Time: $timeDate",
                color = Color.Black,
                style = MaterialTheme.typography.headlineLarge,
                fontSize = 15.sp,
                textAlign = TextAlign.Center
            )
            Text(
                text = dmtGateWayReportItem.gatewayStatus.toString(),
                color = when (dmtGateWayReportItem.gatewayStatus?.uppercase(Locale.ROOT)) {
                    "REFUNDED" -> {
                        Yellow
                    }

                    "SUCCESS" -> {
                       Green
                    }

                    "FAILED" -> {
                        Color.Red
                    }

                    else -> {
                        Color.Black
                    }
                },
                style = MaterialTheme.typography.headlineLarge,
                fontSize = 15.sp
            )
        }
        Spacer(modifier = Modifier.height(5.dp))
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Text(
                text = "Txn ID: ${dmtGateWayReportItem.id}",
                color = Color.Black,
                style = MaterialTheme.typography.headlineLarge,
                fontSize = 15.sp
            )
            Text(
                text = "₹"+dmtGateWayReportItem.amountTransacted.toString(),
                color = Color.Black,
                style = MaterialTheme.typography.headlineLarge,
                fontSize = 15.sp
            )
        }
        Spacer(modifier = Modifier.height(3.dp))
        Text(
            text = "Sender Mob: ${dmtGateWayReportItem.customerMobileNumber}",
            color = Color.Black,
            style = MaterialTheme.typography.headlineLarge,
            fontSize = 15.sp
        )
        Spacer(modifier = Modifier.height(15.dp))
        Text(
            text = "Bank Name: ${dmtGateWayReportItem.beneBankName}",
            color = Color.Black,
            style = MaterialTheme.typography.headlineLarge,
            fontSize = 15.sp
        )
        Spacer(modifier = Modifier.height(3.dp))
        Text(
            text = "Bank A/C: ${dmtGateWayReportItem.beneAccountNumber}",
            color = Color.Black,
            style = MaterialTheme.typography.headlineLarge,
            fontSize = 15.sp
        )
        Spacer(modifier = Modifier.height(3.dp))
        Text(
            text = "Api Comment: ${dmtGateWayReportItem.apiComment}",
            color = Color.Black,
            style = TextStyle( fontSize = 14.sp, lineHeight = 20.sp)
        )
        Spacer(modifier = Modifier.height(5.dp))

        Button(
            onClick = {
                onClick.invoke(dmtGateWayReportItem)
                      },
            modifier = Modifier.wrapContentWidth().wrapContentHeight()
                .background(color = primaryColor, shape = RoundedCornerShape(20.dp))
                .padding(0.dp).align(alignment = End),
            colors = ButtonDefaults.buttonColors(
                containerColor = primaryColor,
                contentColor = Color.White
            )
        ) {
            Text(
                text = "View Receipt",
                style = MaterialTheme.typography.titleSmall
            )
        }
        Spacer(modifier = Modifier.height(8.dp))
        Divider(
            thickness = 1.dp,
            color = Color.Gray
        )
    }
}



