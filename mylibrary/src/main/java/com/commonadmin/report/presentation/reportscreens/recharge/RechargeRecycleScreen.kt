package com.commonadmin.report.presentation.reportscreens.recharge

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Divider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.commonadmin.report.data.remote.models.RechargeReportItem
import com.commonadmin.report.ui.theme.Green
import com.commonadmin.report.ui.theme.Yellow
import java.text.SimpleDateFormat
import java.util.Locale

@Composable
fun RechargeReportRecycleScreen(rechargeReportItem: RechargeReportItem) {
    val timeT: Long = rechargeReportItem.createdDate!!.toLong()
    val formatter = SimpleDateFormat("dd-MM-yyyy  hh:mm a")
    val timeDate = formatter.format(timeT)
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(10.dp)

    ) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Text(
                text = "Date/Time: $timeDate",
                color = Color.Black,
                style = MaterialTheme.typography.headlineLarge,
                fontSize = 15.sp,
                textAlign = TextAlign.Center
            )
            Text(
                text = rechargeReportItem.status.toString(),
                color = when (rechargeReportItem.status?.uppercase(Locale.ROOT)) {
                    "REFUNDED" -> {
                        Yellow
                    }

                    "SUCCESS" -> {
                        Green
                    }

                    "FAILED" -> {
                        Color.Red
                    }

                    else -> {
                        Color.Black
                    }
                },
                style = MaterialTheme.typography.headlineLarge,
                fontSize = 15.sp
            )
        }
        Spacer(modifier = Modifier.height(5.dp))
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Text(
                text = "Txn Id: ${rechargeReportItem.id}",
                color = Color.Black,
                style = MaterialTheme.typography.headlineLarge,
                fontSize = 14.sp
            )
            Text(
                text = "₹"+rechargeReportItem.amountTransacted.toString(),
                color = Color.Black,
                style = MaterialTheme.typography.headlineLarge,
                fontSize = 15.sp
            )
        }
        Spacer(modifier = Modifier.height(3.dp))

        Text(
            text = "User Tracking ID: ${rechargeReportItem.apiTid?.let { it } ?: "N/A"}",
            color = Color.Black,
            style = MaterialTheme.typography.headlineLarge,
            fontSize = 15.sp
        )
        Spacer(modifier = Modifier.height(3.dp))
        Text(
            text = "Customer ID: ${rechargeReportItem.referenceNo?.let { it } ?: "N/A"}",
            color = Color.Black,
            style = MaterialTheme.typography.headlineLarge,
            fontSize = 15.sp
        )
        Spacer(modifier = Modifier.height(3.dp))
        Text(
            text = "Operator Name: ${rechargeReportItem.operationPerformed?.let { it } ?: "N/A"}",
            color = Color.Black,
            style = MaterialTheme.typography.headlineLarge,
            fontSize = 15.sp
        )
        Spacer(modifier = Modifier.height(5.dp))

        Divider(
            thickness = 1.dp,
            color = Color.Gray
        )
    }
}