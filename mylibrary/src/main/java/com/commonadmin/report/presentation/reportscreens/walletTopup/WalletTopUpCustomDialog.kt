package com.commonadmin.report.presentation.reportscreens.walletTopup

import android.util.Log
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import com.commonadmin.report.R
import com.commonadmin.report.data.remote.models.RemarkData
import com.commonadmin.report.presentation.viewmodels.WalletTopUPReportViewModel
import com.commonadmin.report.ui.theme.redColor
import com.commonadmin.report.ui.theme.whiteColor
import com.commonadmin.report.utils.DataConstants
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import com.google.gson.reflect.TypeToken
import org.json.JSONObject


@Composable
fun WalletTopUpCustomDialog(
    walletTopUPReportViewModel: WalletTopUPReportViewModel,
    showDialog: MutableState<Boolean>,
    onDoneClick: () -> Unit
) {

    val jsonString=  walletTopUPReportViewModel.getRemark.value.toString()
    val gson = Gson()
    val type = object : TypeToken<List<RemarkData>>() {}.type
    val remarkList: List<RemarkData> = gson.fromJson("[$jsonString]", type)



    Dialog(onDismissRequest = { showDialog.value = true }) {
        Surface(
            shape = RoundedCornerShape(16.dp), color = whiteColor
        ) {
            Box(contentAlignment = Alignment.Center) {
                Column(
                    Modifier
                        .wrapContentHeight()
                        .padding(0.dp)
                ) {
                    Row(
                        modifier = Modifier.fillMaxWidth(),
                        horizontalArrangement = Arrangement.SpaceBetween
                    ) {
                        Text(
                            text = "Wallet Top Up Remarks",
                            color = androidx.compose.ui.graphics.Color.Black,
                            fontSize = 17.sp,
                            style = MaterialTheme.typography.h6,
                            modifier = Modifier.padding(10.dp)
                        )

                        IconButton(onClick = { onDoneClick.invoke() }) {
                            Icon(
                                painter = painterResource(id = R.drawable.baseline_cancel_24),
                                tint = redColor,
                                contentDescription = "cancel"
                            )
                        }

                    }
                    //Lazy column
                    Column {
                        LazyColumn(
                            contentPadding = PaddingValues(0.dp),
                            verticalArrangement = Arrangement.spacedBy(5.dp)
                        ){
                            items(remarkList.size){item->
                                RemarkDataScreen(remarkList[item])
                            }
                        }

                    }
                }
            }
        }
    }
}

@Composable
fun RemarkDataScreen(remarkData: RemarkData) {
    Column(
        modifier = Modifier
            .wrapContentHeight()
            .fillMaxWidth()
            .padding(vertical = 5.dp),

        horizontalAlignment = Alignment.CenterHorizontally
    ) {

        Text(
            text = "Type: ${remarkData.type}",
            color = androidx.compose.ui.graphics.Color.Black,
            fontSize = 15.sp,
            style = MaterialTheme.typography.h2
        )

        Text(
            text = "Remarks: ${remarkData.remarks}",
            color = androidx.compose.ui.graphics.Color.Black,
            fontSize = 15.sp,
            style = MaterialTheme.typography.h2
        )
        Text(
            text = "Date/Time: ${DataConstants.getDateFromGmt(remarkData.created_time)}",
            color = androidx.compose.ui.graphics.Color.Black,
            fontSize = 15.sp,
            style = MaterialTheme.typography.h2
        )
        Text(
            text = "UserName: ${remarkData.author}",
            color = androidx.compose.ui.graphics.Color.Black,
            fontSize = 15.sp,
            style = MaterialTheme.typography.h2
        )
    }

}