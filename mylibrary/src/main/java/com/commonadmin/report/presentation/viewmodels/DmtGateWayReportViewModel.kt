package com.commonadmin.report.presentation.viewmodels

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.commonadmin.report.data.remote.models.DmtGatewayReqModels
import com.commonadmin.report.data.remote.models.DmtGatewayRespModel
import com.commonadmin.report.domain.usercases.DmtGateWayReportUseCase
import com.commonadmin.report.utils.ApiResult
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DmtGateWayReportViewModel @Inject constructor(private val userCases: DmtGateWayReportUseCase) :
    ViewModel() {
    var dmtGateWayReportState: MutableStateFlow<DmtGateWayReportState> =
        MutableStateFlow(DmtGateWayReportState())
        private set

    var isVisible: MutableState<Boolean> = mutableStateOf(false)
        private set
    var searchText: MutableState<String> = mutableStateOf("")
        private set
    var totalLength: MutableState<String> = mutableStateOf("")
        private set
    var amountTransacted: MutableLiveData<String> = MutableLiveData("")
        private set


    fun getDmtGateWayReportData(url: String, dmtGatewayReqModels: DmtGatewayReqModels,token:String) {
        viewModelScope.launch {
            userCases.invoke(url = url, dmtGatewayReqModels = dmtGatewayReqModels,token = token)
                .collect { networkResult ->
                    when (networkResult) {
                        is ApiResult.Success -> {
                            dmtGateWayReportState.emit(
                                dmtGateWayReportState.value.copy(
                                    dmtGatewayRespModel = networkResult.data,
                                    loading = false,
                                    error = null
                                )
                            )
                        }

                        is ApiResult.Loading -> {
                            dmtGateWayReportState.emit(
                                dmtGateWayReportState.value.copy(
                                    dmtGatewayRespModel = null,
                                    loading = true,
                                    error = null
                                )
                            )
                        }

                        is ApiResult.Error -> {
                            dmtGateWayReportState.emit(
                                dmtGateWayReportState.value.copy(
                                    dmtGatewayRespModel = null,
                                    loading = false,
                                    error = networkResult.message
                                )
                            )
                        }
                    }

                }

        }
    }


}

data class DmtGateWayReportState(
    var dmtGatewayRespModel: DmtGatewayRespModel? = null,
    val loading: Boolean? = null,
    val error: String? = null
)
