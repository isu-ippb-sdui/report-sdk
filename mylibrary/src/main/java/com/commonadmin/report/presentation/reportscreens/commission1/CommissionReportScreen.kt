package com.commonadmin.report.presentation.reportscreens.commission1

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Text
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import com.commonadmin.report.data.remote.models.Commission1ReportItem
import com.commonadmin.report.data.remote.models.Commission1ReportReqModel
import com.commonadmin.report.presentation.viewmodels.Commission1ReportViewModel
import com.commonadmin.report.ui.theme.primaryColor
import com.commonadmin.report.utils.DataConstants
import com.commonadmin.report.utils.SharedPrefClass
import com.commonadmin.report.utils.reportcalender.ReportCalenderConstant
import java.util.Locale

@Composable
fun Commission1ReportScreen(commission1ReportViewModel: Commission1ReportViewModel) {
    val commissionReportState = commission1ReportViewModel.commission1ReportState.collectAsState()
    val context = LocalContext.current
    LaunchedEffect(key1 = true) {

        val json7ArrayList: ArrayList<String> = ArrayList()
        json7ArrayList.add("commission")




        val commission1ReportReqModel = Commission1ReportReqModel(
            "new_commission_common",
            SharedPrefClass.getStringValue(context, DataConstants.USERNAME),
            ReportCalenderConstant.selectFromDate,
            ReportCalenderConstant.selectToDate,
            json7ArrayList
        )

        if (commissionReportState.value.commission1ReportRespModel == null) {
            commission1ReportViewModel.getCommission1ReportData(
                SharedPrefClass.getStringValue(context, "Commission1Report").toString(),
                commission1ReportReqModel,SharedPrefClass.getStringValue(context,DataConstants.TOKEN).toString()
            )
        }
    }

    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.TopCenter
    ) {
        if (commissionReportState.value.loading == true && commissionReportState.value.commission1ReportRespModel == null) {
            Box(modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ){
                CircularProgressIndicator(color = primaryColor)
            }
        } else if (commissionReportState.value.loading == false && commissionReportState.value.commission1ReportRespModel != null) {
            commissionReportState.value.commission1ReportRespModel?.let { walletReport ->

                val filteredList: ArrayList<Commission1ReportItem?> = arrayListOf()
                val reportList: List<Commission1ReportItem?>? = walletReport.report

                var trnsAmount = 0.0
                for (report in reportList?.indices!!) {
                    if(reportList[report]?.status?.equals("SUCCESS",true) == true) {
                        trnsAmount += reportList[report]?.amountTransacted!!
                    }
                }
                val formatted = String.format("%.2f", trnsAmount)
                commission1ReportViewModel.amountTransacted.value = formatted


                Column {
                    LazyColumn(
                        contentPadding = PaddingValues(0.dp),
                        verticalArrangement = Arrangement.spacedBy(10.dp),
                        content = {
                            if (commission1ReportViewModel.searchText.value.isEmpty()) {
                                commission1ReportViewModel.totalLength.value =
                                    reportList.size.toString()
                                items(reportList.size) { item ->
                                    Commission1ReportRecycleScreen(walletReport.report[item])
                                }
                            } else {
                                for (report in walletReport.report) {
                                    if (report.relationalOperation?.lowercase(Locale.ROOT)!!
                                            .contains(
                                                commission1ReportViewModel.searchText.value.lowercase(
                                                    Locale.ROOT
                                                )
                                            ) ||
                                        report.status?.lowercase(Locale.ROOT)!!.contains(
                                            commission1ReportViewModel.searchText.value.lowercase(
                                                Locale.ROOT
                                            )
                                        ) ||
                                        report.id?.lowercase(Locale.ROOT)!!.contains(
                                            commission1ReportViewModel.searchText.value.lowercase(
                                                Locale.ROOT
                                            )
                                        )
                                        ||
                                        report.relationalId?.lowercase(Locale.ROOT)!!.contains(
                                            commission1ReportViewModel.searchText.value.lowercase(
                                                Locale.ROOT
                                            )
                                        )

                                    ) {
                                        filteredList.add(report)
                                    }
                                }
                                filteredList.let {
                                    commission1ReportViewModel.totalLength.value = it.size.toString()
                                    var ammount = 0.0
                                    for (report in it.indices) {
                                        if(filteredList[report]?.status?.equals("SUCCESS",true) == true) {
                                            ammount += filteredList[report]?.amountTransacted!!
                                        }
                                    }
                                    val filFormatted = String.format("%.2f", ammount)
                                    commission1ReportViewModel.amountTransacted.value = filFormatted
                                    items(it.size) { item ->
                                        Commission1ReportRecycleScreen(it[item]!!)
                                    }
                                }
                            }
                        }
                    )
                    if (commission1ReportViewModel.searchText.value != "" && filteredList.isEmpty() || reportList.isEmpty()) {
                        Box(modifier = Modifier.fillMaxSize(),
                            contentAlignment = Alignment.Center
                        ){
                            Text(text = "No Data Available")
                        }
                    }
                }
            }
        } else {
            Box(modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ){
                Text(text = "Something went to wrong")
            }
        }
    }

}