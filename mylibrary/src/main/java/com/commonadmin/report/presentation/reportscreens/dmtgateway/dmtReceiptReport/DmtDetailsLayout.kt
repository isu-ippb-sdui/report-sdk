package com.commonadmin.report.presentation.reportscreens.dmtgateway.dmtReceiptReport

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.commonadmin.report.data.remote.models.DmtGateWayDataItem
import com.commonadmin.report.ui.theme.Green
import com.commonadmin.report.ui.theme.Yellow
import java.util.Locale


@Composable
fun DmtDetailsList(dmtGateWayDataItem: DmtGateWayDataItem) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.Center
    ) {
        Text(
            modifier = Modifier
                .weight(1f)
                .padding(horizontal = 2.dp),
            text = "Txt ID",
            color = Color.Gray,
            fontSize = 14.sp,
            style = MaterialTheme.typography.headlineLarge
        )
        Text(
            modifier = Modifier
                .weight(1f)
                .padding(horizontal = 2.dp),
            text = "RRN",
            color = Color.Gray,
            fontSize = 14.sp,
            style = MaterialTheme.typography.headlineLarge
        )
        Text(
            modifier = Modifier
                .weight(1f)
                .padding(horizontal = 2.dp),
            text = "Amount",
            color = Color.Gray,
            fontSize = 14.sp,
            style = MaterialTheme.typography.headlineLarge
        )
        Text(
            modifier = Modifier
                .weight(1f)
                .padding(horizontal = 2.dp),
            text = "Status",
            color = Color.Gray,
            fontSize = 14.sp,
            style = MaterialTheme.typography.headlineLarge
        )
    }
    Spacer(modifier = Modifier.height(10.dp))
    Row(
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.Center
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center
        ) {
            Text(
                modifier = Modifier.weight(1f),
                text =  dmtGateWayDataItem.id.toString().ifEmpty { "N/A" },
                color = Color.Black,
                fontSize = 16.sp,
                style = MaterialTheme.typography.bodyMedium
            )
            Text(
                modifier = Modifier.weight(1f),
                text =dmtGateWayDataItem.rrn.toString().ifEmpty { "N/A" },
                color = Color.Black,
                fontSize = 16.sp,
                style = MaterialTheme.typography.bodyMedium
            )
            Text(
                modifier = Modifier.weight(1f),
                text = "Rs.${dmtGateWayDataItem.amount.toString().ifEmpty { "N/A" }}",
                color = Color.Black,
                fontSize = 16.sp,
                style = MaterialTheme.typography.bodyMedium
            )
            Text(
                modifier = Modifier.weight(1f),
                  text = dmtGateWayDataItem.status.toString(),
                color = when (dmtGateWayDataItem.status.toString().uppercase(Locale.ROOT)) {
                    "REFUNDED" -> {
                        Yellow
                    }

                    "SUCCESS" -> {
                        Green
                    }

                    "FAILED" -> {
                        Color.Red
                    }

                    else -> {
                        Color.Black
                    }
                },
                fontSize = 16.sp,
                style = MaterialTheme.typography.bodyMedium
            )
        }

    }
    }