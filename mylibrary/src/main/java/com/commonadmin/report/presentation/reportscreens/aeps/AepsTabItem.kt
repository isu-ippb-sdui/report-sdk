package com.commonadmin.report.presentation.reportscreens.aeps

import androidx.compose.runtime.Composable
import com.commonadmin.report.presentation.viewmodels.AepsReportViewModel

sealed class TabItem(val title: String, val screen: @Composable () -> Unit) {
    class AllReport(aepsReportViewModel: AepsReportViewModel) :
        TabItem("All Reports", screen = { AllTrnsReport(aepsReportViewModel) })

    class BalanceEnq(aepsReportViewModel: AepsReportViewModel) : TabItem(
        "Balance Enquiry",
        screen = { BalanceEnqScreen(aepsReportViewModel) }
    )

    class CashWidhDraw(aepsReportViewModel: AepsReportViewModel) : TabItem(
        "Cash Withdraw",
        screen = { CashWithdrawlScreen(aepsReportViewModel) }
    )

    class CashDeposit(aepsReportViewModel: AepsReportViewModel) : TabItem(
        "Cash Deposit",
        screen = { CashDepositScreen(aepsReportViewModel) }
    )

    class MiniSatement(aepsReportViewModel: AepsReportViewModel) : TabItem(
        "Mini Statement",
        screen = { MiniStatementScreen(aepsReportViewModel) }
    )

}