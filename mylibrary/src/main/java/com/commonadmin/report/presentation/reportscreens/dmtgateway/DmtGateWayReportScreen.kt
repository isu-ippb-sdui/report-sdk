package com.commonadmin.report.presentation.reportscreens.dmtgateway

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Text
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.commonadmin.report.data.remote.models.DmtGateWayReportItem
import com.commonadmin.report.data.remote.models.DmtGatewayReqModels
import com.commonadmin.report.presentation.navgraph.Destinations
import com.commonadmin.report.presentation.viewmodels.DmtGateWayReportViewModel
import com.commonadmin.report.ui.theme.primaryColor
import com.commonadmin.report.utils.DataConstants
import com.commonadmin.report.utils.SharedPrefClass
import com.commonadmin.report.utils.reportcalender.ReportCalenderConstant
import java.util.Locale

@Composable
fun DmtGateWayReportScreen(navController: NavController,dmtGateWayReportViewModel: DmtGateWayReportViewModel) {
    val dmtGateWayReportState = dmtGateWayReportViewModel.dmtGateWayReportState.collectAsState()
    val context = LocalContext.current
    LaunchedEffect(key1 = true) {
        val json3ArryList: ArrayList<String> = ArrayList()
        json3ArryList.add("INITIATED")
        json3ArryList.add("RETRY")
        json3ArryList.add("INPROGRESS")
        json3ArryList.add("SUCCESS")
        json3ArryList.add("FAILED")
        json3ArryList.add("REFUNDED")
        json3ArryList.add("REFUNDPENDING")

        val json6ArrayList: ArrayList<String> = ArrayList()
        json6ArrayList.add("DMT")

        val json7ArrayList: ArrayList<String> = ArrayList()
        json7ArrayList.add("BENE_VERIFICATION")
        json7ArrayList.add("NEFT_FUND_TRANSFER")
        json7ArrayList.add("IMPS_FUND_TRANSFER")


        val dmtGateWayReqModel = DmtGatewayReqModels(
            "New_Dmt_v2_web_common",
            SharedPrefClass.getStringValue(context, DataConstants.USERNAME),
            json3ArryList,
            ReportCalenderConstant.selectFromDate,
            ReportCalenderConstant.selectToDate,
            json6ArrayList,
            json7ArrayList
        )

        if (dmtGateWayReportState.value.dmtGatewayRespModel == null) {
            dmtGateWayReportViewModel.getDmtGateWayReportData(
                SharedPrefClass.getStringValue(context, "DmtGateWayReport").toString(),
                dmtGateWayReqModel,
                SharedPrefClass.getStringValue(context,DataConstants.TOKEN).toString()
            )
        }
    }

    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.TopCenter
    ) {
        if (dmtGateWayReportState.value.loading == true && dmtGateWayReportState.value.dmtGatewayRespModel == null) {
            Box(modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ){
                CircularProgressIndicator(color = primaryColor)
            }
        } else if (dmtGateWayReportState.value.loading == false && dmtGateWayReportState.value.dmtGatewayRespModel != null) {
            dmtGateWayReportState.value.dmtGatewayRespModel?.let { walletReport ->

                val filteredList: ArrayList<DmtGateWayReportItem?> = arrayListOf()
                val reportList: List<DmtGateWayReportItem?>? = walletReport.report

                var trnsAmount = 0
                for (report in reportList?.indices!!) {
                    if(reportList[report]?.status?.equals("SUCCESS",true) == true) {
                        trnsAmount += reportList[report]?.amountTransacted?.toInt()!!
                    }
                }
                dmtGateWayReportViewModel.amountTransacted.value = trnsAmount.toString()


                Column {
                    LazyColumn(
                        contentPadding = PaddingValues(0.dp),
                        verticalArrangement = Arrangement.spacedBy(10.dp),
                        content = {
                            if (dmtGateWayReportViewModel.searchText.value.isEmpty()) {
                                dmtGateWayReportViewModel.totalLength.value = reportList.size.toString()
                                items(reportList.size) { item ->
                                    DmtGateWayRecycleScreen(walletReport.report[item]){data->
                                        var trnsIDStr = data.id.toString()
                                        trnsIDStr = trnsIDStr.replace("#","")
                                      //  DataConstants.ReceiptID = trnsIDStr.toLong()
                                       navController.navigate("${Destinations.DMT_RECEIPT_SCREEN_ROUTE}/${trnsIDStr.toLong()}")
                                    }
                                }
                            } else {
                                for (report in walletReport.report) {
                                    if (report.customerMobileNumber?.lowercase(Locale.ROOT)?.contains(
                                                dmtGateWayReportViewModel.searchText.value.lowercase(
                                                    Locale.ROOT
                                                )
                                            )==true ||
                                        report.gatewayStatus?.lowercase(Locale.ROOT)?.contains(
                                            dmtGateWayReportViewModel.searchText.value.lowercase(
                                                Locale.ROOT
                                            )
                                        )==true ||
                                        report.id?.lowercase(Locale.ROOT)?.contains(
                                            dmtGateWayReportViewModel.searchText.value.lowercase(
                                                Locale.ROOT
                                            )
                                        )==true ||
                                        report.beneBankName?.lowercase(Locale.ROOT)?.contains(
                                            dmtGateWayReportViewModel.searchText.value.lowercase(
                                                Locale.ROOT
                                            )
                                        )==true ||
                                        report.beneAccountNumber?.lowercase(Locale.ROOT)?.contains(
                                            dmtGateWayReportViewModel.searchText.value.lowercase(
                                                Locale.ROOT
                                            )
                                        )==true

                                    // report.apiTid?.lowercase(Locale.ROOT)!!.contains(reportViewModel.searchText.value.lowercase(Locale.ROOT))*/

                                    ) {
                                        filteredList.add(report)
                                    }
                                }
                                filteredList.let {
                                    dmtGateWayReportViewModel.totalLength.value = it.size.toString()
                                    var ammount = 0
                                    for (report in it.indices) {
                                        if(filteredList[report]?.status?.equals("SUCCESS",true) == true) {
                                            ammount += filteredList[report]?.amountTransacted?.toInt()!!
                                        }
                                    }
                                   // val filFormatted = String.format("%.4f", ammount)
                                    dmtGateWayReportViewModel.amountTransacted.value = ammount.toString()
                                    items(it.size) { item ->
                                        DmtGateWayRecycleScreen(it[item]!!){
                                            var trnsIDStr = it.id.toString()
                                            trnsIDStr = trnsIDStr.replace("#","")
                                            //  DataConstants.ReceiptID = trnsIDStr.toLong()
                                            navController.navigate("${Destinations.DMT_RECEIPT_SCREEN_ROUTE}/${trnsIDStr.toLong()}")

                                        }
                                    }
                                }
                            }
                        }
                    )
                    if (dmtGateWayReportViewModel.searchText.value != "" && filteredList.isEmpty() || reportList.isEmpty()) {
                        Box(modifier = Modifier.fillMaxSize(),
                            contentAlignment = Alignment.Center
                        ){
                            Text(text = "No Data Available")
                        }
                    }
                }
            }
        } else {
            Box(modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ){
                Text(text = "Something went to wrong")
            }
        }
    }

}