package com.commonadmin.report.presentation.reportscreens.walletTopup

import android.app.DatePickerDialog
import android.content.Context
import android.net.Uri
import android.os.Build
import android.widget.Toast
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.DropdownMenu
import androidx.compose.material.DropdownMenuItem
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.OutlinedButton
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.unit.toSize
import androidx.compose.ui.window.Dialog
import com.commonadmin.report.R
import com.commonadmin.report.data.remote.models.FetchWalletTopUpTxnReqModel
import com.commonadmin.report.data.remote.models.WalletTopUpFetchUserReqModel
import com.commonadmin.report.data.remote.models.WalletTopUpRequestReqModel
import com.commonadmin.report.presentation.viewmodels.WalletTopUPReportViewModel
import com.commonadmin.report.ui.theme.primaryColor
import com.commonadmin.report.ui.theme.whiteColor
import com.commonadmin.report.utils.DataConstants
import com.commonadmin.report.utils.SharedPrefClass
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale


@RequiresApi(Build.VERSION_CODES.O)
@Composable
fun EditRequestScreen(
    walletTopUPReportViewModel: WalletTopUPReportViewModel,
    showDialog: MutableState<Boolean>,
    onDoneClick: () -> Unit
) {
    val walletTopUpReportState =
        walletTopUPReportViewModel.fetchWalletTopUPTxnReportState.collectAsState()
    val walletTopUpRequestState =
        walletTopUPReportViewModel.walletTopUPRequestState.collectAsState()
    val walletTopUpFetchUserState =
        walletTopUPReportViewModel.walletTopUpFetchUserState.collectAsState()
    val isValid = rememberSaveable { mutableStateOf(false) }
    val isLoading = rememberSaveable {
        mutableStateOf(false)
    }
    var selectDeviceFieldSize by remember {
        mutableStateOf(Size.Zero)
    }

    val context = LocalContext.current

    LaunchedEffect(key1 = Unit) {
        val fetchWalletTopUpTxnReqModel =
            FetchWalletTopUpTxnReqModel(walletTopUPReportViewModel.getEditTrnsId.value)
        walletTopUPReportViewModel.getFetchWalletTopUpTxn(
            SharedPrefClass.getStringValue(context, "updateWalletTopupTxn").toString(), fetchWalletTopUpTxnReqModel
        )

        val walletTopUpFetchUserReqModel = WalletTopUpFetchUserReqModel(
            SharedPrefClass.getStringValue(
                context,
                DataConstants.USERNAME
            )
        )
        walletTopUPReportViewModel.getwalletTopUpFetchUser(
            SharedPrefClass.getStringValue(
                context,
                "retailerBankFetch"
            ).toString(), walletTopUpFetchUserReqModel
        )

    }

    val activityResultLauncher =
        rememberLauncherForActivityResult(ActivityResultContracts.GetContent()) { uri: Uri? ->
            uri?.let { documentUri ->
                uploadInfireabse(context, documentUri, walletTopUPReportViewModel, isLoading)
                walletTopUPReportViewModel.getFileName.value = getFileName(context, documentUri)!!
            }
        }

    Dialog(onDismissRequest = { showDialog.value = true }) {
        Surface(modifier= Modifier.wrapContentHeight(),
            shape = RoundedCornerShape(14.dp), color = whiteColor
        ) {
            Box(contentAlignment = Alignment.TopCenter) {
                Column(
                    modifier = Modifier
                        .verticalScroll(rememberScrollState())
                        .padding(horizontal = 10.dp)
                ) {
                    IconButton(
                        onClick = {
                            onDoneClick.invoke()
                        },
                        modifier = Modifier
                            .align(Alignment.End)
                            .padding(top = 5.dp)
                    ) {
                        Icon(
                            painter = painterResource(R.drawable.baseline_cancel_24),
                            contentDescription = "Close"
                        )
                    }

                    Text(
                        text = "Edit Request",
                        fontWeight = FontWeight.Bold,
                        fontSize = 16.sp,
                        color = Color.Black,
                        modifier = Modifier.padding(5.dp)
                    )

                    Spacer(modifier = Modifier.height(1.dp))
                    OutlinedTextField(
                        value = walletTopUPReportViewModel.receiverBank.value,
                        onValueChange = {
                            walletTopUPReportViewModel.receiverBank.value = it
                        },
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(top = 7.dp),
                        label = { Text(text = "RECEIVER BANK") },
                        singleLine = true,
                        readOnly = true

                    )

                    Spacer(modifier = Modifier.height(5.dp))

                /*    val transferTypes =
                        listOf(
                            "IMPS",
                            "NEFT",
                            "RTGS",
                            "FUND TRANSFER",
                            "Cash Deposit",
                            "UPI_COLLECT",
                            "IFT",
                            "QR_COLLECT",
                            "PG_Internet Banking"
                        )

                    val deviceExpanded = remember {
                        mutableStateOf(false)
                    }
                    Box(modifier = Modifier.fillMaxWidth()) {
                    OutlinedTextField(
                        value = walletTopUPReportViewModel.transferType.value,
                        onValueChange = {
                            walletTopUPReportViewModel.transferType.value = it
                        },
                        modifier = Modifier
                            .fillMaxWidth()
                            .onGloballyPositioned { coordinates ->
                                selectDeviceFieldSize = coordinates.size.toSize()
                            },
                        label = {
                            Text(
                                text = "Select Transfer Type",
                                style = TextStyle(
                                    fontSize = 15.sp,
                                    textAlign = TextAlign.Start
                                )
                            )
                        },
                        textStyle = TextStyle(
                            color = Color.Black,
                            fontSize = 15.sp,
                            textAlign = TextAlign.Center
                        ),
                        trailingIcon = {
                            Icon(
                                painter = if (deviceExpanded.value) painterResource(id = R.drawable.baseline_arrow_drop_up_24) else painterResource(id = R.drawable.baseline_arrow_drop_down_24),
                                contentDescription = "",
                                modifier = Modifier.clickable {
                                    deviceExpanded.value = !deviceExpanded.value
                                }
                            )
                        },
                        leadingIcon = {
                        },
                        enabled = false,
                        keyboardOptions = KeyboardOptions(
                            imeAction = ImeAction.Next
                        ),
                        colors = TextFieldDefaults.outlinedTextFieldColors(
                            backgroundColor = Color.White,
                            cursorColor = Color.Black,
                            disabledLabelColor = Color.Gray,
                            disabledBorderColor = Color.DarkGray,
                            focusedBorderColor = primaryColor,
                            focusedLabelColor = primaryColor
                        )
                    )

                    DropdownMenu(
                        expanded = deviceExpanded.value,
                        onDismissRequest = { deviceExpanded.value = false },
                        modifier = Modifier
                            .width(with(LocalDensity.current) { selectDeviceFieldSize.width.toDp() })
                    ) {
                        transferTypes.forEach { type ->
                            DropdownMenuItem(
                                onClick = {
                                    deviceExpanded.value = false
                                    walletTopUPReportViewModel.transferType.value = type
                                }
                            ) {
                                Text(
                                    modifier = Modifier.padding(12.dp),
                                    text = type,
                                    style = TextStyle(
                                        color = Color.Black,
                                        fontSize = 15.sp,
                                        textAlign = TextAlign.Center
                                    )
                                )

                            }
                        }
                    }
                }*/

                    OutlinedTextField(
                        value = walletTopUPReportViewModel.transferType.value,
                        onValueChange = {
                            walletTopUPReportViewModel.transferType.value = it
                        },
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(top = 7.dp),
                        label = { Text(text = "Select Transfer Type") },
                        singleLine = true,
                        readOnly = true
                    )
                    Spacer(modifier = Modifier.height(5.dp))


                    OutlinedTextField(
                        value = walletTopUPReportViewModel.amount.value,
                        onValueChange = {
                            walletTopUPReportViewModel.amount.value = it
                        },

                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(top = 5.dp),
                        label = { Text(text = "Enter Amount") },
                        singleLine = true,
                        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
                        textStyle = TextStyle(color = Color.Black)
                    )
                    if (walletTopUPReportViewModel.transferType.value != "Cash Deposit") {
                        OutlinedTextField(
                            value = walletTopUPReportViewModel.accountNumber.value,
                            onValueChange = { walletTopUPReportViewModel.accountNumber.value = it },
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(top = 5.dp),
                            label = { Text(text = "Enter Account Number") },
                            singleLine = true,
                            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number)
                        )
                        OutlinedTextField(
                            value = walletTopUPReportViewModel.senderBankName.value,
                            onValueChange = {
                                walletTopUPReportViewModel.senderBankName.value = it
                            },
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(top = 5.dp),
                            label = { Text(text = "Sender Bank Name") },
                            singleLine = true
                        )
                    }
                    OutlinedTextField(
                        value = walletTopUPReportViewModel.bankReference.value,
                        onValueChange = {
                            walletTopUPReportViewModel.bankReference.value = it
                        },
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(top = 5.dp),
                        label = { Text(text = "Bank Reference") },
                        singleLine = true,
                        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number)
                    )
                    Spacer(modifier = Modifier.height(5.dp))
                    OutlinedButton(modifier = Modifier
                        .fillMaxWidth(),
                        border = BorderStroke(1.dp, Color.Black),
                        colors = ButtonDefaults.outlinedButtonColors(
                            backgroundColor = whiteColor,
                            contentColor = Color.Black
                        ),
                        onClick = {
                            activityResultLauncher.launch("application/pdf|image/*")

                        }) {

                        Row(verticalAlignment = Alignment.CenterVertically) {
                            if (walletTopUPReportViewModel.transferType.value == "Cash Deposit") {
                                walletTopUPReportViewModel.hints.value = "Upload Document"
                            } else {
                                walletTopUPReportViewModel.hints.value = "Upload Document(Optional)"
                            }

                            if (isLoading.value) {
                                CircularProgressIndicator(color = primaryColor)
                            } else {
                                Text(
                                    text = walletTopUPReportViewModel.getFileName.value.ifEmpty {  walletTopUPReportViewModel.hints.value  },
                                    color = Color.Black,
                                    modifier = Modifier.padding(10.dp),
                                            style = TextStyle(
                                            fontSize = 15.sp,
                                    textAlign = TextAlign.Start
                                )
                                )
                            }
                        }
                    }
                    val selectedDate by remember { mutableStateOf(Calendar.getInstance()) }
                    val dateFormat = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())

                    OutlinedButton(modifier = Modifier
                        .padding(top = 5.dp)
                        .fillMaxWidth(),
                        border = BorderStroke(1.dp, Color.Black),
                        colors = ButtonDefaults.outlinedButtonColors(
                            backgroundColor = whiteColor,
                            contentColor = Color.Black
                        ),
                        onClick = {
                            val year = selectedDate.get(Calendar.YEAR)
                            val month = selectedDate.get(Calendar.MONTH)
                            val day = selectedDate.get(Calendar.DAY_OF_MONTH)

                            val datePickerDialog = DatePickerDialog(
                                context,
                                { _, year, monthOfYear, dayOfMonth ->
                                    selectedDate.set(year, monthOfYear, dayOfMonth)
                                    walletTopUPReportViewModel.selectedDate.value =
                                        dateFormat.format(selectedDate.time)
                                },
                                year,
                                month,
                                day
                            )

                            datePickerDialog.show()

                        }) {
                        Text(

                            text = walletTopUPReportViewModel.selectedDate.value.ifEmpty { "Date" },
                            color = Color.Black,
                            modifier = Modifier.padding(10.dp),
                            textAlign = TextAlign.Start
                        )
                    }


                    OutlinedTextField(
                        value = walletTopUPReportViewModel.remark.value,
                        onValueChange = { walletTopUPReportViewModel.remark.value = it },
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(top = 5.dp),
                        label = { Text(text = "Enter Remark") },
                        maxLines = 3
                    )

                    OutlinedButton(modifier = Modifier
                        .padding(vertical = 5.dp)
                        .fillMaxWidth(),
                        border = BorderStroke(1.dp, primaryColor),
                        colors = ButtonDefaults.outlinedButtonColors(
                            backgroundColor = primaryColor,
                            contentColor = Color.White
                        ),
                        onClick = {
                            val validation = validateFields(walletTopUPReportViewModel)
                            isValid.value = validation.first
                            val message = validation.second

                            if (isValid.value) {
                                val walletTopUpRequestReqModel = WalletTopUpRequestReqModel(
                                    walletTopUPReportViewModel.getEditTrnsId.value,
                                    walletTopUPReportViewModel.amount.value,
                                    walletTopUPReportViewModel.senderBankName.value,
                                    walletTopUPReportViewModel.accountNumber.value,
                                    walletTopUPReportViewModel.receiverBank.value,
                                    walletTopUPReportViewModel.bankReference.value,
                                    SharedPrefClass.getStringValue(context, DataConstants.USERNAME),
                                    walletTopUPReportViewModel.transactionType.value,
                                    walletTopUPReportViewModel.senderName.value,
                                    walletTopUPReportViewModel.selectedDate.value,
                                    walletTopUPReportViewModel.receiptLink.value.ifEmpty { "" },
                                    walletTopUPReportViewModel.transferType.value,
                                    "MOBUSER",
                                    walletTopUPReportViewModel.remark.value
                                )
                                walletTopUPReportViewModel.getwalletTopUpRequest(
                                    SharedPrefClass.getStringValue(
                                        context,
                                        "update_wallettopup_request"
                                    ).toString(), walletTopUpRequestReqModel
                                )
                            } else {
                                showToast(context, message)
                            }

                        }) {
                        Row(verticalAlignment = Alignment.CenterVertically) {
                            if (walletTopUpRequestState.value.loading == true) {
                                CircularProgressIndicator(
                                    color = whiteColor,
                                    modifier = Modifier.size(30.dp),
                                    strokeWidth = 3.dp
                                )
                            } else {
                                Text(
                                    text = "Submit",
                                    color = Color.White,
                                    modifier = Modifier.padding(5.dp)
                                )
                            }
                        }
                        Spacer(modifier = Modifier.height(5.dp))
                    }

                }

                /**
                 * update_wallettopup_request Api Call
                 */

                LaunchedEffect(key1 = walletTopUpRequestState.value) {
                    if (walletTopUpRequestState.value.walletTopUpRequestRespModel != null) {
                        showToast(
                            context,
                            walletTopUpRequestState.value.walletTopUpRequestRespModel!!.message.toString()
                        )
                    }
                    if (walletTopUpRequestState.value.error != null) {
                        showToast(context, walletTopUpRequestState.value.error.toString())
                    }
                }
                /**
                 * retailerBank/fetch Api Call
                 */

                if (walletTopUpFetchUserState.value.loading == true) {
                    Box(
                        modifier = Modifier.fillMaxSize(),
                        contentAlignment = Alignment.Center
                    ) {
                        CircularProgressIndicator(color = primaryColor)
                    }
                }

                LaunchedEffect(key1 = walletTopUpFetchUserState.value) {
                    if (walletTopUpFetchUserState.value.walletTopUpFetchUserRespModel != null) {
                        walletTopUpFetchUserState.value.walletTopUpFetchUserRespModel.let { getData ->
                            showToast(context, getData?.errorMessage.toString())
                        }
                    }
                    if (walletTopUpFetchUserState.value.error != null) {
                        showToast(context, walletTopUpFetchUserState.value.error.toString())
                    }
                }


                /**
                 * fetch_updateWalletTopupTxn Api
                 */

                if (walletTopUpReportState.value.loading == true) {

                    Box(
                        modifier = Modifier.fillMaxSize(),
                        contentAlignment = Alignment.Center
                    ) {
                        CircularProgressIndicator(color = primaryColor)
                    }

                }
                LaunchedEffect(key1 = walletTopUpReportState.value) {
                    if (walletTopUpReportState.value.fetchWalletTopUpTxnRespModel != null) {
                        walletTopUpReportState.value.fetchWalletTopUpTxnRespModel?.let { walletReport ->
                            walletTopUPReportViewModel.receiverBank.value =
                                walletReport.data?.transactionDetils?.receiverBankName.toString()
                            walletTopUPReportViewModel.transactionType.value =
                                walletReport.data?.transactionDetils?.transactionType.toString()
                            walletTopUPReportViewModel.transferType.value =
                                walletReport.data?.transactionDetils?.txnType.toString()
                            walletTopUPReportViewModel.accountNumber.value =
                                walletReport.data?.transactionDetils?.senderAccount.toString()
                            walletTopUPReportViewModel.senderBankName.value =
                                walletReport.data?.transactionDetils?.senderBankName.toString()
                            walletTopUPReportViewModel.bankReference.value =
                                walletReport.data?.transactionDetils?.bnkRefId.toString()
                            walletTopUPReportViewModel.selectedDate.value =
                                walletReport.data?.transactionDetils?.depositeDateTime.toString()
                            walletTopUPReportViewModel.amount.value =
                                walletReport.data?.transactionDetils?.amount.toString()
                            walletTopUPReportViewModel.senderName.value =
                                walletReport.data?.transactionDetils?.senderName.toString()

                        }
                    }
                    if (walletTopUpReportState.value.error != null) {
                        showToast(context, walletTopUpReportState.value.error.toString())
                    }
                }

            }
        }
    }

}

fun getFileName(context: Context, uri: Uri): String? {
    var result: String?
    var getUri = uri
    // If the URI is content://
    try {
        if (getUri.scheme != null && getUri.scheme == "content") {
            val cursor = context.contentResolver.query(getUri, null, null, null, null)
            cursor.use { cursor ->
                if (cursor != null && cursor.moveToFirst()) {
                    // Local filesystem
                    var index = cursor.getColumnIndex("_data")
                    if (index == -1)
                    // Google Drive
                        index = cursor.getColumnIndex("_display_name")
                    result = cursor.getString(index)
                    if (result != null)
                        getUri = Uri.parse(result)
                    else
                        return null
                }
            }
        }
        result = getUri.path
        // Get filename + extension from the path
        val cut = result?.lastIndexOf('/')
        if (cut != -1)
            result = result?.substring(cut!! + 1)
        return result
    } catch (e: Exception) {
        result = e.localizedMessage
    }
    return result
}
fun uploadInfireabse(
    context: Context,
    imageuri: Uri,
    walletTopUPReportViewModel: WalletTopUPReportViewModel,
    isLoading: MutableState<Boolean>
) {
    isLoading.value = true
    val firebaseStorage: FirebaseStorage =
        FirebaseStorage.getInstance("gs://uploadpdf-a1eb1.appspot.com")
    val storageReference = firebaseStorage.reference
    val filepath: StorageReference = storageReference.child(
        "A_WALLET_TOP_UP/${
            SharedPrefClass.getStringValue(
                context,
                DataConstants.USERNAME
            )
        }" + "/" + SharedPrefClass.getStringValue(
            context,
            DataConstants.USERNAME
        ) + imageuri.lastPathSegment
    )


    filepath.putFile(imageuri).addOnSuccessListener {
        if (it.task.isSuccessful) {
            val uri: Uri? = it.task.result.uploadSessionUri
            walletTopUPReportViewModel.receiptLink.value = uri.toString()
            isLoading.value = false
            Toast.makeText(context, "Uploaded Successfully", Toast.LENGTH_SHORT).show()

        }
    }.addOnFailureListener { exception ->
        isLoading.value = false
        Toast.makeText(context, exception.toString(), Toast.LENGTH_SHORT).show()
    }

}

fun showToast(context: Context, message: String) {
    Toast.makeText(context, message, Toast.LENGTH_LONG).show()
}

private fun validateFields(
    walletTopUPReportViewModel: WalletTopUPReportViewModel
): Pair<Boolean, String> {
    var isValid = true
    var validationMessage = ""

    if (walletTopUPReportViewModel.receiverBank.value.isBlank()) {
        isValid = false
        validationMessage += "Please Enter ReceiverBank.\n"
    }

    if (walletTopUPReportViewModel.transferType.value.isBlank()) {
        isValid = false
        validationMessage += "Please Enter TransferType.\n"
    }

    if (walletTopUPReportViewModel.amount.value.isBlank()) {
        isValid = false
        validationMessage += "Please Enter Amount.\n"
    }
    if (walletTopUPReportViewModel.accountNumber.value == "") {
        isValid = false
        validationMessage += "Please Enter AccountNumber.\n"

    }
    if (walletTopUPReportViewModel.senderBankName.value == "") {
        isValid = false
        validationMessage += "Please Enter SenderBankName.\n"

    }

    if (walletTopUPReportViewModel.bankReference.value.isBlank()) {
        isValid = false
        validationMessage += "Please Enter BankReference.\n"
    }

    if (walletTopUPReportViewModel.selectedDate.value.isBlank()) {
        isValid = false
        validationMessage += "Please Select Date.\n"
    }

    if (walletTopUPReportViewModel.remark.value.isBlank()) {
        isValid = false
        validationMessage += "Please Enter Remark.\n"
    }

    if (walletTopUPReportViewModel.transferType.value == "Cash Deposit" && walletTopUPReportViewModel.getFileName.value.isEmpty()) {
        isValid = false
        validationMessage += "Please Upload Document.\n"
    }
    return Pair(isValid, validationMessage.trim())
}
