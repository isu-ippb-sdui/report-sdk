package com.commonadmin.report.presentation.reportscreens.bbps

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Text
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import com.commonadmin.report.data.remote.models.BBPSReportItem
import com.commonadmin.report.data.remote.models.BBPSReportReqModel
import com.commonadmin.report.presentation.viewmodels.BBPSReportViewModel
import com.commonadmin.report.ui.theme.primaryColor
import com.commonadmin.report.utils.DataConstants
import com.commonadmin.report.utils.SharedPrefClass
import com.commonadmin.report.utils.reportcalender.ReportCalenderConstant
import java.util.Locale

@Composable
fun BBPSReportScreen(bbpsReportViewModel: BBPSReportViewModel) {
    val bbpsReportState = bbpsReportViewModel.bbpsReportState.collectAsState()
    val context = LocalContext.current
    LaunchedEffect(key1 = true) {

        val json6ArrayList: ArrayList<String> = ArrayList()
        json6ArrayList.add("BBPS")

        val json7ArrayList: ArrayList<String> = ArrayList()
        json7ArrayList.add("BBPS_LPG GAS")
        json7ArrayList.add("BBPS_INSURANCE")
        json7ArrayList.add("BBPS_HOUSING SOCIETY")
        json7ArrayList.add("BBPS_HEALTH INSURANCE")
        json7ArrayList.add("BBPS_GAS")
        json7ArrayList.add("BBPS_FASTAG")
        json7ArrayList.add("BBPS_ELECTRICITY")
        json7ArrayList.add("BBPS_EDUCATION FEES")
        json7ArrayList.add("BBPS_DATACARD")
        json7ArrayList.add("BBPS_DTH")
        json7ArrayList.add("BBPS_CREDIT CARD")
        json7ArrayList.add("BBPS_CABLE TV")
        json7ArrayList.add("BBPS_BROADBAND POSTPAID")
        json7ArrayList.add("BBPS")
        json7ArrayList.add("BBPS_INSURANCE")
        json7ArrayList.add("BBPS_SUBSCRIPTION")
        json7ArrayList.add("BBPS_WATER")
        json7ArrayList.add("BBPS_STAMP DUTY")
        json7ArrayList.add("BBPS_PREPAIDMOBILE")
        json7ArrayList.add("BBPS_MUNICIPAL TAXES")
        json7ArrayList.add("BBPS_MOBILE POSTPAID")
        json7ArrayList.add("BBPS_LOAN REPAYMENT")
        json7ArrayList.add("BBPS_LIFE INSURANCE")
        json7ArrayList.add("BBPS_LANDLINE POSTPAID")


        val bbpsReportReqModel = BBPSReportReqModel(
            "all_transaction_report_common",
            SharedPrefClass.getStringValue(context, DataConstants.USERNAME),
            "All",
            ReportCalenderConstant.selectFromDate,
            ReportCalenderConstant.selectToDate,
            json6ArrayList,
            json7ArrayList
        )

        if (bbpsReportState.value.bbpsReportRespModel == null) {
            bbpsReportViewModel.getBBPSReportData(
                SharedPrefClass.getStringValue(context, "BBPSReport").toString(),
                bbpsReportReqModel,SharedPrefClass.getStringValue(context,DataConstants.TOKEN).toString()
            )
        }
    }

    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.TopCenter
    ) {
        if (bbpsReportState.value.loading == true && bbpsReportState.value.bbpsReportRespModel == null) {
            Box(modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ){
                CircularProgressIndicator(color = primaryColor)
            }
        } else if (bbpsReportState.value.loading == false && bbpsReportState.value.bbpsReportRespModel != null) {
            bbpsReportState.value.bbpsReportRespModel?.let { walletReport ->

                val filteredList: ArrayList<BBPSReportItem?> = arrayListOf()
                val reportList: List<BBPSReportItem?>? = walletReport.results?.report

                var trnsAmount = 0.0
                for (report in reportList?.indices!!) {
                    if(reportList[report]?.status?.equals("SUCCESS",true) == true) {
                        trnsAmount += reportList[report]?.amountTransacted!!
                    }
                }
                val formatted = String.format("%.2f", trnsAmount)
                bbpsReportViewModel.amountTransacted.value = formatted


                Column {
                    LazyColumn(
                        contentPadding = PaddingValues(0.dp),
                        verticalArrangement = Arrangement.spacedBy(10.dp),
                        content = {
                            if (bbpsReportViewModel.searchText.value.isEmpty()) {
                                bbpsReportViewModel.totalLength.value =
                                    reportList.size.toString()
                                items(reportList.size) { item ->
                                    BBPSReportRecycleScreen(walletReport.results.report[item])
                                }
                            } else {
                                for (report in walletReport.results.report) {
                                    if (report.id?.lowercase(Locale.ROOT)!!
                                            .contains(
                                                bbpsReportViewModel.searchText.value.lowercase(
                                                    Locale.ROOT
                                                )
                                            ) ||
                                        report.status?.lowercase(Locale.ROOT)!!.contains(
                                            bbpsReportViewModel.searchText.value.lowercase(
                                                Locale.ROOT
                                            )
                                        ) ||
                                        report.operationPerformed?.lowercase(Locale.ROOT)!!.contains(
                                            bbpsReportViewModel.searchText.value.lowercase(
                                                Locale.ROOT
                                            )
                                        )

                                    // report.apiTid?.lowercase(Locale.ROOT)!!.contains(reportViewModel.searchText.value.lowercase(Locale.ROOT))*/

                                    ) {
                                        filteredList.add(report)
                                    }
                                }
                                filteredList.let {
                                    bbpsReportViewModel.totalLength.value = it.size.toString()
                                    var ammount = 0.0
                                    for (report in it.indices) {
                                        if(filteredList[report]?.status?.equals("SUCCESS",true) == true) {
                                            ammount += filteredList[report]?.amountTransacted!!
                                        }
                                    }
                                    val filFormatted = String.format("%.2f", ammount)
                                    bbpsReportViewModel.amountTransacted.value = filFormatted
                                    items(it.size) { item ->
                                        BBPSReportRecycleScreen(it[item]!!)
                                    }
                                }
                            }
                        }
                    )
                    if (bbpsReportViewModel.searchText.value != "" && filteredList.isEmpty() || reportList.isEmpty()) {
                        Box(modifier = Modifier.fillMaxSize(),
                            contentAlignment = Alignment.Center
                        ){
                            Text(text = "No Data Available")
                        }
                    }
                }
            }
        } else {
            Box(modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ){
                Text(text = "Something went to wrong")
            }
        }
    }

}