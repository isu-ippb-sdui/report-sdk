package com.commonadmin.report.presentation.viewmodels

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.commonadmin.report.data.remote.models.Commission1ReportReqModel
import com.commonadmin.report.data.remote.models.Commission1ReportRespModel
import com.commonadmin.report.domain.usercases.Commission1ReportUseCase
import com.commonadmin.report.utils.ApiResult
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class Commission1ReportViewModel @Inject constructor(private val userCases: Commission1ReportUseCase) :
    ViewModel() {
    var commission1ReportState: MutableStateFlow<Commission1ReportState> =
        MutableStateFlow(Commission1ReportState())
        private set

    var isVisible: MutableState<Boolean> = mutableStateOf(false)
        private set
    var searchText: MutableState<String> = mutableStateOf("")
        private set
    var totalLength: MutableState<String> = mutableStateOf("")
        private set
    var amountTransacted: MutableLiveData<String> = MutableLiveData("")
        private set


    fun getCommission1ReportData(url:String, commission1ReportReqModel: Commission1ReportReqModel, token:String) {
        viewModelScope.launch {
            userCases.invoke(url=url,commission1ReportReqModel = commission1ReportReqModel, token = token)
                .collect { networkResult ->
                    when (networkResult) {
                        is ApiResult.Success -> {
                            commission1ReportState.emit(
                                commission1ReportState.value.copy(
                                    commission1ReportRespModel = networkResult.data,
                                    loading = false,
                                    error = null
                                )
                            )
                        }

                        is ApiResult.Loading -> {
                            commission1ReportState.emit(
                                commission1ReportState.value.copy(
                                    commission1ReportRespModel = null,
                                    loading = true,
                                    error = null
                                )
                            )
                        }

                        is ApiResult.Error -> {
                            commission1ReportState.emit(
                                commission1ReportState.value.copy(
                                    commission1ReportRespModel = null,
                                    loading = false,
                                    error = networkResult.message
                                )
                            )
                        }
                    }

                }

        }
    }


}

data class Commission1ReportState(
    var commission1ReportRespModel: Commission1ReportRespModel?=null,
    val loading: Boolean? = null,
    val error: String? = null
)
