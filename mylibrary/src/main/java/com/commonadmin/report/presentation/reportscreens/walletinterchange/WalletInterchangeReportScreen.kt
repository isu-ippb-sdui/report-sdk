package com.commonadmin.report.presentation.reportscreens.walletinterchange

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Text
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import com.commonadmin.report.data.remote.models.WalletInterchangeReportItem
import com.commonadmin.report.data.remote.models.WalletInterchangeReqModel
import com.commonadmin.report.presentation.viewmodels.WalletInterchangeReportViewModel
import com.commonadmin.report.ui.theme.primaryColor
import com.commonadmin.report.utils.DataConstants
import com.commonadmin.report.utils.SharedPrefClass
import com.commonadmin.report.utils.reportcalender.ReportCalenderConstant
import java.util.Locale

@Composable
fun WalletCashoutReportScreen(walletInterchangeReportViewModel: WalletInterchangeReportViewModel) {
    val walletInterchangeReportState =
        walletInterchangeReportViewModel.walletInterchangeReportState.collectAsState()
    val context = LocalContext.current

    LaunchedEffect(key1 = true) {
        val json6ArrayList: ArrayList<String> = ArrayList()
        json6ArrayList.add("WALLET_INTERCHANGE")

        val walletInterchangeReqModel = WalletInterchangeReqModel(
            "wallet_interchange_new_common",
            SharedPrefClass.getStringValue(context, DataConstants.USERNAME),
            ReportCalenderConstant.selectFromDate,
            ReportCalenderConstant.selectToDate,
            json6ArrayList
        )

        if (walletInterchangeReportState.value.walletInterchangeRespModel == null) {
            walletInterchangeReportViewModel.getWalletInterchangeReportData(
                SharedPrefClass.getStringValue(context, "WalletInterchangeReport").toString(),
                walletInterchangeReqModel,SharedPrefClass.getStringValue(context,DataConstants.TOKEN).toString()
            )
        }
    }

    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.TopCenter
    ) {
        if (walletInterchangeReportState.value.loading == true && walletInterchangeReportState.value.walletInterchangeRespModel == null) {
            Box(modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ){
                CircularProgressIndicator(color = primaryColor)
            }
        } else if (walletInterchangeReportState.value.loading == false && walletInterchangeReportState.value.walletInterchangeRespModel != null) {
            walletInterchangeReportState.value.walletInterchangeRespModel?.let { walletReport ->

                val filteredList: ArrayList<WalletInterchangeReportItem?> = arrayListOf()
                val reportList: List<WalletInterchangeReportItem?>? = walletReport.report
                var trnsAmount = 0.0
                for (report in reportList?.indices!!) {
                    if (reportList[report]?.w1Status?.equals("SUCCESS", true) == true) {
                        trnsAmount += reportList[report]?.w1AmountTransacted!!
                    }
                }
                // val formatted = String.format("%.4f", trnsAmount)
                walletInterchangeReportViewModel.amountTransacted.value = trnsAmount.toString()


                Column {
                    LazyColumn(
                        contentPadding = PaddingValues(0.dp),
                        verticalArrangement = Arrangement.spacedBy(5.dp),
                        content = {
                            if (walletInterchangeReportViewModel.searchText.value.isEmpty()) {
                                walletInterchangeReportViewModel.totalLength.value =
                                    reportList.size.toString()
                                items(reportList.size) { item ->
                                    WalletInterchangeRecycleScreen(walletReport.report[item])
                                }
                            } else {
                                for (report in walletReport.report) {
                                    if (report.w1Id?.lowercase(Locale.ROOT)!!.contains(
                                            walletInterchangeReportViewModel.searchText.value.lowercase(
                                                Locale.ROOT
                                            )
                                        ) ||
                                        report.w2Id?.lowercase(Locale.ROOT)!!.contains(
                                            walletInterchangeReportViewModel.searchText.value.lowercase(
                                                Locale.ROOT
                                            )
                                        ) ||
                                        report.w1Status?.lowercase(Locale.ROOT)!!.contains(
                                            walletInterchangeReportViewModel.searchText.value.lowercase(
                                                Locale.ROOT
                                            )
                                        ) ||
                                        report.w2Status?.lowercase(Locale.ROOT)!!.contains(
                                            walletInterchangeReportViewModel.searchText.value.lowercase(
                                                Locale.ROOT
                                            )
                                        ) ||
                                        report.w1TransactionType?.lowercase(Locale.ROOT)!!.contains(
                                            walletInterchangeReportViewModel.searchText.value.lowercase(
                                                Locale.ROOT
                                            )
                                        ) ||
                                        report.w2TransactionType?.lowercase(Locale.ROOT)!!.contains(
                                            walletInterchangeReportViewModel.searchText.value.lowercase(
                                                Locale.ROOT
                                            )
                                        )
                                    // report.apiTid?.lowercase(Locale.ROOT)!!.contains(reportViewModel.searchText.value.lowercase(Locale.ROOT))*/

                                    ) {
                                        filteredList.add(report)
                                    }
                                }
                                filteredList.let {
                                    walletInterchangeReportViewModel.totalLength.value =
                                        it.size.toString()
                                    var ammount = 0.0
                                    for (report in it.indices) {
                                        if (filteredList[report]?.w1Status?.equals(
                                                "SUCCESS",
                                                true
                                            ) == true
                                        ) {
                                            ammount += filteredList[report]?.w1AmountTransacted!!
                                        }
                                    }
                                    walletInterchangeReportViewModel.amountTransacted.value =
                                        ammount.toString()
                                    items(it.size) { item ->
                                        WalletInterchangeRecycleScreen(it[item]!!)
                                    }
                                }
                            }
                        }
                    )
                    if (walletInterchangeReportViewModel.searchText.value != "" && filteredList.isEmpty() || reportList.isEmpty()) {
                        Box(modifier = Modifier.fillMaxSize(),
                            contentAlignment = Alignment.Center
                        ){
                            Text(text = "No Data Available")
                        }
                    }
                }
            }
        } else {
            Box(modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ){
                Text(text = "Something went to wrong")
            }
        }
    }

}