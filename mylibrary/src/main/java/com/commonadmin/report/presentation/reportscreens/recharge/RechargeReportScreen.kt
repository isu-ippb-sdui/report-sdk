package com.commonadmin.report.presentation.reportscreens.recharge

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Text
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import com.commonadmin.report.data.remote.models.RechargeReportItem
import com.commonadmin.report.data.remote.models.RechargeReqModel
import com.commonadmin.report.presentation.viewmodels.RechargeReportViewModel
import com.commonadmin.report.ui.theme.primaryColor
import com.commonadmin.report.utils.DataConstants
import com.commonadmin.report.utils.SharedPrefClass
import com.commonadmin.report.utils.reportcalender.ReportCalenderConstant
import java.util.Locale

@Composable
fun RechargeReportScreen(rechargeReportViewModel: RechargeReportViewModel) {
    val rechargeReportState = rechargeReportViewModel.rechargeReportState.collectAsState()
    val context = LocalContext.current
    LaunchedEffect(key1 = true) {

        val json6ArrayList: ArrayList<String> = ArrayList()
        json6ArrayList.add("Recharge")

  val json7ArrayList: ArrayList<String> = ArrayList()
        json7ArrayList.add("Postpaid_Vodafone")
        json7ArrayList.add("Postpaid_Idea")
        json7ArrayList.add("Postpaid_Bsnl")
        json7ArrayList.add("Postpaid_Airtel")
        json7ArrayList.add("Prepaid_Mtnl")
        json7ArrayList.add("Prepaid_Mts")
        json7ArrayList.add("Prepaid_Virgincdma")
        json7ArrayList.add("Prepaid_VirginGsm")
        json7ArrayList.add("Prepaid_Telenor")
        json7ArrayList.add("Prepaid_Aircel")
        json7ArrayList.add("Prepaid_TataDocomo")
        json7ArrayList.add("Prepaid_TataIndicom")
        json7ArrayList.add("Prepaid_RelianceJio")
        json7ArrayList.add("Prepaid_VodafoneIdea")
        json7ArrayList.add("Prepaid_Vodafone")
        json7ArrayList.add("Prepaid_Idea")
        json7ArrayList.add("Prepaid_Bsnl")
        json7ArrayList.add("Prepaid_Airtel")
        json7ArrayList.add("DTH_JioDth")
        json7ArrayList.add("DTH_SunTvDth")
        json7ArrayList.add("DTH_VideoconDth")
        json7ArrayList.add("DTH_TataSkyDth")
        json7ArrayList.add("DTH_DishTvDth")
        json7ArrayList.add("DTH_BigTvDth")
        json7ArrayList.add("DTH_AirtelDth")
        json7ArrayList.add("Postpaid_Mtnl")
        json7ArrayList.add("Postpaid_Mts")
        json7ArrayList.add("Postpaid_Virgincdma")
        json7ArrayList.add("Postpaid_VirginGsm")
        json7ArrayList.add("Postpaid_Aircel")
        json7ArrayList.add("Postpaid_TataDocomo")
        json7ArrayList.add("Postpaid_TataIndicom")
        json7ArrayList.add("Postpaid_RelianceJio")
        json7ArrayList.add("Postpaid_VodafoneIdea")




        val rechargeReqModel = RechargeReqModel(
            "all_transaction_report_common",
            SharedPrefClass.getStringValue(context, DataConstants.USERNAME),
            "All",
            ReportCalenderConstant.selectFromDate,
            ReportCalenderConstant.selectToDate,
            json6ArrayList,
            json7ArrayList

        )

        if (rechargeReportState.value.rechargeReportRespModel == null) {
            rechargeReportViewModel.getRechargeReportData(
                SharedPrefClass.getStringValue(context, "RechargeReport").toString(),
                rechargeReqModel,SharedPrefClass.getStringValue(context,DataConstants.TOKEN).toString()
            )
        }
    }

    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.TopCenter
    ) {
        if (rechargeReportState.value.loading == true && rechargeReportState.value.rechargeReportRespModel == null) {
            Box(modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ){
                CircularProgressIndicator(color = primaryColor)
            }
        } else if (rechargeReportState.value.loading == false && rechargeReportState.value.rechargeReportRespModel != null) {
            rechargeReportState.value.rechargeReportRespModel?.let { walletReport ->

                val filteredList: ArrayList<RechargeReportItem?> = arrayListOf()
                val reportList: List<RechargeReportItem?>? = walletReport.results?.bQReport

                var trnsAmount = 0.0
                for (report in reportList?.indices!!) {
                    if(reportList[report]?.status?.equals("SUCCESS",true) == true) {
                        trnsAmount += reportList[report]?.amountTransacted!!
                    }
                }
                val formatted = String.format("%.2f", trnsAmount)
                rechargeReportViewModel.amountTransacted.value = formatted


                Column {
                    LazyColumn(
                        contentPadding = PaddingValues(0.dp),
                        verticalArrangement = Arrangement.spacedBy(10.dp),
                        content = {
                            if (rechargeReportViewModel.searchText.value.isEmpty()) {
                                rechargeReportViewModel.totalLength.value =
                                    reportList.size.toString()
                                items(reportList.size) { item ->
                                    RechargeReportRecycleScreen(walletReport.results.bQReport[item])
                                }
                            } else {
                                for (report in walletReport.results.bQReport) {
                                    if (report.operationPerformed?.lowercase(Locale.ROOT)!!
                                            .contains(
                                                rechargeReportViewModel.searchText.value.lowercase(
                                                    Locale.ROOT
                                                )
                                            ) ||
                                        report.status?.lowercase(Locale.ROOT)!!.contains(
                                            rechargeReportViewModel.searchText.value.lowercase(
                                                Locale.ROOT
                                            )
                                        ) ||
                                        report.id?.lowercase(Locale.ROOT)!!.contains(
                                            rechargeReportViewModel.searchText.value.lowercase(
                                                Locale.ROOT
                                            )
                                        )||
                                        report.referenceNo?.lowercase(Locale.ROOT)!!.contains(
                                            rechargeReportViewModel.searchText.value.lowercase(
                                                Locale.ROOT
                                            )
                                        )||
                                        report.apiTid?.lowercase(Locale.ROOT)?.contains(rechargeReportViewModel.searchText.value.lowercase(
                                            Locale.ROOT
                                        )) == true
                                    ) {
                                        filteredList.add(report)
                                    }
                                }
                                filteredList.let {
                                    rechargeReportViewModel.totalLength.value = it.size.toString()
                                    var ammount = 0.0
                                    for (report in it.indices) {
                                        if(filteredList[report]?.status?.equals("SUCCESS",true) == true) {
                                            ammount += filteredList[report]?.amountTransacted!!
                                        }
                                    }
                                    val filFormatted = String.format("%.2f", ammount)
                                    rechargeReportViewModel.amountTransacted.value = filFormatted
                                    items(it.size) { item ->
                                        RechargeReportRecycleScreen(it[item]!!)
                                    }
                                }
                            }
                        }
                    )
                    if (rechargeReportViewModel.searchText.value != "" && filteredList.isEmpty() || reportList.isEmpty()) {
                        Box(modifier = Modifier.fillMaxSize(),
                            contentAlignment = Alignment.Center
                        ){
                            Text(text = "No Data Available")
                        }
                    }
                }
            }
        } else {
            Box(modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ){
                Text(text = "Something went to wrong")
            }
        }
    }

}