package com.commonadmin.report.presentation.reportscreens.walletcashout

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Divider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.commonadmin.report.data.remote.models.DmtGateWayReportItem
import com.commonadmin.report.data.remote.models.WalletCashOutReportItem
import com.commonadmin.report.ui.theme.Green
import com.commonadmin.report.ui.theme.Yellow
import com.commonadmin.report.ui.theme.lightGrey
import com.commonadmin.report.ui.theme.primaryColor
import java.text.SimpleDateFormat
import java.util.Locale

@Composable
fun WalletCashoutReportRecycleScreen(walletCashoutReportItem: WalletCashOutReportItem) {

    val timeT: Long = walletCashoutReportItem.createdDate!!.toLong()
    val formatter = SimpleDateFormat("dd-MM-yyyy  hh:mm a")
    val timeDate = formatter.format(timeT)

    val isExpand = remember {
        mutableStateOf(false)
    }

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .wrapContentHeight()
            .padding(10.dp)
            .clickable {
                isExpand.value = !isExpand.value
            }
            .background(color = if (isExpand.value) lightGrey else Color.White)
    ) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Text(
                text = "Date/Time: $timeDate",
                color = Color.Black,
                style = MaterialTheme.typography.headlineLarge,
                fontSize = 15.sp,
                textAlign = TextAlign.Center,
                modifier = Modifier.padding(start = 10.dp)
            )
            Text(
                text = walletCashoutReportItem.status.toString(),
                color = when (walletCashoutReportItem.status?.uppercase(Locale.ROOT)) {
                    "REFUNDED" -> {
                        Yellow
                    }

                    "SUCCESS" -> {
                        Green
                    }

                    "FAILED" -> {
                        Color.Red
                    }

                    else -> {
                        Color.Black
                    }
                },
                style = MaterialTheme.typography.headlineLarge,
                fontSize = 15.sp,
                modifier = Modifier.padding(end = 5.dp)
            )
        }
        Spacer(modifier = Modifier.height(3.dp))
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Text(
                text = "Txn ID: ${walletCashoutReportItem.id}",
                color = Color.Black,
                style = MaterialTheme.typography.headlineLarge,
                fontSize = 15.sp,
                modifier = Modifier.padding(start = 10.dp)
            )
            Text(
                text = "₹" + walletCashoutReportItem.amountTransacted.toString(),
                color = Color.Black,
                style = MaterialTheme.typography.headlineLarge,
                fontSize = 15.sp,
                modifier = Modifier.padding(end = 5.dp)
            )
        }
        Spacer(modifier = Modifier.height(3.dp))
        Text(
            text = "Txn Type: ${walletCashoutReportItem.operationPerformed}",
            color = Color.Black,
            style = MaterialTheme.typography.headlineLarge,
            fontSize = 15.sp,
            modifier = Modifier.padding(start = 10.dp)
        )
        Spacer(modifier = Modifier.height(10.dp))
        AnimatedVisibility(visible = isExpand.value) {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .wrapContentHeight()
                    .padding(10.dp)
            ) {
        Text(
            text = "RRN No: ${walletCashoutReportItem.apiTid?.let { "$it" } ?: "N/A"}",
            color = Color.Black,
            style = MaterialTheme.typography.headlineLarge,
            fontSize = 15.sp
        )
        Spacer(modifier = Modifier.height(3.dp))
        Text(
            text = "Previous Balance: ${walletCashoutReportItem.previousAmount?.let { "₹$it" } ?: "N/A"}",
            color = Color.Black,
            style = MaterialTheme.typography.headlineLarge,
            fontSize = 15.sp
        )
        Spacer(modifier = Modifier.height(3.dp))
        Text(
            text = "Current Balance: ${walletCashoutReportItem.balanceAmount?.let { "₹$it" } ?: "N/A"}",
            color = Color.Black,
            style = MaterialTheme.typography.headlineLarge,
            fontSize = 15.sp
        )
        Spacer(modifier = Modifier.height(3.dp))
        Text(
            text = "To Account: ${walletCashoutReportItem.toAccount}",
            color = Color.Black,
            style = MaterialTheme.typography.headlineLarge,
            fontSize = 15.sp
        )
    }
}
        Spacer(modifier = Modifier.height(3.dp))

        Divider(
            thickness = 1.dp,
            color = Color.Gray
        )
    }
}