package com.commonadmin.report.presentation.viewmodels

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.commonadmin.report.data.remote.models.Matm2ReportItem
import com.commonadmin.report.data.remote.models.Matm2ReqModel
import com.commonadmin.report.data.remote.models.Matm2RespModel
import com.commonadmin.report.domain.usercases.Matm2ReportUseCase
import com.commonadmin.report.utils.ApiResult
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class Matm2ReportViewModel @Inject constructor(private val userCases: Matm2ReportUseCase) :
    ViewModel() {
    var matm2ReportState: MutableStateFlow<Matm2ReportState> =
        MutableStateFlow(Matm2ReportState())
        private set

    var isVisible: MutableState<Boolean> = mutableStateOf(false)
        private set
    var searchText: MutableState<String> = mutableStateOf("")
        private set

    var balanceEnqList: MutableState<List<Matm2ReportItem?>?> =
        mutableStateOf(emptyList())
    var cashWidrawList: MutableState<List<Matm2ReportItem?>?> =
        mutableStateOf(emptyList())
    var reportList: MutableState<List<Matm2ReportItem?>?> =
        mutableStateOf(emptyList())

    var setAllTrnsLength: MutableState<String> = mutableStateOf("")
        private set
    var setcashTrnsLength: MutableState<String> = mutableStateOf("")
        private set
    var setBalanceTrnsLength: MutableState<String> = mutableStateOf("")
        private set

    var setAllTrnsAmount: MutableState<String> = mutableStateOf("")
        private set

    var setBalanceEnqTrnsAmount: MutableState<String> = mutableStateOf("")
        private set

    var setCashWidTrnsAmount: MutableState<String> = mutableStateOf("")
        private set

    fun getMatm2ReportData(url: String, matm2ReqModel: Matm2ReqModel, token: String) {
        viewModelScope.launch {
            userCases.invoke(url = url, matm2ReqModel = matm2ReqModel,token = token)
                .collect { networkResult ->
                    when (networkResult) {
                        is ApiResult.Success -> {
                            matm2ReportState.emit(
                                matm2ReportState.value.copy(
                                    matm2RespModel = networkResult.data,
                                    loading = false,
                                    error = null
                                )
                            )
                        }

                        is ApiResult.Loading -> {
                            matm2ReportState.emit(
                                matm2ReportState.value.copy(
                                    matm2RespModel = null,
                                    loading = true,
                                    error = null
                                )
                            )
                        }

                        is ApiResult.Error -> {
                            matm2ReportState.emit(
                                matm2ReportState.value.copy(
                                    matm2RespModel = null,
                                    loading = false,
                                    error = networkResult.message
                                )
                            )
                        }
                    }

                }

        }
    }


}

data class Matm2ReportState(
    var matm2RespModel: Matm2RespModel? = null,
    val loading: Boolean? = null,
    val error: String? = null
)
