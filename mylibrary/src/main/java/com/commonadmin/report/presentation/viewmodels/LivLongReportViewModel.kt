package com.commonadmin.report.presentation.viewmodels

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope

import com.commonadmin.report.data.remote.models.LivLongReportReqModel
import com.commonadmin.report.data.remote.models.LivLongRespModel
import com.commonadmin.report.domain.usercases.LivLongReportUseCase
import com.commonadmin.report.utils.ApiResult
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LivLongReportViewModel @Inject constructor(private val userCases: LivLongReportUseCase) :
    ViewModel() {
    var livLongReportState: MutableStateFlow<LivLongReportState> =
        MutableStateFlow(LivLongReportState())
        private set

    var isVisible: MutableState<Boolean> = mutableStateOf(false)
        private set
    var searchText: MutableState<String> = mutableStateOf("")
        private set
    var totalLength: MutableState<String> = mutableStateOf("")
        private set
    var amountTransacted: MutableLiveData<String> = MutableLiveData("")
        private set


    fun getLivLongReportData(url: String, livLongReportReqModel: LivLongReportReqModel, token: String) {
        viewModelScope.launch {
            userCases.invoke(url=url,livLongReportReqModel = livLongReportReqModel,token=token)
                .collect { networkResult ->
                    when (networkResult) {
                        is ApiResult.Success -> {
                            livLongReportState.emit(
                                livLongReportState.value.copy(
                                    livLongRespModel = networkResult.data,
                                    loading = false,
                                    error = null
                                )
                            )
                        }

                        is ApiResult.Loading -> {
                            livLongReportState.emit(
                                livLongReportState.value.copy(
                                    livLongRespModel = null,
                                    loading = true,
                                    error = null
                                )
                            )
                        }

                        is ApiResult.Error -> {
                            livLongReportState.emit(
                                livLongReportState.value.copy(
                                    livLongRespModel = null,
                                    loading = false,
                                    error = networkResult.message
                                )
                            )
                        }
                    }

                }

        }
    }


}

data class LivLongReportState(
    var livLongRespModel: LivLongRespModel?=null,
    val loading: Boolean? = null,
    val error: String? = null
)
