package com.commonadmin.report.presentation.reportscreens.commission2

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Divider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.commonadmin.report.data.remote.models.Commission2ReportItem
import com.commonadmin.report.ui.theme.Green
import com.commonadmin.report.ui.theme.Yellow
import java.text.SimpleDateFormat
import java.util.Locale

@Composable
fun Commission2ReportRecycleScreen(commission2ReportItem: Commission2ReportItem) {
    val timeT: Long = commission2ReportItem.createdDate!!.toLong()
    val formatter = SimpleDateFormat("dd-MM-yyyy  hh:mm a")
    val timeDate = formatter.format(timeT)
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(10.dp)

    ) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Text(
                text = "Date/Time: $timeDate",
                color = Color.Black,
                style = MaterialTheme.typography.headlineLarge,
                fontSize = 15.sp,
                textAlign = TextAlign.Center
            )
            Text(
                text = commission2ReportItem.status.toString(),
                color = when (commission2ReportItem.status?.uppercase(Locale.ROOT)) {
                    "REFUNDED" -> {
                        Yellow
                    }

                    "SUCCESS" -> {
                        Green
                    }

                    "FAILED" -> {
                        Color.Red
                    }

                    else -> {
                        Color.Black
                    }
                },
                style = MaterialTheme.typography.headlineLarge,
                fontSize = 15.sp
            )
        }
        Spacer(modifier = Modifier.height(5.dp))
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Text(
                text = "Txn Id: ${commission2ReportItem.id}",
                color = Color.Black,
                style = MaterialTheme.typography.headlineLarge,
                fontSize = 14.sp
            )
            Text(
                text = "₹"+commission2ReportItem.amountTransacted.toString(),
                color = Color.Black,
                style = MaterialTheme.typography.headlineLarge,
                fontSize = 15.sp
            )
        }
        Spacer(modifier = Modifier.height(3.dp))

        Text(
            text = "Relational ID: ${commission2ReportItem.relationalId}",
            color = Color.Black,
            style = MaterialTheme.typography.headlineLarge,
            fontSize = 15.sp
        )
        Spacer(modifier = Modifier.height(3.dp))
        Text(
            text = "Balance Amount: ${commission2ReportItem.balanceAmount?.let { "₹$it" } ?: "N/A"}",
            color = Color.Black,
            style = MaterialTheme.typography.headlineLarge,
            fontSize = 15.sp
        )
        Spacer(modifier = Modifier.height(3.dp))
        Text(
            text = "Previous Amount: ${commission2ReportItem.previousAmount?.let { "₹$it" } ?: "N/A"}",
            color = Color.Black,
            style = MaterialTheme.typography.headlineLarge,
            fontSize = 15.sp
        )
        Spacer(modifier = Modifier.height(3.dp))
        Text(
            text = "Relational Amount: ${"₹"+commission2ReportItem.relationalAmount}",
            color = Color.Black,
            style = MaterialTheme.typography.headlineLarge,
            fontSize = 15.sp
        )
        Spacer(modifier = Modifier.height(3.dp))
        Text(
            text = "Relational Operation: ${commission2ReportItem.relationalOperation}",
            color = Color.Black,
            style = MaterialTheme.typography.headlineLarge,
            fontSize = 15.sp
        )
        Spacer(modifier = Modifier.height(3.dp))
        Text(
            text = "Type: ${commission2ReportItem.type}",
            color = Color.Black,
            style = MaterialTheme.typography.headlineLarge,
            fontSize = 15.sp
        )
        Spacer(modifier = Modifier.height(5.dp))
        Divider(
            thickness = 1.dp,
            color = Color.Gray
        )
    }
}