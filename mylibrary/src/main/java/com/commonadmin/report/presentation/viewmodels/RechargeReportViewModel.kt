package com.commonadmin.report.presentation.viewmodels

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.commonadmin.report.data.remote.models.RechargeReqModel
import com.commonadmin.report.data.remote.models.RechargeResponseModel
import com.commonadmin.report.domain.usercases.RechargeReportUseCase
import com.commonadmin.report.utils.ApiResult
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class RechargeReportViewModel @Inject constructor(private val userCases: RechargeReportUseCase) :
    ViewModel() {
     var rechargeReportState: MutableStateFlow<RechargeReportState> =
        MutableStateFlow(RechargeReportState())
        private set

    var isVisible: MutableState<Boolean> = mutableStateOf(false)
        private set
    var searchText: MutableState<String> = mutableStateOf("")
        private set
    var totalLength: MutableState<String> = mutableStateOf("")
        private set
    var amountTransacted: MutableLiveData<String> = MutableLiveData("")
        private set


    fun getRechargeReportData(url:String,rechargeReqModel: RechargeReqModel,token:String) {
        viewModelScope.launch {
            userCases.invoke(url=url,rechargeReqModel = rechargeReqModel, token = token)
                .collect { networkResult ->
                    when (networkResult) {
                        is ApiResult.Success -> {
                            rechargeReportState.emit(
                                rechargeReportState.value.copy(
                                    rechargeReportRespModel = networkResult.data,
                                    loading = false,
                                    error = null
                                )
                            )
                        }

                        is ApiResult.Loading -> {
                            rechargeReportState.emit(
                                rechargeReportState.value.copy(
                                    rechargeReportRespModel = null,
                                    loading = true,
                                    error = null
                                )
                            )
                        }

                        is ApiResult.Error -> {
                            rechargeReportState.emit(
                                rechargeReportState.value.copy(
                                    rechargeReportRespModel = null,
                                    loading = false,
                                    error = networkResult.message
                                )
                            )
                        }
                    }

                }

        }
    }


}

data class RechargeReportState(
    var rechargeReportRespModel: RechargeResponseModel?=null,
    val loading: Boolean? = null,
    val error: String? = null
)
