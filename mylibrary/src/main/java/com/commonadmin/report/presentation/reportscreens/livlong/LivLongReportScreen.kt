package com.commonadmin.report.presentation.reportscreens.livlong

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Text
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import com.commonadmin.report.data.remote.models.LivLongReportItem
import com.commonadmin.report.data.remote.models.LivLongReportReqModel
import com.commonadmin.report.presentation.viewmodels.LivLongReportViewModel
import com.commonadmin.report.ui.theme.primaryColor
import com.commonadmin.report.utils.DataConstants
import com.commonadmin.report.utils.SharedPrefClass
import com.commonadmin.report.utils.reportcalender.ReportCalenderConstant
import java.util.Locale

@Composable
fun LivLongReportScreen(livLongReportViewModel: LivLongReportViewModel) {
    val livLongReportState = livLongReportViewModel.livLongReportState.collectAsState()
    val context = LocalContext.current
    LaunchedEffect(key1 = true) {

        val json3ArrayList: ArrayList<String> = ArrayList()
        json3ArrayList.add("SUCCESS")
        json3ArrayList.add("FAILED")


        val json6ArrayList: ArrayList<String> = ArrayList()
        json6ArrayList.add("INSURANCE")
        json6ArrayList.add("GROUP_INSURANCE")


        val json7ArrayList: ArrayList<String> = ArrayList()
        json7ArrayList.add("LIVLONG")
        json7ArrayList.add("INSURANCE")


        val livLongReportReqModel = LivLongReportReqModel(
            "all_transaction_report_common",
            SharedPrefClass.getStringValue(context, DataConstants.USERNAME),
            json3ArrayList,
            ReportCalenderConstant.selectFromDate,
            ReportCalenderConstant.selectToDate,
            json6ArrayList,
            json7ArrayList
        )

        if (livLongReportState.value.livLongRespModel == null) {
            livLongReportViewModel.getLivLongReportData(
                SharedPrefClass.getStringValue(context, "livLongReport").toString(),
                livLongReportReqModel,
                SharedPrefClass.getStringValue(context, DataConstants.TOKEN).toString()
            )
        }
    }

    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.TopCenter
    ) {
        if (livLongReportState.value.loading == true && livLongReportState.value.livLongRespModel == null) {
            Box(
                modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ) {
                CircularProgressIndicator(color = primaryColor)
            }
        } else if (livLongReportState.value.loading == false && livLongReportState.value.livLongRespModel != null) {
            livLongReportState.value.livLongRespModel?.let { walletReport ->

                val filteredList: ArrayList<LivLongReportItem?> = arrayListOf()
                val reportList: List<LivLongReportItem?>? = walletReport.results?.bQReport

                var trnsAmount = 0.0
                for (report in reportList?.indices!!) {
                    if (reportList[report]?.status?.equals("SUCCESS", true) == true) {
                        trnsAmount += reportList[report]?.amountTransacted!!
                    }
                }
                val formatted = String.format("%.2f", trnsAmount)
                livLongReportViewModel.amountTransacted.value = formatted


                Column {
                    LazyColumn(
                        contentPadding = PaddingValues(0.dp),
                        verticalArrangement = Arrangement.spacedBy(10.dp),
                        content = {
                            if (livLongReportViewModel.searchText.value.isEmpty()) {
                                livLongReportViewModel.totalLength.value =
                                    reportList.size.toString()
                                items(reportList.size) { item ->
                                    LivLongReportRecycleScreen(walletReport.results.bQReport[item])
                                }
                            } else {
                                for (report in walletReport.results.bQReport) {
                                    if (report.operationPerformed?.lowercase(Locale.ROOT)?.contains(
                                            livLongReportViewModel.searchText.value.lowercase(
                                                Locale.ROOT
                                            )
                                        ) == true ||
                                        report.status?.lowercase(Locale.ROOT)?.contains(
                                            livLongReportViewModel.searchText.value.lowercase(
                                                Locale.ROOT
                                            )
                                        ) == true ||
                                        report.id?.lowercase(Locale.ROOT)?.contains(
                                            livLongReportViewModel.searchText.value.lowercase(
                                                Locale.ROOT
                                            )
                                        ) == true
                                        ||
                                        report.transactionMode?.lowercase(Locale.ROOT)?.contains(
                                            livLongReportViewModel.searchText.value.lowercase(
                                                Locale.ROOT
                                            )
                                        ) == true ||
                                        report.userName?.lowercase(Locale.ROOT)?.contains(
                                            livLongReportViewModel.searchText.value.lowercase(
                                                Locale.ROOT
                                            )
                                        ) == true

                                    ) {
                                        filteredList.add(report)
                                    }
                                }
                                filteredList.let {
                                    livLongReportViewModel.totalLength.value = it.size.toString()
                                    var ammount = 0.0
                                    for (report in it.indices) {
                                        if (filteredList[report]?.status?.equals(
                                                "SUCCESS",
                                                true
                                            ) == true
                                        ) {
                                            ammount += filteredList[report]?.amountTransacted!!
                                        }
                                    }
                                    val filFormatted = String.format("%.2f", ammount)
                                    livLongReportViewModel.amountTransacted.value = filFormatted
                                    items(it.size) { item ->
                                        LivLongReportRecycleScreen(it[item]!!)
                                    }
                                }
                            }
                        }
                    )
                    if (livLongReportViewModel.searchText.value != "" && filteredList.isEmpty() || reportList.isEmpty()) {
                        Box(
                            modifier = Modifier.fillMaxSize(),
                            contentAlignment = Alignment.Center
                        ) {
                            Text(text = "No Data Available")
                        }
                    }
                }
            }
        } else {
            Box(
                modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ) {
                Text(text = "Something went to wrong")
            }
        }
    }

}