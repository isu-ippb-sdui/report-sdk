package com.commonadmin.report.presentation.reportscreens.dmtgateway.dmtReceiptReport

import android.Manifest
import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothManager
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Paint
import android.graphics.Typeface
import android.graphics.pdf.PdfDocument
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.os.RemoteException
import android.util.Log
import android.widget.Toast
import androidx.activity.compose.ManagedActivityResultLauncher
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.sp
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.navigation.NavHostController
import com.commonadmin.report.R
import com.commonadmin.report.ReportSDKActivity
import com.commonadmin.report.presentation.viewmodels.DmtReceiptReportViewModel
import com.commonadmin.report.ui.theme.primaryColor
import com.commonadmin.report.ui.theme.whiteColor
import com.commonadmin.report.utils.DataConstants
import com.commonadmin.report.utils.GetPosConnectPrinter
import com.commonadmin.report.utils.SharedPrefClass
import com.isu.printer_library.vriddhi.AEMPrinter
import wangpos.sdk4.libbasebinder.Printer
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ReceiptToolBar(
    trnsID: Long,
    dmtReceiptReportViewModel: DmtReceiptReportViewModel,
    navController: NavHostController
) {
    val context = LocalContext.current
    val dmtReceiptState = dmtReceiptReportViewModel.dmtReceiptReportState.collectAsState()
    val getResp = dmtReceiptState.value.dmtReceiptResponse?.results?.get(0)

    val printClicked = remember {
        mutableStateOf(false)
    }


    val gateWayStatus = remember {
        mutableStateOf("")
    }
    val gateWayamount = remember {
        mutableStateOf("")
    }

    if(dmtReceiptState.value.dmtReceiptResponse?.results?.get(0)?.gateWayData?.size!=0){
        gateWayStatus.value = dmtReceiptState.value.dmtReceiptResponse?.results?.get(0)?.gateWayData?.get(0)?.status.toString()
        gateWayamount.value = dmtReceiptState.value.dmtReceiptResponse?.results?.get(0)?.gateWayData?.get(0)?.amount.toString()

    }else{
        gateWayStatus.value = getResp?.status.toString()
        gateWayamount.value = getResp?.amountTransacted.toString()
    }




    /**
     * Generate PDF
     */

    fun generatePDF() {
        val date = Date(getResp?.createdDate!!)
        val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        val formattedDateTime = format.format(date)

        val pdfDocument = PdfDocument()
        val faildStaus = Paint()
        val keys = Paint()
        val value = Paint()
        val amount = Paint()
        val successStatus = Paint()
        val txnDetails = Paint()
        val mypageInfo = PdfDocument.PageInfo.Builder(792, 1120, 1).create()
        val myPage = pdfDocument.startPage(mypageInfo)
        val canvas = myPage.canvas

        /* below line is used for adding typeface for
 our text which we will be adding in our PDF file.*/
        faildStaus.typeface = Typeface.create(Typeface.DEFAULT, Typeface.BOLD)
        keys.typeface = Typeface.create(Typeface.DEFAULT, Typeface.BOLD)
        value.typeface = Typeface.create(Typeface.DEFAULT, Typeface.NORMAL)
        amount.typeface = Typeface.create(Typeface.DEFAULT, Typeface.BOLD)
        successStatus.typeface = Typeface.create(Typeface.DEFAULT, Typeface.BOLD)
        txnDetails.typeface = Typeface.create(Typeface.DEFAULT, Typeface.BOLD)
        /* below line is used for setting text size
        which we will be displaying in our PDF file.*/
        faildStaus.textSize = 35f
        keys.textSize = 20f
        value.textSize = 20f
        amount.textSize = 40f
        successStatus.textSize = 35f
// below line is used for text color inside our PDF file.
        keys.color = ContextCompat.getColor(context, R.color.black)
        value.color = ContextCompat.getColor(context, R.color.black)
        txnDetails.color = ContextCompat.getColor(context, R.color.colorPrimary)

        successStatus.color = ContextCompat.getColor(context, R.color.green)

        canvas.drawText(SharedPrefClass.getStringValue(context,DataConstants.ShopName).toString(), 300f, 240f, faildStaus)

        if (gateWayStatus.value.trim().equals("Failed", ignoreCase = true)) {
            faildStaus.color = ContextCompat.getColor(context, R.color.red)
            canvas.drawText("Transaction Failed!", 250f, 300f, faildStaus)

        } else if(gateWayStatus.value.trim().equals("Success", ignoreCase = true)) {
            successStatus.color = ContextCompat.getColor(context, R.color.green)
            canvas.drawText("Transaction Success!", 250f, 300f, successStatus)
        }else{
            successStatus.color = ContextCompat.getColor(context, R.color.black)
            canvas.drawText("Transaction ${gateWayStatus.value}!", 200f, 300f, successStatus)
        }
        canvas.drawText("Operation Performed", 50f, 400f, keys)
        canvas.drawText(": Dmt", 300f, 400f, value)

        canvas.drawText("Transaction ID", 50f, 450f, keys)
        canvas.drawText(": ${getResp.id ?:"N/A"}", 300f, 450f, value)

        canvas.drawText("Contact", 50f, 500f, keys)
        canvas.drawText(": ${getResp.customerMobileNumber ?:"N/A"}", 300f, 500f, value)

        canvas.drawText("Account No ", 50f, 550f, keys)
            canvas.drawText(": ${getResp.beneAccountNumber ?:"N/A"}", 300f, 550f, value)


        if (getResp.beneName?.length!! > 30) {
            canvas.drawText("Bene Name", 50f, 590f, keys)

            val beneNameLength = getResp.beneName.length

            val beneName = getResp.beneName.substring(0, 30)
            val beneName2 = getResp.beneName.substring(30, beneNameLength)
            canvas.drawText(": $beneName", 300f, 590f, value)
            canvas.drawText(" $beneName2", 300f, 615f, value)

        } else {
            canvas.drawText("Bene Name", 50f, 600f, keys)
            canvas.drawText(": ${getResp.beneName?:"N/A"}", 300f, 600f, value)
        }
        canvas.drawText("Bene Mobile", 50f, 650f, keys)

            canvas.drawText(": N/A", 300f, 650f, value)


        canvas.drawText("Bank Name", 50f, 700f, keys)
        canvas.drawText(": ${getResp.beneBankName?:"N/A"}", 300f, 700f, value)

        canvas.drawText("Transaction Amount", 50f, 750f, keys)
        canvas.drawText(": Rs.${gateWayamount.value}", 300f, 750f, value)

        canvas.drawText("Transaction Type", 50f, 800f, keys)
        canvas.drawText(": ${getResp.operationPerformed ?:"N/A"}", 300f, 800f, value)

        canvas.drawText("Date ", 50f, 850f, keys)
        canvas.drawText(": $formattedDateTime", 300f, 850f, value)

        canvas.drawText("Thank You", 500f, 900f, keys)
        canvas.drawText(SharedPrefClass.getStringValue(context,DataConstants.BrandName).toString(), 500f, 930f, keys)
        canvas.drawText("* For detail transaction report go to report section", 50f, 1030f, value)
        pdfDocument.finishPage(myPage)
// below line is used to set the name of PDF file and its path.
// below line is used to set the name of PDF file and its path.
        var save: String
        var num = 0
        var folder = File(
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),
            "/DMTTrnsaction_Report.pdf"
        )
        while (folder.exists()) {
            save = "DMTTrnsaction_Report" + num++ + ".pdf"
            folder = File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), save
            )
        }
        try {
// folder.createNewFile();
            pdfDocument.writeTo(FileOutputStream(folder))
            Toast.makeText(context, "PDF file generated successfully.", Toast.LENGTH_SHORT).show()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        pdfDocument.close()
        val path: Uri = FileProvider.getUriForFile(
            context, context.applicationContext.packageName + ".provider", folder
        )
        val pdfOpenintent = Intent(Intent.ACTION_VIEW)
        pdfOpenintent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        pdfOpenintent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        pdfOpenintent.setDataAndType(path, "application/pdf")
        try {
            context.startActivity(pdfOpenintent)
        } catch (e: ActivityNotFoundException) {
            e.printStackTrace()
        }
    }


    /**
     * BlueTooth Enable and Print the Receipt
     */

    var mAemprinter: AEMPrinter?
    var bluetoothManager: BluetoothManager? = null
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        bluetoothManager = context.getSystemService(
            BluetoothManager::class.java
        )
    }
    val bluetoothAdapter: BluetoothAdapter? = bluetoothManager!!.adapter
    val activityResultLauncher =
        rememberLauncherForActivityResult(contract = ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                Toast.makeText(context, "Bluetooth Enabled", Toast.LENGTH_SHORT).show()
            }
        }
    if (printClicked.value) {
        if (dmtReceiptReportViewModel.isStartPrinting.value) {
            val posSdkActivity = ReportSDKActivity()
            posSdkActivity.ShowBluetoothDevices(context, dmtReceiptReportViewModel, printClicked) {
                mAemprinter = GetPosConnectPrinter.aemPrinter
                callBluetoothFunction(
                    context,
                    mAemprinter,
                    gateWayStatus.value,
                    getResp?.id ?: "N/A",
                    getResp?.beneAccountNumber ?: "N/A",
                    getResp?.beneName ?: "N/A",
                    getResp?.customerMobileNumber ?: "N/A",
                    getResp?.beneBankName ?: "N/A",
                    gateWayamount.value,
                    getResp?.createdDate!!,
                    getResp.operationPerformed


                )
                printClicked.value = false
            }
        }
    }

    Column(
        modifier = Modifier
            .fillMaxWidth()
    ) {
        TopAppBar(
            title = {
                Text(
                    text = "Receipt",
                    fontSize = 16.sp
                )

            }, //title
            navigationIcon = {
                IconButton(onClick = { navController.popBackStack()  }) {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_arrow_back),
                        contentDescription = "Back"
                    )
                }
            },

            actions = {
                IconButton(onClick = {
                    if(getResp!=null){
                        generatePDF()
                    }else{
                        Toast.makeText(context,"Please wait!",Toast.LENGTH_SHORT).show()
                    }
                    }) {
                    Icon(
                        painter = painterResource(id = R.drawable.baseline_file_download_24),
                        "Download"
                    )
                }
                IconButton(onClick = {
                    printClicked.value = !printClicked.value

                    if(getResp!=null){
                        startPrintFunction(
                            context,
                            dmtReceiptReportViewModel,
                            DataConstants.permissionsList,
                            bluetoothAdapter,
                            activityResultLauncher,
                            printClicked
                        )
                    }else{
                        Toast.makeText(context,"Please wait!",Toast.LENGTH_SHORT).show()
                    }


                }) {
                    Icon(painter = painterResource(id = R.drawable.baseline_print_24), "Print")
                }

            },

            modifier = Modifier.fillMaxWidth(),
            colors = TopAppBarDefaults.mediumTopAppBarColors(
                containerColor = primaryColor,
                titleContentColor = whiteColor,
                navigationIconContentColor = whiteColor,
                actionIconContentColor = whiteColor

            )
        )
        DmtReceiptReportScreen(dmtReceiptReportViewModel, trnsID)


    }

}

private fun callBluetoothFunction(
    context: Context,
    mAemPrinter: AEMPrinter?,
    strStatus: String,
    id: String,
    accountNumber: String,
    beneficiaryName: String,
    beneMobile: String,
    bankName: String,
    reqAmount: String,
    date: Long,
    operationPerformed: String?
) {
    try {
        val printDate = Date(date)
        val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        val formattedDateTime = format.format(printDate)

        mAemPrinter?.let {
            mAemPrinter.setFontType(AEMPrinter.DOUBLE_HEIGHT)
            mAemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER)
            mAemPrinter.setFontType(AEMPrinter.FONT_NORMAL)
            mAemPrinter.setFontType(AEMPrinter.FONT_002)
            mAemPrinter.POS_FontThreeInchCENTER()
            mAemPrinter.print(SharedPrefClass.getStringValue(context,DataConstants.ShopName))
            mAemPrinter.print("\n")
            mAemPrinter.setFontType(AEMPrinter.DOUBLE_HEIGHT)
            mAemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER)
            mAemPrinter.print("-----Transaction Report-----\n")
            mAemPrinter.POS_FontThreeInchCENTER()
            mAemPrinter.print(strStatus)
            mAemPrinter.print("\n\n")
            mAemPrinter.print("Operation Performed: DMT")
            mAemPrinter.print("\n")
            mAemPrinter.print("Transaction ID: $id")
            mAemPrinter.print("\n")
            mAemPrinter.print("Contact: $beneMobile")
            mAemPrinter.print("\n")
            mAemPrinter.print("Bene Name: $beneficiaryName")
            mAemPrinter.print("\n")
            mAemPrinter.print("Bene Account No:$accountNumber")
            mAemPrinter.print("\n")
            mAemPrinter.print("Bank Name: $bankName")
            mAemPrinter.print("\n")
            mAemPrinter.print("Txn Amount: Rs.$reqAmount")
            mAemPrinter.print("\n")
            mAemPrinter.print("Txn Type: $operationPerformed")
            mAemPrinter.print("\n")
            mAemPrinter.print("Date&Time: $formattedDateTime")
            mAemPrinter.print("\n")
            mAemPrinter.print("\n")
            mAemPrinter.setFontType(AEMPrinter.FONT_002)
            mAemPrinter.POS_FontThreeInchRIGHT()
            mAemPrinter.print("Thank You \n")
            mAemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_CENTER)
            mAemPrinter.setFontType(AEMPrinter.TEXT_ALIGNMENT_RIGHT)
            mAemPrinter.POS_FontThreeInchRIGHT()
            mAemPrinter.print(SharedPrefClass.getStringValue(context,DataConstants.BrandName).toString())
            mAemPrinter.print("\n")
            mAemPrinter.print("\n")
            mAemPrinter.setCarriageReturn()
            mAemPrinter.setCarriageReturn()
            mAemPrinter.setCarriageReturn()
            mAemPrinter.setCarriageReturn()

            mAemPrinter.print("\n")
        }

    } catch (e: IOException) {
        e.printStackTrace()
    }
}


fun splitString(data: String, splitText: String): Array<String> {
    return data.split(splitText).toTypedArray()
}

fun startPrintFunction(
    context: Context,
    viewModel: DmtReceiptReportViewModel,
    permissions12: Array<String>,
    bluetoothAdapter: BluetoothAdapter?,
    activityResultLauncher: ManagedActivityResultLauncher<Intent, ActivityResult>,
    printClicked: MutableState<Boolean>
) {
    val deviceModel = Build.MODEL
    if (deviceModel.equals("WPOS-3", ignoreCase = true)) {
        //start printing with wiseasy internal printer

        UnifiedPrintReceiptThread(context).start()
    } else {
        if (ActivityCompat.checkSelfPermission(
                context, Manifest.permission.BLUETOOTH_CONNECT
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                context as Activity, permissions12, 1
            )
            printClicked.value = false
            return
        } else {
            if (bluetoothAdapter!!.isEnabled) {
                viewModel.isStartPrinting.value = true
            } else {
                GetPosConnectPrinter.aemPrinter = null
                val turnOn = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
                activityResultLauncher.launch(turnOn)
                printClicked.value = false
                viewModel.isStartPrinting.value = false
            }
        }
    }
}

class UnifiedPrintReceiptThread(
    private val context: Context
) : Thread() {
    private var mPrinter: Printer? = null
    override fun run() {
        mPrinter = Printer(context)
        try {
            mPrinter!!.setPrintType(0) //Printer type 0 means it's an internal printer
        } catch (e: RemoteException) {
            e.printStackTrace()
        }
        checkPrinterStatus()
    }

    private fun checkPrinterStatus() {
        try {
            val status = IntArray(1)
            mPrinter?.getPrinterStatus(status)
            Log.e("TAG", "Printer Status is " + status[0])
            val msg: String
            when (status[0]) {
                0x00 -> {
                    msg = "Printer status OK"
                    Log.e("TAG", "check printer status: $msg")
                    startPrinting()
                }

                0x01 -> {
                    msg = "Parameter error"
                    showLog(msg)
                }

                0x8A -> {
                    msg = "Out of Paper"
                    showLog(msg)
                }

                0x8B -> {
                    msg = "Overheat"
                    showLog(msg)
                }

                else -> {
                    msg = "Printer Error"
                    showLog(msg)
                }
            }
        } catch (e: RemoteException) {
            e.printStackTrace()
        }
    }

    private fun startPrinting() {
        val result: Int
        try {
            result = mPrinter!!.printInit()
            Log.e("TAG", "startPrinting: Printer init result $result")
            mPrinter!!.clearPrintDataCache()
            if (result == 0) {
                // printReceipt()
            } else {
                Toast.makeText(
                    context, "Printer initialization failed", Toast.LENGTH_SHORT
                ).show()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun showLog(msg: String) {
        (context as Activity).runOnUiThread {
            Toast.makeText(
                context, msg, Toast.LENGTH_SHORT
            ).show()
        }
        Log.e("TAG", "Printer status: $msg")
    }
}

@Throws(IOException::class)
fun printerStatus(mAemprinter: AEMPrinter): String {
    val printerStatus = charArrayOf(
        0x1B.toChar(),
        0x7E.toChar(),
        0x42.toChar(),
        0x50.toChar(),
        0x7C.toChar(),
        0x47.toChar(),
        0x45.toChar(),
        0x54.toChar(),
        0x7C.toChar(),
        0x50.toChar(),
        0x52.toChar(),
        0x4E.toChar(),
        0x5F.toChar(),
        0x53.toChar(),
        0x54.toChar(),
        0x5E.toChar()
    )
    val data = String(printerStatus)
    mAemprinter.print(data)
    return data
}
