package com.commonadmin.report.presentation.reportscreens.aadharpay

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Text
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import com.commonadmin.report.data.remote.models.AadharpayReportItem
import com.commonadmin.report.data.remote.models.AadharpayReqModel
import com.commonadmin.report.presentation.viewmodels.AadharPayReportViewModel
import com.commonadmin.report.ui.theme.primaryColor
import com.commonadmin.report.utils.DataConstants
import com.commonadmin.report.utils.SharedPrefClass
import com.commonadmin.report.utils.reportcalender.ReportCalenderConstant
import java.util.Locale

@Composable
fun AadhaarReportScreen(aadharPayReportViewModel: AadharPayReportViewModel) {
    val aadharpayState = aadharPayReportViewModel.aadharPayReportState.collectAsState()
    val context = LocalContext.current
    LaunchedEffect(key1 = true) {
        val trnsTrns: ArrayList<String> = ArrayList()
        trnsTrns.add("INITIATED")
        trnsTrns.add("PENDING")
        trnsTrns.add("SUCCESS")
        trnsTrns.add("FAILED")

        val unifiedAeps: ArrayList<String> = ArrayList()
        unifiedAeps.add("AADHAAR_PAY")

        val trnsType: ArrayList<String> = ArrayList()
        trnsType.add("UnifiedAEPS")



        val aadharPayReqModel = AadharpayReqModel(
            "aeps_txn_common",
            SharedPrefClass.getStringValue(context, DataConstants.USERNAME),trnsTrns,
            ReportCalenderConstant.selectFromDate, ReportCalenderConstant.selectToDate, trnsType,unifiedAeps
        )

        if (aadharpayState.value.aadharpayRespModel == null) {
            aadharPayReportViewModel.getAadharPayReportData(
                SharedPrefClass.getStringValue(context, "AadhaarPayReport").toString(),
                aadharPayReqModel,
                SharedPrefClass.getStringValue(context,DataConstants.TOKEN).toString()
            )
        }
    }

    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.TopCenter
    ) {
        if (aadharpayState.value.loading == true && aadharpayState.value.aadharpayRespModel == null) {
            Box(modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ){
                CircularProgressIndicator(color = primaryColor)
            }
        } else if (aadharpayState.value.loading == false && aadharpayState.value.aadharpayRespModel != null) {
            aadharpayState.value.aadharpayRespModel?.let { walletReport ->

                val filteredList: ArrayList<AadharpayReportItem?> = arrayListOf()
                val reportList: List<AadharpayReportItem?>? = walletReport.results?.bQReport

                var trnsAmount = 0.0
                for (report in reportList?.indices!!) {
                    if(reportList[report]?.status?.equals("SUCCESS",true) == true) {
                        trnsAmount += reportList[report]?.amountTransacted!!
                    }
                }
                val formatted = String.format("%.2f", trnsAmount)
                aadharPayReportViewModel.amountTransacted.value = formatted


                Column {
                    LazyColumn(
                        contentPadding = PaddingValues(0.dp),
                        verticalArrangement = Arrangement.spacedBy(10.dp),
                        content = {
                            if (aadharPayReportViewModel.searchText.value.isEmpty()) {
                                aadharPayReportViewModel.totalLength.value =
                                    reportList.size.toString()
                                items(reportList.size) { item ->
                                    AadhaarReportRecycleScreen(walletReport.results.bQReport[item])
                                }
                            } else {
                                for (report in walletReport.results.bQReport) {
                                    if (report.id?.lowercase(Locale.ROOT)?.contains(
                                                aadharPayReportViewModel.searchText.value.lowercase(
                                                    Locale.ROOT
                                                )
                                            )==true ||
                                        report.status?.lowercase(Locale.ROOT)?.contains(
                                            aadharPayReportViewModel.searchText.value.lowercase(
                                                Locale.ROOT
                                            )
                                        )==true ||
                                        report.operationPerformed?.lowercase(Locale.ROOT)?.contains(
                                            aadharPayReportViewModel.searchText.value.lowercase(
                                                Locale.ROOT
                                            )
                                        )==true ||
 report.userName?.lowercase(Locale.ROOT)?.contains(
                                            aadharPayReportViewModel.searchText.value.lowercase(
                                                Locale.ROOT
                                            )
                                        )==true ||
report.transactionMode?.lowercase(Locale.ROOT)?.contains(
                                            aadharPayReportViewModel.searchText.value.lowercase(
                                                Locale.ROOT
                                            )
                                        )==true

                                    // report.apiTid?.lowercase(Locale.ROOT)!!.contains(reportViewModel.searchText.value.lowercase(Locale.ROOT))*/

                                    ) {
                                        filteredList.add(report)
                                    }
                                }
                                filteredList.let {
                                    aadharPayReportViewModel.totalLength.value = it.size.toString()
                                    var ammount = 0.0
                                    for (report in it.indices) {
                                        if(filteredList[report]?.status?.equals("SUCCESS",true) == true) {
                                            ammount += filteredList[report]?.amountTransacted!!
                                        }
                                    }
                                    val filFormatted = String.format("%.2f", ammount)
                                    aadharPayReportViewModel.amountTransacted.value = filFormatted
                                    items(it.size) { item ->
                                        AadhaarReportRecycleScreen(it[item]!!)
                                    }
                                }
                            }
                        }
                    )
                    if (aadharPayReportViewModel.searchText.value != "" && filteredList.isEmpty() || reportList.isEmpty()) {
                        Box(modifier = Modifier.fillMaxSize(),
                            contentAlignment = Alignment.Center
                        ){
                            Text(text = "No Data Available")
                        }
                    }
                }
            }
        } else {
            Box(modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ){
                Text(text = "Something went to wrong")
            }
        }
    }

}