package com.commonadmin.report.presentation.reportscreens.wallet1

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Text
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import com.commonadmin.report.data.remote.models.Wallet1ReqModel
import com.commonadmin.report.data.remote.models.WalletReportItem
import com.commonadmin.report.presentation.viewmodels.Wallet1ReportViewModel
import com.commonadmin.report.ui.theme.primaryColor
import com.commonadmin.report.utils.DataConstants
import com.commonadmin.report.utils.SharedPrefClass
import com.commonadmin.report.utils.reportcalender.ReportCalenderConstant
import java.util.Locale

@Composable
fun WalletReportScreen(wallet1ReportViewModel: Wallet1ReportViewModel) {
    val wallet1ReportState = wallet1ReportViewModel.wallet1ReportState.collectAsState()
    val context = LocalContext.current
    LaunchedEffect(key1 = true) {
        val trnsType: ArrayList<String> = ArrayList()

        trnsType.add("virtual_balance")
        trnsType.add("Inter_Wallet")
        trnsType.add("Virtual_Balance_Transfer")
        trnsType.add("IMPS_FUND_TRANSFER")
        trnsType.add("BENE_VERIFICATION")
        trnsType.add("COMMISSION")
        trnsType.add("NEFT_FUND_TRANSFER")
        trnsType.add("WALLET_INTERCHANGE")
        trnsType.add("Recharge_Prepaid")
        trnsType.add("Recharge_Postpaid")
        trnsType.add("Recharge_DTH")
        trnsType.add("QR_COLLECT")
        trnsType.add("UPI_COLLECT")
        trnsType.add("PG_Internet Banking")
        trnsType.add("Recharge_Prepaid")
        trnsType.add("Recharge_Postpaid")
        trnsType.add("Recharge_DTH")
        trnsType.add("Prepaid_Airtel")
        trnsType.add("Prepaid_Bsnl")
        trnsType.add("Prepaid_Idea")
        trnsType.add("Prepaid_Vodafone")
        trnsType.add("Prepaid_VodafoneIdea")
        trnsType.add("Prepaid_RelianceJio")
        trnsType.add("Prepaid_TataIndicom")
        trnsType.add("Prepaid_TataDocomo")
        trnsType.add("Prepaid_Aircel")
        trnsType.add("Prepaid_Telenor")
        trnsType.add("Prepaid_VirginGsm")
        trnsType.add("Prepaid_Virgincdma")
        trnsType.add("Prepaid_Mts")
        trnsType.add("Prepaid_Mtnl")
        trnsType.add("Postpaid_Airtel")
        trnsType.add("Postpaid_Bsnl")
        trnsType.add("Postpaid_Idea")
        trnsType.add("Postpaid_Vodafone")
        trnsType.add("Postpaid_VodafoneIdea")
        trnsType.add("Postpaid_RelianceJio")
        trnsType.add("Postpaid_TataIndicom")
        trnsType.add("Postpaid_TataDocomo")
        trnsType.add("Postpaid_Aircel")
        trnsType.add("Postpaid_Telenor")
        trnsType.add("Postpaid_VirginGsm")
        trnsType.add("Postpaid_Virgincdma")
        trnsType.add("Postpaid_Mts")
        trnsType.add("Postpaid_Mtnl")
        trnsType.add("DTH_AirtelDth")
        trnsType.add("DTH_BigTvDth")
        trnsType.add("DTH_DishTvDth")
        trnsType.add("DTH_TataSkyDth")
        trnsType.add("DTH_VideoconDth")
        trnsType.add("DTH_SunTvDth")
        trnsType.add("DTH_JioDth")
        trnsType.add("DTH_SunTvDth")
        trnsType.add("DTH_JioDth")
        trnsType.add("BBPS_ELECTRICITY")
        trnsType.add("BBPS_BROADBAND POSTPAID")
        trnsType.add("BBPS_GAS")
        trnsType.add("BBPS_LANDLINE POSTPAID")
        trnsType.add("BBPS_MOBILE POSTPAID")
        trnsType.add("BBPS_WATER")
        trnsType.add("BBPS_LOAN REPAYMENT")
        trnsType.add("BBPS_LIFE INSURANCE")
        trnsType.add("BBPS_LPG GAS")
        trnsType.add("BBPS_CABLE TV")
        trnsType.add("BBPS_HEALTH INSURANCE")
        trnsType.add("BBPS_EDUCATION FEES")
        trnsType.add("BBPS_INSURANCE")
        trnsType.add("BBPS_MUNICIPAL TAXES")
        trnsType.add("BBPS_HOSPITAL")
        trnsType.add("BBPS_MUNICIPAL SERVICES")
        trnsType.add("BBPS_HOUSING SOCIETY")
        trnsType.add("BBPS_SUBSCRIPTION")
        trnsType.add("BBPS_FASTAG")
        trnsType.add("BBPS_DTH")
        trnsType.add("LIC_Premium")
        trnsType.add("INSURANCE")

        val wallet1ReqModel = Wallet1ReqModel(
            "wallet1_new_web_common", SharedPrefClass.getStringValue(context, DataConstants.USERNAME),
            ReportCalenderConstant.selectFromDate, ReportCalenderConstant.selectToDate,trnsType
        )

        if (wallet1ReportState.value.wallet1RespModel == null) {
            wallet1ReportViewModel.getWallet1ReportData(
                SharedPrefClass.getStringValue(context, "Wallet1Report").toString(), wallet1ReqModel,SharedPrefClass.getStringValue(context,DataConstants.TOKEN).toString()
            )
        }
    }

    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.TopCenter
    ) {
        if (wallet1ReportState.value.loading == true && wallet1ReportState.value.wallet1RespModel == null) {
            Box(modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ){
                CircularProgressIndicator(color = primaryColor)
            }
        } else if (wallet1ReportState.value.loading == false && wallet1ReportState.value.wallet1RespModel != null) {
            wallet1ReportState.value.wallet1RespModel?.let { walletReport ->

                val filteredList: ArrayList<WalletReportItem?> = arrayListOf()
                val reportList: List<WalletReportItem?>? = walletReport.report
                var crAmount = 0.0
                for (report in reportList?.indices!!) {
                    if(reportList[report]?.status?.equals("SUCCESS",true) == true) {
                        crAmount += reportList[report]?.crAmount!!
                    }
                }
                val formatted = String.format("%.2f", crAmount)
                wallet1ReportViewModel.crAmount.value = formatted

                var drAmount = 0.0
                for (report in reportList.indices) {
                    if(reportList[report]?.status?.equals("SUCCESS",true) == true) {
                        drAmount += reportList[report]?.drAmount!!
                    }
                }
                wallet1ReportViewModel.drAmount.value = drAmount.toString()

                Column {
                    LazyColumn(
                        contentPadding = PaddingValues(0.dp),
                        verticalArrangement = Arrangement.spacedBy(10.dp),
                        content = {
                            if (wallet1ReportViewModel.searchText.value.isEmpty()) {
                                wallet1ReportViewModel.totalLength.value =
                                    reportList.size.toString()
                                items(reportList.size) { item ->
                                    Wallet1RecycleScreen(walletReport.report[item])
                                }
                            } else {
                                for (report in walletReport.report) {
                                    if (report.relationalOperation?.lowercase(Locale.ROOT)!!
                                            .contains(
                                                wallet1ReportViewModel.searchText.value.lowercase(
                                                    Locale.ROOT
                                                )
                                            ) ||
                                        report.status?.lowercase(Locale.ROOT)!!.contains(
                                            wallet1ReportViewModel.searchText.value.lowercase(
                                                Locale.ROOT
                                            )
                                        ) ||
                                        report.id?.lowercase(Locale.ROOT)!!.contains(
                                            wallet1ReportViewModel.searchText.value.lowercase(
                                                Locale.ROOT
                                            )
                                        )||
                                        report.relationalId?.lowercase(Locale.ROOT)?.contains(
                                            wallet1ReportViewModel.searchText.value.lowercase(
                                                Locale.ROOT
                                            )
                                        )==true
                                    // report.apiTid?.lowercase(Locale.ROOT)!!.contains(reportViewModel.searchText.value.lowercase(Locale.ROOT))*/

                                    ) {
                                        filteredList.add(report)
                                    }
                                }
                                filteredList.let {
                                    wallet1ReportViewModel.totalLength.value = it.size.toString()

                                    var fCrAmount = 0.0
                                    for (report in filteredList.indices) {
                                        if(filteredList[report]?.status?.equals("SUCCESS",true) == true) {
                                            fCrAmount += filteredList[report]?.crAmount!!
                                        }
                                    }
                                    val formatted = String.format("%.2f", fCrAmount)
                                    wallet1ReportViewModel.crAmount.value = formatted

                                    var fDrAmount = 0.0
                                    for (report in filteredList.indices) {
                                        if(filteredList[report]?.status?.equals("SUCCESS",true) == true) {
                                            fDrAmount += filteredList[report]?.drAmount!!
                                        }
                                    }
                                    wallet1ReportViewModel.drAmount.value = fDrAmount.toString()

                                    items(it.size) { item ->
                                        Wallet1RecycleScreen(it[item]!!)
                                    }
                                }
                            }
                        }
                    )
                    if (wallet1ReportViewModel.searchText.value != "" && filteredList.isEmpty() || reportList.isEmpty()) {
                        Box(modifier = Modifier.fillMaxSize(),
                            contentAlignment = Alignment.Center
                        ){
                            Text(text = "No Data Available")
                        }
                    }
                }
            }
        } else {
            Box(modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ){
                Text(text = "Something went to wrong")
            }
        }
    }

}