package com.commonadmin.report.presentation.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.commonadmin.report.data.remote.models.SduiFetchReqModels
import com.commonadmin.report.data.remote.models.SduiFetchRespModels
import com.commonadmin.report.domain.usercases.SduiFetchUseCase
import com.commonadmin.report.utils.ApiResult
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SduiFetchViewModel @Inject constructor(private val userCases: SduiFetchUseCase) :
    ViewModel() {
    var sduiFetchState: MutableStateFlow<SduiFetchState> =
        MutableStateFlow(SduiFetchState())
        private set


    fun getSduiFetchData(sduiFetchReqModels: SduiFetchReqModels) {
        viewModelScope.launch {
            userCases.invoke(sduiFetchReqModels = sduiFetchReqModels)
                .collect { networkResult ->
                    when (networkResult) {
                        is ApiResult.Success -> {
                            sduiFetchState.emit(
                                sduiFetchState.value.copy(
                                    sduiFetchRespModels = networkResult.data,
                                    loading = false,
                                    error = null,
                                    apiCall = true
                                )
                            )
                        }

                        is ApiResult.Loading -> {
                            sduiFetchState.emit(
                                sduiFetchState.value.copy(
                                    sduiFetchRespModels = null,
                                    loading = true,
                                    error = null,
                                    apiCall = false
                                )
                            )
                        }

                        is ApiResult.Error -> {
                            sduiFetchState.emit(
                                sduiFetchState.value.copy(
                                    sduiFetchRespModels = null,
                                    loading = false,
                                    error = networkResult.message,
                                    apiCall = false
                                )
                            )
                        }
                    }

                }

        }
    }


}

data class SduiFetchState(
    var sduiFetchRespModels: SduiFetchRespModels? = null,
    val loading: Boolean? = null,
    val error: String? = null,
    val apiCall:Boolean?= null
)
