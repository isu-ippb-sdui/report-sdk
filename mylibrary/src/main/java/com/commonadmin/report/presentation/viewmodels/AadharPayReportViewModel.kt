package com.commonadmin.report.presentation.viewmodels

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.commonadmin.report.data.remote.models.AadharpayReqModel
import com.commonadmin.report.data.remote.models.AadharpayRespModel
import com.commonadmin.report.domain.usercases.AadharPayReportUseCase
import com.commonadmin.report.utils.ApiResult
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AadharPayReportViewModel @Inject constructor(private val userCases: AadharPayReportUseCase) :
    ViewModel() {
    var aadharPayReportState: MutableStateFlow<AadharPayReportState> =
        MutableStateFlow(AadharPayReportState())
        private set
    var isVisible: MutableState<Boolean> = mutableStateOf(false)
        private set
    var searchText: MutableState<String> = mutableStateOf("")
        private set
    var totalLength: MutableState<String> = mutableStateOf("")
        private set
    var amountTransacted: MutableLiveData<String> = MutableLiveData("")
        private set





    fun getAadharPayReportData(url: String, aadharpayReqModel: AadharpayReqModel, token: String) {
        viewModelScope.launch {
            userCases.invoke(url = url, aadharpayReqModel = aadharpayReqModel, token = token)
                .collect { networkResult ->
                    when (networkResult) {
                        is ApiResult.Success -> {
                            aadharPayReportState.emit(
                                aadharPayReportState.value.copy(
                                    aadharpayRespModel = networkResult.data,
                                    loading = false,
                                    error = null
                                )
                            )
                        }

                        is ApiResult.Loading -> {
                            aadharPayReportState.emit(
                                aadharPayReportState.value.copy(
                                    aadharpayRespModel = null,
                                    loading = true,
                                    error = null
                                )
                            )
                        }

                        is ApiResult.Error -> {
                            aadharPayReportState.emit(
                                aadharPayReportState.value.copy(
                                    aadharpayRespModel = null,
                                    loading = false,
                                    error = networkResult.message
                                )
                            )
                        }
                    }

                }

        }
    }


}

data class AadharPayReportState(
    var aadharpayRespModel: AadharpayRespModel? = null,
    val loading: Boolean? = null,
    val error: String? = null
)
