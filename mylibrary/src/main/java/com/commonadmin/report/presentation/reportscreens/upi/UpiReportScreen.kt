package com.commonadmin.report.presentation.reportscreens.upi

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Text
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import com.commonadmin.report.data.remote.models.UpiReportItem
import com.commonadmin.report.data.remote.models.UpiReqModel
import com.commonadmin.report.presentation.viewmodels.UpiReportViewModel
import com.commonadmin.report.ui.theme.primaryColor
import com.commonadmin.report.utils.DataConstants
import com.commonadmin.report.utils.SharedPrefClass
import com.commonadmin.report.utils.reportcalender.ReportCalenderConstant
import java.util.Locale

@Composable
fun UpiReportScreen(upiReportViewModel: UpiReportViewModel) {
    val upiReportState = upiReportViewModel.upiReportState.collectAsState()
    val context = LocalContext.current
    LaunchedEffect(key1 = true) {

        val trnsType: ArrayList<String> = ArrayList()
        trnsType.add("UPI")

 val operationPerfomed: ArrayList<String> = ArrayList()
        operationPerfomed.add("QR_COLLECT")
        operationPerfomed.add("UPI_COLLECT")
        operationPerfomed.add("QR_STATIC")
        operationPerfomed.add("UPI_INTENT")


        val upiReqModel= UpiReqModel(
            ReportCalenderConstant.selectFromDate,
            ReportCalenderConstant.selectToDate,
            SharedPrefClass.getStringValue(context, DataConstants.USERNAME),
            trnsType,
            operationPerfomed,
            "All"
        )

        if (upiReportState.value.upiReportRespModel == null) {
            upiReportViewModel.getUpiReportData(
                SharedPrefClass.getStringValue(context, "UPIReport").toString(),
                upiReqModel,SharedPrefClass.getStringValue(context,DataConstants.TOKEN).toString()
            )
        }
    }

    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.TopCenter
    ) {
        if (upiReportState.value.loading == true && upiReportState.value.upiReportRespModel == null) {
            Box(modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ){
                CircularProgressIndicator(color = primaryColor)
            }
        } else if (upiReportState.value.loading == false && upiReportState.value.upiReportRespModel != null) {
            upiReportState.value.upiReportRespModel?.let { walletReport ->

                val filteredList: ArrayList<UpiReportItem?> = arrayListOf()
                val reportList: List<UpiReportItem?>? = walletReport.results?.bQReport

                var trnsAmount = 0.0
                for (report in reportList?.indices!!) {
                    if(reportList[report]?.status?.equals("SUCCESS",true) == true) {
                        trnsAmount += reportList[report]?.amountTransacted!!.toDouble()
                    }
                }
                val formatted = String.format("%.2f", trnsAmount)
                upiReportViewModel.amountTransacted.value = formatted


                Column {
                    LazyColumn(
                        contentPadding = PaddingValues(0.dp),
                        verticalArrangement = Arrangement.spacedBy(10.dp),
                        content = {
                            if (upiReportViewModel.searchText.value.isEmpty()) {
                                upiReportViewModel.totalLength.value =
                                    reportList.size.toString()
                                items(reportList.size) { item ->
                                    UpiReportRecycleScreen(walletReport.results.bQReport[item])
                                }
                            } else {
                                for (report in walletReport.results.bQReport) {
                                    if (report.status.lowercase(Locale.ROOT).contains(
                                            upiReportViewModel.searchText.value.lowercase(
                                                Locale.ROOT
                                            )
                                        ) ||
                                        report.id.lowercase(Locale.ROOT).contains(
                                            upiReportViewModel.searchText.value.lowercase(
                                                Locale.ROOT
                                            )
                                        )

                                    ) {
                                        filteredList.add(report)
                                    }
                                }
                                filteredList.let {
                                    upiReportViewModel.totalLength.value = it.size.toString()
                                    var ammount = 0.0
                                    for (report in it.indices) {
                                        if(filteredList[report]?.status?.equals("SUCCESS",true) == true) {
                                            ammount += filteredList[report]?.amountTransacted!!.toDouble()
                                        }
                                    }
                                    val filFormatted = String.format("%.2f", ammount)
                                    upiReportViewModel.amountTransacted.value = filFormatted
                                    items(it.size) { item ->
                                        UpiReportRecycleScreen(it[item]!!)
                                    }
                                }
                            }
                        }
                    )
                    if (upiReportViewModel.searchText.value != "" && filteredList.isEmpty() || reportList.isEmpty()) {
                        Box(modifier = Modifier.fillMaxSize(),
                            contentAlignment = Alignment.Center
                        ){
                            Text(text = "No Data Available")
                        }
                    }
                }
            }
        } else {
            Box(modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ){
                Text(text = "Something went to wrong")
            }
        }
    }

}