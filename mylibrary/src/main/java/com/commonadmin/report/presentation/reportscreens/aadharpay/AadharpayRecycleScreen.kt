package com.commonadmin.report.presentation.reportscreens.aadharpay

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.commonadmin.report.data.remote.models.AadharpayReportItem
import com.commonadmin.report.ui.theme.Green
import com.commonadmin.report.ui.theme.GreyPrimary
import com.commonadmin.report.ui.theme.Yellow
import java.text.SimpleDateFormat
import java.util.Locale

@Composable
fun AadhaarReportRecycleScreen(aadharpayReportItem: AadharpayReportItem?) {
    val isClicked = remember {
        mutableStateOf(false)
    }
    val timeT: Long = aadharpayReportItem?.createdDate!!.toLong()
    val formatter = SimpleDateFormat("dd-MM-yyyy  hh:mm a")
    val formatted = formatter.format(timeT)

    Card(
        colors = CardDefaults.cardColors(
            containerColor = GreyPrimary,

            ),
        modifier = Modifier
            .wrapContentHeight()
            .clickable {
                isClicked.value = !isClicked.value
            },
        elevation = CardDefaults.cardElevation(defaultElevation = 10.dp)
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(20.dp)

        ) {
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Text(
                    text = "Date/Time: $formatted",
                    color = Color.Black,
                    style = MaterialTheme.typography.headlineLarge,
                    fontSize = 15.sp
                )
                Text(
                    text = aadharpayReportItem.status ?: "N/A",
                    color = when (aadharpayReportItem.status?.uppercase(Locale.ROOT)) {
                        "REFUNDED" -> {
                            Yellow
                        }

                        "SUCCESS" -> {
                           Green
                        }

                        "FAILED" -> {
                            Color.Red
                        }

                        else -> {
                            Color.Black
                        }
                    },
                    style = MaterialTheme.typography.headlineLarge,
                    fontSize = 15.sp
                )
            }
            Spacer(modifier = Modifier.height(10.dp))

                Row(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    Text(
                        text = "RRN No: ${aadharpayReportItem.referenceNo ?: "N/A"}",
                        color = Color.Black,
                        style = MaterialTheme.typography.headlineLarge,
                        fontSize = 15.sp
                    )
                    Spacer(modifier = Modifier.height(5.dp))
                    Text(
                        text = "₹"+aadharpayReportItem.amountTransacted.toString(),
                        color = Color.Black,
                        style = MaterialTheme.typography.headlineLarge,
                        fontSize = 15.sp
                    )
                }
                Spacer(modifier = Modifier.height(5.dp))

            AnimatedVisibility(visible = isClicked.value ) {
                Row(modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 10.dp)) {
                    Column(Modifier.weight(1.5f)) {
                        Spacer(modifier = Modifier.height(8.dp))
                        Text(
                            text = "UserName: ",
                            color = Color.Black,
                            style = MaterialTheme.typography.labelLarge,
                            fontSize = 15.sp
                        )
                        Spacer(modifier = Modifier.height(3.dp))
                        Text(
                            text = aadharpayReportItem.userName ?: "N/A",
                            color = Color.Black,
                            style = MaterialTheme.typography.headlineLarge,
                            fontSize = 13.sp
                        )
                        Spacer(modifier = Modifier.height(5.dp))

                        Text(
                            text = "Txn ID: ",
                            color = Color.Black,
                            style = MaterialTheme.typography.labelLarge,
                            fontSize = 15.sp
                        )
                        Spacer(modifier = Modifier.height(3.dp))
                        Text(
                            text = aadharpayReportItem.id.toString(),
                            color = Color.Black,
                            style = MaterialTheme.typography.headlineLarge,
                            fontSize = 13.sp
                        )
                        Text(
                            text = "Opening Balance: ",
                            color = Color.Black,
                            style = MaterialTheme.typography.labelLarge,
                            fontSize = 15.sp
                        )
                        Spacer(modifier = Modifier.height(3.dp))
                        Text(
                            text = "₹"+aadharpayReportItem.previousAmount.toString(),
                            color = Color.Black,
                            style = MaterialTheme.typography.headlineLarge,
                            fontSize = 13.sp
                        )
                    }
                    Column(Modifier.weight(1f)) {

                        Spacer(modifier = Modifier.height(5.dp))
                        Text(
                            text = "Txn Type",
                            color = Color.Black,
                            style = MaterialTheme.typography.labelLarge,
                            fontSize = 15.sp
                        )
                        Spacer(modifier = Modifier.height(3.dp))
                        Text(
                            text = aadharpayReportItem.operationPerformed ?: "N/A",
                            color = Color.Black,
                            style = MaterialTheme.typography.headlineLarge,
                            fontSize = 13.sp
                        )
                        Spacer(modifier = Modifier.height(5.dp))
                        Text(
                            text = "Txn Mode: ",
                            color = Color.Black,
                            style = MaterialTheme.typography.labelLarge,
                            fontSize = 15.sp
                        )
                        Spacer(modifier = Modifier.height(3.dp))
                        Text(
                            text = aadharpayReportItem.transactionMode.toString(),
                            color = Color.Black,
                            style = MaterialTheme.typography.headlineLarge,
                            fontSize = 13.sp
                        )
                        Spacer(modifier = Modifier.height(3.dp))
                        Text(
                            text = "Closing Balance: ",
                            color = Color.Black,
                            style = MaterialTheme.typography.labelLarge,
                            fontSize = 15.sp
                        )
                        Spacer(modifier = Modifier.height(3.dp))
                        Text(
                            text = "₹"+aadharpayReportItem.balanceAmount.toString(),
                            color = Color.Black,
                            style = MaterialTheme.typography.headlineLarge,
                            fontSize = 13.sp
                        )

                    }
                }
            }

        }

    }//column
}