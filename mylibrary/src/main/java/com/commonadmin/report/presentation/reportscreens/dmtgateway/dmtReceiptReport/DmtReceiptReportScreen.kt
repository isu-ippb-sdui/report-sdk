package com.commonadmin.report.presentation.reportscreens.dmtgateway.dmtReceiptReport


import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.commonadmin.report.data.remote.models.DmtGateWayDataItem
import com.commonadmin.report.data.remote.models.DmtReceiptReportReqModel
import com.commonadmin.report.presentation.viewmodels.DmtReceiptReportViewModel
import com.commonadmin.report.ui.theme.fingerGrey
import com.commonadmin.report.ui.theme.primaryColor
import com.commonadmin.report.ui.theme.very_light_grey
import com.commonadmin.report.utils.DataConstants
import com.commonadmin.report.utils.SharedPrefClass
import com.commonadmin.report.utils.reportcalender.ReportCalenderConstant
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

@Composable
fun DmtReceiptReportScreen(
    dmtReceiptReportViewModel: DmtReceiptReportViewModel?,
    trnsID: Long
) {
    val dmtReceiptReportState = dmtReceiptReportViewModel?.dmtReceiptReportState?.collectAsState()
    val context = LocalContext.current


    LaunchedEffect(key1 = true) {
        val json3ArryList: ArrayList<String> = ArrayList()
        json3ArryList.add("INITIATED")
        json3ArryList.add("RETRY")
        json3ArryList.add("INPROGRESS")
        json3ArryList.add("SUCCESS")
        json3ArryList.add("FAILED")
        json3ArryList.add("REFUNDED")
        json3ArryList.add("REFUNDPENDING")

        val json6ArrayList: ArrayList<String> = ArrayList()
        json6ArrayList.add("DMT")

        val json7ArrayList: ArrayList<String> = ArrayList()
        json7ArrayList.add("BENE_VERIFICATION")
        json7ArrayList.add("NEFT_FUND_TRANSFER")
        json7ArrayList.add("IMPS_FUND_TRANSFER")

        val json10ArrayList: ArrayList<Long> = ArrayList()
        json10ArrayList.add(trnsID)


        val dmtReceiptReqModels = DmtReceiptReportReqModel(
            "Email_report_common",
            SharedPrefClass.getStringValue(context, DataConstants.USERNAME),
            json3ArryList,
            ReportCalenderConstant.selectFromDate,
            ReportCalenderConstant.selectToDate,
            json6ArrayList,
            json7ArrayList,
            json10ArrayList
        )


            dmtReceiptReportViewModel?.getReceiptReportData(
                SharedPrefClass.getStringValue(context, "DmtReceiptReport").toString(),
                dmtReceiptReqModels
            )

    }


    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.TopCenter
    ) {
        if (dmtReceiptReportState?.value?.loading == true && dmtReceiptReportState.value.dmtReceiptResponse == null) {
            Box(modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ){
                CircularProgressIndicator(color = primaryColor)
            }
        } else if (dmtReceiptReportState?.value?.loading == false && dmtReceiptReportState.value.dmtReceiptResponse != null) {
            dmtReceiptReportState.value.dmtReceiptResponse?.let { walletReport ->
                var timeDate = ""
                var gateWayList: ArrayList<DmtGateWayDataItem> = ArrayList()

                    walletReport.results?.forEach { result ->
                        gateWayList = result.gateWayData!!

                        val date = Date(result.createdDate!!)
                        val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
                        timeDate = format.format(date)
                    }

                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                ) {
                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .wrapContentHeight()
                            .background(very_light_grey)
                            .padding(15.dp),
                    ) {
                        Text(
                            text = "From",
                            color = Color.Black,
                            fontWeight = FontWeight.Bold,
                            fontSize = 15.sp,
                            textAlign = TextAlign.Start,
                            modifier = Modifier.weight(0.1f)
                        )
                        Text(
                            text = "To",
                            color = Color.Black,
                            fontWeight = FontWeight.Bold,
                            fontSize = 15.sp,
                            textAlign = TextAlign.Start,
                            modifier = Modifier.weight(0.1f)
                        )
                    }

                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .wrapContentHeight()
                            .background(fingerGrey)
                            .padding(15.dp),
                        horizontalArrangement = Arrangement.SpaceAround,
                    ) {
                        Column(modifier = Modifier.weight(0.1f)) {
                            Text(
                                text = "Customer Mobile Number",
                                color = Color.Black,
                                fontWeight = FontWeight.W300,
                                fontSize = 12.sp,
                                textAlign = TextAlign.Start
                            )
                            Text(
                                text = walletReport.results?.get(0)?.customerMobileNumber ?: "N/A",
                                color = Color.Black,
                                fontWeight = FontWeight.W300,
                                fontSize = 12.sp,
                                textAlign = TextAlign.Start
                            )
                        }

                        Column(modifier = Modifier.weight(0.1f)) {
                            Text(
                                text = "Name: ${walletReport.results?.get(0)?.beneName ?: "N/A"}",
                                color = Color.Black,
                                fontWeight = FontWeight.Bold,
                                fontSize = 20.sp,
                                textAlign = TextAlign.Start
                            )
                            Text(
                                text = "A/C No:: ${walletReport.results?.get(0)?.beneAccountNumber ?: "N/A"}",
                                color = Color.Black,
                                fontWeight = FontWeight.W300,
                                fontSize = 12.sp,
                                textAlign = TextAlign.Start
                            )
                            Text(
                                text = "Bank name: ${walletReport.results?.get(0)?.beneBankName ?: "N/A"}",
                                color = Color.Black,
                                fontWeight = FontWeight.W300,
                                fontSize = 12.sp,
                                textAlign = TextAlign.Start
                            )
                        }
                    }
                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .wrapContentHeight()
                            .background(color = Color.White)
                            .padding(15.dp),
                        horizontalArrangement = Arrangement.SpaceAround,
                    ) {
                        Column(modifier = Modifier.weight(0.1f)) {
                            Text(
                                text = "Transaction ID:",
                                fontWeight = FontWeight.Medium,
                                fontSize = 13.sp,
                                textAlign = TextAlign.Start
                            )
                            Spacer(modifier = Modifier.height(5.dp))
                            Text(
                                text = "Transaction Mode:",
                                fontWeight = FontWeight.Medium,
                                fontSize = 13.sp,
                                textAlign = TextAlign.Start
                            )
                            Spacer(modifier = Modifier.height(5.dp))
                            Text(
                                text = "Shop name:",
                                fontWeight = FontWeight.Medium,
                                fontSize = 13.sp,
                                textAlign = TextAlign.Start
                            )
                            Spacer(modifier = Modifier.height(5.dp))
                            Text(
                                text = "Date and Time:",
                                fontWeight = FontWeight.Medium,
                                fontSize = 13.sp,
                                textAlign = TextAlign.Start
                            )
                            Spacer(modifier = Modifier.height(5.dp))
                            Text(
                                text = "Description: ",
                                fontWeight = FontWeight.Medium,
                                fontSize = 13.sp,
                                textAlign = TextAlign.Start
                            )
                        }

                        Column(modifier = Modifier.weight(0.1f)) {
                            Text(
                                text = walletReport.results?.get(0)?.id ?: "N/A",
                                fontWeight = FontWeight.Medium,
                                fontSize = 13.sp,
                                textAlign = TextAlign.Start
                            )
                            Spacer(modifier = Modifier.height(5.dp))
                            Text(
                                text = walletReport.results?.get(0)?.operationPerformed ?: "N/A",
                                fontWeight = FontWeight.Medium,
                                fontSize = 13.sp,
                                textAlign = TextAlign.Start
                            )
                            Spacer(modifier = Modifier.height(5.dp))
                            Text(
                                text = if(SharedPrefClass.getStringValue(context,DataConstants.ShopName).toString()=="") "N/A" else SharedPrefClass.getStringValue(context,DataConstants.ShopName).toString(),
                                fontWeight = FontWeight.Medium,
                                fontSize = 13.sp,
                                textAlign = TextAlign.Start
                            )
                            Spacer(modifier = Modifier.height(5.dp))
                            Text(
                                text = timeDate,
                                fontWeight = FontWeight.Medium,
                                fontSize = 13.sp,
                                textAlign = TextAlign.Start
                            )
                            Spacer(modifier = Modifier.height(5.dp))
                            Text(
                                text = walletReport.results?.get(0)?.apiComment ?: "N/A",
                                fontWeight = FontWeight.Medium,
                                fontSize = 13.sp,
                                textAlign = TextAlign.Start
                            )
                        }
                    }

                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .wrapContentHeight()
                            .background(fingerGrey)
                            .padding(15.dp),
                        horizontalArrangement = Arrangement.SpaceAround,
                    ) {
                        Column {
                            Text(
                                text = "Details",
                                fontWeight = FontWeight.Medium,
                                fontSize = 13.sp,
                                textAlign = TextAlign.Start
                            )
                            Spacer(modifier = Modifier.height(10.dp))

                            LazyColumn(
                                contentPadding = PaddingValues(0.dp),
                                verticalArrangement = Arrangement.spacedBy(10.dp),
                                content = {
                                    if(gateWayList.size==0){
                                        val detailsModel = DmtGateWayDataItem(
                                            transactionStatusCode = "N/A",
                                            transactedBankName = "N/A",
                                            amount = walletReport.results?.get(0)?.amountTransacted?.toInt(),
                                            status = walletReport.results?.get(0)?.status,
                                            statusDesc = "N/A",
                                            id = "N/A",
                                            rrn = "N/A"
                                        )
                                        gateWayList.add(detailsModel)

                                    }
                                    items(gateWayList.size) { item ->
                                        DmtDetailsList(gateWayList[item])

                                    }
                                })
                        }
                    }
                }//Column
            }
        } else {
            androidx.compose.material.Text(text = "Something went to wrong..!!")
        }

    }
}


