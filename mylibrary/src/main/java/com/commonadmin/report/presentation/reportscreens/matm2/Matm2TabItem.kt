package com.commonadmin.report.presentation.reportscreens.matm2

import androidx.compose.runtime.Composable
import com.commonadmin.report.presentation.viewmodels.Matm2ReportViewModel

sealed class Matm2TabItem(val title: String, val screen: @Composable () -> Unit) {
    class Matm2AllReport(matm2ReportViewModel: Matm2ReportViewModel) :
        Matm2TabItem("All Reports", screen = { Matm2AllTrnsReport(matm2ReportViewModel) })

    class Matm2BalanceEnq(matm2ReportViewModel: Matm2ReportViewModel) : Matm2TabItem(
        "Balance Enquiry",
        screen = { Matm2BalanceEnqScreen(matm2ReportViewModel) }
    )

    class Matm2CashWidhDraw(matm2ReportViewModel: Matm2ReportViewModel) : Matm2TabItem(
        "Cash Withdraw",
        screen = { Matm2CashWithdrawlScreen(matm2ReportViewModel) }
    )
}