package com.commonadmin.report.presentation.reportscreens.matm2

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.commonadmin.report.data.remote.models.Matm2ReportItem
import com.commonadmin.report.ui.theme.Green
import com.commonadmin.report.ui.theme.GreyPrimary
import com.commonadmin.report.ui.theme.Yellow
import java.text.SimpleDateFormat
import java.util.Locale

@Composable
fun Matm2ReportLayout(matm2ReportItem: Matm2ReportItem?) {
    val isClicked = remember {
        mutableStateOf(false)
    }

    val timeT: Long = matm2ReportItem!!.createdDate!!.toLong()
    val formatter = SimpleDateFormat("dd-MM-yyyy  hh:mm a")
    val timeDate = formatter.format(timeT)

    Card(
        colors = CardDefaults.cardColors(
            containerColor = GreyPrimary,

            ),
        modifier = Modifier
            .wrapContentHeight()
            .clickable {
                isClicked.value = !isClicked.value
            },
        elevation = CardDefaults.cardElevation(defaultElevation = 10.dp)
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(20.dp)

        ) {
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Text(
                    text = "Date/Time: $timeDate",
                    color = Color.Black,
                    style = MaterialTheme.typography.headlineLarge,
                    fontSize = 15.sp
                )
                Text(
                    text = matm2ReportItem.status ?: "N/A",
                    color = when (matm2ReportItem.status?.uppercase(Locale.ROOT)) {
                        "REFUNDED" -> {
                            Yellow
                        }

                        "SUCCESS" -> {
                           Green
                        }

                        "FAILED" -> {
                            Color.Red
                        }

                        else -> {
                            Color.Black
                        }
                    },
                    style = MaterialTheme.typography.headlineLarge,
                    fontSize = 15.sp
                )
            }
            Spacer(modifier = Modifier.height(10.dp))

            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Text(
                    text = "Txn Type: ${matm2ReportItem.operationPerformed ?: "N/A"}",
                    color = Color.Black,
                    style = MaterialTheme.typography.headlineLarge,
                    fontSize = 15.sp
                )
                Spacer(modifier = Modifier.height(5.dp))
                Text(
                    text = "₹"+matm2ReportItem.amountTransacted.toString(),
                    color = Color.Black,
                    style = MaterialTheme.typography.headlineLarge,
                    fontSize = 15.sp
                )
            }
            Spacer(modifier = Modifier.height(5.dp))
            Text(
                text = "Card No: ${matm2ReportItem.cardDetails ?: "N/A"}",
                color = Color.Black,
                style = MaterialTheme.typography.headlineLarge,
                fontSize = 15.sp
            )
            Spacer(modifier = Modifier.height(5.dp))
            Text(
                text = "Card Holder Name: ${matm2ReportItem.cardHoldername.toString().ifEmpty { "N/A" }}",
                color = Color.Black,
                style = MaterialTheme.typography.headlineLarge,
                fontSize = 15.sp
            )
            Spacer(modifier = Modifier.height(5.dp))

            AnimatedVisibility(visible = isClicked.value) {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(vertical = 10.dp)
                ) {
                    Column(Modifier.weight(1.5f)) {
                        Spacer(modifier = Modifier.height(8.dp))
                        Text(
                            text = "Transaction ID:",
                            color = Color.Black,
                            style = MaterialTheme.typography.labelLarge,
                            fontSize = 15.sp
                        )
                        Spacer(modifier = Modifier.height(3.dp))
                        Text(
                            text = matm2ReportItem.id ?: "N/A",
                            color = Color.Black,
                            style = MaterialTheme.typography.headlineLarge,
                            fontSize = 13.sp
                        )
                        Spacer(modifier = Modifier.height(5.dp))


                        Text(
                            text = "Device Type:",
                            color = Color.Black,
                            style = MaterialTheme.typography.labelLarge,
                            fontSize = 15.sp
                        )
                        Spacer(modifier = Modifier.height(3.dp))
                        Text(
                            text = matm2ReportItem.deviceType ?: "N/A",
                            color = Color.Black,
                            style = MaterialTheme.typography.headlineLarge,
                            fontSize = 13.sp
                        )
                        Spacer(modifier = Modifier.height(5.dp))

                        Text(
                            text = "Device Version:",
                            color = Color.Black,
                            style = MaterialTheme.typography.labelLarge,
                            fontSize = 15.sp
                        )
                        Spacer(modifier = Modifier.height(3.dp))
                        Text(
                            text = matm2ReportItem.deviceVersion ?: "N/A",
                            color = Color.Black,
                            style = MaterialTheme.typography.headlineLarge,
                            fontSize = 13.sp
                        )
                        Spacer(modifier = Modifier.height(5.dp))

                        Text(
                            text = "Opening Balance:",
                            color = Color.Black,
                            style = MaterialTheme.typography.labelLarge,
                            fontSize = 15.sp
                        )
                        Spacer(modifier = Modifier.height(3.dp))
                        Text(
                            text = "₹"+matm2ReportItem.previousAmount.toString(),
                            color = Color.Black,
                            style = MaterialTheme.typography.headlineLarge,
                            fontSize = 13.sp
                        )
                    }
                    Column(Modifier.weight(1f)) {

                        Spacer(modifier = Modifier.height(5.dp))
                        Text(
                            text = "Card Type:",
                            color = Color.Black,
                            style = MaterialTheme.typography.labelLarge,
                            fontSize = 15.sp
                        )
                        Spacer(modifier = Modifier.height(3.dp))
                        Text(
                            text = matm2ReportItem.cardType ?: "N/A",
                            color = Color.Black,
                            style = MaterialTheme.typography.headlineLarge,
                            fontSize = 13.sp
                        )
                        Spacer(modifier = Modifier.height(5.dp))

                        Text(
                            text = "Device Serial No:",
                            color = Color.Black,
                            style = MaterialTheme.typography.labelLarge,
                            fontSize = 15.sp
                        )
                        Spacer(modifier = Modifier.height(3.dp))
                        Text(
                            text = matm2ReportItem.deviceSerialNo ?: "N/A",
                            color = Color.Black,
                            style = MaterialTheme.typography.headlineLarge,
                            fontSize = 13.sp
                        )
                        Spacer(modifier = Modifier.height(5.dp))
                        Text(
                            text = "",
                            color = Color.Black,
                            style = MaterialTheme.typography.labelLarge,
                            fontSize = 15.sp
                        )
                        Spacer(modifier = Modifier.height(3.dp))
                        Text(
                            text = "",
                            color = Color.Black,
                            style = MaterialTheme.typography.headlineLarge,
                            fontSize = 13.sp
                        )
                        Spacer(modifier = Modifier.height(8.dp))
                        Text(
                            text = "Closing Balance:",
                            color = Color.Black,
                            style = MaterialTheme.typography.labelLarge,
                            fontSize = 15.sp
                        )
                        Spacer(modifier = Modifier.height(3.dp))
                        Text(
                            text = "₹"+matm2ReportItem.balanceAmount.toString(),
                            color = Color.Black,
                            style = MaterialTheme.typography.headlineLarge,
                            fontSize = 13.sp
                        )

                    }
                }
            }

        }

    }//column
}