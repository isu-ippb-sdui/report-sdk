package com.commonadmin.report.presentation.viewmodels

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.commonadmin.report.data.remote.models.WalletCashoutReqModel
import com.commonadmin.report.data.remote.models.WalletCashoutRespModel
import com.commonadmin.report.domain.usercases.WalletCashoutReportUseCase
import com.commonadmin.report.utils.ApiResult
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class WalletCashoutReportViewModel @Inject constructor(private val userCases: WalletCashoutReportUseCase) :
    ViewModel() {
    var walletCashoutReportState: MutableStateFlow<WalletCashOutReportState> =
        MutableStateFlow(WalletCashOutReportState())
        private set

    var isVisible: MutableState<Boolean> = mutableStateOf(false)
        private set
    var searchText: MutableState<String> = mutableStateOf("")
        private set
    var totalLength: MutableState<String> = mutableStateOf("")
        private set
    var amountTransacted: MutableLiveData<String> = MutableLiveData("")
        private set


    fun getWalletCashOutReportData(
        url: String,
        walletCashoutReqModel: WalletCashoutReqModel,
        token: String
    ) {
        viewModelScope.launch {
            userCases.invoke(url=url,walletCashoutReqModel = walletCashoutReqModel,token=token)
                .collect { networkResult ->
                    when (networkResult) {
                        is ApiResult.Success -> {
                            walletCashoutReportState.emit(
                                walletCashoutReportState.value.copy(
                                    walletCashoutRespModel = networkResult.data,
                                    loading = false,
                                    error = null
                                )
                            )
                        }

                        is ApiResult.Loading -> {
                            walletCashoutReportState.emit(
                                walletCashoutReportState.value.copy(
                                    walletCashoutRespModel = null,
                                    loading = true,
                                    error = null
                                )
                            )
                        }

                        is ApiResult.Error -> {
                            walletCashoutReportState.emit(
                                walletCashoutReportState.value.copy(
                                    walletCashoutRespModel = null,
                                    loading = false,
                                    error = networkResult.message
                                )
                            )
                        }
                    }

                }

        }
    }


}

data class WalletCashOutReportState(
    var walletCashoutRespModel: WalletCashoutRespModel? =null,
    val loading: Boolean? = null,
    val error: String? = null
)
