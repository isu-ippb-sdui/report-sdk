package com.commonadmin.report.presentation.reportscreens.walletcashout

import android.app.Activity
import android.content.Intent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Divider
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalTextInputService
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import com.commonadmin.report.R
import com.commonadmin.report.ReportSDKActivity
import com.commonadmin.report.presentation.reportscreens.aeps.TxnIdAndSearch
import com.commonadmin.report.presentation.viewmodels.WalletCashoutReportViewModel
import com.commonadmin.report.ui.theme.primaryColor
import com.commonadmin.report.ui.theme.whiteColor
import com.commonadmin.report.utils.reportcalender.ReportCalenderConstant
import kotlinx.coroutines.delay


@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun WalletCashoutToolBarScreen(walletCashoutReportViewModel: WalletCashoutReportViewModel = hiltViewModel()) {
    val context = LocalContext.current
    val focusRequester = remember { FocusRequester() }
    val inputService = LocalTextInputService.current
    val length = remember {
        mutableStateOf("")
    }
    length.value = walletCashoutReportViewModel.totalLength.value

    val txnAmount = remember {
        mutableStateOf("")
    }

    txnAmount.value = walletCashoutReportViewModel.amountTransacted.value.toString()



    LaunchedEffect(walletCashoutReportViewModel.isVisible.value) {
        if (walletCashoutReportViewModel.isVisible.value) {
            delay(300)
            inputService?.showSoftwareKeyboard()
            focusRequester.requestFocus()
        }
    }

    Column(
        modifier = Modifier
            .fillMaxWidth()
    ) {
        TopAppBar(
            title = {
                if (!walletCashoutReportViewModel.isVisible.value) {
                    Column(verticalArrangement = Arrangement.Center) {
                        Text(
                            text = "Wallet CashOut Report",
                            fontSize = 16.sp
                        )
                        Text(
                            text = "${ReportCalenderConstant.selectToolbarFromDate} To ${ReportCalenderConstant.selectToolbarToDate}",
                            fontSize = 14.sp
                        )
                    }
                }
            }, //title
            navigationIcon = {
                IconButton(onClick = {  (context as Activity).finish() }) {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_arrow_back),
                        contentDescription = "Back"
                    )
                }
            },

            actions = {
                /*  IconButton(onClick = { *//* Handle first icon click *//* }) {
                    Icon(Icons.Filled.Search, "Search")
                }*/
                if (walletCashoutReportViewModel.isVisible.value) {
                    TxnIdAndSearch(
                        text = walletCashoutReportViewModel.searchText.value,
                        onTextChange = {
                            walletCashoutReportViewModel.searchText.value = it
                        },
                        onSearchClicked = {
                        },
                        onCloseClicked = {
                            walletCashoutReportViewModel.isVisible.value = false
                        },
                        modifier = Modifier.focusRequester(focusRequester)
                    )
                }
                if (!walletCashoutReportViewModel.isVisible.value) {
                    IconButton(
                        onClick = {
                            walletCashoutReportViewModel.isVisible.value =
                                !walletCashoutReportViewModel.isVisible.value
                        }
                    ) {
                        Icon(
                            painter = painterResource(id = R.drawable.ic_baseline_search_24),
                            contentDescription = "Search",
                            tint = Color.White
                        )

                    }
                }
                IconButton(onClick = {
                    val intent = Intent(context, ReportSDKActivity::class.java)
                    context.startActivity(intent)
                    (context as Activity).finish()
                }) {
                    Icon(
                        painter = painterResource(id = R.drawable.baseline_calendar_month_24),
                        "Calender"
                    )
                }
            },

            modifier = Modifier.fillMaxWidth(),
            colors = TopAppBarDefaults.mediumTopAppBarColors(
                containerColor = primaryColor,
                titleContentColor = whiteColor,
                navigationIconContentColor = whiteColor,
                actionIconContentColor = whiteColor

            )
        )
        Row() {
            Text(
                text = "Entities: ${length.value}",
                color = Color.Black,
                style = MaterialTheme.typography.headlineLarge,
                fontSize = 15.sp,
                modifier = Modifier
                    .weight(1f)
                    .padding(
                        top = 10.dp,
                        start = 10.dp
                    )
            )

            Column(modifier = Modifier.weight(2f)) {
                Text(
                    text = "Amount: ${"₹"+txnAmount.value}",
                    color = Color.Black,
                    style = MaterialTheme.typography.headlineLarge,
                    fontSize = 15.sp,
                    modifier = Modifier
                        .padding(
                            top = 10.dp,
                            end = 10.dp
                        )
                )
            }
        }
        Spacer(modifier = Modifier.height(5.dp))
        Divider(
            thickness = 1.dp,
            color = Color.Gray
        )
        WalletCashoutReportScreen(walletCashoutReportViewModel)
    }

}