package com.commonadmin.report.presentation.reportscreens.aeps

import android.app.Activity
import android.content.Intent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import com.commonadmin.report.R
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalTextInputService
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import com.commonadmin.report.ReportSDKActivity
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.HorizontalPager
import com.google.accompanist.pager.PagerState
import com.google.accompanist.pager.pagerTabIndicatorOffset
import com.google.accompanist.pager.rememberPagerState
import com.commonadmin.report.presentation.viewmodels.AepsReportViewModel
import com.commonadmin.report.ui.theme.primaryColor
import com.commonadmin.report.ui.theme.whiteColor
import com.commonadmin.report.utils.reportcalender.ReportCalenderConstant
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@Preview(showSystemUi = true, showBackground = true)
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AepsToolBarAppBar(aepsReportViewModel: AepsReportViewModel = hiltViewModel()) {

    val context = LocalContext.current
    val focusRequester = remember { FocusRequester() }
    val inputService = LocalTextInputService.current





    LaunchedEffect(aepsReportViewModel.isVisible.value) {
        if (aepsReportViewModel.isVisible.value) {
            delay(300)
            inputService?.let { it.showSoftwareKeyboard() }
            focusRequester.requestFocus()
        }
    }

    Column(
        modifier = Modifier
            .fillMaxWidth()
    ) {
        TopAppBar(
            title = {
                if (!aepsReportViewModel.isVisible.value) {
                    Column(verticalArrangement = Arrangement.Center) {
                        Text(
                            text = "Unified Aeps Report",
                            fontSize = 16.sp
                        )
                        Text(
                            text = "${ReportCalenderConstant.selectToolbarFromDate} To ${ReportCalenderConstant.selectToolbarToDate}",
                            fontSize = 14.sp
                        )
                    }
                }
            }, //title
            navigationIcon = {
                IconButton(onClick = {  (context as Activity).finish() }) {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_arrow_back),
                        contentDescription = "Back"
                    )
                }
            },

            actions = {
                /*  IconButton(onClick = { *//* Handle first icon click *//* }) {
                    Icon(Icons.Filled.Search, "Search")
                }*/
                if (aepsReportViewModel.isVisible.value) {
                    TxnIdAndSearch(
                        text = aepsReportViewModel.searchText.value,
                        onTextChange = {
                            aepsReportViewModel.searchText.value = it
                        },
                        onSearchClicked = {
                        },
                        onCloseClicked = {
                            aepsReportViewModel.isVisible.value = false
                        },
                        modifier = Modifier.focusRequester(focusRequester)
                    )
                }
                if (!aepsReportViewModel.isVisible.value) {
                    IconButton(
                        onClick = {
                            aepsReportViewModel.isVisible.value =
                                !aepsReportViewModel.isVisible.value
                        }
                    ) {
                        Icon(
                            painter = painterResource(id = R.drawable.ic_baseline_search_24),
                            contentDescription = "Search",
                            tint = Color.White
                        )

                    }
                }
                IconButton(onClick = {
                    val intent = Intent(context, ReportSDKActivity::class.java)
                    context.startActivity(intent)
                    (context as Activity).finish()
                }) {
                    Icon(
                        painter = painterResource(id = R.drawable.baseline_calendar_month_24),
                        "Calender"
                    )
                }
            },

            modifier = Modifier.fillMaxWidth(),
            colors = TopAppBarDefaults.mediumTopAppBarColors(
                containerColor = primaryColor,
                titleContentColor = whiteColor,
                navigationIconContentColor = whiteColor,
                actionIconContentColor = whiteColor

            )
        )
        MainTabLayoutContent(aepsReportViewModel)
    }

}

@Composable
fun TxnIdAndSearch(
    text: String,
    onTextChange: (String) -> Unit,
    onCloseClicked: () -> Unit,
    onSearchClicked: (String) -> Unit,
    modifier: Modifier = Modifier
) {
    TextField(
        modifier = modifier,
        value = text, onValueChange = {
            onTextChange(it)
        },
        placeholder = {
            Text(
                modifier = Modifier.alpha(ContentAlpha.medium),
                text = "Search Here",
                color = Color.White
            )
        },

        textStyle = TextStyle(
            fontSize = MaterialTheme.typography.bodyMedium.fontSize
        ),
        singleLine = true,


        trailingIcon = {
            IconButton(
                onClick = {
                    if (text.isNotEmpty()) {
                        onTextChange("")
                    } else {
                        onCloseClicked()
                    }
                }
            ) {
                Icon(
                    imageVector = Icons.Default.Close,
                    contentDescription = "Close",
                    tint = Color.White
                )

            }
        },
        keyboardOptions = KeyboardOptions(
            imeAction = ImeAction.Search
        ),
        keyboardActions = KeyboardActions(
            onSearch = {
                onSearchClicked(text)
            }
        ),
        colors = TextFieldDefaults.textFieldColors(
            backgroundColor = Color.Transparent,
            cursorColor = Color.White.copy(alpha = ContentAlpha.medium),
            textColor = Color.White,
            focusedIndicatorColor = Color.Transparent,
            unfocusedIndicatorColor = Color.Transparent,
        )

    )

}


@OptIn(ExperimentalPagerApi::class)
@Composable
fun MainTabLayoutContent(aepsReportViewModel: AepsReportViewModel) {
    Column(modifier = Modifier.fillMaxSize()) {
        val list = listOf(
            TabItem.AllReport(aepsReportViewModel),
            TabItem.BalanceEnq(aepsReportViewModel),
            TabItem.CashWidhDraw(aepsReportViewModel),
            TabItem.CashDeposit(aepsReportViewModel),
            TabItem.MiniSatement(aepsReportViewModel)
        )
        val pagerState = rememberPagerState()
        funTabLayout(list,pagerState)
    }
}

@OptIn(ExperimentalPagerApi::class)
@Composable
fun funTabLayout(
    tabs: List<TabItem>,
    pagerState: PagerState) {

    val scope = rememberCoroutineScope()

    Column(modifier = Modifier.wrapContentSize()) {
        ScrollableTabRow(
            selectedTabIndex = pagerState.currentPage,
            backgroundColor = primaryColor,
            indicator =  { tabPositions ->
                TabRowDefaults.Indicator(
                    Modifier.pagerTabIndicatorOffset(pagerState, tabPositions)
                )
            },
            contentColor = whiteColor
        ) {
            tabs.forEachIndexed { index, tabItem ->
                LeadingIconTab(selected = pagerState.currentPage == index,
                    onClick = {
                        scope.launch {
                            pagerState.animateScrollToPage(index)
                            when (index) {
                                0 -> {
                                    /*aepsReportViewModel.totalLength.value =
                                        aepsReportViewModel.reportList.value?.size.toString()
                                    aepsReportViewModel.trnsAmount.value = aepsReportViewModel.alltrnsAmount.value*/
                                }
                                1 -> {
                                   /* aepsReportViewModel.totalLength.value =
                                        aepsReportViewModel.balanceEnqList.value?.size.toString()
                                    aepsReportViewModel.trnsAmount.value = aepsReportViewModel.balEnqtrnsAmount.value*/
                                }
                                else -> {
                                    /*aepsReportViewModel.totalLength.value =
                                        aepsReportViewModel.cashWidrawList.value?.size.toString()
                                    aepsReportViewModel.trnsAmount.value = aepsReportViewModel.cashWidhtrnsAmount.value*/
                                }
                            }
                        }
                    },
                    text = {
                        Text(
                            text = tabItem.title,
                            color = Color.White
                        )
                    },
                    icon = {}
                )
            }
        }
        /*Row(modifier = Modifier.fillMaxWidth()) {
            Text(
                text = "Entities: ${aepsReportViewModel.totalLength.value}",
                color = Color.Black,
                style = MaterialTheme.typography.headlineLarge,
                fontSize = 15.sp,
                modifier = Modifier
                    .weight(1f)
                    .padding(
                        top = 10.dp,
                        start = 10.dp
                    )

            )
            Text(
                text = "Amount: ${txnAmount.value}",
                color = Color.Black,
                style = MaterialTheme.typography.headlineLarge,
                fontSize = 15.sp,
                modifier = Modifier
                    .weight(1f)
                    .padding(
                        top = 10.dp,
                        start = 10.dp
                    )
            )
        }*/

        HorizontalPager(count = tabs.size, state = pagerState, userScrollEnabled = false) { page ->
            tabs[page].screen()
        }

    }
}


