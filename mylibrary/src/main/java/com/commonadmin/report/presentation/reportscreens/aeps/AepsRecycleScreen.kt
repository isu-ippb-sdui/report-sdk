package com.commonadmin.report.presentation.reportscreens.aeps

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.commonadmin.report.data.remote.models.AepsReportItem
import com.commonadmin.report.ui.theme.*
import java.text.SimpleDateFormat
import java.util.Locale

@Composable
fun UnifiedAepsReportLayout(aepsReportItem: AepsReportItem?) {
    val isClicked = remember {
        mutableStateOf(false)
    }
    val timeT: Long = aepsReportItem?.createdDate!!.toLong()
    val formatter = SimpleDateFormat("dd-MM-yyyy  hh:mm a")
    val formatted = formatter.format(timeT)

    Card(
        colors = CardDefaults.cardColors(
            containerColor = GreyPrimary,

            ),
        modifier = Modifier
            .wrapContentHeight()
            .clickable {
                isClicked.value = !isClicked.value
            },
        elevation = CardDefaults.cardElevation(defaultElevation = 10.dp)
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(20.dp)

        ) {
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Text(
                    text = "Date/Time: $formatted",
                    color = Color.Black,
                    style = MaterialTheme.typography.headlineLarge,
                    fontSize = 15.sp
                )
                Text(
                    text = aepsReportItem.status ?: "N/A",
                    color = when (aepsReportItem.status?.uppercase(Locale.ROOT)) {
                        "REFUNDED" -> {
                            Yellow
                        }

                        "SUCCESS" -> {
                           Green
                        }

                        "FAILED" -> {
                            Color.Red
                        }

                        else -> {
                            Color.Black
                        }
                    },
                    style = MaterialTheme.typography.headlineLarge,
                    fontSize = 15.sp
                )
            }
            Spacer(modifier = Modifier.height(10.dp))

                Row(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    Text(
                        text = "Txn Type: ${aepsReportItem.operationPerformed ?: "N/A"}",
                        color = Color.Black,
                        style = MaterialTheme.typography.headlineLarge,
                        fontSize = 15.sp
                    )
                    Spacer(modifier = Modifier.height(5.dp))
                    Text(
                        text = "₹"+aepsReportItem.amountTransacted.toString(),
                        color = Color.Black,
                        style = MaterialTheme.typography.headlineLarge,
                        fontSize = 15.sp
                    )
                }
                Spacer(modifier = Modifier.height(5.dp))

                Text(
                    text = "Aadhaar No: ${aepsReportItem.referenceNo ?: "N/A"}",
                    color = Color.Black,
                    style = MaterialTheme.typography.headlineLarge,
                    fontSize = 15.sp
                )
                Spacer(modifier = Modifier.height(5.dp))

            AnimatedVisibility(visible = isClicked.value ) {
                Row(modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 10.dp)) {
                    Column(Modifier.weight(1.5f)) {
                        Spacer(modifier = Modifier.height(8.dp))
                        Text(
                            text = "Transaction ID: ",
                            color = Color.Black,
                            style = MaterialTheme.typography.labelLarge,
                            fontSize = 15.sp
                        )
                        Spacer(modifier = Modifier.height(3.dp))
                        Text(
                            text = aepsReportItem.id ?: "N/A",
                            color = Color.Black,
                            style = MaterialTheme.typography.headlineLarge,
                            fontSize = 13.sp
                        )
                        Spacer(modifier = Modifier.height(5.dp))

                        Text(
                            text = "Opening Balance: ",
                            color = Color.Black,
                            style = MaterialTheme.typography.labelLarge,
                            fontSize = 15.sp
                        )
                        Spacer(modifier = Modifier.height(3.dp))
                        Text(
                            text = "₹"+aepsReportItem.previousAmount.toString(),
                            color = Color.Black,
                            style = MaterialTheme.typography.headlineLarge,
                            fontSize = 13.sp
                        )
                    }
                    Column(Modifier.weight(1f)) {

                        Spacer(modifier = Modifier.height(5.dp))
                        Text(
                            text = "User Track ID:",
                            color = Color.Black,
                            style = MaterialTheme.typography.labelLarge,
                            fontSize = 15.sp
                        )
                        Spacer(modifier = Modifier.height(3.dp))
                        Text(
                            text = aepsReportItem.apiTid ?: "N/A",
                            color = Color.Black,
                            style = MaterialTheme.typography.headlineLarge,
                            fontSize = 13.sp
                        )
                        Spacer(modifier = Modifier.height(5.dp))
                        Text(
                            text = "Closing Balance: ",
                            color = Color.Black,
                            style = MaterialTheme.typography.labelLarge,
                            fontSize = 15.sp
                        )
                        Spacer(modifier = Modifier.height(3.dp))
                        Text(
                            text = "₹"+aepsReportItem.balanceAmount.toString(),
                            color = Color.Black,
                            style = MaterialTheme.typography.headlineLarge,
                            fontSize = 13.sp
                        )

                    }
                }
            }

        }

    }//column
}