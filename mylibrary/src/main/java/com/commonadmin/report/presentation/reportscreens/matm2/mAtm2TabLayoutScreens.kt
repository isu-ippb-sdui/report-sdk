package com.commonadmin.report.presentation.reportscreens.matm2

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Text
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.commonadmin.report.data.remote.models.Matm2ReportItem
import com.commonadmin.report.data.remote.models.Matm2ReqModel
import com.commonadmin.report.presentation.viewmodels.Matm2ReportViewModel
import com.commonadmin.report.ui.theme.primaryColor
import com.commonadmin.report.utils.DataConstants
import com.commonadmin.report.utils.SharedPrefClass
import com.commonadmin.report.utils.reportcalender.ReportCalenderConstant
import java.util.Locale

@Composable
fun Matm2AllTrnsReport(matm2ReportViewModel: Matm2ReportViewModel) {
    val matm2ReportState = matm2ReportViewModel.matm2ReportState.collectAsState()
    val context = LocalContext.current

    val remTrnsAmount = remember {
        mutableStateOf("")
    }
    LaunchedEffect(key1 = true) {
        val trnsTrns: ArrayList<String> = ArrayList()
        trnsTrns.add("INITIATED")
        trnsTrns.add("AUTH_SUCCESS")
        trnsTrns.add("AUTH_DECLINE")
        trnsTrns.add("FAILED")
        val matmArray: ArrayList<String> = ArrayList()
        matmArray.add("mATM")
        matmArray.add("MATM2")

        val trnsType: ArrayList<String> = ArrayList()
        trnsType.add("mATM_BALANCE_ENQUIRY")
        trnsType.add("mATM_CASH_WITHDRAWAL")
        trnsType.add("MATM2_BALANCE_ENQUIRY")
        trnsType.add("MATM2_CASH_WITHDRAWAL")


        val matm2ReportReqModel = Matm2ReqModel(
            "matm_txn_common", SharedPrefClass.getStringValue(context, DataConstants.USERNAME),
            trnsTrns, ReportCalenderConstant.selectFromDate, ReportCalenderConstant.selectToDate, matmArray,trnsType
        )

        if (matm2ReportState.value.matm2RespModel == null) {
            matm2ReportViewModel.getMatm2ReportData(
                SharedPrefClass.getStringValue(context, "Matm2Report").toString(),
                matm2ReportReqModel,SharedPrefClass.getStringValue(context,DataConstants.TOKEN).toString()
            )
        }


    }

    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.TopCenter
    ) {
        if (matm2ReportState.value.loading == true && matm2ReportState.value.matm2RespModel == null) {
            Box(
                modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ) {
                CircularProgressIndicator(color = primaryColor)
            }
        } else if (matm2ReportState.value.loading == false && matm2ReportState.value.matm2RespModel != null) {
            matm2ReportState.value.matm2RespModel?.let { aepsReport ->

                val filteredList: ArrayList<Matm2ReportItem?> = arrayListOf()
                matm2ReportViewModel.reportList.value = aepsReport.results?.report



                Column {
                    remTrnsAmount.value = matm2ReportViewModel.setAllTrnsAmount.value
                    Row(modifier = Modifier.fillMaxWidth()) {
                        androidx.compose.material3.Text(
                            text = "Entities: ${matm2ReportViewModel.setAllTrnsLength.value}",
                            color = Color.Black,
                            style = MaterialTheme.typography.headlineLarge,
                            fontSize = 15.sp,
                            modifier = Modifier
                                .weight(1f)
                                .padding(
                                    top = 10.dp,
                                    start = 10.dp
                                )

                        )
                        androidx.compose.material3.Text(
                            text = "Amount: ${"₹"+matm2ReportViewModel.setAllTrnsAmount.value}",
                            color = Color.Black,
                            style = MaterialTheme.typography.headlineLarge,
                            fontSize = 15.sp,
                            modifier = Modifier
                                .weight(1f)
                                .padding(
                                    top = 10.dp,
                                    start = 10.dp
                                )
                        )
                    }
                    LazyColumn(
                        contentPadding = PaddingValues(10.dp),
                        verticalArrangement = Arrangement.spacedBy(10.dp),
                        content = {
                            if (matm2ReportViewModel.searchText.value.isEmpty()) {
                                matm2ReportViewModel.setAllTrnsLength.value =
                                    matm2ReportViewModel.reportList.value?.size.toString()
                                var ammount = 0
                                matm2ReportViewModel.reportList.value?.forEach {
                                    if (it?.status.equals("SUCCESS", true)) {
                                        ammount += it?.amountTransacted ?: 0
                                    }
                                }
                                matm2ReportViewModel.setAllTrnsAmount.value = ammount.toString()
                                items(matm2ReportViewModel.reportList.value?.size!!) { item ->
                                    Matm2ReportLayout(aepsReport.results?.report?.get(item))
                                }
                            } else {
                                filteredList.clear()
                                for (report in aepsReport.results?.report!!) {
                                    if (report.operationPerformed?.lowercase(Locale.ROOT)!!.contains(
                                                matm2ReportViewModel.searchText.value.lowercase(
                                                    Locale.ROOT
                                                )
                                            ) ||
                                        report.status?.lowercase(Locale.ROOT)!!.contains(
                                            matm2ReportViewModel.searchText.value.lowercase(Locale.ROOT)
                                        ) ||
                                        report.id?.lowercase(Locale.ROOT)!!.contains(
                                            matm2ReportViewModel.searchText.value.lowercase(Locale.ROOT)
                                        )||
                                        report.cardHoldername?.lowercase(Locale.ROOT)!!.contains(
                                            matm2ReportViewModel.searchText.value.lowercase(Locale.ROOT)
                                        )||
                                        report.deviceType?.lowercase(Locale.ROOT)!!.contains(
                                            matm2ReportViewModel.searchText.value.lowercase(Locale.ROOT)
                                        )||
                                        report.deviceVersion?.lowercase(Locale.ROOT)!!.contains(
                                            matm2ReportViewModel.searchText.value.lowercase(Locale.ROOT)
                                        )||
                                        report.deviceSerialNo?.lowercase(Locale.ROOT)!!.contains(
                                            matm2ReportViewModel.searchText.value.lowercase(Locale.ROOT)
                                        )||
                                        report.cardType?.lowercase(Locale.ROOT)!!.contains(
                                            matm2ReportViewModel.searchText.value.lowercase(Locale.ROOT)
                                        )

                                    ) {
                                        filteredList.add(report)
                                    }
                                }
                                filteredList.let {
                                    matm2ReportViewModel.setAllTrnsLength.value = it.size.toString()
                                    var ammount = 0
                                    for (report in it.indices) {
                                        if (it[report]?.status?.equals("SUCCESS", true) == true) {
                                            ammount += it[report]?.amountTransacted!!
                                        }
                                    }
                                    matm2ReportViewModel.setAllTrnsAmount.value = ammount.toString()
                                    items(it.size) { item ->
                                        Matm2ReportLayout(it[item])
                                    }
                                }
                            }
                        }
                    )
                    if (matm2ReportViewModel.searchText.value != "" && filteredList.isEmpty() || matm2ReportViewModel.reportList.value?.isEmpty() == true) {
                        matm2ReportViewModel.setAllTrnsAmount.value = 0.toString()
                        Box(
                            modifier = Modifier.fillMaxSize(),
                            contentAlignment = Alignment.Center
                        ) {
                            Text(text = "No Data Available")
                        }
                    }
                }
            }
        } else {
            matm2ReportViewModel.setAllTrnsAmount.value = 0.toString()
            Box(
                modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ) {
                Text(text = "Something went to wrong")
            }
        }
    }

}

@Composable
fun Matm2BalanceEnqScreen(matm2ReportViewModel: Matm2ReportViewModel) {
    val matm2ReportState = matm2ReportViewModel.matm2ReportState.collectAsState()
    matm2ReportState.value.matm2RespModel?.let {
        matm2ReportViewModel.balanceEnqList.value = it.results?.report?.filter { item ->
            item.operationPerformed.equals("mATM_BALANCE_ENQUIRY")
        }
    }

    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.TopCenter
    ) {
        if (matm2ReportState.value.loading == true && matm2ReportState.value.matm2RespModel == null) {
            Box(
                modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ) {
                CircularProgressIndicator(color = primaryColor)
            }
        } else if (matm2ReportState.value.loading == false && matm2ReportState.value.matm2RespModel != null) {
            matm2ReportViewModel.balanceEnqList.value?.let { aepsReport ->
                val filteredList: ArrayList<Matm2ReportItem?> = arrayListOf()
                val reportList: List<Matm2ReportItem?> = aepsReport


                Column {
                    Row(modifier = Modifier.fillMaxWidth()) {
                        androidx.compose.material3.Text(
                            text = "Entities: ${matm2ReportViewModel.setBalanceTrnsLength.value}",
                            color = Color.Black,
                            style = MaterialTheme.typography.headlineLarge,
                            fontSize = 15.sp,
                            modifier = Modifier
                                .weight(1f)
                                .padding(
                                    top = 10.dp,
                                    start = 10.dp
                                )

                        )
                        androidx.compose.material3.Text(
                            text = "Amount: ${"₹"+matm2ReportViewModel.setBalanceEnqTrnsAmount.value}",
                            color = Color.Black,
                            style = MaterialTheme.typography.headlineLarge,
                            fontSize = 15.sp,
                            modifier = Modifier
                                .weight(1f)
                                .padding(
                                    top = 10.dp,
                                    start = 10.dp
                                )
                        )
                    }
                    LazyColumn(
                        contentPadding = PaddingValues(10.dp),
                        verticalArrangement = Arrangement.spacedBy(10.dp),
                        content = {
                            if (matm2ReportViewModel.searchText.value.isEmpty()) {
                                var ammount = 0
                                matm2ReportViewModel.balanceEnqList.value?.forEach {
                                    if (it?.status.equals("SUCCESS", true)) {
                                        ammount += it?.amountTransacted ?: 0
                                    }
                                }
                                matm2ReportViewModel.setBalanceEnqTrnsAmount.value = ammount.toString()

                                matm2ReportViewModel.setBalanceTrnsLength.value = matm2ReportViewModel.balanceEnqList.value!!.size.toString()
                                items(reportList.size) { item ->
                                    Matm2ReportLayout(aepsReport[item])
                                }
                            } else {
                                for (report in aepsReport) {
                                    if (report?.operationPerformed?.lowercase(Locale.ROOT)!!.contains(
                                            matm2ReportViewModel.searchText.value.lowercase(
                                                Locale.ROOT
                                            )
                                        ) ||
                                        report.status?.lowercase(Locale.ROOT)!!.contains(
                                            matm2ReportViewModel.searchText.value.lowercase(Locale.ROOT)
                                        ) ||
                                        report.id?.lowercase(Locale.ROOT)!!.contains(
                                            matm2ReportViewModel.searchText.value.lowercase(Locale.ROOT)
                                        )||
                                        report.cardHoldername?.lowercase(Locale.ROOT)!!.contains(
                                            matm2ReportViewModel.searchText.value.lowercase(Locale.ROOT)
                                        )||
                                        report.deviceType?.lowercase(Locale.ROOT)!!.contains(
                                            matm2ReportViewModel.searchText.value.lowercase(Locale.ROOT)
                                        )||
                                        report.deviceVersion?.lowercase(Locale.ROOT)!!.contains(
                                            matm2ReportViewModel.searchText.value.lowercase(Locale.ROOT)
                                        )||
                                        report.deviceSerialNo?.lowercase(Locale.ROOT)!!.contains(
                                            matm2ReportViewModel.searchText.value.lowercase(Locale.ROOT)
                                        )||
                                        report.cardType?.lowercase(Locale.ROOT)!!.contains(
                                            matm2ReportViewModel.searchText.value.lowercase(Locale.ROOT)
                                        )

                                    ) {
                                        filteredList.add(report)
                                    }
                                }

                                filteredList.let {
                                    matm2ReportViewModel.setBalanceTrnsLength.value =
                                        it.size.toString()
                                    var ammount = 0
                                    for (report in it.indices) {
                                        if (filteredList[report]?.status?.equals("SUCCESS", true) == true) {
                                            ammount += it[report]?.amountTransacted!!
                                        }
                                    }
                                    matm2ReportViewModel.setBalanceEnqTrnsAmount.value =
                                        ammount.toString()
                                    items(it.size) { item ->
                                        Matm2ReportLayout(it[item])
                                    }
                                }
                            }

                        }
                    )
                    if (matm2ReportViewModel.searchText.value != "" && filteredList.isEmpty() || reportList.isEmpty()) {
                        matm2ReportViewModel.setBalanceEnqTrnsAmount.value = 0.toString()
                        Box(
                            modifier = Modifier.fillMaxSize(),
                            contentAlignment = Alignment.Center
                        ) {
                            Text(text = "No Data Available")
                        }
                    }
                }
            }
        } else {
            matm2ReportViewModel.setBalanceEnqTrnsAmount.value = 0.toString()
            Box(
                modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ) {
                Text(text = "Something went to wrong")
            }
        }
    }


}

@Composable
fun Matm2CashWithdrawlScreen(matm2ReportViewModel: Matm2ReportViewModel) {
    val matm2ReportState = matm2ReportViewModel.matm2ReportState.collectAsState()
    matm2ReportState.value.matm2RespModel?.let {
        matm2ReportViewModel.cashWidrawList.value = it.results?.report?.filter { item ->
            item.operationPerformed.equals("mATM_CASH_WITHDRAWAL")
        }
    }


    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.TopCenter
    ) {
        if (matm2ReportState.value.loading == true && matm2ReportState.value.matm2RespModel == null) {
            Box(
                modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ) {
                CircularProgressIndicator(color = primaryColor)
            }
        } else if (matm2ReportState.value.loading == false && matm2ReportState.value.matm2RespModel != null) {
            matm2ReportViewModel.cashWidrawList.value?.let { aepsReport ->
                val filteredList: ArrayList<Matm2ReportItem?> = arrayListOf()
                val reportList: List<Matm2ReportItem?> = aepsReport

                Column {
                    Row(modifier = Modifier.fillMaxWidth()) {
                        androidx.compose.material3.Text(
                            text = "Entities: ${matm2ReportViewModel.setcashTrnsLength.value}",
                            color = Color.Black,
                            style = MaterialTheme.typography.headlineLarge,
                            fontSize = 15.sp,
                            modifier = Modifier
                                .weight(1f)
                                .padding(
                                    top = 10.dp,
                                    start = 10.dp
                                )

                        )
                        androidx.compose.material3.Text(
                            text = "Amount: ${"₹"+matm2ReportViewModel.setCashWidTrnsAmount.value}",
                            color = Color.Black,
                            style = MaterialTheme.typography.headlineLarge,
                            fontSize = 15.sp,
                            modifier = Modifier
                                .weight(1f)
                                .padding(
                                    top = 10.dp,
                                    start = 10.dp
                                )
                        )
                    }
                    LazyColumn(
                        contentPadding = PaddingValues(10.dp),
                        verticalArrangement = Arrangement.spacedBy(10.dp),
                        content = {
                            if (matm2ReportViewModel.searchText.value.isEmpty()) {

                                var ammount = 0
                                matm2ReportViewModel.cashWidrawList.value?.forEach {
                                    if (it?.status.equals("SUCCESS", true)) {
                                        ammount += it?.amountTransacted ?: 0
                                    }
                                }
                                matm2ReportViewModel.setCashWidTrnsAmount.value = ammount.toString()

                                matm2ReportViewModel.setcashTrnsLength.value = matm2ReportViewModel.cashWidrawList.value!!.size.toString()
                                items(aepsReport.size) { item ->
                                    Matm2ReportLayout(aepsReport[item])
                                }
                            } else {
                                for (report in aepsReport) {
                                    if (report?.operationPerformed?.lowercase(Locale.ROOT)!!.contains(
                                            matm2ReportViewModel.searchText.value.lowercase(
                                                Locale.ROOT
                                            )
                                        ) ||
                                        report.status?.lowercase(Locale.ROOT)!!.contains(
                                            matm2ReportViewModel.searchText.value.lowercase(Locale.ROOT)
                                        ) ||
                                        report.id?.lowercase(Locale.ROOT)!!.contains(
                                            matm2ReportViewModel.searchText.value.lowercase(Locale.ROOT)
                                        )||
                                        report.cardHoldername?.lowercase(Locale.ROOT)!!.contains(
                                            matm2ReportViewModel.searchText.value.lowercase(Locale.ROOT)
                                        )||
                                        report.deviceType?.lowercase(Locale.ROOT)!!.contains(
                                            matm2ReportViewModel.searchText.value.lowercase(Locale.ROOT)
                                        )||
                                        report.deviceVersion?.lowercase(Locale.ROOT)!!.contains(
                                            matm2ReportViewModel.searchText.value.lowercase(Locale.ROOT)
                                        )||
                                        report.deviceSerialNo?.lowercase(Locale.ROOT)!!.contains(
                                            matm2ReportViewModel.searchText.value.lowercase(Locale.ROOT)
                                        )||
                                        report.cardType?.lowercase(Locale.ROOT)!!.contains(
                                            matm2ReportViewModel.searchText.value.lowercase(Locale.ROOT)
                                        )

                                    ) {
                                        filteredList.add(report)
                                    }
                                }
                                filteredList.let {
                                    matm2ReportViewModel.setcashTrnsLength.value =
                                        it.size.toString()
                                    var ammount = 0
                                    for (report in it.indices) {
                                        if (it[report]?.status?.equals(
                                                "SUCCESS",
                                                true
                                            ) == true
                                        ) {
                                            ammount += it[report]?.amountTransacted!!
                                        }
                                    }
                                    matm2ReportViewModel.setCashWidTrnsAmount.value =
                                        ammount.toString()
                                    items(it.size) { item ->
                                        Matm2ReportLayout(it[item])
                                    }
                                }
                            }
                        }
                    )
                    if (matm2ReportViewModel.searchText.value != "" && filteredList.isEmpty() || reportList.isEmpty()) {
                        matm2ReportViewModel.setCashWidTrnsAmount.value = 0.toString()
                        Box(
                            modifier = Modifier.fillMaxSize(),
                            contentAlignment = Alignment.Center
                        ) {
                            Text(text = "No Data Available")
                        }
                    }
                }
            }
        } else {
            matm2ReportViewModel.setCashWidTrnsAmount.value = 0.toString()
            Box(
                modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ) {
                Text(text = "Something went to wrong")
            }
        }
    }
}




