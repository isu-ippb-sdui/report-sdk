package com.commonadmin.report.presentation.viewmodels

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.commonadmin.report.data.remote.models.BBPSReportReqModel
import com.commonadmin.report.data.remote.models.BBPSReportRespModel
import com.commonadmin.report.domain.usercases.BBPSReportUseCase
import com.commonadmin.report.utils.ApiResult
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class BBPSReportViewModel @Inject constructor(private val userCases: BBPSReportUseCase) :
    ViewModel() {
    var bbpsReportState: MutableStateFlow<BBPSReportState> =
        MutableStateFlow(BBPSReportState())
        private set

    var isVisible: MutableState<Boolean> = mutableStateOf(false)
        private set
    var searchText: MutableState<String> = mutableStateOf("")
        private set
    var totalLength: MutableState<String> = mutableStateOf("")
        private set
    var amountTransacted: MutableLiveData<String> = MutableLiveData("")
        private set


    fun getBBPSReportData(url: String, bbpsReportReqModel: BBPSReportReqModel, token: String) {
        viewModelScope.launch {
            userCases.invoke(url=url,bbpsReportReqModel = bbpsReportReqModel,token=token)
                .collect { networkResult ->
                    when (networkResult) {
                        is ApiResult.Success -> {
                            bbpsReportState.emit(
                                bbpsReportState.value.copy(
                                    bbpsReportRespModel = networkResult.data,
                                    loading = false,
                                    error = null
                                )
                            )
                        }

                        is ApiResult.Loading -> {
                            bbpsReportState.emit(
                                bbpsReportState.value.copy(
                                    bbpsReportRespModel = null,
                                    loading = true,
                                    error = null
                                )
                            )
                        }

                        is ApiResult.Error -> {
                            bbpsReportState.emit(
                                bbpsReportState.value.copy(
                                    bbpsReportRespModel = null,
                                    loading = false,
                                    error = networkResult.message
                                )
                            )
                        }
                    }

                }

        }
    }


}

data class BBPSReportState(
    var bbpsReportRespModel: BBPSReportRespModel?=null,
    val loading: Boolean? = null,
    val error: String? = null
)
