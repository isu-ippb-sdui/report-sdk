package com.commonadmin.report.presentation.reportscreens.aeps

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Text
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.commonadmin.report.data.remote.models.AepsReportItem
import com.commonadmin.report.data.remote.models.UnifiedAepsReqModel
import com.commonadmin.report.presentation.viewmodels.AepsReportViewModel
import com.commonadmin.report.ui.theme.primaryColor
import com.commonadmin.report.utils.DataConstants
import com.commonadmin.report.utils.SharedPrefClass
import com.commonadmin.report.utils.reportcalender.ReportCalenderConstant
import java.util.Locale

@Composable
fun AllTrnsReport(aepsReportViewModel: AepsReportViewModel) {
    val aepsReportState = aepsReportViewModel.aepsReportState.collectAsState()
    val context = LocalContext.current
    LaunchedEffect(key1 = true) {
        val trnsTrns: ArrayList<String> = ArrayList()
        trnsTrns.add("INITIATED")
        trnsTrns.add("PENDING")
        trnsTrns.add("SUCCESS")
        trnsTrns.add("FAILED")
        val unifiedAeps: ArrayList<String> = ArrayList()
        unifiedAeps.add("UnifiedAEPS")
        unifiedAeps.add("AEPS_BIO_AUTH")
        unifiedAeps.add("AEPS_CASH_DEPOSIT")
        unifiedAeps.add("AEPS_CASH_WITHDRAWAL")
        unifiedAeps.add("AEPS_BALANCE_ENQUIRY")
        unifiedAeps.add("AEPS_MINI_STATEMENT")


        val trnsType: ArrayList<String> = ArrayList()
        trnsType.add("AEPS_CASH_WITHDRAWAL")
        trnsType.add("AEPS_BALANCE_ENQUIRY")
        trnsType.add("AEPS_MINI_STATEMENT")
        trnsType.add("AEPS_CASH_DEPOSIT")
        trnsType.add("AEPS_BIO_AUTH")


        val aepsRequesModel = UnifiedAepsReqModel(
            "aeps_txn_common", SharedPrefClass.getStringValue(context, DataConstants.USERNAME),
            trnsTrns, ReportCalenderConstant.selectFromDate, ReportCalenderConstant.selectToDate, unifiedAeps,trnsType
        )

        if (aepsReportState.value.unifiedAepsRespModel == null) {
            aepsReportViewModel.getUnifiedAepsReportData(
                SharedPrefClass.getStringValue(context, "UnifiedAepsReport").toString(),
                aepsRequesModel,
                SharedPrefClass.getStringValue(context,DataConstants.TOKEN).toString()
            )
        }
    }

    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.TopCenter
    ) {
        if (aepsReportState.value.loading == true && aepsReportState.value.unifiedAepsRespModel == null) {
            Box(modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ){
                CircularProgressIndicator(color = primaryColor)
            }
        } else if (aepsReportState.value.loading == false && aepsReportState.value.unifiedAepsRespModel != null) {
            aepsReportState.value.unifiedAepsRespModel?.let { aepsReport ->

                val filteredList: ArrayList<AepsReportItem?> = arrayListOf()
                aepsReportViewModel.reportList.value = aepsReport.results?.report


                Column {
                    Row(modifier = Modifier.fillMaxWidth()) {
                        androidx.compose.material3.Text(
                            text = "Entities: ${aepsReportViewModel.setTrnsLength.value}",
                            color = Color.Black,
                            style = MaterialTheme.typography.headlineLarge,
                            fontSize = 15.sp,
                            modifier = Modifier
                                .weight(1f)
                                .padding(
                                    top = 10.dp,
                                    start = 10.dp
                                )

                        )
                        androidx.compose.material3.Text(
                            text = "Amount: ${"₹"+aepsReportViewModel.setAllTrnsAmount.value}",
                            color = Color.Black,
                            style = MaterialTheme.typography.headlineLarge,
                            fontSize = 15.sp,
                            modifier = Modifier
                                .weight(1f)
                                .padding(
                                    top = 10.dp,
                                    start = 10.dp
                                )
                        )
                    }
                    LazyColumn(
                        contentPadding = PaddingValues(10.dp),
                        verticalArrangement = Arrangement.spacedBy(10.dp),
                        content = {
                            if (aepsReportViewModel.searchText.value.isEmpty()) {
                                aepsReportViewModel.setTrnsLength.value = aepsReportViewModel.reportList.value?.size.toString()

                                var trnsAmount = 0
                                for (report in  aepsReportViewModel.reportList.value?.indices!!) {
                                    if(aepsReportViewModel.reportList.value!![report]?.status?.equals("SUCCESS",true) == true) {
                                        trnsAmount += aepsReportViewModel.reportList.value!![report]?.amountTransacted!!
                                    }
                                }
                                aepsReportViewModel.setAllTrnsAmount.value = trnsAmount.toString()

                                items(aepsReportViewModel.reportList.value?.size!!) { item ->
                                    UnifiedAepsReportLayout(aepsReport.results?.report?.get(item))
                                }
                            } else {
                                filteredList.clear()
                                for (report in aepsReport.results?.report!!) {
                                    if (report.operationPerformed?.lowercase(Locale.ROOT)!!
                                            .contains(
                                                aepsReportViewModel.searchText.value.lowercase(
                                                    Locale.ROOT
                                                )
                                            ) ||
                                        report.status?.lowercase(Locale.ROOT)!!.contains(
                                            aepsReportViewModel.searchText.value.lowercase(Locale.ROOT)
                                        ) ||
                                        report.id?.lowercase(Locale.ROOT)!!.contains(
                                            aepsReportViewModel.searchText.value.lowercase(Locale.ROOT)
                                        )

                                    ) {
                                        filteredList.add(report)
                                    }
                                }
                                filteredList.let {
                                    aepsReportViewModel.setTrnsLength.value = it.size.toString()
                                    var ammount = 0
                                    for (report in it.indices) {
                                        if(filteredList[report]?.status?.equals("SUCCESS",true) == true) {
                                            ammount += filteredList[report]?.amountTransacted!!
                                        }
                                    }
                                    aepsReportViewModel.setAllTrnsAmount.value = ammount.toString()
                                    items(it.size) { item ->
                                        UnifiedAepsReportLayout(it[item])
                                    }
                                }
                            }
                        }
                    )
                    if (aepsReportViewModel.searchText.value != "" && filteredList.isEmpty()) {
                        Box(modifier = Modifier.fillMaxSize(),
                            contentAlignment = Alignment.Center
                        ){
                            Text(text = "No Data Available")
                        }
                    }
                    if (aepsReportViewModel.reportList.value?.isEmpty() == true) {
                        Box(modifier = Modifier.fillMaxSize(),
                            contentAlignment = Alignment.Center
                        ){
                            Text(text = "No Data Available")
                        }
                    }
                }


            }
        } else {
            Box(modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ){
                Text(text = "Something went to wrong!")
            }

        }
    }

}

@Composable
fun BalanceEnqScreen(aepsReportViewModel: AepsReportViewModel) {
    val aepsReportState = aepsReportViewModel.aepsReportState.collectAsState()
    aepsReportState.value.unifiedAepsRespModel?.let {
        aepsReportViewModel.balanceEnqList.value = it.results?.report?.filter { item ->
            item?.operationPerformed.equals("AEPS_BALANCE_ENQUIRY")
        }
    }

    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.TopCenter
    ) {
        if (aepsReportState.value.loading == true && aepsReportState.value.unifiedAepsRespModel == null) {
            Box(modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ){
                CircularProgressIndicator(color = primaryColor)
            }
        } else if (aepsReportState.value.loading == false && aepsReportState.value.unifiedAepsRespModel != null) {
            aepsReportViewModel.balanceEnqList.value?.let { aepsReport ->
                val filteredList: ArrayList<AepsReportItem?> = arrayListOf()
                val reportList: List<AepsReportItem?> = aepsReport

                Column {
                    Row(modifier = Modifier.fillMaxWidth()) {
                        androidx.compose.material3.Text(
                            text = "Entities: ${aepsReportViewModel.setBalanceTrnsLength.value}",
                            color = Color.Black,
                            style = MaterialTheme.typography.headlineLarge,
                            fontSize = 15.sp,
                            modifier = Modifier
                                .weight(1f)
                                .padding(
                                    top = 10.dp,
                                    start = 10.dp
                                )

                        )
                        androidx.compose.material3.Text(
                            text = "Amount: ${"₹"+aepsReportViewModel.setBalanceEnqTrnsAmount.value}",
                            color = Color.Black,
                            style = MaterialTheme.typography.headlineLarge,
                            fontSize = 15.sp,
                            modifier = Modifier
                                .weight(1f)
                                .padding(
                                    top = 10.dp,
                                    start = 10.dp
                                )
                        )
                    }
                    LazyColumn(
                        contentPadding = PaddingValues(10.dp),
                        verticalArrangement = Arrangement.spacedBy(10.dp),
                        content = {
                            if (aepsReportViewModel.searchText.value.isEmpty()) {
                                aepsReportViewModel.setBalanceTrnsLength.value = aepsReportViewModel.balanceEnqList.value!!.size.toString()

                                var totalAmount = 0
                                for (report in reportList.indices) {
                                    if(reportList[report]?.status?.equals("SUCCESS",true) == true) {
                                        totalAmount += reportList[report]?.amountTransacted!!
                                    }
                                }
                                aepsReportViewModel.setBalanceEnqTrnsAmount.value = totalAmount.toString()

                                items(reportList.size) { item ->
                                    UnifiedAepsReportLayout(aepsReport[item])
                                }
                            } else {
                                for (report in aepsReport) {
                                    if (report?.operationPerformed?.lowercase(Locale.ROOT)!!
                                            .contains(
                                                aepsReportViewModel.searchText.value.lowercase(
                                                    Locale.ROOT
                                                )
                                            ) ||
                                        report.status?.lowercase(Locale.ROOT)!!.contains(
                                            aepsReportViewModel.searchText.value.lowercase(Locale.ROOT)
                                        ) ||
                                        report.id?.lowercase(Locale.ROOT)!!.contains(
                                            aepsReportViewModel.searchText.value.lowercase(Locale.ROOT)
                                        )

                                    ) {
                                        filteredList.add(report)
                                    }
                                }

                                filteredList.let {
                                    aepsReportViewModel.setBalanceTrnsLength.value = it.size.toString()
                                    var ammount = 0
                                    for (report in it.indices) {
                                        if(filteredList[report]?.status?.equals("SUCCESS",true) == true) {
                                            ammount += filteredList[report]?.amountTransacted!!
                                        }
                                    }
                                    aepsReportViewModel.setBalanceEnqTrnsAmount.value = ammount.toString()
                                    items(it.size) { item ->
                                        UnifiedAepsReportLayout(it[item])
                                    }
                                }
                            }

                        }
                    )
                    if (aepsReportViewModel.searchText.value != "" && filteredList.isEmpty() || reportList.isEmpty()) {
                        Box(modifier = Modifier.fillMaxSize(),
                            contentAlignment = Alignment.Center
                        ){
                            Text(text = "No Data Available")
                        }
                    }

                }
            }
        } else {
            Box(modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ){
                Text(text = "Something went to wrong!")
            }

        }
    }


}

@Composable
fun CashWithdrawlScreen(aepsReportViewModel: AepsReportViewModel) {
    val aepsReportState = aepsReportViewModel.aepsReportState.collectAsState()
    aepsReportState.value.unifiedAepsRespModel?.let {
        aepsReportViewModel.cashWidrawList.value = it.results?.report?.filter { item ->
            item?.operationPerformed.equals("AEPS_CASH_WITHDRAWAL")
        }
    }


    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.TopCenter
    ) {
        if (aepsReportState.value.loading == true && aepsReportState.value.unifiedAepsRespModel == null) {
            Box(modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ){
                CircularProgressIndicator(color = primaryColor)
            }
        } else if (aepsReportState.value.loading == false && aepsReportState.value.unifiedAepsRespModel != null) {
            aepsReportViewModel.cashWidrawList.value?.let { aepsReport ->
                val filteredList: ArrayList<AepsReportItem?> = arrayListOf()
                val reportList: List<AepsReportItem?> = aepsReport


                Column {
                Row(modifier = Modifier.fillMaxWidth()) {
                    androidx.compose.material3.Text(
                        text = "Entities: ${aepsReportViewModel.setcashTrnsLength.value}",
                        color = Color.Black,
                        style = MaterialTheme.typography.headlineLarge,
                        fontSize = 15.sp,
                        modifier = Modifier
                            .weight(1f)
                            .padding(
                                top = 10.dp,
                                start = 10.dp
                            )

                    )
                    androidx.compose.material3.Text(
                        text = "Amount: ${"₹"+aepsReportViewModel.setCashWidTrnsAmount.value}",
                        color = Color.Black,
                        style = MaterialTheme.typography.headlineLarge,
                        fontSize = 15.sp,
                        modifier = Modifier
                            .weight(1f)
                            .padding(
                                top = 10.dp,
                                start = 10.dp
                            )
                    )
                }

                LazyColumn(
                    contentPadding = PaddingValues(10.dp),
                    verticalArrangement = Arrangement.spacedBy(10.dp),
                    content = {
                        if (aepsReportViewModel.searchText.value.isEmpty()) {
                            aepsReportViewModel.setcashTrnsLength.value = aepsReportViewModel.cashWidrawList.value!!.size.toString()

                            var trnsAmount = 0
                            for (report in reportList.indices) {
                                if (reportList[report]?.status?.equals("SUCCESS", true) == true) {
                                    trnsAmount += reportList[report]?.amountTransacted!!
                                }
                            }
                            aepsReportViewModel.setCashWidTrnsAmount.value = trnsAmount.toString()

                            items(aepsReport.size) { item ->
                                UnifiedAepsReportLayout(aepsReport[item])
                            }
                        } else {
                            for (report in aepsReport) {
                                if (report?.operationPerformed?.lowercase(Locale.ROOT)!!
                                        .contains(
                                            aepsReportViewModel.searchText.value.lowercase(
                                                Locale.ROOT
                                            )
                                        ) ||
                                    report.status?.lowercase(Locale.ROOT)!!
                                        .contains(
                                            aepsReportViewModel.searchText.value.lowercase(
                                                Locale.ROOT
                                            )
                                        ) ||
                                    report.id?.lowercase(Locale.ROOT)!!
                                        .contains(
                                            aepsReportViewModel.searchText.value.lowercase(
                                                Locale.ROOT
                                            )
                                        )

                                ) {
                                    filteredList.add(report)
                                }
                            }
                            filteredList.let {
                                aepsReportViewModel.setcashTrnsLength.value = it.size.toString()
                                var ammount = 0
                                for (report in it.indices) {
                                    if (filteredList[report]?.status?.equals(
                                            "SUCCESS",
                                            true
                                        ) == true
                                    ) {
                                        ammount += filteredList[report]?.amountTransacted!!
                                    }
                                }
                                aepsReportViewModel.setCashWidTrnsAmount.value = ammount.toString()
                                items(it.size) { item ->
                                    UnifiedAepsReportLayout(it[item])
                                }
                            }
                        }
                    }
                )
                if (aepsReportViewModel.searchText.value != "" && filteredList.isEmpty() || reportList.isEmpty()) {
                    Box(modifier = Modifier.fillMaxSize(),
                        contentAlignment = Alignment.Center
                    ){
                        Text(text = "No Data Available")
                    }
                }
            }
            }
        } else {
            Box(modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ){
                Text(text = "Something went to wrong")
            }
        }
    }
}


@Composable
fun MiniStatementScreen(aepsReportViewModel: AepsReportViewModel) {
    val aepsReportState = aepsReportViewModel.aepsReportState.collectAsState()
    aepsReportState.value.unifiedAepsRespModel?.let {
        aepsReportViewModel.miniStementList.value = it.results?.report?.filter { item ->
            item?.operationPerformed.equals("AEPS_MINI_STATEMENT")
        }
    }


    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.TopCenter
    ) {
        if (aepsReportState.value.loading == true && aepsReportState.value.unifiedAepsRespModel == null) {
            Box(modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ){
                CircularProgressIndicator(color = primaryColor)
            }
        } else if (aepsReportState.value.loading == false && aepsReportState.value.unifiedAepsRespModel != null) {
            aepsReportViewModel.miniStementList.value?.let { aepsReport ->
                val filteredList: ArrayList<AepsReportItem?> = arrayListOf()
                val reportList: List<AepsReportItem?> = aepsReport


                Column {
                    Row(modifier = Modifier.fillMaxWidth()) {
                        androidx.compose.material3.Text(
                            text = "Entities: ${aepsReportViewModel.setMinistatementTrnsLength.value}",
                            color = Color.Black,
                            style = MaterialTheme.typography.headlineLarge,
                            fontSize = 15.sp,
                            modifier = Modifier
                                .weight(1f)
                                .padding(
                                    top = 10.dp,
                                    start = 10.dp
                                )

                        )
                        androidx.compose.material3.Text(
                            text = "Amount: ${"₹"+aepsReportViewModel.setMiniStatemntTrnsAmount.value}",
                            color = Color.Black,
                            style = MaterialTheme.typography.headlineLarge,
                            fontSize = 15.sp,
                            modifier = Modifier
                                .weight(1f)
                                .padding(
                                    top = 10.dp,
                                    start = 10.dp
                                )
                        )
                    }

                    LazyColumn(
                        contentPadding = PaddingValues(10.dp),
                        verticalArrangement = Arrangement.spacedBy(10.dp),
                        content = {
                            if (aepsReportViewModel.searchText.value.isEmpty()) {
                                aepsReportViewModel.setMinistatementTrnsLength.value = aepsReportViewModel.miniStementList.value!!.size.toString()

                                var trnsAmount = 0
                                for (report in reportList.indices) {
                                    if (reportList[report]?.status?.equals("SUCCESS", true) == true) {
                                        trnsAmount += reportList[report]?.amountTransacted!!
                                    }
                                }
                                aepsReportViewModel.setMiniStatemntTrnsAmount.value = trnsAmount.toString()

                                items(aepsReport.size) { item ->
                                    UnifiedAepsReportLayout(aepsReport[item])
                                }
                            } else {
                                for (report in aepsReport) {
                                    if (report?.operationPerformed?.lowercase(Locale.ROOT)!!
                                            .contains(
                                                aepsReportViewModel.searchText.value.lowercase(
                                                    Locale.ROOT
                                                )
                                            ) ||
                                        report.status?.lowercase(Locale.ROOT)!!
                                            .contains(
                                                aepsReportViewModel.searchText.value.lowercase(
                                                    Locale.ROOT
                                                )
                                            ) ||
                                        report.id?.lowercase(Locale.ROOT)!!
                                            .contains(
                                                aepsReportViewModel.searchText.value.lowercase(
                                                    Locale.ROOT
                                                )
                                            )

                                    ) {
                                        filteredList.add(report)
                                    }
                                }
                                filteredList.let {
                                    aepsReportViewModel.setMinistatementTrnsLength.value = it.size.toString()
                                    var ammount = 0
                                    for (report in it.indices) {
                                        if (filteredList[report]?.status?.equals(
                                                "SUCCESS",
                                                true
                                            ) == true
                                        ) {
                                            ammount += filteredList[report]?.amountTransacted!!
                                        }
                                    }
                                    aepsReportViewModel.setMiniStatemntTrnsAmount.value = ammount.toString()
                                    items(it.size) { item ->
                                        UnifiedAepsReportLayout(it[item])
                                    }
                                }
                            }
                        }
                    )
                    if (aepsReportViewModel.searchText.value != "" && filteredList.isEmpty() || reportList.isEmpty()) {
                        Box(modifier = Modifier.fillMaxSize(),
                            contentAlignment = Alignment.Center
                        ){
                            Text(text = "No Data Available")
                        }
                    }
                }
            }
        } else {
            Box(modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ){
                Text(text = "Something went to wrong")
            }
        }
    }
}
@Composable
fun CashDepositScreen(aepsReportViewModel: AepsReportViewModel) {
    val aepsReportState = aepsReportViewModel.aepsReportState.collectAsState()
    aepsReportState.value.unifiedAepsRespModel?.let {
        aepsReportViewModel.depositCashList.value = it.results?.report?.filter { item ->
            item.operationPerformed.equals("AEPS_CASH_DEPOSIT")
        }
    }


    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.TopCenter
    ) {
        if (aepsReportState.value.loading == true && aepsReportState.value.unifiedAepsRespModel == null) {
            Box(modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ){
                CircularProgressIndicator(color = primaryColor)
            }
        } else if (aepsReportState.value.loading == false && aepsReportState.value.unifiedAepsRespModel != null) {
            aepsReportViewModel.depositCashList.value?.let { aepsReport ->
                val filteredList: ArrayList<AepsReportItem?> = arrayListOf()
                val reportList: List<AepsReportItem?> = aepsReport


                Column {
                    Row(modifier = Modifier.fillMaxWidth()) {
                        androidx.compose.material3.Text(
                            text = "Entities: ${aepsReportViewModel.setCashDepositTrnsLength.value}",
                            color = Color.Black,
                            style = MaterialTheme.typography.headlineLarge,
                            fontSize = 15.sp,
                            modifier = Modifier
                                .weight(1f)
                                .padding(
                                    top = 10.dp,
                                    start = 10.dp
                                )

                        )
                        androidx.compose.material3.Text(
                            text = "Amount: ${"₹"+aepsReportViewModel.setCashDepositTrnsAmount.value}",
                            color = Color.Black,
                            style = MaterialTheme.typography.headlineLarge,
                            fontSize = 15.sp,
                            modifier = Modifier
                                .weight(1f)
                                .padding(
                                    top = 10.dp,
                                    start = 10.dp
                                )
                        )
                    }

                    LazyColumn(
                        contentPadding = PaddingValues(10.dp),
                        verticalArrangement = Arrangement.spacedBy(10.dp),
                        content = {
                            if (aepsReportViewModel.searchText.value.isEmpty()) {
                                aepsReportViewModel.setCashDepositTrnsLength.value = aepsReportViewModel.depositCashList.value!!.size.toString()

                                var trnsAmount = 0
                                for (report in reportList.indices) {
                                    if (reportList[report]?.status?.equals("SUCCESS", true) == true) {
                                        trnsAmount += reportList[report]?.amountTransacted!!
                                    }
                                }
                                aepsReportViewModel.setCashDepositTrnsAmount.value = trnsAmount.toString()

                                items(aepsReport.size) { item ->
                                    UnifiedAepsReportLayout(aepsReport[item])
                                }
                            } else {
                                for (report in aepsReport) {
                                    if (report?.operationPerformed?.lowercase(Locale.ROOT)!!
                                            .contains(
                                                aepsReportViewModel.searchText.value.lowercase(
                                                    Locale.ROOT
                                                )
                                            ) ||
                                        report.status?.lowercase(Locale.ROOT)!!
                                            .contains(
                                                aepsReportViewModel.searchText.value.lowercase(
                                                    Locale.ROOT
                                                )
                                            ) ||
                                        report.id?.lowercase(Locale.ROOT)!!
                                            .contains(
                                                aepsReportViewModel.searchText.value.lowercase(
                                                    Locale.ROOT
                                                )
                                            )

                                    ) {
                                        filteredList.add(report)
                                    }
                                }
                                filteredList.let {
                                    aepsReportViewModel.setCashDepositTrnsLength.value = it.size.toString()
                                    var ammount = 0
                                    for (report in it.indices) {
                                        if (filteredList[report]?.status?.equals(
                                                "SUCCESS",
                                                true
                                            ) == true
                                        ) {
                                            ammount += filteredList[report]?.amountTransacted!!
                                        }
                                    }
                                    aepsReportViewModel.setCashDepositTrnsLength.value = ammount.toString()
                                    items(it.size) { item ->
                                        UnifiedAepsReportLayout(it[item])
                                    }
                                }
                            }
                        }
                    )
                    if (aepsReportViewModel.searchText.value != "" && filteredList.isEmpty() || reportList.isEmpty()) {
                        Box(modifier = Modifier.fillMaxSize(),
                            contentAlignment = Alignment.Center
                        ){
                            Text(text = "No Data Available")
                        }
                    }
                }
            }
        } else {
            Box(modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ){
                Text(text = "Something went to wrong")
            }
        }
    }
}




