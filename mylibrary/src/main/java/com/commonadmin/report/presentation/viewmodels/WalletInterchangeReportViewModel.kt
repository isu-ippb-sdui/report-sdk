package com.commonadmin.report.presentation.viewmodels

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.commonadmin.report.data.remote.models.WalletInterchangeReqModel
import com.commonadmin.report.data.remote.models.WalletInterchangeRespModel
import com.commonadmin.report.domain.usercases.WalletInterchangeReportUseCase
import com.commonadmin.report.utils.ApiResult
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class WalletInterchangeReportViewModel @Inject constructor(private val userCases: WalletInterchangeReportUseCase) :
    ViewModel() {
    var walletInterchangeReportState: MutableStateFlow<WalletInterchangeReportState> =
        MutableStateFlow(WalletInterchangeReportState())
        private set

    var isVisible: MutableState<Boolean> = mutableStateOf(false)
        private set
    var searchText: MutableState<String> = mutableStateOf("")
        private set
    var totalLength: MutableState<String> = mutableStateOf("")
        private set
    var amountTransacted: MutableLiveData<String> = MutableLiveData("")
        private set


    fun getWalletInterchangeReportData(
        url: String,
        walletInterchangeReqModel: WalletInterchangeReqModel,
        token: String
    ) {
        viewModelScope.launch {
            userCases.invoke(url = url, walletInterchangeReqModel = walletInterchangeReqModel,token=token)
                .collect { networkResult ->
                    when (networkResult) {
                        is ApiResult.Success -> {
                            walletInterchangeReportState.emit(
                                walletInterchangeReportState.value.copy(
                                    walletInterchangeRespModel = networkResult.data,
                                    loading = false,
                                    error = null
                                )
                            )
                        }

                        is ApiResult.Loading -> {
                            walletInterchangeReportState.emit(
                                walletInterchangeReportState.value.copy(
                                    walletInterchangeRespModel = null,
                                    loading = true,
                                    error = null
                                )
                            )
                        }

                        is ApiResult.Error -> {
                            walletInterchangeReportState.emit(
                                walletInterchangeReportState.value.copy(
                                    walletInterchangeRespModel = null,
                                    loading = false,
                                    error = networkResult.message
                                )
                            )
                        }
                    }

                }

        }
    }


}

data class WalletInterchangeReportState(
    var walletInterchangeRespModel: WalletInterchangeRespModel? = null,
    val loading: Boolean? = null,
    val error: String? = null
)
