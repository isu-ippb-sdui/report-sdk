package com.commonadmin.report.presentation.viewmodels

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.commonadmin.report.data.remote.models.Commission2ReportReqModel
import com.commonadmin.report.data.remote.models.Commission2ReportRespModel
import com.commonadmin.report.domain.usercases.Commission2ReportUseCase
import com.commonadmin.report.utils.ApiResult
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class Commission2ReportViewModel @Inject constructor(private val userCases: Commission2ReportUseCase) :
    ViewModel() {
    var commission2ReportState: MutableStateFlow<Commission2ReportState> =
        MutableStateFlow(Commission2ReportState())
        private set

    var isVisible: MutableState<Boolean> = mutableStateOf(false)
        private set
    var searchText: MutableState<String> = mutableStateOf("")
        private set
    var totalLength: MutableState<String> = mutableStateOf("")
        private set
    var amountTransacted: MutableLiveData<String> = MutableLiveData("")
        private set


    fun getCommission2ReportData(url:String, commission2ReportReqModel: Commission2ReportReqModel, token:String) {
        viewModelScope.launch {
            userCases.invoke(url=url,commission2ReportReqModel = commission2ReportReqModel, token = token)
                .collect { networkResult ->
                    when (networkResult) {
                        is ApiResult.Success -> {
                            commission2ReportState.emit(
                                commission2ReportState.value.copy(
                                    commission2ReportRespModel = networkResult.data,
                                    loading = false,
                                    error = null
                                )
                            )
                        }

                        is ApiResult.Loading -> {
                            commission2ReportState.emit(
                                commission2ReportState.value.copy(
                                    commission2ReportRespModel = null,
                                    loading = true,
                                    error = null
                                )
                            )
                        }

                        is ApiResult.Error -> {
                            commission2ReportState.emit(
                                commission2ReportState.value.copy(
                                    commission2ReportRespModel = null,
                                    loading = false,
                                    error = networkResult.message
                                )
                            )
                        }
                    }

                }

        }
    }


}

data class Commission2ReportState(
    var commission2ReportRespModel: Commission2ReportRespModel?=null,
    val loading: Boolean? = null,
    val error: String? = null
)
