package com.commonadmin.report.presentation.reportscreens.wallet2

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Text
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import com.commonadmin.report.data.remote.models.Wallet2ReportItem
import com.commonadmin.report.data.remote.models.Wallet2ReqModel
import com.commonadmin.report.presentation.viewmodels.Wallet2ReportViewModel
import com.commonadmin.report.ui.theme.primaryColor
import com.commonadmin.report.utils.DataConstants
import com.commonadmin.report.utils.SharedPrefClass
import com.commonadmin.report.utils.reportcalender.ReportCalenderConstant
import java.util.Locale

@Composable
fun Wallet2ReportScreen(wallet2ReportViewModel: Wallet2ReportViewModel) {
    val wallet2ReportState = wallet2ReportViewModel.wallet2ReportState.collectAsState()
    val context = LocalContext.current
    LaunchedEffect(key1 = true) {

        val trnsList:ArrayList<String> = ArrayList()

        trnsList.add("COMMISION")
        trnsList.add("COMMISSION")
        trnsList.add("MATM1_CASH_WITHDRAWAL")
        trnsList.add("MATM2_CASH_WITHDRAWAL")
        trnsList.add("mATM_CASH_WITHDRAWAL")
        trnsList.add("mATM_BALANCE_ENQUIRY")
        trnsList.add("Prepaid Recharge")
        trnsList.add("SMS")
        trnsList.add("WALLET2CASHOUT")
        trnsList.add("virtual_balance")
        trnsList.add("Inter_Wallet")
        trnsList.add("WALLET2CASHOUT_IMPS")
        trnsList.add("WALLET2CASHOUT_NEFT")
        trnsList.add("Virtual_Balance_Transfer")
        trnsList.add("IMPS_FUND_TRANSFER")
        trnsList.add("BENE_VERIFICATION")
        trnsList.add("COMMISSION")
        trnsList.add("AEPS_CASH_WITHDRAWAL")
        trnsList.add("AADHAAR_PAY")
        trnsList.add("NEFT_FUND_TRANSFER")
        trnsList.add("WALLET_INTERCHANGE")
        trnsList.add("QR_COLLECT")
        trnsList.add("UPI_COLLECT")
        trnsList.add("PG_Internet Banking")
        trnsList.add("WALLET2CASHOUT_ADDBANK")
        trnsList.add("AEPS_CASH_DEPOSIT")
        trnsList.add("BBPS_BROADBAND POSTPAID")
        trnsList.add("LOAN")
        trnsList.add("QR_STATIC")

        trnsList.add("BBPS_ELECTRICITY")
        trnsList.add("BBPS_BROADBAND POSTPAID")
        trnsList.add("BBPS_GAS")
        trnsList.add("BBPS_LANDLINE POSTPAID")
        trnsList.add("BBPS_MOBILE POSTPAID")
        trnsList.add("BBPS_WATER")
        trnsList.add("BBPS_LOAN REPAYMENT")
        trnsList.add("BBPS_LIFE INSURANCE")
        trnsList.add("BBPS_LPG GAS")
        trnsList.add("BBPS_CABLE TV")
        trnsList.add("BBPS_HEALTH INSURANCE")
        trnsList.add("BBPS_EDUCATION FEES")
        trnsList.add("BBPS_INSURANCE")
        trnsList.add("BBPS_MUNICIPAL TAXES")
        trnsList.add("BBPS_HOSPITAL")
        trnsList.add("BBPS_MUNICIPAL SERVICES")
       trnsList.add( "BBPS_HOUSING SOCIETY")
        trnsList.add("BBPS_SUBSCRIPTION")
        trnsList.add("BBPS_FASTAG")
        trnsList.add("BBPS_DTH")


        val wallet2ReqModel = Wallet2ReqModel(
            "wallet2_new_web_common", SharedPrefClass.getStringValue(context, DataConstants.USERNAME),
            ReportCalenderConstant.selectFromDate, ReportCalenderConstant.selectToDate,trnsList
        )

        if (wallet2ReportState.value.wallet2RespModel == null) {
            wallet2ReportViewModel.getWallet2ReportData(
                SharedPrefClass.getStringValue(context, "Wallet2Report").toString(), wallet2ReqModel,SharedPrefClass.getStringValue(context,DataConstants.TOKEN).toString()
            )
        }
    }

    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.TopCenter
    ) {
        if (wallet2ReportState.value.loading == true && wallet2ReportState.value.wallet2RespModel == null) {
            Box(modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ){
                CircularProgressIndicator(color = primaryColor)
            }
        } else if (wallet2ReportState.value.loading == false && wallet2ReportState.value.wallet2RespModel != null) {
            wallet2ReportState.value.wallet2RespModel?.let { walletReport ->

                val filteredList: ArrayList<Wallet2ReportItem?> = arrayListOf()
                val reportList: List<Wallet2ReportItem?>? = walletReport.report

                var crAmount = 0.0
                for (report in reportList?.indices!!) {
                    if(reportList[report]?.status?.equals("SUCCESS",true) == true) {
                        crAmount += reportList[report]?.creditAmount!!
                    }
                }
                val formatted = String.format("%.2f", crAmount)
                wallet2ReportViewModel.crAmount.value = formatted

                var drAmount = 0.0
                for (report in reportList.indices) {
                    if(reportList[report]?.status?.equals("SUCCESS",true) == true) {
                        drAmount += reportList[report]?.debitAmount!!
                    }
                }
                val crFormatted = String.format("%.2f", drAmount)
                wallet2ReportViewModel.drAmount.value = crFormatted

                Column {
                    LazyColumn(
                        contentPadding = PaddingValues(0.dp),
                        verticalArrangement = Arrangement.spacedBy(10.dp),
                        content = {
                            if (wallet2ReportViewModel.searchText.value.isEmpty()) {
                                wallet2ReportViewModel.totalLength.value =
                                    reportList.size.toString()
                                items(reportList.size) { item ->
                                    Wallet2RecycleScreen(walletReport.report[item])
                                }
                            } else {
                                for (report in walletReport.report) {
                                    if (report.relatioanlId?.lowercase(Locale.ROOT)!!
                                            .contains(
                                                wallet2ReportViewModel.searchText.value.lowercase(
                                                    Locale.ROOT
                                                )
                                            ) ||
                                        report.status?.lowercase(Locale.ROOT)!!.contains(
                                            wallet2ReportViewModel.searchText.value.lowercase(
                                                Locale.ROOT
                                            )
                                        ) ||
                                        report.id?.lowercase(Locale.ROOT)!!.contains(
                                            wallet2ReportViewModel.searchText.value.lowercase(
                                                Locale.ROOT
                                            )
                                        ) ||
                                        report.operationPerformed?.lowercase(Locale.ROOT)!!.contains(
                                            wallet2ReportViewModel.searchText.value.lowercase(
                                                Locale.ROOT
                                            )
                                        )

                                    // report.apiTid?.lowercase(Locale.ROOT)!!.contains(reportViewModel.searchText.value.lowercase(Locale.ROOT))

                                    ) {
                                        filteredList.add(report)
                                    }
                                }
                                filteredList.let {
                                    wallet2ReportViewModel.totalLength.value = it.size.toString()

                                    var fCrAmount = 0.0
                                    for (report in filteredList.indices) {
                                        if(filteredList[report]?.status?.equals("SUCCESS",true) == true) {
                                            fCrAmount += filteredList[report]?.creditAmount!!
                                        }
                                    }
                                    val fltFormatted = String.format("%.2f", fCrAmount)
                                    wallet2ReportViewModel.crAmount.value = fltFormatted

                                    var fDrAmount = 0.0
                                    for (report in filteredList.indices) {
                                        if(filteredList[report]?.status?.equals("SUCCESS",true) == true) {
                                            fDrAmount += filteredList[report]?.debitAmount!!
                                        }
                                    }
                                    val fltDrAmount = String.format("%.2f", fDrAmount)
                                    wallet2ReportViewModel.drAmount.value = fltDrAmount

                                    items(it.size) { item ->
                                        Wallet2RecycleScreen(it[item]!!)
                                    }
                                }
                            }
                        }
                    )
                    if (wallet2ReportViewModel.searchText.value != "" && filteredList.isEmpty() || reportList.isEmpty()) {
                        Box(modifier = Modifier.fillMaxSize(),
                            contentAlignment = Alignment.Center
                        ){
                            Text(text = "No Data Available")
                        }
                    }
                }
            }
        } else {
            Box(modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ){
                Text(text = "Something went to wrong")
            }
        }
    }

}