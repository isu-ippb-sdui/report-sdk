package com.commonadmin.report.presentation.reportscreens.pos

import android.annotation.SuppressLint
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Divider
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.commonadmin.report.R
import com.commonadmin.report.ui.theme.primaryColor

@SuppressLint("SimpleDateFormat")
@Composable
fun PosReportRecycleScreen() {
    /*val timeT: Long = upiReportItem?.createdDate!!
    val formatter = SimpleDateFormat("dd-MM-yyyy  hh:mm a")
    val timeDate = formatter.format(timeT)*/

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(10.dp)


    ) {

        Text(
            text = "Date/Time:",
            color = Color.Black,
            style = MaterialTheme.typography.headlineLarge,
            fontSize = 15.sp,
            textAlign = TextAlign.Center
        )
        Spacer(modifier = Modifier.height(5.dp))
        Row {
            Text(
                text = "Txn ID:",
                color = primaryColor,
                style = MaterialTheme.typography.headlineLarge,
                fontSize = 14.sp
            )
            Spacer(modifier = Modifier.width(10.dp))
            Text(
                text = "1234567890",
                color = Color.Black,
                style = MaterialTheme.typography.headlineLarge,
                fontSize = 14.sp
            )
        }
        Spacer(modifier = Modifier.height(10.dp))

        Row(verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center) {
            Icon(
                painter = painterResource(id = R.drawable.ic_pos_image),
                contentDescription = "pos",
                modifier = Modifier.align(alignment= Alignment.CenterVertically)
            )
            Spacer(modifier = Modifier.width(10.dp))
            Column() {
                Text(
                    text = "Previous Balance: ",
                    color = Color.Black,
                    style = MaterialTheme.typography.headlineLarge,
                    fontSize = 15.sp
                )
                Spacer(modifier = Modifier.height(7.dp) )
                Row(modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.SpaceBetween) {
                    Text(
                        text = "Transected Amount: ",
                        color = Color.Black,
                        style = MaterialTheme.typography.headlineLarge,
                        fontSize = 15.sp
                    )
                    Spacer(modifier = Modifier.height(7.dp) )
                    Text(
                        text = "Txn Type",
                        color = Color.Black,
                        style = MaterialTheme.typography.headlineLarge,
                        fontSize = 15.sp
                    )
                }
                Spacer(modifier = Modifier.height(7.dp) )
                Text(
                    text = "Current Balance: ",
                    color = Color.Black,
                    style = MaterialTheme.typography.headlineLarge,
                    fontSize = 15.sp
                )
            }

        }

        Spacer(modifier = Modifier.height(10.dp))
        Divider(
            thickness = 2.dp,
            color = primaryColor
        )
    }
}

@Preview(showSystemUi = true, showBackground = true)
@Composable
fun PreviewPosScreen() {
    PosReportRecycleScreen()
}