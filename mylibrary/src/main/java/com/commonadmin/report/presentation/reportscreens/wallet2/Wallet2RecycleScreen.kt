package com.commonadmin.report.presentation.reportscreens.wallet2

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Divider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.commonadmin.report.data.remote.models.Wallet2ReportItem
import com.commonadmin.report.ui.theme.Green
import com.commonadmin.report.ui.theme.Yellow
import java.text.SimpleDateFormat
import java.util.Locale

@Composable
fun Wallet2RecycleScreen(walle2ReportItem: Wallet2ReportItem) {


    val timeT: Long = walle2ReportItem.createdDate!!.toLong()
    val formatter = SimpleDateFormat("dd-MM-yyyy  hh:mm a")
    val timeDate = formatter.format(timeT)

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(10.dp)

    ) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Text(
                text = "Date/Time: $timeDate",
                color = Color.Black,
                style = MaterialTheme.typography.headlineLarge,
                fontSize = 15.sp
            )
            Text(
                text = walle2ReportItem.status.toString(),
                color = when (walle2ReportItem.status?.uppercase(Locale.ROOT)) {
                    "REFUNDED" -> {
                        Yellow
                    }

                    "SUCCESS" -> {
                       Green
                    }

                    "FAILED" -> {
                        Color.Red
                    }

                    else -> {
                        Color.Black
                    }
                },
                style = MaterialTheme.typography.headlineLarge,
                fontSize = 15.sp
            )
        }
        Spacer(modifier = Modifier.height(5.dp))
        Text(
            text = "Txn ID: ${walle2ReportItem.id}",
            color = Color.Black,
            style = MaterialTheme.typography.headlineLarge,
            fontSize = 15.sp
        )
        Spacer(modifier = Modifier.height(3.dp))
        Text(
            text = "Relational ID: ${walle2ReportItem.relatioanlId}",
            color = Color.Black,
            style = MaterialTheme.typography.headlineLarge,
            fontSize = 15.sp
        )
        Spacer(modifier = Modifier.height(15.dp))
        Text(
            text = "Cr Amount: ${"₹"+walle2ReportItem.creditAmount}",
            color = Color.Black,
            style = MaterialTheme.typography.headlineLarge,
            fontSize = 15.sp
        )
        Spacer(modifier = Modifier.height(3.dp))
        Text(
            text = "Dr Amount: ${"₹"+walle2ReportItem.debitAmount}",
            color = Color.Black,
            style = MaterialTheme.typography.headlineLarge,
            fontSize = 15.sp
        )
        Spacer(modifier = Modifier.height(3.dp))
        Text(
            text = "Previous Balance: ${"₹"+walle2ReportItem.previousAmount}",
            color = Color.Black,
            style = MaterialTheme.typography.headlineLarge,
            fontSize = 15.sp
        )
        Spacer(modifier = Modifier.height(3.dp))
        Text(
            text = "Current Balance: ${"₹"+walle2ReportItem.currentAmount}",
            color = Color.Black,
            style = MaterialTheme.typography.headlineLarge,
            fontSize = 15.sp
        )
        Spacer(modifier = Modifier.height(3.dp))
        Text(
            text = "Relational Operation: ${walle2ReportItem.operationPerformed}",
            color = Color.Black,
            style = MaterialTheme.typography.headlineLarge,
            fontSize = 13.sp
        )
        Spacer(modifier = Modifier.height(7.dp))
        Divider(
            thickness = 1.dp,
            color = Color.Gray
        )
    }
}