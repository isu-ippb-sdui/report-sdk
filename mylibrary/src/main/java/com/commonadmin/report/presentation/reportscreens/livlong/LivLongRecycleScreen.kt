package com.commonadmin.report.presentation.reportscreens.livlong

import android.annotation.SuppressLint
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Divider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.commonadmin.report.data.remote.models.LivLongReportItem
import com.commonadmin.report.ui.theme.Green
import com.commonadmin.report.ui.theme.Yellow
import com.commonadmin.report.ui.theme.primaryColor
import java.text.SimpleDateFormat
import java.util.Locale

@SuppressLint("SimpleDateFormat")
@Composable
fun LivLongReportRecycleScreen(livLongReportItem: LivLongReportItem) {
    val timeT: Long = livLongReportItem.createdDate!!.toLong()
    val formatter = SimpleDateFormat("dd-MM-yyyy  hh:mm a")
    val timeDate = formatter.format(timeT)

     val timeT2: Long = livLongReportItem.updatedDate!!.toLong()
    val formatter2 = SimpleDateFormat("dd-MM-yyyy  hh:mm a")
    val timeDate2 = formatter2.format(timeT2)



    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(10.dp)

    ) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Text(
                text = "Created Date: $timeDate",
                color = Color.Black,
                style = MaterialTheme.typography.headlineLarge,
                fontSize = 15.sp,
                textAlign = TextAlign.Center
            )
            Text(
                text = livLongReportItem.status.toString(),
                color = when (livLongReportItem.status?.uppercase(Locale.ROOT)) {
                    "REFUNDED" -> {
                        Yellow
                    }

                    "SUCCESS" -> {
                        Green
                    }

                    "FAILED" -> {
                        Color.Red
                    }

                    else -> {
                        Color.Black
                    }
                },
                style = MaterialTheme.typography.headlineLarge,
                fontSize = 15.sp
            )
        }
        Spacer(modifier = Modifier.height(5.dp))
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Text(
                text = "Updated Date: $timeDate2",
                color = Color.Black,
                style = MaterialTheme.typography.headlineLarge,
                fontSize = 14.sp
            )
            Text(
                text = "₹"+livLongReportItem.amountTransacted.toString(),
                color = Color.Black,
                style = MaterialTheme.typography.headlineLarge,
                fontSize = 15.sp
            )
        }
        Spacer(modifier = Modifier.height(3.dp))
        Text(
            text = "ID: ${livLongReportItem.id?.let { "$it" } ?: "N/A"}",
            color = Color.Black,
            style = MaterialTheme.typography.headlineLarge,
            fontSize = 15.sp
        )
        Spacer(modifier = Modifier.height(3.dp))

        Text(
            text = "Previous Amount: ${livLongReportItem.previousAmount?.let { "₹$it" } ?: "N/A"}",
            color = Color.Black,
            style = MaterialTheme.typography.headlineLarge,
            fontSize = 15.sp
        )
        Spacer(modifier = Modifier.height(3.dp))

        Text(
            text = "Balance Amount: ${livLongReportItem.balanceAmount?.let { "₹$it" } ?: "N/A"}",
            color = Color.Black,
            style = MaterialTheme.typography.headlineLarge,
            fontSize = 15.sp
        )
        Spacer(modifier = Modifier.height(3.dp))

        Text(
            text = "Api Comment: ${livLongReportItem.apiComment ?: "N/A"}",
            color = Color.Black,
            style = MaterialTheme.typography.headlineLarge,
            fontSize = 15.sp
        )
        Spacer(modifier = Modifier.height(3.dp))
        Text(
            text = "Operation Performed: ${livLongReportItem.operationPerformed ?: "N/A"}",
            color = Color.Black,
            style = MaterialTheme.typography.headlineLarge,
            fontSize = 15.sp
        )
        Spacer(modifier = Modifier.height(3.dp))
        Text(
            text = "Txn Type: ${livLongReportItem.transactionMode ?: "N/A"}",
            color = Color.Black,
            style = MaterialTheme.typography.headlineLarge,
            fontSize = 15.sp
        )
        Spacer(modifier = Modifier.height(3.dp))
        Text(
            text = "User Name: ${livLongReportItem.userName ?: "N/A"}",
            color = Color.Black,
            style = MaterialTheme.typography.headlineLarge,
            fontSize = 15.sp
        )
        Spacer(modifier = Modifier.height(5.dp))
        Divider(
            thickness = 2.dp,
            color = primaryColor
        )
    }
}