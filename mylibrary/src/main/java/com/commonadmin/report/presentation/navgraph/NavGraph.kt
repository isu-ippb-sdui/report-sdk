package com.commonadmin.report.presentation.navgraph

import androidx.compose.animation.*
import androidx.compose.animation.core.tween
import androidx.compose.runtime.Composable
import androidx.navigation.NavBackStackEntry
import androidx.navigation.NavType
import androidx.navigation.navArgument
import com.commonadmin.report.presentation.reportscreens.aadharpay.AadhaarToolBarScreen
import com.commonadmin.report.presentation.reportscreens.aeps.AepsToolBarAppBar
import com.commonadmin.report.presentation.reportscreens.bbps.BBPSToolBarScreen
import com.commonadmin.report.presentation.reportscreens.commission1.Commission1ToolBarScreen
import com.commonadmin.report.presentation.reportscreens.commission2.Commission2ToolBarScreen
import com.commonadmin.report.presentation.reportscreens.dmtgateway.DmtGateWayToolBarScreen
import com.commonadmin.report.presentation.reportscreens.dmtgateway.dmtReceiptReport.ReceiptToolBar
import com.commonadmin.report.presentation.reportscreens.livlong.LivLongToolBarScreen
import com.commonadmin.report.presentation.reportscreens.matm2.Matm2ToolBarAppBar
import com.commonadmin.report.presentation.reportscreens.recharge.RechargeToolBarScreen
import com.commonadmin.report.presentation.reportscreens.upi.UpiToolBarScreen
import com.commonadmin.report.presentation.reportscreens.wallet1.Wallet1ToolBarScreen
import com.commonadmin.report.presentation.reportscreens.wallet2.Wallet2ToolBarScreen
import com.commonadmin.report.presentation.reportscreens.walletTopup.WalletTopUpToolBarScreen
import com.commonadmin.report.presentation.reportscreens.walletcashout.WalletCashoutToolBarScreen
import com.commonadmin.report.presentation.reportscreens.walletinterchange.WalletInterchangeToolBarScreen
import com.commonadmin.report.presentation.viewmodels.DmtReceiptReportViewModel
import com.google.accompanist.navigation.animation.AnimatedNavHost
import com.google.accompanist.navigation.animation.composable
import com.google.accompanist.navigation.animation.rememberAnimatedNavController


@OptIn(ExperimentalAnimationApi::class)
@Composable
fun SetUpNavGraph(typeName: String, viewModel: DmtReceiptReportViewModel) {
    val navController = rememberAnimatedNavController()
    AnimatedNavHost(
        navController = navController,
        startDestination = typeName
    )
    /**
     * Start Destination Screen
     */
    {
        composable(route = typeName,
            enterTransition = {
                enterTransition()
            },
            exitTransition = {
                exitTransition()
            },
            popEnterTransition = {
                popEnterTransition()
            }) {
            when (typeName) {
                "UnifiedAePSReport" -> {
                    AepsToolBarAppBar()
                }
                "Commission1Report" -> {
                    Commission1ToolBarScreen()
                }
                "Commission2Report" -> {
                    Commission2ToolBarScreen()
                }
                "DmtGatewayReport" -> {
                    DmtGateWayToolBarScreen(navController = navController)
                }
                "WalletInterchangeReport" -> {
                    WalletInterchangeToolBarScreen()
                }
                "WalletCashoutReport" -> {
                    WalletCashoutToolBarScreen()
                }
                "WalletTopupReport" -> {
                    WalletTopUpToolBarScreen()
                }
                "Wallet2Report" -> {
                    Wallet2ToolBarScreen()
                }
                "Wallet1Report" -> {
                    Wallet1ToolBarScreen()
                }
                "mATMReport" -> {
                    Matm2ToolBarAppBar()
                }
                "BBPSReport" -> {
                    BBPSToolBarScreen()
                }
                "RechargeReport" -> {
                    RechargeToolBarScreen()
                }
                "UpiReport" -> {
                    UpiToolBarScreen()
                }
                "LivLongReport" -> {
                    LivLongToolBarScreen()
                }
                "AadhaarPayReport" -> {
                    AadhaarToolBarScreen()
                }
            }
        }
        composable(route = "${Destinations.DMT_RECEIPT_SCREEN_ROUTE}/{trnsId}",
            arguments = listOf(navArgument("trnsId") {
                type = NavType.LongType

            }),
            enterTransition = {
                enterTransition()
            },
            exitTransition = {
                exitTransition()
            },
            popEnterTransition = {
                popEnterTransition()
            }) { backStackEntry ->
            ReceiptToolBar(
                trnsID = backStackEntry.arguments?.getLong("trnsId")!!,
                viewModel,
                navController
            )
        }


    }
}

@OptIn(ExperimentalAnimationApi::class)
private fun AnimatedContentScope<NavBackStackEntry>.popEnterTransition() =
    slideInHorizontally(
        initialOffsetX = { -300 },
        animationSpec = tween(300)
    ) + fadeIn(animationSpec = tween(300))


@OptIn(ExperimentalAnimationApi::class)
private fun AnimatedContentScope<NavBackStackEntry>.exitTransition() =
    slideOutHorizontally(
        targetOffsetX = { -300 },
        animationSpec = tween(300)
    ) + fadeOut(animationSpec = tween(300))


@OptIn(ExperimentalAnimationApi::class)
private fun AnimatedContentScope<NavBackStackEntry>.enterTransition() =
    slideInHorizontally(
        initialOffsetX = { 300 },
        animationSpec = tween(300)
    ) + fadeIn(animationSpec = tween(300))