package com.commonadmin.report.presentation.viewmodels

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.commonadmin.report.data.remote.models.Commission1ReportReqModel
import com.commonadmin.report.data.remote.models.Commission1ReportRespModel
import com.commonadmin.report.data.remote.models.UpiReqModel
import com.commonadmin.report.data.remote.models.UpiRespModel
import com.commonadmin.report.domain.usercases.Commission1ReportUseCase
import com.commonadmin.report.domain.usercases.UpiReportUseCase
import com.commonadmin.report.utils.ApiResult
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class UpiReportViewModel @Inject constructor(private val userCases: UpiReportUseCase) :
    ViewModel() {
    var upiReportState: MutableStateFlow<UpiReportState> =
        MutableStateFlow(UpiReportState())
        private set

    var isVisible: MutableState<Boolean> = mutableStateOf(false)
        private set
    var searchText: MutableState<String> = mutableStateOf("")
        private set
    var totalLength: MutableState<String> = mutableStateOf("")
        private set
    var amountTransacted: MutableLiveData<String> = MutableLiveData("")
        private set


    fun getUpiReportData(url:String, upiReqModel: UpiReqModel, token:String) {
        viewModelScope.launch {
            userCases.invoke(url=url,upiReqModel = upiReqModel, token = token)
                .collect { networkResult ->
                    when (networkResult) {
                        is ApiResult.Success -> {
                            upiReportState.emit(
                                upiReportState.value.copy(
                                    upiReportRespModel = networkResult.data,
                                    loading = false,
                                    error = null
                                )
                            )
                        }

                        is ApiResult.Loading -> {
                            upiReportState.emit(
                                upiReportState.value.copy(
                                    upiReportRespModel = null,
                                    loading = true,
                                    error = null
                                )
                            )
                        }

                        is ApiResult.Error -> {
                            upiReportState.emit(
                                upiReportState.value.copy(
                                    upiReportRespModel = null,
                                    loading = false,
                                    error = networkResult.message
                                )
                            )
                        }
                    }

                }

        }
    }


}

data class UpiReportState(
    var upiReportRespModel: UpiRespModel?=null,
    val loading: Boolean? = null,
    val error: String? = null
)
