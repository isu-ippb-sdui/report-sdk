package com.commonadmin.report

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color.parseColor
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.compose.setContent
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.flowWithLifecycle
import androidx.lifecycle.lifecycleScope
import com.commonadmin.report.data.remote.models.SduiFetchReqModels
import com.commonadmin.report.presentation.navgraph.SetUpNavGraph
import com.commonadmin.report.presentation.reportscreens.dmtgateway.dmtReceiptReport.BluetoothDeviceDialog
import com.commonadmin.report.presentation.viewmodels.DmtReceiptReportViewModel
import com.commonadmin.report.presentation.viewmodels.SduiFetchState
import com.commonadmin.report.presentation.viewmodels.SduiFetchViewModel
import com.commonadmin.report.ui.theme.primaryColor
import com.commonadmin.report.utils.DataConstants
import com.commonadmin.report.utils.DataConstants.Companion.sduiFetchSaveData
import com.commonadmin.report.utils.GetPosConnectPrinter
import com.commonadmin.report.utils.SharedPrefClass
import com.commonadmin.report.utils.reportcalender.ReportCalenderActivity
import com.commonadmin.report.utils.reportcalender.ReportCalenderConstant
import com.isu.printer_library.vriddhi.AEMPrinter
import com.isu.printer_library.vriddhi.AEMScrybeDevice
import com.isu.printer_library.vriddhi.CardReader
import com.isu.printer_library.vriddhi.IAemCardScanner
import com.isu.printer_library.vriddhi.IAemScrybe
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import timber.log.Timber
import java.io.IOException
import java.net.SocketException

@AndroidEntryPoint
class ReportSDKActivity : ComponentActivity(), IAemCardScanner, IAemScrybe {

    private val sduiFetchViewModel: SduiFetchViewModel by viewModels()
    private lateinit var viewModel: DmtReceiptReportViewModel

    private var printerList: ArrayList<String>? = null
    private var mAemscrybedevice: AEMScrybeDevice? = null
    private var mAemprinter: AEMPrinter? = null
    private var mCardreader: CardReader? = null
    private var creditData: String? = null
    private var tempdata: String? = null
    private var replacedData: String? = null
    private var responseString: String? = null
    private var response: String? = null
    private var cardTrackType: CardReader.CARD_TRACK? = null
    private var responseArray = arrayOfNulls<String>(1)

    private val intentSuccess = mutableStateOf(false)
    private var dateData = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            InitView()
        }

    }

    private fun uiApiCall() {

        val sduiRequestModel = SduiFetchReqModels(
            SharedPrefClass.getStringValue(this, DataConstants.ADMINNAME).toString(),
            SharedPrefClass.getStringValue(this, DataConstants.BANKCODE).toString()
        )
        sduiFetchViewModel.getSduiFetchData(sduiRequestModel)
        sduiFetchViewModel.sduiFetchState
            .flowWithLifecycle(lifecycle, Lifecycle.State.STARTED)
            .onEach { result ->
                handleLoginApiCall(result)
            }
            .launchIn(lifecycleScope)

    }

    private fun handleLoginApiCall(state: SduiFetchState) {
        if (state.loading == true) {
            Log.e("TAG", "pleasewait...!!!: ")
        } else if (state.loading == false && state.sduiFetchRespModels != null) {
            if (state.sduiFetchRespModels!!.status == 1) {
                Log.e("TAG", "SuccessFetchApi...!!!: ")
                SharedPrefClass.saveUnifiedAepsReportApi(
                    this,
                    "UnifiedAepsReport",
                    state.sduiFetchRespModels!!.data?.uiJson?.aPIs?.unifiedAeps
                )
                SharedPrefClass.saveWallet1ReportApi(
                    this,
                    "Wallet1Report",
                    state.sduiFetchRespModels!!.data?.uiJson?.aPIs?.wallet1
                )
                SharedPrefClass.saveWallet2ReportApi(
                    this,
                    "Wallet2Report",
                    state.sduiFetchRespModels!!.data?.uiJson?.aPIs?.wallet2
                )
                SharedPrefClass.saveMatm2ReportApi(
                    this,
                    "Matm2Report",
                    state.sduiFetchRespModels!!.data?.uiJson?.aPIs?.matm2
                )
                SharedPrefClass.saveCommission1ReportApi(
                    this,
                    "Commission1Report",
                    state.sduiFetchRespModels!!.data?.uiJson?.aPIs?.commission1
                )
                SharedPrefClass.saveCommission2ReportApi(
                    this,
                    "Commission2Report",
                    state.sduiFetchRespModels!!.data?.uiJson?.aPIs?.commission2
                )
                SharedPrefClass.saveWallettopupReportApi(
                    this,
                    "WalletTopupReport",
                    state.sduiFetchRespModels!!.data?.uiJson?.aPIs?.wallettopup
                )
                SharedPrefClass.saveWalletcashoutReportApi(
                    this,
                    "WalletCashoutReport",
                    state.sduiFetchRespModels!!.data?.uiJson?.aPIs?.walletcashout
                )
                SharedPrefClass.saveWalletInterchangeReportApi(
                    this,
                    "WalletInterchangeReport",
                    state.sduiFetchRespModels!!.data?.uiJson?.aPIs?.walletInterchange
                )
                SharedPrefClass.saveDmtReportApi(
                    this,
                    "DmtGateWayReport",
                    state.sduiFetchRespModels!!.data?.uiJson?.aPIs?.dmt
                )
                SharedPrefClass.saveDmtReceiptReportApi(
                    this,
                    "DmtReceiptReport",
                    state.sduiFetchRespModels!!.data?.uiJson?.aPIs?.emailReport
                )
                SharedPrefClass.saveBBPSReportApi(
                    this,
                    "BBPSReport",
                    state.sduiFetchRespModels!!.data?.uiJson?.aPIs?.bbpsreport
                )
                SharedPrefClass.saveRechargeReportApi(
                    this,
                    "RechargeReport",
                    state.sduiFetchRespModels!!.data?.uiJson?.aPIs?.recharge
                )
                SharedPrefClass.saveUPIReportApi(
                    this,
                    "UPIReport",
                    state.sduiFetchRespModels!!.data?.uiJson?.aPIs?.upi
                )
                SharedPrefClass.saveFetchWalletTopUpTxn(
                    this,
                    "updateWalletTopupTxn",
                    state.sduiFetchRespModels!!.data?.uiJson?.aPIs?.updateWalletTopupTxn
                )
                SharedPrefClass.saveWalleUpRequest(
                    this,
                    "update_wallettopup_request",
                    state.sduiFetchRespModels!!.data?.uiJson?.aPIs?.update_wallettopup_request
                )
                SharedPrefClass.saveBankFetchUser(
                    this,
                    "retailerBankFetch",
                    state.sduiFetchRespModels!!.data?.uiJson?.aPIs?.retailerBankFetch
                )
                SharedPrefClass.saveLivLongReport(
                    this,
                    "livLongReport",
                    state.sduiFetchRespModels!!.data?.uiJson?.aPIs?.liveLong
                )
                SharedPrefClass.saveAadharPayReport(
                    this,
                    "AadhaarPayReport",
                    state.sduiFetchRespModels!!.data?.uiJson?.aPIs?.aadhaarPay
                )


                primaryColor =
                    Color(parseColor(state.sduiFetchRespModels!!.data?.uiJson?.primaryColor))
            }

        } else if (state.loading == false && state.sduiFetchRespModels == null) {
            Log.e("TAG", "errorFetchApi!!!: " + state.error)
        }

    }

    @Composable
    fun InitView() {
        if (DataConstants.ActivityName == "") {
            val bundle: Bundle? = intent.getBundleExtra("sendReportData")
            if (bundle != null) {
                val token = bundle.getString("TOKEN")
                DataConstants.ActivityName = bundle.getString("ActivityName").toString()

                if (DataConstants.ActivityName != "" && token != null) {
                    val userName = bundle.getString("USERNAME")
                    val adminName = bundle.getString("ADMINNAME")

                    if (bundle.getString("ShopName") == null) {
                        SharedPrefClass.saveShopName(this, DataConstants.ShopName, "")
                    } else {
                        SharedPrefClass.saveShopName(
                            this,
                            DataConstants.ShopName,
                            bundle.getString("ShopName").toString()
                        )
                    }
                    if (bundle.getString("BrandName") == null) {
                        SharedPrefClass.saveBrandName(this, DataConstants.BrandName, "")
                    } else {
                        SharedPrefClass.saveBrandName(
                            this,
                            DataConstants.BrandName,
                            bundle.getString("BrandName").toString()
                        )
                    }

                    SharedPrefClass.saveMobile(
                        this,
                        DataConstants.MobileNumber,
                        bundle.getString("MobileNumber").toString()
                    )
                    SharedPrefClass.saveEmail(
                        this,
                        DataConstants.UserEmail,
                        bundle.getString("UserEmail").toString()
                    )
                    SharedPrefClass.saveUserName(this, DataConstants.USERNAME, userName)
                    SharedPrefClass.saveAdminName(this, DataConstants.ADMINNAME, adminName)
                    SharedPrefClass.saveToken(this, DataConstants.TOKEN, token)
                    SharedPrefClass.saveBankCode(
                        this,
                        DataConstants.BANKCODE,
                        bundle.getString("bankCode").toString()
                    )


                    InitApiAndUi()

                } else {
                    showAlertDialog("Token or ActivityName not Found!!")
                }
            } else {
                showAlertDialog("Invalid transaction data, Please use this app with parent application")
            }
        } else {
            val bundle: Bundle? = intent.getBundleExtra("sendReportData")
            if (bundle?.getString("TOKEN") != null) {
                val token = bundle.getString("TOKEN")
                DataConstants.ActivityName = bundle.getString("ActivityName").toString()
                val userName = bundle.getString("USERNAME")
                val adminName = bundle.getString("ADMINNAME")

                if (bundle.getString("ShopName") == null) {
                    SharedPrefClass.saveShopName(this, DataConstants.ShopName, "")
                } else {
                    SharedPrefClass.saveShopName(
                        this,
                        DataConstants.ShopName,
                        bundle.getString("ShopName").toString()
                    )
                }
                if (bundle.getString("BrandName") == null) {
                    SharedPrefClass.saveBrandName(this, DataConstants.BrandName, "")
                } else {
                    SharedPrefClass.saveBrandName(
                        this, DataConstants.BrandName, bundle.getString("BrandName").toString()
                    )
                }

                SharedPrefClass.saveMobile(
                    this,
                    DataConstants.MobileNumber,
                    bundle.getString("MobileNumber").toString()
                )
                SharedPrefClass.saveEmail(
                    this,
                    DataConstants.UserEmail,
                    bundle.getString("UserEmail").toString()
                )
                SharedPrefClass.saveUserName(this, DataConstants.USERNAME, userName)
                SharedPrefClass.saveAdminName(this, DataConstants.ADMINNAME, adminName)
                SharedPrefClass.saveToken(this, DataConstants.TOKEN, token)
                SharedPrefClass.saveBankCode(
                    this,
                    DataConstants.BANKCODE,
                    bundle.getString("bankCode").toString()
                )

                InitApiAndUi()
            } else {
                InitApiAndUi()
            }


        }
    }

    @Composable
    private fun InitApiAndUi() {
        viewModel = hiltViewModel()

        val fromDate = remember {
            mutableStateOf("")
        }

        var sduiFetchState: State<SduiFetchState> =
            sduiFetchViewModel.sduiFetchState.collectAsState()
        if (sduiFetchSaveData?.value?.sduiFetchRespModels == null) {
            sduiFetchSaveData = sduiFetchState
        }


        val activityResultLauncher =
            rememberLauncherForActivityResult(contract = ActivityResultContracts.StartActivityForResult()) { result ->
                if (result.resultCode == RESULT_OK) {
                    val data = result.data
                    if (data != null) {
                        fromDate.value = ReportCalenderConstant.selectFromDate
                        dateData = ReportCalenderConstant.selectFromDate
                        Timber.tag("ReportActivityTag")
                            .e("ToDate: " + ReportCalenderConstant.selectToDate + " FromDate: " + ReportCalenderConstant.selectFromDate)

                    } else {
                        Toast.makeText(this, "Something went to Wrong..!!", Toast.LENGTH_SHORT)
                            .show()
                    }
                }
            }


        LaunchedEffect(key1 = true) {
            if (sduiFetchSaveData!!.value.apiCall == false || sduiFetchSaveData!!.value.apiCall == null) {
                uiApiCall()
            } else {
                sduiFetchState = sduiFetchSaveData as State<SduiFetchState>
            }
        }
        LaunchedEffect(key1 = sduiFetchState.value) {
            sduiFetchState.value.sduiFetchRespModels?.let {
                dateData = ""
                intentSuccess.value = true
                val intent = Intent(this@ReportSDKActivity, ReportCalenderActivity::class.java)
                activityResultLauncher.launch(intent)
            }

            //Remove this code
        }
        if (fromDate.value.isNotEmpty()) {
            SetUpNavGraph(DataConstants.ActivityName, viewModel)
        }

        if (sduiFetchState.value.loading == true) {
            Box(
                modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ) {
                CircularProgressIndicator(color = primaryColor)
            }
        }
    }


    private fun showAlertDialog(message: String) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Warning...")
        builder.setMessage(message)
        builder.setCancelable(false)

        builder.setPositiveButton(android.R.string.yes) { _, _ ->
            finishAffinity()
        }
        builder.show()

    }


    @Composable
    fun ShowBluetoothDevices(
        context: Context,
        viewModel: DmtReceiptReportViewModel,
        printClicked: MutableState<Boolean>,
        onClick: () -> Unit
    ) {
        val scope = rememberCoroutineScope()
        printerList = ArrayList()
        mAemscrybedevice = AEMScrybeDevice(this)
        if (GetPosConnectPrinter.aemPrinter == null) {
            printerList = mAemscrybedevice!!.pairedPrinters
            printerList?.let { list ->
                if (list.size > 0) {
                    BluetoothDeviceDialog(
                        printerName = list,
                        viewModel = viewModel
                    ) {
                        scope.launch(Dispatchers.Main) {
                            try {
                                mAemscrybedevice!!.connectToPrinter(it)
                                mCardreader =
                                    mAemscrybedevice!!.getCardReader(this@ReportSDKActivity)
                                mAemprinter = mAemscrybedevice!!.aemPrinter
                                GetPosConnectPrinter.aemPrinter = mAemprinter
                                Toast.makeText(
                                    context,
                                    "Connected with $it",
                                    Toast.LENGTH_SHORT
                                ).show()
                                viewModel.isStartPrinting.value =
                                    !viewModel.isStartPrinting.value
                                printClicked.value = !printClicked.value

                            } catch (e: IOException) {
                                viewModel.isStartPrinting.value =
                                    !viewModel.isStartPrinting.value
                                if (e.message!!.contains("Service discovery failed")) {
                                    Toast.makeText(
                                        context,
                                        "Not Connected\n$it is unreachable or off otherwise it is connected with other device",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                } else if (e.message!!.contains("Device or resource busy")) {
                                    Toast.makeText(
                                        context,
                                        "the device is already connected",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                } else {
                                    Toast.makeText(
                                        context,
                                        "Unable to connect",
                                        Toast.LENGTH_SHORT
                                    ).show()

                                }
                                printClicked.value = !printClicked.value
                            } catch (e: Exception) {
                                viewModel.isStartPrinting.value =
                                    !viewModel.isStartPrinting.value
                                Toast.makeText(
                                    context,
                                    "Unable to connect",
                                    Toast.LENGTH_SHORT
                                ).show()
                                printClicked.value = !printClicked.value
                            } catch (e: SocketException) {
                                viewModel.isStartPrinting.value =
                                    !viewModel.isStartPrinting.value
                                Toast.makeText(
                                    context,
                                    "Unable to connect",
                                    Toast.LENGTH_SHORT
                                ).show()
                                printClicked.value = !printClicked.value
                            }
                        }
                    }
                } else {
                    viewModel.showPrinterDialogue.value = true
                }
            }
        } else {
            onClick.invoke()
        }

    }

    override fun onScanMSR(buffer: String?, cardtrack: CardReader.CARD_TRACK?) {
        cardTrackType = cardtrack
        creditData = buffer
        runOnUiThread {
        }
    }

    override fun onScanDLCard(buffer: String?) {
        val dlCardData: CardReader.DLCardData = mCardreader!!.decodeDLData(buffer)
        val name = """
            NAME:${dlCardData.NAME}
            
            """.trimIndent()
        val SWD = """
            SWD Of: ${dlCardData.SWD_OF}
            
            """.trimIndent()
        val dob = """
            DOB: ${dlCardData.DOB}
            
            """.trimIndent()
        val dlNum = """
            DLNUM: ${dlCardData.DL_NUM}
            
            """.trimIndent()
        val issAuth = """
            ISS AUTH: ${dlCardData.ISS_AUTH}
            
            """.trimIndent()
        val doi = """
            DOI: ${dlCardData.DOI}
            
            """.trimIndent()
        val tp = """
            VALID TP: ${dlCardData.VALID_TP}
            
            """.trimIndent()
        val ntp = """
            VALID NTP: ${dlCardData.VALID_NTP}
            
            """.trimIndent()
    }

    override fun onScanRCCard(buffer: String?) {
        val rcCardData: CardReader.RCCardData = mCardreader!!.decodeRCData(buffer)
        val regNum = """
            REG NUM: ${rcCardData.REG_NUM}
            
            """.trimIndent()
        val regName = """
            REG NAME: ${rcCardData.REG_NAME}
            
            """.trimIndent()
        val regUpto = """
            REG UPTO: ${rcCardData.REG_UPTO}
            
            """.trimIndent()

        val data = regNum + regName + regUpto

        runOnUiThread {
            //                editText.setText(data);
        }
    }

    override fun onScanRFD(buffer: String?) {
        val stringBuffer = StringBuffer()
        stringBuffer.append(buffer)
        var temp = ""
        try {
            temp = stringBuffer.deleteCharAt(8).toString()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        val data = temp

        runOnUiThread { //rfText.setText("RF ID:   " + data);
            //                editText.setText("ID " + data);
            try {
                mAemprinter!!.print(data)
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    override fun onScanPacket(buffer: String?) {
        if (buffer == "PRINTEROK") {
            val stringBuffer = StringBuffer()
            stringBuffer.append(buffer)
            var temp = ""
            try {
                temp = stringBuffer.toString()
            } catch (e: Exception) {
                e.printStackTrace()
            }
            tempdata = temp
            val strData: String = tempdata!!.replace("|", "&")
            //Log.e("BufferData",data);
            val formattedData = arrayOf(strData.split("&".toRegex(), 3).toTypedArray())
            // Log.e("Response Data",formattedData[2]);
            responseString = formattedData[0][2]
            responseArray[0] = responseString!!.replace("^", "")
            Log.e("Response Array", responseArray[0]!!)
            runOnUiThread {
                replacedData = tempdata!!.replace("|", "&")
                formattedData[0] = replacedData!!.split("&".toRegex(), 3).toTypedArray()
                response = formattedData[0][2]

            }
        } else {
            val stringBuffer = StringBuffer()
            stringBuffer.append(buffer)
            var temp = ""
            try {
                temp = stringBuffer.toString()
            } catch (e: Exception) {
                e.printStackTrace()
            }
            tempdata = temp
            val strData: String = tempdata!!.replace("|", "&")
            //Log.e("BufferData",data);
            val formattedData = arrayOf(strData.split("&".toRegex(), 3).toTypedArray())
            // Log.e("Response Data",formattedData[2]);
            responseString = formattedData[0][2]
            responseArray[0] = responseString!!.replace("^", "")
            Log.e("Response Array", responseArray[0]!!)
            runOnUiThread {
                replacedData = tempdata!!.replace("|", "&")
                formattedData[0] = replacedData!!.split("&".toRegex(), 3).toTypedArray()
                response = formattedData[0][2]

            }
        }
    }

    override fun onDiscoveryComplete(aemPrinterList: ArrayList<String>?) {
        printerList = aemPrinterList
        for (i in aemPrinterList!!.indices) {
            val deviceName = aemPrinterList[i]
            val status = mAemscrybedevice!!.pairPrinter(deviceName)
            Log.e("STATUS", status)
        }
    }

    override fun onResume() {
        super.onResume()
        if (intentSuccess.value && dateData.isEmpty()) {
            finish()
        }
    }

}
